package com.movimento.service;

import android.accounts.NetworkErrorException;
import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.movimento.errors.FileAlreadyExistException;
import com.movimento.errors.NoMemoryException;
import com.movimento.motreeparser.DownloadDescriptorParser;
import com.movimento.motreeparser.FumoParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.otaclient.ErrorActivity;
import com.movimento.otaclient.InstallUpdateActivity;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.otaclient.R;
import com.movimento.utils.Constants;
import com.movimento.utils.FileUtils;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.PrefUtils;
import com.movimento.utils.Utils;

import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * The type Dlota intent service.
 */
public class DlotaIntentService extends IntentService {
    private final String TAG = DlotaIntentService.class.getSimpleName();
    NotificationManager mNotifyManager;
    NotificationCompat.Builder mBuilder;
    /**
     * The constant shouldServiceRun.
     */
    public static volatile boolean shouldServiceRun = true;
    private static final int BUFFER_SIZE = 4096;
    private boolean restartService = false;

    /**
     * Instantiates a new Dlota intent service.
     *
     * @param name the name
     */
    public DlotaIntentService(String name) {
        super(name);
    }

    /**
     * Instantiates a new Dlota intent service.
     */
    public DlotaIntentService() {
        super(DlotaIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        notificationProgress();
        //Extracting URL from the file saved by DownloadDescriptor Service.
        DownloadDescriptorParser downloadDescriptorParser = (DownloadDescriptorParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_DOWNLOAD_DESCRIPTOR);
        String urlToDownloadFirmware = downloadDescriptorParser.getDownloadObjectURI().replaceAll(" ", "%20");
        Log.v(TAG, "DownloadURL: " + urlToDownloadFirmware);
        Utils.logMessage(TAG, "DownloadURL: " + urlToDownloadFirmware);
        String fileName = urlToDownloadFirmware.substring(urlToDownloadFirmware.lastIndexOf("/") + 1, urlToDownloadFirmware.indexOf("?"));
        try {
            Utils.logMessage(TAG, "shouldServiceRun : " + shouldServiceRun);
            if (!shouldServiceRun) {
                stopSelf();
                return;
            }
            if (downloadFile(urlToDownloadFirmware, fileName)) {
                // TODO : start final confirmation activity with file name to confirm before restart.
                Intent installIntent = new Intent(OTAClientApp.getmAppInstance(), InstallUpdateActivity.class);
                installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(installIntent);
            }
        } catch (FileAlreadyExistException | NetworkErrorException | NoMemoryException | IOException e) {
            e.printStackTrace();
            Utils.logMessage(TAG, Log.getStackTraceString(e));
        }
    }

    /*private void sendDlotaAlert(String alertTypeResult, String serverURI, int resultCode, String state) {
        Intent intent = new Intent(OTAClientApp.getmAppInstance(), AlertIntentService.class);
        intent.putExtra(Constants.KEY_ALERT_NOTIFICATION_TYPE, Constants.ALERT_TYPE_FUMO);
        intent.putExtra(Constants.IS_INSTALL_NOTIFY_ALERT, true);
        intent.putExtra(Constants.KEY_ALERT_TYPE, alertTypeResult);
        intent.putExtra(Constants.KEY_ALERT_SERVER_URI, serverURI);
        intent.putExtra(Constants.KEY_ALERT_RESULT_CODE, resultCode);
        intent.putExtra(Constants.KEY_ALERT_STATE, Integer.valueOf(state));
        OTAClientApp.getmAppInstance().startService(intent);
    }*/

    private void updateFumoMOTree(String nodeName, String parentNodeName, String key, String value) {
        /*String key = intent.getStringExtra(ParserConstants.KEY);
        String value = intent.getStringExtra(ParserConstants.VALUE);*/
        FumoParser fumoParser = (FumoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO);
        if (fumoParser != null) {
            try {
                //fumoParser.createUpdateMOTree(key, value);
                //TODO need to modify this method to accept only key-value pair and inside method
                // there will be logic to fetch particular key and save data against it.
                fumoParser.updateNodeAttribute(nodeName, parentNodeName, key, value);

            } catch (Exception e) {
                e.printStackTrace();
                Utils.logMessage(TAG, Log.getStackTraceString(e));
            }

        }
    }


    /**
     * This method will start downloading the file from provided URL and has download resume capability
     *
     * @param fileURL
     * @return true when downloading is successful
     * @throws IOException
     * @throws FileAlreadyExistException
     * @throws NetworkErrorException
     */
    private boolean downloadFile(String fileURL, String fileName)
            throws IOException, FileAlreadyExistException, NetworkErrorException, NoMemoryException {
        Log.v(TAG, "DownloadAsyncTask.downloadFile");
        Utils.logMessage(TAG, "DownloadAsyncTask.downloadFile");
        int totalFileSize = 0;
        Intent sendUpdateStatusIntent = new Intent();
        sendUpdateStatusIntent.setAction(Constants.DLOTA_DOWNLOAD_STATUS_EVENT);
        /*LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).sendBroadcast(sendUpdateStatusIntent);*/
        //create a new file, to save the downloaded file

        // TODO : this flag is only for development view , it will be removed on production.
        File file = null;
        if (Constants.PRODUCTION_BUILD) {
            file = new File(FileUtils.getCachetFolder(), fileName);
        } else {
            file = new File(FileUtils.getOTAClientFolder(FileUtils.OTACLIENT_DOWNLOAD_FOLDER), fileName);
        }
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
            Log.v(TAG, "File created: " + file.toString());
            Utils.logMessage(TAG, "File created: " + file.toString());
        } else {
            Log.v(TAG, "File to download already created...");
            Utils.logMessage(TAG, "File to download already created...");
        }
        int downloaded = 0;
        URL url = new URL(fileURL);
        if (!Utils.isNetworkAvailable()) {
            sendUpdateStatusIntent.putExtra(Constants.DLOTA_DOWNLOAD_STATUS, Constants.DLOTA_DOWNLOAD_FAILURE);
            //TODO update network error in FUMO MO Tree.
            LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).sendBroadcast(sendUpdateStatusIntent);
            Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_DOWNLOAD_PROGRESSING));
            Utils.logMessage(TAG, "Network blocked.");
            throw new NetworkErrorException("Network blocked.");
        }
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();

        if (file.exists()) {
            Log.v(TAG, "file.exists()");
            Utils.logMessage(TAG, "file.exists()");
            downloaded = (int) file.length();
            totalFileSize = httpConn.getContentLength();
            Log.v(TAG, "FileSize on Server==" + totalFileSize);
            Utils.logMessage(TAG, "FileSize on Server==" + totalFileSize);
            if (totalFileSize == downloaded) {
                Log.v(TAG, "This File already exists, no need to download");
                Utils.logMessage(TAG, "This File already exists, no need to download");
                httpConn.disconnect();
                sendUpdateStatusIntent.putExtra(Constants.DLOTA_DOWNLOAD_STATUS, Constants.DLOTA_DOWNLOAD_FAILURE);
                LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).sendBroadcast(sendUpdateStatusIntent);

                //SFM
                Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_DOWNLOAD_COMPLETE));
                // TODO : NEED TO PLACE 'READY_TO_UPDATE' STATE ACCORDING TO DOCUMENTATION
                Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_READY_TO_UPDATE));
                Log.v(TAG, "Output file already exists. Skipping download.");
                Utils.logMessage(TAG, "Output file already exists. Skipping download.");
                //throw new FileAlreadyExistException("Output file already exists. Skipping download.");
                return true;
            } else {
                httpConn.disconnect();
                httpConn = (HttpURLConnection) url.openConnection();
                Log.v(TAG, "setRequestProperty");
                Utils.logMessage(TAG, "setRequestProperty");
                httpConn.setRequestProperty("Range", "bytes=" + file.length() + "-");
                //long totalFileSize = downloaded + httpConn.getContentLength();
                if ((totalFileSize - downloaded) > Utils.getAvailableSpace(Environment.getExternalStorageDirectory().getAbsolutePath())) {
                   /* sendDlotaAlert(Constants.FUMO_ALERT_TYPE_DOWNLOAD_AND_UPDATE, Constants.FUMO_SOURCE_URI_DOWNLOAD_AND_UPDATE,
                            Constants.FUMO_RESULT_FIRMWARE_UPDATE_FAILS_DEVICE_OUT_OF_MEMORY, String.valueOf(Constants.FUMO_STATE_DOWNLOAD_FAILED));*/

                    //SFM
                    Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_DOWNLOAD_FAILED));

                    //throw new NoMemoryException("Cache Dir Memory Full");
                    Log.e(TAG, "Cache Dir Memory Full");
                    Utils.logMessage(TAG, "Cache Dir Memory Full");

                    Utils.logMessage(TAG, "Cache Dir Memory Full");


                    /** Not restarting the service as it is displaying error screen again and again.
                     * Sending intent to start error activity and stopping service till user clean memmory and restart manually*/
                    restartService = false;
                    Intent failureIntent = new Intent(OTAClientApp.getmAppInstance(), ErrorActivity.class);
                    failureIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    failureIntent.putExtra(Constants.KEY_ERRROR_TYPE, Constants.ERROR_TYPE_MEMORY);
                    failureIntent.putExtra(Constants.KEY_ERROR_HEADER,  OTAClientApp.getmAppInstance().getResources().getString(R.string.memory_error_screen_header));
                    failureIntent.putExtra(Constants.KEY_ERROR_INFO,OTAClientApp.getmAppInstance().getResources().getString(R.string.memory_error_screen_info_text));
                    OTAClientApp.getmAppInstance().startActivity(failureIntent);
                    stopSelf();
                    return false;
                }
            }
        } else {
            httpConn.setRequestProperty("Range", "bytes=" + downloaded + "-");
        }
        int responseCode;
        try {
            responseCode = httpConn.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
            Utils.logMessage(TAG, Log.getStackTraceString(e));
            Log.v(TAG, "Exception in connecting");
            Utils.logMessage(TAG, "Exception in connecting");
            sendUpdateStatusIntent.putExtra(Constants.DLOTA_DOWNLOAD_STATUS, Constants.DLOTA_DOWNLOAD_FAILURE);
           /* sendDlotaAlert(Constants.FUMO_ALERT_TYPE_DOWNLOAD_AND_UPDATE, Constants.FUMO_SOURCE_URI_DOWNLOAD_AND_UPDATE,
                    Constants.FUMO_RESULT_UNIDENTIFIED_ERROR, String.valueOf(Constants.FUMO_STATE_DOWNLOAD_FAILED));*/
            LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).sendBroadcast(sendUpdateStatusIntent);

            //SFM
            Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_DOWNLOAD_PROGRESSING));
            httpConn.disconnect();
            //Restart Service in case of download failure
            restartService = true;
            stopSelf();
            return false;
        }
        Log.v(TAG, "responseCode = " + responseCode);
        Utils.logMessage(TAG, "responseCode = " + responseCode);
        boolean result = true;
        if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_PARTIAL) {
            String disposition = httpConn.getHeaderField("Content-Disposition");

                /* not needed only for looking into some result set */
            if (disposition != null) {
                    /* extracts file name from header field*/

                Log.v(TAG, "disposition = " + disposition);
                Utils.logMessage(TAG, "disposition = " + disposition);
            }
            for (String field : httpConn.getHeaderFields().keySet()) {
                Log.v(TAG, "Header Field " + field + " = = = " + httpConn.getHeaderFields().get(field).get(0));
                Utils.logMessage(TAG, "Header Field " + field + " = = = " + httpConn.getHeaderFields().get(field).get(0));
            }
                /* opens input stream from the HTTP connection*/
            InputStream inputStream = httpConn.getInputStream();
            //TODO Comment out to test download locally, need to uncomment production release
            if (Constants.PRODUCTION_BUILD && !canWriteToCache()) {
                Log.v(TAG, "No Write Permission");
                Utils.logMessage(TAG, "No Write Permission");
                return false;
            }
            // opens an output stream to save into file
            FileOutputStream outputStream = (downloaded == 0) ? new FileOutputStream(file) : new FileOutputStream(file, true);
            int bytesRead = -1;
            byte[] buffer = new byte[BUFFER_SIZE];

            // TODO : update download state here in fumo_state json
            Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_DOWNLOAD_PROGRESSING));
            Utils.logMessage(TAG, "File download started...");
            PrefUtils.putBoolean(Constants.IS_INSTALLATION_STARTED,false);
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                if (!shouldServiceRun) {
                    stopSelf();
                    mBuilder.setContentText(getResources().getString(R.string.download_paused)).setProgress(0, 0, false);
                    mNotifyManager.notify(1, mBuilder.build());
                    //here intentionally false, cause it is forcely stopoed by user.
                    return false;
                }

                downloaded = downloaded + bytesRead;
                int percentageDownload = (int) (0.5d + ((double) downloaded / (double) totalFileSize) * 100);
                Log.v(TAG, "downloading...." + downloaded + " Percentage:" + percentageDownload + "%");
                //Utils.logMessage(TAG, "downloading...." + downloaded + " Percentage:" + percentageDownload + "%");
                updateProgress(percentageDownload);
                outputStream.write(buffer, 0, bytesRead);
            }
            Utils.logMessage(TAG, "File download completed...");
            outputStream.close();
            inputStream.close();

           /* sendDlotaAlert(Constants.FUMO_ALERT_TYPE_DOWNLOAD_AND_UPDATE, Constants.FUMO_SOURCE_URI_DOWNLOAD_AND_UPDATE,
                    Constants.FUMO_RESULT_SUCCESSFUL, String.valueOf(Constants.DLOTA_STATE_DOWNLOAD_COMPLETE));*/
            //SFM
            Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_DOWNLOAD_COMPLETE));
            sendUpdateStatusIntent.putExtra(Constants.DLOTA_DOWNLOAD_STATUS, Constants.DLOTA_DOWNLOAD_SUCCESS);
            mBuilder.setContentText(getResources().getString(R.string.download_complete)).setProgress(0, 0, false);
            mNotifyManager.notify(1, mBuilder.build());
            sendUpdateStatusIntent.putExtra(Constants.KEY_DLOTA_DOWNLOAD_FILENAME, file.getName());
            LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).sendBroadcast(sendUpdateStatusIntent);

            Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_READY_TO_UPDATE));
            Log.v(TAG, "File downloaded");
            Utils.logMessage(TAG, "File downloaded");
            result = true;
        } else if (responseCode == 416) {
            Log.v(TAG, "Range is not correct: " + responseCode);
            Utils.logMessage(TAG, "Range is not correct: " + responseCode);
            Log.v(TAG, "Range is not correct: getContentLength " + httpConn.getContentLength());
            Utils.logMessage(TAG, "Range is not correct: getContentLength " + httpConn.getContentLength());
            //Restart Service in case of download failure
            restartService = true;
            stopSelf();
            result = false;
        } else {
            Log.v(TAG, "No file to download. Server replied HTTP code: " + responseCode);
            Utils.logMessage(TAG, "No file to download. Server replied HTTP code: " + responseCode);
            sendUpdateStatusIntent.putExtra(Constants.DLOTA_DOWNLOAD_STATUS, Constants.DLOTA_DOWNLOAD_FAILURE);
           /* sendDlotaAlert(Constants.FUMO_ALERT_TYPE_DOWNLOAD_AND_UPDATE, Constants.FUMO_SOURCE_URI_DOWNLOAD_AND_UPDATE,
                    Constants.FUMO_RESULT_MALFORMED_URL, String.valueOf(Constants.FUMO_STATE_DOWNLOAD_FAILED));*/
            //SFM
            Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_DOWNLOAD_FAILED));
            LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).sendBroadcast(sendUpdateStatusIntent);
            //Restart Service in case of download failure
            restartService = true;
            stopSelf();
            //throw new InterruptedIOException("Try Again");
            Log.v(TAG, "Try Again");
            Utils.logMessage(TAG, "Try Again");
            result = false;

        }
        httpConn.disconnect();
        Log.v(TAG, "<< DownloadAsyncTask.downloadFile");
        Utils.logMessage(TAG, "<< DownloadAsyncTask.downloadFile");
        return result;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (restartService) {
            startService(new Intent(OTAClientApp.getmAppInstance(), DlotaIntentService.class));
        }
    }

    private void updateProgress(int percentageDownload) {
        Intent intent = new Intent();
        intent.setAction(Constants.DLOTA_DOWNLOAD_STATUS_EVENT);
        intent.putExtra(Constants.DLOTA_DOWNLOAD_STATUS, Constants.DLOTA_DOWNLOAD_IN_PROGRESS);
        intent.putExtra(Constants.DLOTA_DOWNLOAD_PERCENTAGE, percentageDownload);
        LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).sendBroadcast(intent);

        mBuilder.setProgress(100, percentageDownload, false);
        mNotifyManager.notify(1, mBuilder.build());
    }

    private boolean canWriteToCache() {
        File file = new File("/cache");
        return file.canWrite();
    }

    private void notificationProgress() {

        mNotifyManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(getResources().getString(R.string.download_in_progress))
                .setSmallIcon(R.mipmap.app_logo);

    }

}
