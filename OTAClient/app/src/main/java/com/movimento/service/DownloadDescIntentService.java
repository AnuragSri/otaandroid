package com.movimento.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.movimento.http.ConnectionHandler;
import com.movimento.http.response.model.downdesc.DownloadDescModel;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.Utils;

import static com.movimento.utils.Constants.KEY_DOWNLOAD_DESC_URL;


/**
 * The type Download desc intent service.
 */
public class DownloadDescIntentService extends IntentService {
    private final String TAG = DownloadDescIntentService.class.getCanonicalName();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public DownloadDescIntentService(String name) {
        super(name);
    }

    /**
     * Instantiates a new Dev acc intent service.
     */
    public DownloadDescIntentService() {
        super(DevInfoIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String url = intent.getStringExtra(KEY_DOWNLOAD_DESC_URL);
            if (url != null && !url.isEmpty()) {
                DownloadDescModel downDescModel = getDownDescData(url);
                if (downDescModel != null) {

                    String urlToDownloadFirmware = downDescModel.getMedia().getProduct().getMediaObject().getObjectURI().getServer();
                    Log.v(TAG, "DownloadURL: " + urlToDownloadFirmware);
                    Utils.logMessage(TAG, "DownloadURL: " + urlToDownloadFirmware);

                    Intent dlotaIntent = new Intent(OTAClientApp.getmAppInstance(), DlotaIntentService.class);
                    DlotaIntentService.shouldServiceRun = true;
                    OTAClientApp.getmAppInstance().startService(dlotaIntent);


                   /* try {
                        FumoParser fumoParser = (FumoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO);
                        fumoParser.setNodeDFTitle(ParserConstants.KEY_FUMO_DOWNLOAD_AND_UPDATE_PKG_URL, urlToDownloadFirmware, ParserConstants.KEY_FUMO_DOWNLOAD_AND_UPDATE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
                }
            }
        }
    }

    private DownloadDescModel getDownDescData(String url) {
        DownloadDescModel downloadDescModel = null;
        String responseString = null;
        try {
            ConnectionHandler handler = new ConnectionHandler();
            responseString = handler.getRequest(url);
            Log.v(TAG, "Response: " + responseString);
            if (responseString != null && !responseString.isEmpty()) {
                downloadDescModel = new Gson().fromJson(responseString, DownloadDescModel.class);
                Utils.writeToFIle(OTAClientApp.getmAppInstance(), responseString, ParserConstants.DOWNLOAD_DESCRIPTION_MO_TREE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return downloadDescModel;
    }


}
