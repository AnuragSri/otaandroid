package com.movimento.service;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;
import com.movimento.http.request.model.capability.CapabilityModel;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.otaclient.R;
import com.movimento.utils.Constants;
import com.movimento.utils.PrefUtils;
import com.movimento.utils.Utils;

import java.util.TimerTask;

import static com.movimento.utils.Constants.KEY_REGISTRATION_STATUS;

/**
 * The type Ota scheduler.
 */
public class OTAScheduler extends TimerTask {
    private final String TAG = OTAScheduler.class.getCanonicalName();

    public void run() {
        //  Log.v(TAG, "Scheduler executed...");
        //TODO FIrst  check newtwork connectivity,
        // then registration flag,
        // then keep continue polling capability exachange.

        if (Utils.isNetworkAvailable()) {
            if (PrefUtils.getBoolean(Constants.IS_REGISTERED, false)) {
                Intent intent = new Intent(OTAClientApp.getmAppInstance(), CapabilityIntentService.class);
                OTAClientApp.getmAppInstance().startService(intent);

            } else {
                // Registration begins here
             //   sendRegistrationBroadcast();
                Intent intent = new Intent(OTAClientApp.getmAppInstance(), RegistrationIntentService.class);
                //here we will set the values in intent
                OTAClientApp.getmAppInstance().startService(intent);
            }
        } else {
            Log.v(TAG, "Please check network connectivity.");
        }
    }

    private void sendRegistrationBroadcast() {
        //Sending broadcast of registration to Cheching Update Activity.
        Intent sendUpdateStatusIntent = new Intent();
        sendUpdateStatusIntent.setAction(Constants.CHECKING_UPDATE_STATUS_EVENT);
        sendUpdateStatusIntent.putExtra(KEY_REGISTRATION_STATUS, OTAClientApp.getmAppInstance().getResources().getString(R.string.device_registration_in_progress));
        LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).sendBroadcast(sendUpdateStatusIntent);
    }

    private String updateModel(CapabilityModel capabilityModel) {
        //Here to update capability model
        return new Gson().toJson(capabilityModel, CapabilityModel.class);
    }

}
