package com.movimento.service;

import android.app.IntentService;
import android.content.Intent;

import com.movimento.http.ConnectionHandler;
import com.movimento.http.RequestUtils;
import com.movimento.http.request.model.RequestModel;
import com.movimento.utils.Constants;


/**
 * The type Registration intent service.
 */
public class RegistrationIntentService extends IntentService {
    private final String TAG = RegistrationIntentService.class.getCanonicalName();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public RegistrationIntentService(String name) {
        super(name);
    }

    /**
     * Instantiates a new Registration intent service.
     */
    public RegistrationIntentService() {
        super(RegistrationIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        RequestModel model = RequestUtils.createRequest(Constants.MO_TYPE_REGISTRATION);
        try {
            ConnectionHandler handler = new ConnectionHandler();
            handler.postRequest(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
