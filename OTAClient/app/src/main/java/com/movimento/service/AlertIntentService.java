package com.movimento.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.movimento.http.ConnectionHandler;
import com.movimento.http.request.model.RequestModel;
import com.movimento.motreeparser.DownloadDescriptorParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.utils.Constants;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.internal.Util;

import static com.movimento.utils.Constants.KEY_ALERT_RESULT_CODE;
import static com.movimento.utils.Constants.KEY_ALERT_STATE;
import static com.movimento.utils.Constants.KEY_JSON_ALERT;
import static com.movimento.utils.Constants.KEY_JSON_ALERT_DATA;
import static com.movimento.utils.Constants.KEY_JSON_ALERT_RESULT_CODE;
import static com.movimento.utils.Constants.KEY_JSON_ALERT_SOURCE_URI;
import static com.movimento.utils.Constants.KEY_JSON_ALERT_STATE;


/**
 * The type Alert intent service.
 */
public class AlertIntentService extends IntentService {
    private final String TAG = AlertIntentService.class.getCanonicalName();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public AlertIntentService(String name) {
        super(name);
    }

    /**
     * Instantiates a new Dev acc intent service.
     */
    public AlertIntentService() {
        super(DevInfoIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        RequestModel model = com.movimento.http.RequestUtils.createRequest(Constants.MO_TYPE_ALERT);
        if (intent.getBooleanExtra(Constants.IS_INSTALL_NOTIFY_ALERT, false)) {
            DownloadDescriptorParser downloadDescriptorParser = (DownloadDescriptorParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_DOWNLOAD_DESCRIPTOR);
            model.setUrl(downloadDescriptorParser.getMediaObjMeta(ParserConstants.KEY_INSTALL_NOTIFY_URI));
        }

        String body = model.getJsonBody();
        body = updateAlert(body, intent);
        model.setJsonBody(body);


        String responseString = null;
        try {
            Log.v(TAG, "Request Alert Body: " + body);
            Log.i(TAG,"Alert URL : "+model.getUrl());
            Utils.logMessage(TAG,"Alert URL : "+model.getUrl());
            Utils.logMessage(TAG, "Request Alert Body: " + body);
            ConnectionHandler handler = new ConnectionHandler();
            responseString = handler.postRequest(model);
            Log.v(TAG, "Response: " + responseString);
        } catch (Exception e) {
            Log.i(TAG,Log.getStackTraceString(e));
            e.printStackTrace();
        }
    }

    private String updateAlert(String body, Intent intent) {
        String jsonBody = null;
        try {
            JSONObject alertJsonObj = new JSONObject(body);
            JSONObject alert = (JSONObject) alertJsonObj.getJSONArray(KEY_JSON_ALERT).get(0);
            String alertNotifType = intent.getStringExtra(Constants.KEY_ALERT_NOTIFICATION_TYPE);
            if (!alertNotifType.equals(Constants.ALERT_TYPE_REGISTRATION)) {
                alert.put(KEY_JSON_ALERT_SOURCE_URI, intent.getStringExtra(Constants.KEY_ALERT_SERVER_URI));
            }
            if(alertNotifType.equals(Constants.ALERT_TYPE_FUMO)){
                alert.put(Constants.KEY_JSON_ALERT_TYPE,intent.getStringExtra(Constants.KEY_ALERT_TYPE));
            }
            JSONObject data = alert.getJSONObject(KEY_JSON_ALERT_DATA);
            data.put(KEY_JSON_ALERT_RESULT_CODE, intent.getIntExtra(KEY_ALERT_RESULT_CODE, 0));
            data.put(KEY_JSON_ALERT_STATE, String.valueOf(intent.getIntExtra(KEY_ALERT_STATE, 0)));
            jsonBody = alertJsonObj.toString();
        } catch (JSONException e) {
            Log.i(TAG,Log.getStackTraceString(e));
            e.printStackTrace();
        }

        return jsonBody;
    }
}
