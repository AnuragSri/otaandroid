package com.movimento.service;

import android.app.IntentService;
import android.content.Intent;

import com.google.gson.Gson;
import com.movimento.http.ConnectionHandler;
import com.movimento.http.request.model.RequestModel;
import com.movimento.http.request.model.registration.RegistrationModel;
import com.movimento.utils.Constants;


/**
 * The type Capability intent service.
 */
public class CapabilityIntentService extends IntentService {
    private final String TAG = CapabilityIntentService.class.getCanonicalName();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public CapabilityIntentService(String name) {
        super(name);
    }

    /**
     * Instantiates a new Capability intent service.
     */
    public CapabilityIntentService() {
        super(CapabilityIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        RequestModel model = com.movimento.http.RequestUtils.createRequest(Constants.MO_TYPE_CAPABILITY);
        try {
            ConnectionHandler handler = new ConnectionHandler();
            handler.postRequest(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String updateModel(RegistrationModel registrationModel, Intent intent) {
        //here to update jsonBody
        return new Gson().toJson(registrationModel, RegistrationModel.class);
    }
}
