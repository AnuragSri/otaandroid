package com.movimento.service;

import android.app.IntentService;
import android.content.Intent;

import com.google.gson.Gson;
import com.movimento.http.ConnectionHandler;
import com.movimento.http.request.model.RequestModel;
import com.movimento.http.request.model.fumo.FUMO;
import com.movimento.http.request.model.fumo.FumoModel;
import com.movimento.http.request.model.fumo.FumoStatus;
import com.movimento.motreeparser.FumoParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.utils.Constants;
import com.movimento.utils.ParserConstants;

import org.json.JSONException;

import java.util.List;


/**
 * The type Fumo intent service.
 */
public class FumoIntentService extends IntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public FumoIntentService(String name) {
        super(name);
    }

    /**
     * Instantiates a new Fumo intent service.
     */
    public FumoIntentService() {
        super(FumoIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        RequestModel model = com.movimento.http.RequestUtils.createRequest(Constants.MO_TYPE_FIRMWARE);
        String body = model.getJsonBody();

        updateFumoMOTreee(intent);

        body = updateFumoAlert(body, Constants.FUMO_STATE_IDLE_START, Constants.FUMO_RESULT_SUCCESSFUL);
        model.setJsonBody(body);
        ConnectionHandler handler = new ConnectionHandler();
        handler.postRequest(model);
    }

    private void updateFumoMOTreee(Intent intent) {
        String key = intent.getStringExtra(ParserConstants.KEY);
        String value = intent.getStringExtra(ParserConstants.VALUE);
        FumoParser fumoParser = (FumoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO);
        if (fumoParser != null) {
            try {
                if (key.equals(ParserConstants.KEY_DDF_NAME)) {
                    fumoParser.setParentDFType(ParserConstants.KEY_DDF_NAME, value);
                    //TODO need to modify this method to accept only key-value pair and inside method
                    // there will be logic to fetch particular key and save data againt it.
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private String updateFumoAlert(String jsonBody, int state, int resultCode) {
        FumoModel fumoModel = new Gson().fromJson(jsonBody, FumoModel.class);
        if (fumoModel != null) {
            List<FumoStatus> statusList = fumoModel.getStatus();
            FumoStatus fumoStatus = statusList.get(0);
            fumoStatus.setSc(resultCode);
            statusList.set(0, fumoStatus);
            fumoModel.setStatus(statusList);

            FUMO fumo = fumoModel.getMOData().getFumo();
            fumo.setState(String.valueOf(state));
            fumoModel.getMOData().setFumo(fumo);
        }
        return new Gson().toJson(fumoModel, FumoModel.class);

    }
}
