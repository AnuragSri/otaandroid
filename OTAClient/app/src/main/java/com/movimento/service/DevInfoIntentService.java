package com.movimento.service;

import android.app.IntentService;
import android.content.Intent;

import com.movimento.http.ConnectionHandler;
import com.movimento.http.request.model.RequestModel;
import com.movimento.utils.Constants;


/**
 * The type Dev info intent service.
 */
public class DevInfoIntentService extends IntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public DevInfoIntentService(String name) {
        super(name);
    }

    /**
     * Instantiates a new Dev info intent service.
     */
    public DevInfoIntentService() {
        super(DevInfoIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        //  getDeviceInfo();
        RequestModel model = com.movimento.http.RequestUtils.createRequest(Constants.MO_TYPE_DEV_INFO);
        ConnectionHandler handler = new ConnectionHandler();
        handler.postRequest(model);
    }

}
