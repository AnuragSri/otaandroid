package com.movimento.service;

import android.app.IntentService;
import android.content.Intent;


/**
 * The type Dev acc intent service.
 */
public class DevAccIntentService extends IntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public DevAccIntentService(String name) {
        super(name);
    }

    /**
     * Instantiates a new Dev acc intent service.
     */
    public DevAccIntentService() {
        super(DevInfoIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }
}
