package com.movimento.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Timer;


/**
 * The type Ota client service.
 */
public class OTAClientService extends Service {
    private final String TAG = OTAClientService.class.getCanonicalName();
    //Delays in milliseconds
    private final int INITIAL_DELAY = 5000; //5 seconds
    private final int TIME_PERIOD = 5*60*1000; //5 minute
    private Timer timer;

    /**
     * Instantiates a new Ota client service.
     *
     * @param context the context
     */
    public OTAClientService(Context context) {
        super();
    }

    /**
     * Instantiates a new Ota client service.
     */
    public OTAClientService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        startTimer();
        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent broadcastIntent = new Intent("com.movimento.receiver.ServiceStarter.Restart");
        sendBroadcast(broadcastIntent);
        stopTimerTask();
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void startTimer() {
        OTAScheduler scheduler = new OTAScheduler();
        timer = new Timer();

        timer.scheduleAtFixedRate(scheduler, INITIAL_DELAY, TIME_PERIOD);
    }

    private void stopTimerTask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }


}
