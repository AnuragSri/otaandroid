package com.movimento.service;

import android.app.IntentService;
import android.content.Intent;

import com.movimento.motreeparser.LawmoParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.utils.Constants;
import com.movimento.utils.ParserConstants;


/**
 * The type Lawmo intent service.
 */
public class LawmoIntentService extends IntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public LawmoIntentService(String name) {
        super(name);
    }

    /**
     * Instantiates a new Lawmo intent service.
     */
    public LawmoIntentService() {
        super(LawmoIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // TODO : lock or wipe device according to intent and send lawmo alert to server

        updateLawmoMOTree(intent.getStringExtra(ParserConstants.KEY_NODE_NAME), intent.getStringExtra(ParserConstants.KEY_ACCESS_TYPE));
        sendLawmoAlert(intent.getStringExtra(Constants.KEY_LAWMO_TYPE));
    }

    private void updateLawmoMOTree(String nodeName, String accessTypeValue) {
        LawmoParser lawmoParser = (LawmoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_LAWMO);
        if (lawmoParser != null) {
            //lawmoParser.createUpdateOperationNode(nodeName, accessTypeValue);
            //TODO need to write business logic to update/create node and insert value in it
        }
    }

    private void sendLawmoAlert(String lawmoType) {

        Intent intent = new Intent(OTAClientApp.getmAppInstance(), AlertIntentService.class);
        intent.putExtra(Constants.KEY_ALERT_NOTIFICATION_TYPE, Constants.ALERT_TYPE_LAWMO);
        intent.putExtra(Constants.KEY_ALERT_TYPE, Constants.LAWMO_ALERT_TYPE);
        if (lawmoType.equals(Constants.LAWMO_ACTION_WIPE)) {
            intent.putExtra(Constants.KEY_ALERT_SERVER_URI, Constants.LAWMO_WIPE_SOURCE_URI);
            intent.putExtra(Constants.KEY_ALERT_RESULT_CODE, Constants.LAWMO_RESULT_OPERATION_SUCCEEDED);
            intent.putExtra(Constants.KEY_ALERT_STATE, Constants.LAWMO_STATE_UNLOCKED);

        } else if (lawmoType.equals(Constants.LAWMO_ACTION_LOCK)) {
            intent.putExtra(Constants.KEY_ALERT_SERVER_URI, Constants.LAWMO_LOCK_SOURCE_URI);
            intent.putExtra(Constants.KEY_ALERT_RESULT_CODE, Constants.LAWMO_RESULT_OPERATION_SUCCEEDED);
            intent.putExtra(Constants.KEY_ALERT_STATE, Constants.LAWMO_STATE_FULLY_LOCKED);
        }
        OTAClientApp.getmAppInstance().startService(intent);
    }
}
