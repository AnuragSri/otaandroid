package com.movimento.test;

import okhttp3.Headers;

import static com.movimento.utils.Constants.ACCEPT_KEY;
import static com.movimento.utils.Constants.ACCEPT_VALUE;
import static com.movimento.utils.Constants.CONTENT_TYPE_KEY;
import static com.movimento.utils.Constants.CONTENT_TYPE_VALUE;
import static com.movimento.utils.Constants.OMADM_DevID_KEY;
import static com.movimento.utils.Constants.TOKEN_KEY;
import static com.movimento.utils.Constants.VIN_KEY;


/**
 * The type App utils.
 */
public class AppUtils {
    private static final String TAG = AppUtils.class.getCanonicalName();

    /**
     * Load json from asset string.
     *
     * @param context  the context
     * @param fileName the file name
     * @return the string
     *//*
    public static String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            //InputStream is = context.getResources().openRawResource(R.raw.sess
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }*/


    /**
     * Gets headers.
     *
     * @param mapHeaders the map headers
     * @return the headers
     */
   /* public static Headers getHeaders(Map<String, String> mapHeaders) {
        Map<String, String> someStrings = mapHeaders;
        Headers.Builder hb = new Headers.Builder();
        for (Map.Entry<String, String> entry : someStrings.entrySet()) {
            hb.add(entry.getKey(), entry.getValue());
        }
        return hb.build();
    }*/


    /**
     * Gets device info headers.
     *
     * @return the device info headers
     */
    public static Headers getDeviceInfoHeaders() {
        Headers.Builder hb = new Headers.Builder();
        hb.add(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);
        hb.add(ACCEPT_KEY, ACCEPT_VALUE);
        hb.add(OMADM_DevID_KEY, "7777");
        hb.add(VIN_KEY, "1FA6P0V74HR783777");
        hb.add(TOKEN_KEY, "UrXBnH+dLs1jqt4Cu5SGcMbr2c2pUNm0yvRfY527ex8TkaSwEiH7LXBPhRWV66nxS4cGhDdDWRl9Ulxm0ci77cYx+I+2qbOU/NGBGgNo3Av2qz2l+nyshs7PkOcZPox2FokikXFEVcD6Hb3QS4R5h0rytBD9j8RfHwYx4z+GT4w=");

        return hb.build();
    }

    /**
     * Gets device regist headers.
     *
     * @return the device regist headers
     */
    public static Headers getDeviceRegistHeaders() {
        Headers.Builder hb = new Headers.Builder();
        hb.add(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);
        hb.add(ACCEPT_KEY, ACCEPT_VALUE);
        hb.add(OMADM_DevID_KEY, "1234");
        hb.add(VIN_KEY, "CALRM4H76CC024102");
        //hb.add(AppConstants.TOKEN_KEY, "UrXBnH+dLs1jqt4Cu5SGcMbr2c2pUNm0yvRfY527ex8TkaSwEiH7LXBPhRWV66nxS4cGhDdDWRl9Ulxm0ci77cYx+I+2qbOU/NGBGgNo3Av2qz2l+nyshs7PkOcZPox2FokikXFEVcD6Hb3QS4R5h0rytBD9j8RfHwYx4z+GT4w=");

        return hb.build();
    }

}
