package com.movimento.test;

import android.content.Context;

import com.movimento.http.request.model.devinfo.DeviceInfo;
import com.movimento.http.request.model.devinfo.DeviceInfoModel;
import com.movimento.http.request.model.fumo.FUMO;
import com.movimento.http.request.model.fumo.FumoModel;
import com.movimento.http.request.model.registration.RegDevInfo;
import com.movimento.http.request.model.registration.RegExt;
import com.movimento.http.request.model.registration.RegistrationModel;

import java.io.IOException;


/**
 * The type Test request utils.
 */
public class TestRequestUtils {

    /*public static RegistrationModel populateRegisterRequest(Context context, String deviceId, String vinNumber, RegistrationModel registerRequest) throws IOException {
        RegistrationModel registrationRequest = registerRequest;
        if(registrationRequest!=null) {
            RegDevInfo deviceInfo = registrationRequest.getMOData().getDevInfo();
            deviceInfo.setDevID(deviceId);

            RegExt ext = deviceInfo.getExt();
            ext.setVIN(vinNumber);

            //PublicKey publicKey = AppUtils.getPublicKey(context, AppConstants.PUBLIC_KEY);

            //ext.setEncryptedVIN(AppUtils.encryptKey(context, vinNumber, publicKey));
            deviceInfo.setExt(ext);
            registerRequest.getMOData().setDevInfo(deviceInfo);
        }
        return registrationRequest;
    }*/

    /*public static DeviceInfoModel populateDevInfoRequest(String deviceId, DeviceInfoModel devInfoRequest) {
        DeviceInfoModel deviceInfoRequest = devInfoRequest;
        if(deviceInfoRequest!=null){
            DeviceInfo devInfo = deviceInfoRequest.getMOData().getDevInfo();
            devInfo.setDevID(deviceId);
            deviceInfoRequest.getMOData().setDevInfo(devInfo);
        }
        return deviceInfoRequest;
    }*/

    /**
     * Populate register request registration model.
     *
     * @param context         the context
     * @param deviceId        the device id
     * @param vinNumber       the vin number
     * @param registerRequest the register request
     * @return the registration model
     * @throws IOException the io exception
     */
    public static RegistrationModel populateRegisterRequest(Context context, String deviceId, String vinNumber, RegistrationModel registerRequest) throws IOException {
        RegistrationModel registrationRequest = registerRequest;
        if (registrationRequest != null) {
            RegDevInfo deviceInfo = registrationRequest.getMOData().getDevInfo();
            deviceInfo.setDevID(deviceId);

            RegExt ext = deviceInfo.getExt();
            ext.setVIN(vinNumber);

            //PublicKey publicKey = AppUtils.getPublicKey(context, AppConstants.PUBLIC_KEY);

            //ext.setEncryptedVIN(AppUtils.encryptKey(context, vinNumber, publicKey));
            deviceInfo.setExt(ext);
            registerRequest.getMOData().setDevInfo(deviceInfo);

        }

        return registrationRequest;
    }

    /**
     * Populate dev info request device info model.
     *
     * @param deviceId       the device id
     * @param devInfoRequest the dev info request
     * @return the device info model
     */
    public static DeviceInfoModel populateDevInfoRequest(String deviceId, DeviceInfoModel devInfoRequest) {
        DeviceInfoModel deviceInfoRequest = devInfoRequest;
        if (deviceInfoRequest != null) {
            DeviceInfo devInfo = deviceInfoRequest.getMOData().getDevInfo();
            devInfo.setDevID(deviceId);

            deviceInfoRequest.getMOData().setDevInfo(devInfo);
        }
        return deviceInfoRequest;
    }

    /**
     * Populate fumo model fumo model.
     *
     * @param deviceId  the device id
     * @param fumoModel the fumo model
     * @return the fumo model
     */
    public static FumoModel populateFumoModel(String deviceId, FumoModel fumoModel) {
        FumoModel fumoModel1 = fumoModel;
        if (fumoModel1 != null) {
            FUMO fumo = fumoModel1.getMOData().getFumo();
            //Here to set updated fumo request fields
        }
        return fumoModel1;
    }
}
