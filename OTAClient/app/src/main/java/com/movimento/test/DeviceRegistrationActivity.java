package com.movimento.test;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.movimento.asynctask.HttpClientAsyncTask;
import com.movimento.http.request.model.RequestModel;
import com.movimento.otaclient.DownloadUpdateActivity;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.otaclient.R;
import com.movimento.service.DlotaIntentService;
import com.movimento.utils.Constants;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.movimento.utils.Constants.ACCEPT_KEY;
import static com.movimento.utils.Constants.ACCEPT_VALUE;
import static com.movimento.utils.Constants.CONTENT_TYPE_KEY;
import static com.movimento.utils.Constants.CONTENT_TYPE_VALUE;
import static com.movimento.utils.Constants.OMADM_DevID_KEY;
import static com.movimento.utils.Constants.TOKEN_KEY;
import static com.movimento.utils.Constants.VIN_KEY;

/**
 * The type Device registration activity.
 */
public class DeviceRegistrationActivity extends AppCompatActivity implements View.OnClickListener {
    private final String TAG = DeviceRegistrationActivity.class.getCanonicalName();
    private Context mContext;
    private EditText etRegisDevId;
    private EditText etRegVinNo;
    private Button btnDeviceRegistration;
    private Button btnDeviceInfo;
    private Button btnFumo;
    private Button btnAlert;
    private Button btnCapability;
    private Button btnDownload;
    private Button btnLaunchDownloadUpdateActivity;
    private TextView tvResponseMsg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_device);
        mContext = this;

        initUI();
    }

    @Override
    public void onClick(View view) {

        if (view == btnDeviceRegistration) {
            if (!valid()) {
                return;
            }

            try {
                registerDeviceRequest();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (view == btnDeviceInfo) {
            if (!valid()) {
                return;
            }
            try {
                getDeviceInfo();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (view == btnFumo) {
            getDeviceFumo();

        } else if (view == btnAlert) {
            sendDeviceAlert();
        } else if (view == btnCapability) {
            sendCapabilityRequest();

        } else if (view == btnDownload) {
            downloadDemo();

        } else if (view == btnLaunchDownloadUpdateActivity){
            launchDownloadUpdateActivity();
        }


    }

    private void launchDownloadUpdateActivity() {
        Intent intent = new Intent(mContext, DownloadUpdateActivity.class);
        startActivity(intent);
    }

    private void downloadDemo() {
        //TODO here to start download
    }

    private void sendCapabilityRequest() {
        RequestModel model = com.movimento.http.RequestUtils.createRequest(Constants.MO_TYPE_CAPABILITY);
        HttpClientAsyncTask httpClientAsyncTask = new HttpClientAsyncTask(model);
        httpClientAsyncTask.execute();
    }

    private void sendDeviceAlert() {
        RequestModel model = com.movimento.http.RequestUtils.createRequest(Constants.MO_TYPE_ALERT);
        HttpClientAsyncTask httpClientAsyncTask = new HttpClientAsyncTask(model);
        httpClientAsyncTask.execute();
    }

    private void getDeviceFumo() {
        RequestModel model = com.movimento.http.RequestUtils.createRequest(Constants.MO_TYPE_FIRMWARE);
        HttpClientAsyncTask httpClientAsyncTask = new HttpClientAsyncTask(model);
        httpClientAsyncTask.execute();
    }

    private void getDeviceInfo() throws IOException {
        RequestModel model = com.movimento.http.RequestUtils.createRequest(Constants.MO_TYPE_DEV_INFO);
        HttpClientAsyncTask httpClientAsyncTask = new HttpClientAsyncTask(model);
        httpClientAsyncTask.execute();
    }

    private boolean valid() {
        boolean flag = true;
        String devId = etRegisDevId.getText().toString();
        String vinNmber = etRegVinNo.getText().toString();
        if (devId.isEmpty() || vinNmber.isEmpty()) {
            Toast.makeText(mContext, "Either DevId or VIN is empty.", Toast.LENGTH_SHORT).show();
            flag = false;
        }
        return flag;
    }

    private void registerDeviceRequest() throws IOException {
        RequestModel model = com.movimento.http.RequestUtils.createRequest(Constants.MO_TYPE_REGISTRATION);
        HttpClientAsyncTask httpClientAsyncTask = new HttpClientAsyncTask(model);
        httpClientAsyncTask.execute();
    }

    private Map<String, String> initRegistrationHeaders(String deviceId, String vinNumber) {
        Map<String, String> map = new HashMap<>();
        map.put(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);
        map.put(ACCEPT_KEY, ACCEPT_VALUE);
        map.put(OMADM_DevID_KEY, deviceId);
        map.put(VIN_KEY, vinNumber);

        return map;
    }

    private Map<String, String> initDeviceInfoHeaders(String deviceId, String vinNumber, String token) {
        Map<String, String> map = new HashMap<>();
        map.put(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);
        map.put(ACCEPT_KEY, ACCEPT_VALUE);
        map.put(OMADM_DevID_KEY, deviceId);
        map.put(VIN_KEY, vinNumber);
        map.put(TOKEN_KEY, token);

        return map;
    }

    private void initUI() {
        etRegisDevId = (EditText) findViewById(R.id.etRegisDevId);
        etRegVinNo = (EditText) findViewById(R.id.etRegVinNo);
        btnDeviceRegistration = (Button) findViewById(R.id.btnDeviceRegistration);
        btnDeviceInfo = (Button) findViewById(R.id.btnDeviceInfo);
        btnFumo = (Button) findViewById(R.id.btnFumo);
        btnAlert = (Button) findViewById(R.id.btnAlert);
        tvResponseMsg = (TextView) findViewById(R.id.tvResponseMsg);
        btnCapability = (Button) findViewById(R.id.btnCapability);
        btnDownload = (Button) findViewById(R.id.btnDownload);
        btnLaunchDownloadUpdateActivity = (Button) findViewById(R.id.btnLaunchDownloadUpdateActivity);

        btnDeviceRegistration.setOnClickListener(this);
        btnDeviceInfo.setOnClickListener(this);
        btnFumo.setOnClickListener(this);
        btnAlert.setOnClickListener(this);
        btnCapability.setOnClickListener(this);
        btnDownload.setOnClickListener(this);
        btnLaunchDownloadUpdateActivity.setOnClickListener(this);
    }

}
