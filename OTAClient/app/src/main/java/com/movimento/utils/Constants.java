package com.movimento.utils;


/**
 * The interface Constants.
 */
public interface Constants {

    /**
     * The constant DEBUG.
     */
    boolean DEBUG = true;
    /**
     * The constant BASE_URL.
     */
//String BASE_URL = "https://omadmnew.movimentogroup.com/";
    String BASE_URL = "https://panapacific.movimentogroup.com/";
    /**
     * The constant URL_REGISTRATION.
     */
    String URL_REGISTRATION = "message/omadm/deviceRegister";
    /**
     * The constant URL_DEVICE_INFO.
     */
    String URL_DEVICE_INFO = "message/omadm/deviceInfo";
    /**
     * The constant URL_DEVICE_FUMO.
     */
    String URL_DEVICE_FUMO = "message/omadm/fumo";
    /**
     * The constant URL_ALERT_NOTIF.
     */
    String URL_ALERT_NOTIF = "message/omadm/bootstrapComplete";
    /**
     * The constant URL_CAPABILITY.
     */
    String URL_CAPABILITY = "message/omadm/sessionInitialize";

    /**
     * The constant URL_DEVICE_LAWMO.
     */
//TODO need to update exact LAWMO url.
    String URL_DEVICE_LAWMO = "message/omadm/lawmo";


    /**
     * The constant JSON.
     */
//Headers Constants
    String JSON = "json";
    /**
     * The constant CONTENT_TYPE_KEY.
     */
    String CONTENT_TYPE_KEY = "Content-Type";
    /**
     * The constant CONTENT_TYPE_VALUE.
     */
    String CONTENT_TYPE_VALUE = "application/vnd.oma.dm.initiation+json";
    /**
     * The constant ACCEPT_KEY.
     */
    String ACCEPT_KEY = "Accept";
    /**
     * The constant ACCEPT_VALUE.
     */
    String ACCEPT_VALUE = "application/vnd.oma.dm.request+json";
    /**
     * The constant HOST_VALUE.
     */
    String HOST_VALUE = "www.dms.com";

    /**
     * The constant OMADM_DevID_KEY.
     */
    String OMADM_DevID_KEY = "OMADM-DevID";
    /**
     * The constant VIN_KEY.
     */
    String VIN_KEY = "VIN";
    /**
     * The constant TOKEN_KEY.
     */
    String TOKEN_KEY = "Token";

    /**
     * The constant PUBLIC_KEY.
     */
    String PUBLIC_KEY = "public.key";
    /**
     * The constant PRIVATE_KEY.
     */
    String PRIVATE_KEY = "private.key";
    /**
     * The constant ALGO_RSA.
     */
    String ALGO_RSA = "RSA";


    /**
     * The constant MO_TYPE_REGISTRATION.
     */
//MO Types
    int MO_TYPE_REGISTRATION = 1;
    /**
     * The constant MO_TYPE_DEV_INFO.
     */
    int MO_TYPE_DEV_INFO = 2;
    /**
     * The constant MO_TYPE_FIRMWARE.
     */
    int MO_TYPE_FIRMWARE = 3;
    /**
     * The constant MO_TYPE_LOCK_AND_WIPE.
     */
    int MO_TYPE_LOCK_AND_WIPE = 4;
    /**
     * The constant MO_TYPE_DLOTA.
     */
    int MO_TYPE_DLOTA = 5;
    /**
     * The constant MO_TYPE_DEV_ACCOUNT.
     */
    int MO_TYPE_DEV_ACCOUNT = 6;
    /**
     * The constant MO_TYPE_ALERT.
     */
    int MO_TYPE_ALERT = 7;
    /**
     * The constant MO_TYPE_CAPABILITY.
     */
    int MO_TYPE_CAPABILITY = 8;

    /**
     * The constant MO_TYPE_DOWNLOAD_DESCRIPTOR.
     */
    int MO_TYPE_DOWNLOAD_DESCRIPTOR = 9;

    /**
     * The constant ERROR_RESPONSE_TYPE.
     */
//Error Reponse Handler
    String ERROR_RESPONSE_TYPE = "error_resonse_type";

    /**
     * The constant REGISTRATION_HANDLER.
     */
//Handler Type
    int REGISTRATION_HANDLER = 101;
    /**
     * The constant DEVICE_INFO_HANDLER.
     */
    int DEVICE_INFO_HANDLER = 102;
    /**
     * The constant DEVICE_ACCOUNT_HANDLER.
     */
    int DEVICE_ACCOUNT_HANDLER = 103;
    /**
     * The constant FUMO_HANDLER.
     */
    int FUMO_HANDLER = 104;
    /**
     * The constant LAWMO_HANDLER.
     */
    int LAWMO_HANDLER = 105;
    /**
     * The constant DLOTA_HANDLER.
     */
    int DLOTA_HANDLER = 106;

    /**
     * The constant REGISTRATION_HEADER.
     */
//Headers type
    int REGISTRATION_HEADER = 201;
    /**
     * The constant DEVICE_INFO_HEADER.
     */
    int DEVICE_INFO_HEADER = 202;
    /**
     * The constant DEVICE_ACCOUNT_HEADER.
     */
    int DEVICE_ACCOUNT_HEADER = 203;
    /**
     * The constant FUMO_HEADER.
     */
    int FUMO_HEADER = 204;
    /**
     * The constant LAWMO_HEADER.
     */
    int LAWMO_HEADER = 205;
    /**
     * The constant DLOTA_HEADER.
     */
    int DLOTA_HEADER = 206;


    /**
     * The constant IS_REGISTERED.
     */
    String IS_REGISTERED = "isRegistered";
    /**
     * The constant REG_SRV_ID.
     */
    String REG_SRV_ID = "REGISTRTION_SERVER_ID";
    /**
     * The constant REG_SRV_URI.
     */
    String REG_SRV_URI = "REGISTRATION_SERVER_URI";
    /**
     * The constant REG_CLIENT_URI.
     */
    String REG_CLIENT_URI = "REGISTRATION_CIENT_URI";
    /**
     * The constant REG_AUTH_TRSP_DATA.
     */
    String REG_AUTH_TRSP_DATA = "REGISTRATION_AUTH_DATA";
    /**
     * The constant REGI_AUTH_TRSP_SECRET.
     */
    String REGI_AUTH_TRSP_SECRET = "REGISTRATION_AUTH_SECRET";

    /**
     * The constant KEY_ALERT_NOTIFICATION_TYPE.
     */
/*Alert Type Constants*/
    //TODO
    //KEYS
    String KEY_ALERT_NOTIFICATION_TYPE = "ALERT_NOTIFICATION_TYPE";
    /**
     * The constant KEY_ALERT_TYPE.
     */
    String KEY_ALERT_TYPE = "ALERT_TYPE";
    /**
     * The constant KEY_ALERT_SERVER_URI.
     */
    String KEY_ALERT_SERVER_URI = "ALERT_SERVER_URI";
    /**
     * The constant KEY_ALERT_RESULT_CODE.
     */
    String KEY_ALERT_RESULT_CODE = "ALERT_RESULT_CODE";
    /**
     * The constant KEY_ALERT_STATE.
     */
    String KEY_ALERT_STATE = "ALERT_STATE";

    /**
     * The constant ALERT_TYPE_REGISTRATION.
     */
    String ALERT_TYPE_REGISTRATION = "REGISRATION_ALERT";
    /**
     * The constant ALERT_TYPE_FUMO.
     */
    String ALERT_TYPE_FUMO = "FUMO_ALERT";
    /**
     * The constant ALERT_TYPE_LAWMO.
     */
    String ALERT_TYPE_LAWMO = "LAWMO_ALERT";
    /**
     * The constant ALERT_TYPE_DLOTA.
     */
    String ALERT_TYPE_DLOTA = "DLOTA_ALERT";
    /**
     * The constant ALERT_TYPE_DEVINFO.
     */
    String ALERT_TYPE_DEVINFO = "DEVINFO_ALERT";
    /**
     * The constant ALERT_TYPE_DEVACC.
     */
    String ALERT_TYPE_DEVACC = "DEVACC_ALERT";
    /**
     * The constant ALERT_TYPE_CAPABILITY.
     */
    String ALERT_TYPE_CAPABILITY = "CAPABILITY_ALERT";
    /**
     * The constant ALERT_TYPE_ALERT_ERROR_RESPONSE.
     */
    String ALERT_TYPE_ALERT_ERROR_RESPONSE = "Alert_Error_Response";


    /**
     * The constant REG_ALERT_TYPE.
     */
//Registration
    String REG_ALERT_TYPE = "urn:oma:at:dm:2.0:BootstrapComplete";
    /**
     * The constant REG_ALERT_RESULT_CODE.
     */
    int REG_ALERT_RESULT_CODE = 200;
    /**
     * The constant REG_STATE.
     */
    String REG_STATE = "";

    //DevInfo Alert Type Constants

    /**
     * The constant DEV_INFO_STATUS_CODE.
     */
    int DEV_INFO_STATUS_CODE = 200;

    /**
     * The constant FUMO_ALERT_TYPE_DOWNLOAD_AND_UPDATE.
     */
//Fumo Alert Type Constants
    String FUMO_ALERT_TYPE_DOWNLOAD_AND_UPDATE = "urn:oma:at:fumo:1.0:downloadandupdate";
    /**
     * The constant FUMO_SOURCE_URI_DOWNLOAD_AND_UPDATE.
     */
    String FUMO_SOURCE_URI_DOWNLOAD_AND_UPDATE = "urn:oma:mo:oma-dm- fumo:1.0//DownloadAndUpdate";
    /**
     * The constant FUMO_ALERT_RESULT_CODE.
     */
    int FUMO_ALERT_RESULT_CODE = 200;
    /**
     * The constant FUMO_STATE.
     */

    String FUMO_STATE = "100";

    /**
     * The constant KEY_DOWNLOAD_DESC_URL.
     */
    String KEY_DOWNLOAD_DESC_URL = "DOWNLOAD_DESC_URL";

    /**
     * The constant LAWMO_ALERT_TYPE.
     */
//Lawmo Alet Type Constants
    String LAWMO_ALERT_TYPE = "urn:oma:at:lawmo:1.0:OperationComplete";
    /**
     * The constant LAWMO_SOURCE_URI.
     */
    String LAWMO_WIPE_SOURCE_URI = "urn:oma:mo:oma-dm-lawmo:1.0//Operations/Wipe";
    /**
     * The constant LAWMO_LOCK_SOURCE_URI.
     */
//TODO need to update value
    String LAWMO_LOCK_SOURCE_URI = "";


    /**
     * The constant KEY_DLOTA_URL.
     */
    //String KEY_DLOTA_URL = "key_dlota_url";
    /**
     * The constant KEY_LAWMO_TYPE.
     */
    String KEY_LAWMO_TYPE = "lawmo_type";
    /**
     * The constant LAWMO_ACTION_WIPE.
     */
    String LAWMO_ACTION_WIPE = "Lawmo_Wipe";
    /**
     * The constant LAWMO_ACTION_LOCK.
     */
    String LAWMO_ACTION_LOCK = "Lawmo_Lock";


    /**
     * The constant CHECKING_UPDATE_STATUS_EVENT.
     */
//Checking update Events and Status Constants
    String CHECKING_UPDATE_STATUS_EVENT = "com.movimento.checking.update";
    /**
     * The constant REGISTRATION_STATUS_EVENT.
     */
    String REGISTRATION_STATUS_EVENT = "com.movimento.device.registration";
    /**
     * The constant KEY_REGISTRATION_STATUS.
     */
//    String KEY_REGISTRATION_IN_PROGRESS = "registration_in_progress";
    String KEY_REGISTRATION_STATUS = "registration_status";

    /**
     * The constant DLOTA_DOWNLOAD_STATUS_EVENT.
     */
//DLota Alert Events and Status Constants
    String DLOTA_DOWNLOAD_STATUS_EVENT = "com.movimento.download.status";
    /**
     * The constant DLOTA_DOWNLOAD_STATUS.
     */
    String DLOTA_DOWNLOAD_STATUS = "download_status";
    /**
     * The constant DLOTA_DOWNLOAD_SUCCESS.
     */
    String DLOTA_DOWNLOAD_SUCCESS = "download_success";
    /**
     * The constant DLOTA_DOWNLOAD_IN_PROGRESS.
     */
    String DLOTA_DOWNLOAD_IN_PROGRESS = "download_progress";
    /**
     * The constant DLOTA_DOWNLOAD_PERCENTAGE.
     */
    String DLOTA_DOWNLOAD_PERCENTAGE = "download_percentage";
    /**
     * The constant DLOTA_DOWNLOAD_FAILURE.
     */
    String DLOTA_DOWNLOAD_FAILURE = "download_failure";
    /**
     * The constant DLOTA_DOWNLOAD_FILENAME.
     */
    String KEY_DLOTA_DOWNLOAD_FILENAME = "download_fileName";

    /**
     * The constant KEY_DLOTA_FILE.
     */
    // String KEY_DLOTA_FILE = "download_file";

    //DDA Alert Events and Status Constants
    String DDA_CHANGED_STATUS_EVENT = "com.movimento.application.dda.changed";

    /**
     * The constant LAWMO_STATE_FULLY_LOCKED.
     */
/*Request and Response Success/Error Codes*/
    //LAWMO STATE CODE
    int LAWMO_STATE_FULLY_LOCKED = 10;
    /**
     * The constant LAWMO_STATE_PARTIALLY_LOCKED.
     */
    int LAWMO_STATE_PARTIALLY_LOCKED = 20;
    /**
     * The constant LAWMO_STATE_UNLOCKED.
     */
    int LAWMO_STATE_UNLOCKED = 30;

    /**
     * The constant LAWMO_RESULT_OPERATION_SUCCEEDED.
     */
//LAWMO Result Code
    int LAWMO_RESULT_OPERATION_SUCCEEDED = 1200;
    /**
     * The constant LAWMO_RESULT_WIPE_OPERATION_SUCCEEDED_WITH_DATA_WIPE.
     */
    int LAWMO_RESULT_WIPE_OPERATION_SUCCEEDED_WITH_DATA_WIPE = 1201;
    /**
     * The constant LAWMO_RESULT_WIPE_OPERATION_SUCCEEDED_WITH_DATA_LIST_WIPE.
     */
    int LAWMO_RESULT_WIPE_OPERATION_SUCCEEDED_WITH_DATA_LIST_WIPE = 1202;
    /**
     * The constant LAWMO_SUCCESSFUL_VENDOR_SPECIFIED_MIN.
     */
    int LAWMO_SUCCESSFUL_VENDOR_SPECIFIED_MIN = 1250;
    /**
     * The constant LAWMO_SUCCESSFUL_VENDOR_SPECIFIED_MAX.
     */
    int LAWMO_SUCCESSFUL_VENDOR_SPECIFIED_MAX = 1299;
    /**
     * The constant LAWMO_RESULT_CLIENT_ERROR.
     */
    int LAWMO_RESULT_CLIENT_ERROR = 1400;
    /**
     * The constant LAWMO_RESULT_USER_CANCELLED.
     */
    int LAWMO_RESULT_USER_CANCELLED = 1401;
    /**
     * The constant LAWMO_RESULT_FULLY_LOCKED_DEVICE_FAILED.
     */
    int LAWMO_RESULT_FULLY_LOCKED_DEVICE_FAILED = 1402;
    /**
     * The constant LAWMO_RESULT_PARTIALLY_LOCKED_DEVICE_FAILED.
     */
    int LAWMO_RESULT_PARTIALLY_LOCKED_DEVICE_FAILED = 1403;
    /**
     * The constant LAWMO_RESULT_UNLCOK_DEVICE_FAILED.
     */
    int LAWMO_RESULT_UNLCOK_DEVICE_FAILED = 1404;
    /**
     * The constant LAWMO_RESULT_WIPE_DEVICE_DATA_FAILED.
     */
    int LAWMO_RESULT_WIPE_DEVICE_DATA_FAILED = 1405;
    /**
     * The constant LAWMO_RESULT_WIPE_OPERATION_NOT_PERFORMED.
     */
    int LAWMO_RESULT_WIPE_OPERATION_NOT_PERFORMED = 1406;


    /**
     * The constant FUMO_STATE_IDLE_START.
     */
//FUMO state code
    int FUMO_STATE_IDLE_START = 10;
    /**
     * The constant FUMO_STATE_DOWNLOAD_FAILED.
     */
    int FUMO_STATE_DOWNLOAD_FAILED = 20;
    /**
     * The constant FUMO_STATE_DOWNLOAD_PROGRESSING.
     */
    int FUMO_STATE_DOWNLOAD_PROGRESSING = 30;
    /**
     * The constant FUMO_STATE_DOWNLOAD_COMPLETE.
     */
    int FUMO_STATE_DOWNLOAD_COMPLETE = 40;
    /**
     * The constant FUMO_STATE_READY_TO_UPDATE.
     */
    int FUMO_STATE_READY_TO_UPDATE = 50;
    /**
     * The constant FUMO_STATE_UPDATE_PROGRESSING.
     */
    int FUMO_STATE_UPDATE_PROGRESSING = 60;
    /**
     * The constant FUMO_STATE_UPDATE_FAILED_HAVE_DATA.
     */
    int FUMO_STATE_UPDATE_FAILED_HAVE_DATA = 70;
    /**
     * The constant FUMO_STATE_UPDATE_FAILED_NO_DATA.
     */
    int FUMO_STATE_UPDATE_FAILED_NO_DATA = 80;
    /**
     * The constant FUMO_STATE_UPDATE_SUCCESSFUL_HAVE_DATA.
     */
    int FUMO_STATE_UPDATE_SUCCESSFUL_HAVE_DATA = 90;
    /**
     * The constant FUMO_STATE_UPDATE_SUCCESSFUL_NO_DATA.
     */
    int FUMO_STATE_UPDATE_SUCCESSFUL_NO_DATA = 100;


    /**
     * The constant FUMO_RESULT_SUCCESSFUL.
     */
//FUMO Result code
    int FUMO_RESULT_SUCCESSFUL = 200;
    /**
     * The constant FUMO_RESULT_MANAGEMENT_CLIENT_ERROR.
     */
    int FUMO_RESULT_MANAGEMENT_CLIENT_ERROR = 400;
    /**
     * The constant FUMO_RESULT_USER_CANCELLED.
     */
    int FUMO_RESULT_USER_CANCELLED = 401;
    /**
     * The constant FUMO_RESULT_CORRUPTED_FIRMWARE_PACKAGE.
     */
    int FUMO_RESULT_CORRUPTED_FIRMWARE_PACKAGE = 402;
    /**
     * The constant FUMO_RESULT_FIRMWARE_UPDATE_PACKAGE_DEVICE_MISMATCH.
     */
    int FUMO_RESULT_FIRMWARE_UPDATE_PACKAGE_DEVICE_MISMATCH = 403;
    /**
     * The constant FUMO_RESULT_FAILED_FIRMWARE_UPDATE_PACKAGE_VALIDATION.
     */
    int FUMO_RESULT_FAILED_FIRMWARE_UPDATE_PACKAGE_VALIDATION = 404;
    /**
     * The constant FUMO_RESULT_FIRMWARE_UPDATE_PACKAGE_NOT_ACCEPTABLE.
     */
    int FUMO_RESULT_FIRMWARE_UPDATE_PACKAGE_NOT_ACCEPTABLE = 405;
    /**
     * The constant FUMO_RESULT_ALTERNATE_DOWNLOAD_AUTHENTICATION_FAILURE.
     */
    int FUMO_RESULT_ALTERNATE_DOWNLOAD_AUTHENTICATION_FAILURE = 406;
    /**
     * The constant FUMO_RESULT_ALTERNATE_DOWNLOAD_REQUEST_TIME_OUT.
     */
    int FUMO_RESULT_ALTERNATE_DOWNLOAD_REQUEST_TIME_OUT = 407;
    /**
     * The constant FUMO_RESULT_NOT_IMPLEMENTED.
     */
    int FUMO_RESULT_NOT_IMPLEMENTED = 408;
    /**
     * The constant FUMO_RESULT_UNIDENTIFIED_ERROR.
     */
    int FUMO_RESULT_UNIDENTIFIED_ERROR = 409;
    /**
     * The constant FUMO_RESULT_FIRMWARE_UPDATE_FAILED.
     */
    int FUMO_RESULT_FIRMWARE_UPDATE_FAILED = 410;
    /**
     * The constant FUMO_RESULT_MALFORMED_URL.
     */
    int FUMO_RESULT_MALFORMED_URL = 411;
    /**
     * The constant FUMO_RESULT_ALLTERNATE_DOWNLOAD_SERVER_UNAVAILABLE.
     */
    int FUMO_RESULT_ALLTERNATE_DOWNLOAD_SERVER_UNAVAILABLE = 412;
    /**
     * The constant FUMO_RESULT_CLIENT_ERROR_VENDOR_SPECIFIED_MIN.
     */
    int FUMO_RESULT_CLIENT_ERROR_VENDOR_SPECIFIED_MIN = 450;
    /**
     * The constant FUMO_RESULT_CLIENT_ERROR_VENDOR_SPECIFIED_MAX.
     */
    int FUMO_RESULT_CLIENT_ERROR_VENDOR_SPECIFIED_MAX = 499;
    /**
     * The constant FUMO_RESULT_ALTERNATE_DOWNLOAD_SERVER_ERROR.
     */
    int FUMO_RESULT_ALTERNATE_DOWNLOAD_SERVER_ERROR = 500;
    /**
     * The constant FUMO_RESULT_DOWNLOAD_FAIL_DEVICE_OUT_OF_MEMORY.
     */
    int FUMO_RESULT_DOWNLOAD_FAIL_DEVICE_OUT_OF_MEMORY = 501;
    /**
     * The constant FUMO_RESULT_FIRMWARE_UPDATE_FAILS_DEVICE_OUT_OF_MEMORY.
     */
    int FUMO_RESULT_FIRMWARE_UPDATE_FAILS_DEVICE_OUT_OF_MEMORY = 502;
    /**
     * The constant FUMO_RESULT_DOWNLOAD_FAILS_NETWORK_ISSUE.
     */
    int FUMO_RESULT_DOWNLOAD_FAILS_NETWORK_ISSUE = 503;


    /**
     * The constant DLOTA_STATE_IDLE.
     */
//DLOTA state code
    int DLOTA_STATE_IDLE = 10;
    /**
     * The constant DLOTA_STATE_DOWNLOAD_FAILED.
     */
    int DLOTA_STATE_DOWNLOAD_FAILED = 20;
    /**
     * The constant DLOTA_STATE_DOWNLOAD_PROGRESSING.
     */
    int DLOTA_STATE_DOWNLOAD_PROGRESSING = 30;
    /**
     * The constant DLOTA_STATE_DOWNLOAD_COMPLETE.
     */
    int DLOTA_STATE_DOWNLOAD_COMPLETE = 40;
    /**
     * The constant DLOTA_STATE_READY_TO_UPDATE.
     */
    int DLOTA_STATE_READY_TO_UPDATE = 50;
    /**
     * The constant DLOTA_STATE_UPDATE_PROGRESSING.
     */
    int DLOTA_STATE_UPDATE_PROGRESSING = 60;
    /**
     * The constant DLOTA_STATE_UPDATE_FAILED_HAVE_DATA.
     */
    int DLOTA_STATE_UPDATE_FAILED_HAVE_DATA = 70;


    /**
     * The constant DLOTA_RESULT_SUCCESS.
     */
//DLOTA Result code
    int DLOTA_RESULT_SUCCESS = 900;
    /**
     * The constant DLOTA_RESULT_INSUFFICIENT_MEMORY.
     */
    int DLOTA_RESULT_INSUFFICIENT_MEMORY = 901;
    /**
     * The constant DLOTA_RESULT_USER_CANCELLED.
     */
    int DLOTA_RESULT_USER_CANCELLED = 902;
    /**
     * The constant DLOTA_RESULT_LOSS_OF_SERVICE.
     */
    int DLOTA_RESULT_LOSS_OF_SERVICE = 903;
    /**
     * The constant DLOTA_RESULT_ATTRIBUTE_MISMATCH.
     */
    int DLOTA_RESULT_ATTRIBUTE_MISMATCH = 905;
    /**
     * The constant DLOTA_RESULT_INVALID_DECRIPTOR.
     */
    int DLOTA_RESULT_INVALID_DECRIPTOR = 906;
    /**
     * The constant DLOTA_RESULT_INVALID_TYPE.
     */
    int DLOTA_RESULT_INVALID_TYPE = 907;
    /**
     * The constant DLOTA_RESULT_INVALID_DDVERSION.
     */
    int DLOTA_RESULT_INVALID_DDVERSION = 951;
    /**
     * The constant DLOTA_RESULT_DEVICE_ABORTED.
     */
    int DLOTA_RESULT_DEVICE_ABORTED = 952;
    /**
     * The constant DLOTA_RESULT_NON_ACCEPTABLE_CONTENT.
     */
    int DLOTA_RESULT_NON_ACCEPTABLE_CONTENT = 953;
    /**
     * The constant DLOTA_RESULT_LOADER_ERROR.
     */
    int DLOTA_RESULT_LOADER_ERROR = 954;
    /**
     * The constant DLOTA_RESULT_MEDIA_OBJECT_UPDATED.
     */
    int DLOTA_RESULT_MEDIA_OBJECT_UPDATED = 955;
    /**
     * The constant DLOTA_RESULT_LICENCE_RETRIEVAL_SUCCESS.
     */
    int DLOTA_RESULT_LICENCE_RETRIEVAL_SUCCESS = 956;
    /**
     * The constant DLOTA_RESULT_LICENCE_RETRIEVAL_FAILED.
     */
    int DLOTA_RESULT_LICENCE_RETRIEVAL_FAILED = 957;
    /**
     * The constant DLOTA_RESULT_VERSION_ALREADY_AVAILABLE.
     */
    int DLOTA_RESULT_VERSION_ALREADY_AVAILABLE = 958;
    /**
     * The constant DLOTA_RESULT_ENVTYPE_NOT_SUPPORTED.
     */
    int DLOTA_RESULT_ENVTYPE_NOT_SUPPORTED = 959;
    /**
     * The constant DLOTA_RESULT_ENVIRONMENT_INTERNAL_STATUS.
     */
    int DLOTA_RESULT_ENVIRONMENT_INTERNAL_STATUS = 960;
    /**
     * The constant DLOTA_RESULT_MIXED_STATUS.
     */
    int DLOTA_RESULT_MIXED_STATUS = 970;

    /**
     * The constant MT_PARSER_DEVICE_INFO.
     */
//Message Tree Parser codes
    int MT_PARSER_DEVICE_INFO = 1;
    /**
     * The constant MT_PARSER_FUMO.
     */
    int MT_PARSER_FUMO = 2;
    /**
     * The constant MT_PARSER_LAWMO.
     */
    int MT_PARSER_LAWMO = 3;
    /**
     * The constant MT_PARSER_DEVICE_ACCOUNT.
     */
    int MT_PARSER_DEVICE_ACCOUNT = 4;
    /**
     * The constant MT_PARSER_FUMO_STATE.
     */
    int MT_PARSER_FUMO_STATE = 5;
    /**
     * The constant MT_PARSER_CAPABILITY.
     */
    int MT_PARSER_CAPABILITY = 6;
    /**
     * The constant MT_PARSER_DOWNLOAD_DESCRIPTOR.
     */
    int MT_PARSER_DOWNLOAD_DESCRIPTOR = 7;

    /**
     * The constant KEY_ERRROR_TYPE.
     */
//Error Handling Constants
    String KEY_ERRROR_TYPE = "ERROR_TYPE";
    /**
     * The constant KEY_ERROR_HEADER.
     */
    String KEY_ERROR_HEADER = "ERROR_HEADER";
    /**
     * The constant KEY_ERROR_INFO.
     */
    String KEY_ERROR_INFO = "ERROR_INFO";

    /**
     * The constant ERROR_TYPE_NETWORK_CONNECTION.
     */
    String ERROR_TYPE_NETWORK_CONNECTION = "ERROR_NETWORK_CONNECTION";
    /**
     * The constant ERROR_TYPE_CERTIFICATE.
     */
    String ERROR_TYPE_CERTIFICATE = "ERROR_CERTIFICATE";
    /**
     * The constant ERROR_TYPE_INSTALLATION_DOWNLOADING.
     */
    String ERROR_TYPE_INSTALLATION_DOWNLOADING = "ERROR_DOWNLOAD_INSTALL";


    /**
     * The constant KEY_JSON_ALERT.
     */
//Alert Json Parser Constancts
    String KEY_JSON_ALERT = "Alert";
    /**
     * The constant KEY_JSON_ALERT_SOURCE_URI.
     */
    String KEY_JSON_ALERT_SOURCE_URI = "SourceURI";
    /**
     * The constant KEY_JSON_ALERT_DATA.
     */

    String KEY_JSON_ALERT_TYPE = "AlertType";

    String KEY_JSON_ALERT_DATA = "Data";
    /**
     * The constant KEY_JSON_ALERT_RESULT_CODE.
     */
    String KEY_JSON_ALERT_RESULT_CODE = "ResultCode";
    /**
     * The constant KEY_JSON_ALERT_STATE.
     */
    String KEY_JSON_ALERT_STATE = "State";


    /**
     * The constant PRODUCTION_BUILD.
     */
    boolean PRODUCTION_BUILD = true;

    /**
     * The constant DLOTA_DOWNLOAD_FILENAME.
     */
    String DLOTA_DOWNLOAD_FILENAME = "download_file_name";
    /**
     * The constant DEBUGGABLE_MODE.
     */
//Turn on/off debugging logs
    boolean DEBUGGABLE_MODE = true;
    /**
     * The constant IS_REMINDER_SET.
     */
    String IS_REMINDER_SET = "false";
    /**
     * The constant REMINDER_VALUE.
     */
    String REMINDER_VALUE = "0";

    /**
     * The constant FIRST_APP_RUN.
     */
    String FIRST_APP_RUN = "firstapprun";
    /**
     * The constant IS_INSTALL_NOTIFY_ALERT.
     */
    String IS_INSTALL_NOTIFY_ALERT = "isinstallnotifyalert";

    String CERTIFICATE_ERROR_TIME="certificateerrortimediff";

    String ERROR_TYPE_INSTALLATION ="Installation Error" ;

    /*
    * Check if installation has been started or not
    * */
    String IS_INSTALLATION_STARTED = "is_installation_started";

    String ERROR_TYPE_MEMORY ="No Memory Error";

}
