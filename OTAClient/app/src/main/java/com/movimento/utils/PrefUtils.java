package com.movimento.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.movimento.otaclient.OTAClientApp;

/**
 * The type Pref utils.
 */
public class PrefUtils {
    private static final String TAG = PrefUtils.class.getCanonicalName();
    private static SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(OTAClientApp.getmAppInstance());

    /**
     * Put string.
     *
     * @param key   the key
     * @param value the value
     */
    public static void putString(String key, String value) {
        prefs.edit().putString(key, value).apply();
    }

    /**
     * Gets string.
     *
     * @param key      the key
     * @param defValue the def value
     * @return the string
     */
    public static String getString(String key, String defValue) {
        return prefs.getString(key, defValue);
    }

    /**
     * Put boolean.
     *
     * @param key   the key
     * @param value the value
     */
    public static void putBoolean(String key, boolean value) {
        prefs.edit().putBoolean(key, value).apply();
    }

    /**
     * Gets boolean.
     *
     * @param key      the key
     * @param defValue the def value
     * @return the boolean
     */
    public static boolean getBoolean(String key, boolean defValue) {
        return prefs.getBoolean(key, defValue);
    }

    /**
     * Put int.
     *
     * @param key   the key
     * @param value the value
     */
    public static void putInt(String key, int value) {
        prefs.edit().putInt(key, value).apply();
    }

    /**
     * Gets int.
     *
     * @param key      the key
     * @param defValue the def value
     * @return the int
     */
    public static int getInt(String key, int defValue) {
        return prefs.getInt(key, defValue);
    }

    /**
     * Put long.
     *
     * @param key   the key
     * @param value the value
     */
    public static void putLong(String key, long value) {
        prefs.edit().putLong(key, value).apply();
    }

    /**
     * Gets long.
     *
     * @param key      the key
     * @param defValue the def value
     * @return the long
     */
    public static long getLong(String key, long defValue) {
        return prefs.getLong(key, defValue);
    }

    /**
     * Put double.
     *
     * @param key   the key
     * @param value the value
     */
    public static void putDouble(String key, double value) {
        putLong(key, Double.doubleToLongBits(value));
    }

    /**
     * Gets double.
     *
     * @param key      the key
     * @param defValue the def value
     * @return the double
     */
    public static double getDouble(String key, double defValue) {
        return Double.longBitsToDouble(getLong(key, Double.doubleToLongBits(defValue)));
    }

    /**
     * Gets double.
     *
     * @param key      the key
     * @param defValue the def value
     * @return the double
     */
    public static double getDouble(String key, long defValue) {
        return Double.longBitsToDouble(getLong(key, defValue));
    }

    /**
     * Clear.
     */
    public static void clear() {
        prefs.edit().clear().apply();
    }

}
