package com.movimento.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.movimento.motreeparser.DeviceInfoParser;
import com.movimento.motreeparser.FumoParser;
import com.movimento.motreeparser.FumoStateParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.otaclient.ErrorActivity;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.otaclient.R;
import com.movimento.service.AlertIntentService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static android.content.Context.ACTIVITY_SERVICE;
import static com.movimento.utils.FileUtils.LOG_FILE_NAME;

/**
 * The type Utils.
 */
public class Utils {
    private static final String TAG = Utils.class.getCanonicalName();

    /**
     * The constant WIRELESS_PORT.
     */
    public static final String WIRELESS_PORT = "wlan0";

    /**
     * Sets header.
     *
     * @param context  the context
     * @param tvHeader the tv header
     */
/*This Method will set the header "Device Update" and also set the font style of the same*/
    @SuppressWarnings("deprecation")
    public static void setHeader(Context context, TextView tvHeader) {
        String update = context.getResources().getString(R.string.header_update);
        String mobile = "<font color='#F5EF2F'>Device</font>";
        FaceTypeUtil.setTypeFace(context, tvHeader, FaceTypeUtil.BAUHAUS_FONT);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            tvHeader.setText(Html.fromHtml(mobile + " " + update, Html.FROM_HTML_MODE_LEGACY));
        } else {
            tvHeader.setText(Html.fromHtml(mobile + " " + update));
        }
    }


    /**
     * Is my service running boolean.
     *
     * @param serviceClass the service class
     * @return the boolean
     */
    public static boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) OTAClientApp.getmAppInstance().getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i(TAG, "Service Running status: " + false + " Need to start OTAClientService...");
        return false;
    }

    /**
     * Is connected boolean.
     *
     * @return the boolean
     */
    public static boolean isNetworkAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) OTAClientApp.getmAppInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    /**
     * Load json from asset string.
     *
     * @param context  the context
     * @param fileName the file name
     * @return the string
     */
    public static String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            //InputStream is = context.getResources().openRawResource(R.raw.sess
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    /**
     * Write to f ile.
     *
     * @param mContext     the m context
     * @param json         the json
     * @param jsonFileName the json file name
     */
/*This method will write the updated values in the json file stored on internal location*/
    public static void writeToFIle(Context mContext, String json, String jsonFileName) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(mContext.openFileOutput(jsonFileName, Context.MODE_PRIVATE));
            outputStreamWriter.write(json);
            outputStreamWriter.close();

            // readFromFile(mContext,jsonFileName);
        } catch (Exception e) {

        }
    }


    /**
     * Read from file string.
     *
     * @param mContext     the m context
     * @param jsonFileName the json file name
     * @return the string
     */
/*This method will read the updated values from the json file stored on internal location*/
    public static String readFromFile(Context mContext, String jsonFileName) {

        String ret = "";

        try {
            InputStream inputStream = mContext.openFileInput(jsonFileName);
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
                Log.i("read values:-", ret);
            }
        } catch (FileNotFoundException e) {
            Log.e("", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("", "Can not read file: " + e.toString());
        }

        return ret;
    }

    /**
     * This method reads the json file from the asset folder and returns the json string.
     *
     * @param jsonFileName - Json file to be read from the asset folder.
     * @param mContext     - Context of the calling class.
     * @return - json string.
     */
    public static String getJsonString(String jsonFileName, Context mContext) {
        AssetManager assetManager = mContext.getAssets();
        InputStream ims = null;
        String json = null;
        try {
            ims = assetManager.open(jsonFileName);

            int size = ims.available();

            byte[] buffer = new byte[size];
            ims.read(buffer);
            ims.close();
            json = new String(buffer, "UTF-8");
            Log.d("Json String :--", "hksdfgisdfgisdg");

            return json;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Gets json obj.
     *
     * @param nodeName the node name
     * @param jArray   the j array
     * @return the json obj
     * @throws JSONException the json exception
     */
    public static JSONObject getJsonObj(String nodeName, JSONArray jArray) throws JSONException {
        String nName = "";
        JSONObject nodeJObj = null;
        JSONObject jObj = null;

        int childNodesCount = jArray.length();
        for (int i = 0; i < childNodesCount; i++) {
            nodeJObj = jArray.getJSONObject(i);
            if (nodeJObj.has("NodeName")) {
                nName = nodeJObj.get("NodeName").toString();
                if (nName.equalsIgnoreCase(nodeName)) {
                    jObj = nodeJObj;
                    return jObj;
                }
            }
        }
        return jObj;
    }

    /**
     * Send error.
     *
     * @param mContext     the m context
     * @param mErrorType   the m error type
     * @param mErrorHeader the m error header
     * @param mErrorMsg    the m error msg
     */
    public static void sendError(Context mContext, String mErrorType, String mErrorHeader, String mErrorMsg) {
            Intent intent = new Intent(mContext, ErrorActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Constants.KEY_ERRROR_TYPE, mErrorType);
            intent.putExtra(Constants.KEY_ERROR_HEADER, mErrorHeader);
            intent.putExtra(Constants.KEY_ERROR_INFO, mErrorMsg);
            mContext.startActivity(intent);
    }

    private static boolean generateFile(String fileName) {
        boolean flag = false;
        File file = new File(fileName);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
        }
        try {
            flag = file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * Log message.
     *
     * @param TAG     the tag
     * @param message the message
     */
    public static void logMessage(String TAG, String message) {
        FileOutputStream fOut = null;
        OutputStreamWriter osw = null;
        File loggedFile = new File(FileUtils.getOTAClientFolder(FileUtils.OTACLIENT_LOG_FOLDER), LOG_FILE_NAME);
        if (!loggedFile.exists()) {
            generateFile(loggedFile.toString());
        }
        Date date = new Date();
        Log.i(TAG, "Time: " + date);

        try {
            fOut = new FileOutputStream(loggedFile, true);
            osw = new OutputStreamWriter(fOut);
            osw.append(date + " : " + TAG + " : " + message);
            osw.append("\n\n");
            osw.flush();
            osw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets available space.
     *
     * @param directory the directory
     * @return the available space
     */
    public static long getAvailableSpace(String directory) {
        File f = new File(directory);
        return f.getFreeSpace();
    }

    /**
     * Gets total space.
     *
     * @param directory the directory
     * @return the total space
     */
    public static long getTotalSpace(String directory) {
        File f = new File(directory);
        return f.getTotalSpace();
    }

    /**
     * Gets device id.
     *
     * @return the device id
     */
    public static String getDeviceId1() {
        // TODO: Need to change the method name as we have to use MAC ID instead of device id(which is getting reset after device flash)
        String deviceId = Settings.Secure.getString(OTAClientApp.getmAppInstance().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.v(TAG, "AndroidDevice Id:" + deviceId);
        return deviceId;
    }

    /**
     * Stop running service.
     *
     * @param serviceClass the service class
     */
    public static void stopRunningService(Class<?> serviceClass) {
        if (isServiceRunning(serviceClass))
            OTAClientApp.getmAppInstance().stopService(new Intent(OTAClientApp.getmAppInstance(), serviceClass));
    }

    /**
     * This method will set the font Style and color on the action bar
     *
     * @param mContext the m context
     */
    public static void createCustomActionBarTitle(Context mContext) {
        ((AppCompatActivity) mContext).getSupportActionBar().setDisplayShowCustomEnabled(true);
        ((AppCompatActivity) mContext).getSupportActionBar().setDisplayShowTitleEnabled(false);

        LayoutInflater inflator = LayoutInflater.from(mContext);
        View v = inflator.inflate(R.layout.custom_action_bar, null);


        TextView titleTv = ((TextView) v.findViewById(R.id.title));
        FaceTypeUtil.setTypeFace(mContext, titleTv, FaceTypeUtil.BAUHAUS_FONT);
        titleTv.setText(mContext.getResources().getString(R.string.app_name));

        //assign the view to the actionbar
        ((AppCompatActivity) mContext).getSupportActionBar().setCustomView(v);
        ((AppCompatActivity) mContext).getSupportActionBar().setBackgroundDrawable(new ColorDrawable(mContext.getResources().getColor(R.color.yellow)));
    }

    /**
     * Gets mac address.
     *
     * @param interfaceName the interface name
     * @return the mac address
     */
    public static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) return "";
                StringBuilder buf = new StringBuilder();
                for (int idx = 0; idx < mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);
                return buf.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    /**
     * Update mac address boolean.
     *
     * @param macAddress the mac address
     * @return the boolean
     */
    public static boolean updateMacAddress(String macAddress) {
        Log.v(TAG, "Device Mac Address: " + macAddress);
        boolean flag = false;
        if (macAddress != null && !macAddress.isEmpty()) {
            macAddress = macAddress.replaceAll(":", "");
            if (macAddress.length() == 12) {
                flag = true;
                Log.v(TAG, "Mac address " + macAddress + " is of length 12 which is to be send on server");
                DeviceInfoParser deviceInfoParser = (DeviceInfoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_DEVICE_INFO);
                if (deviceInfoParser != null) {
                    try {
                        //update devId and VIN(aka MAC_ADDRESS) in devInfoMOTree
                        //USing MAC ID instead of device ID
                        deviceInfoParser.updateChildNodeDFTitle(ParserConstants.KEY_DEV_ID, macAddress);
                        deviceInfoParser.updateChildNodeDFTitle(ParserConstants.KEY_VIN, macAddress);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            flag = false;
            Log.v(TAG, "DEVICE MAC ID IS EMPTY");
        }

        return flag;
    }

    /**
     * This method sets the boolean value when the reminder duration is set by the user and saves the duration interval as well.
     *
     * @param isReminderSet    - boolean value stating that if the reminder is set by the user.
     * @param reminderDuration - the duration selected by the user.
     * @param context          - calling activity context.
     */
    public static void setReminderFlag(boolean isReminderSet, int reminderDuration, Context context) {

        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = PreferenceManager.getDefaultSharedPreferences(context);
        editor = settings.edit();

        editor.putBoolean(Constants.IS_REMINDER_SET, isReminderSet);
        editor.putInt(Constants.REMINDER_VALUE, reminderDuration);


        editor.commit();
    }

    /**
     * This method returns a boolean value stating that whether or not a reminder has been set by the user.
     *
     * @param context - calling activity context.
     * @return - boolean value.
     */
    public static boolean getReminderFlagVal(Context context) {

        boolean isReminderSet = false;
        SharedPreferences settings;
        settings = PreferenceManager.getDefaultSharedPreferences(context);

        isReminderSet = settings.getBoolean(Constants.IS_REMINDER_SET, false);
        return isReminderSet;
    }

    /**
     * This methods returns the reminder time duration selected by the user.
     *
     * @param context - calling activity context.
     * @return - int value having the reminding time duration
     */
    public static int getReminderDuration(Context context) {
        int duration = 0;

        SharedPreferences settings;
        settings = PreferenceManager.getDefaultSharedPreferences(context);

        duration = settings.getInt(Constants.REMINDER_VALUE, 0);

        return duration;
    }


    /**
     * If previous version boolean.
     *
     * @return the boolean
     */
    public static boolean ifPreviousVersion() {
        FumoParser fumoParser = (FumoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO);
        boolean result = true;
        if (PrefUtils.getBoolean(Constants.FIRST_APP_RUN, true)) {
            try {
                fumoParser.setNodeDFTitle(ParserConstants.KEY_FUMO_PKG_VERSION, Build.VERSION.INCREMENTAL, "");
            } catch (JSONException e) {
                e.printStackTrace();
                Utils.logMessage(TAG, Log.getStackTraceString(e));
            }
            PrefUtils.putBoolean(Constants.FIRST_APP_RUN, false);
            result = true;
        } else {
            String buildIncrementalVersion = null;
            try {
                buildIncrementalVersion = fumoParser.getNodeDfTitle(ParserConstants.KEY_FUMO_PKG_VERSION, "");
                result = Build.VERSION.INCREMENTAL.equals(buildIncrementalVersion);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logMessage(TAG, Log.getStackTraceString(e));
            }
        }
        return result;
    }

    /**
     * Do if updated.
     */
    public static void doIfUpdated() {
        try {
            FumoParser fumoParser = (FumoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO);
            fumoParser.setNodeDFTitle(ParserConstants.KEY_FUMO_PKG_VERSION, Build.VERSION.INCREMENTAL, "");
            Intent alertIntent = new Intent(OTAClientApp.getmAppInstance(), AlertIntentService.class);
            alertIntent.putExtra(Constants.IS_INSTALL_NOTIFY_ALERT, true);
            alertIntent.putExtra(Constants.KEY_ALERT_NOTIFICATION_TYPE, Constants.ALERT_TYPE_FUMO);
            alertIntent.putExtra(Constants.KEY_ALERT_TYPE, Constants.FUMO_ALERT_TYPE_DOWNLOAD_AND_UPDATE);
            alertIntent.putExtra(Constants.KEY_ALERT_SERVER_URI, Constants.FUMO_SOURCE_URI_DOWNLOAD_AND_UPDATE);
            alertIntent.putExtra(Constants.KEY_ALERT_RESULT_CODE, Constants.FUMO_RESULT_SUCCESSFUL);
            alertIntent.putExtra(Constants.KEY_ALERT_STATE, Constants.FUMO_STATE_UPDATE_SUCCESSFUL_NO_DATA);
            OTAClientApp.getmAppInstance().startService(alertIntent);

            Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_IDLE_START));

        } catch (Exception e) {
            e.printStackTrace();
            Utils.logMessage(TAG, Log.getStackTraceString(e));
        }
    }


    public static void doIfUpdateFailed(){
        try {
            Intent alertIntent = new Intent(OTAClientApp.getmAppInstance(), AlertIntentService.class);
            alertIntent.putExtra(Constants.IS_INSTALL_NOTIFY_ALERT, true);
            alertIntent.putExtra(Constants.KEY_ALERT_NOTIFICATION_TYPE, Constants.ALERT_TYPE_FUMO);
            alertIntent.putExtra(Constants.KEY_ALERT_TYPE, Constants.FUMO_ALERT_TYPE_DOWNLOAD_AND_UPDATE);
            alertIntent.putExtra(Constants.KEY_ALERT_SERVER_URI, Constants.FUMO_SOURCE_URI_DOWNLOAD_AND_UPDATE);
            alertIntent.putExtra(Constants.KEY_ALERT_RESULT_CODE, Constants.FUMO_RESULT_FIRMWARE_UPDATE_FAILED);
            alertIntent.putExtra(Constants.KEY_ALERT_STATE, Constants.FUMO_STATE_UPDATE_FAILED_NO_DATA);
            OTAClientApp.getmAppInstance().startService(alertIntent);

            Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_IDLE_START));

        } catch (Exception e) {
            e.printStackTrace();
            Utils.logMessage(TAG, Log.getStackTraceString(e));
        }
    }


    /**
     * Update dlota states.
     *
     * @param key   the key
     * @param value the value
     */
    public static void updateDlotaStates(String key, String value) {
        FumoStateParser fumoStateParser = (FumoStateParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO_STATE);
        if (fumoStateParser != null) {
            try {
                fumoStateParser.updateFumoState(key, value);
                logMessage(TAG, "Fumo State updated. KEY:" + key + " Value:" + value);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logMessage(TAG, Log.getStackTraceString(e));
            }
        }

        FumoParser fumoParser = (FumoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO);
        try {
            fumoParser.updateNodeAttribute(ParserConstants.KEY_FUMO_STATE, "",
                    ParserConstants.DF_TITLE, value);
        } catch (JSONException e) {
            e.printStackTrace();
            Utils.logMessage(TAG, Log.getStackTraceString(e));
        }
    }


    public static int getDlotaState(){
        FumoStateParser fumoStateParser = (FumoStateParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO_STATE);
        return Integer.parseInt(fumoStateParser.getState(ParserConstants.CURRENT_STATE));
    }
}

