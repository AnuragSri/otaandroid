package com.movimento.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.movimento.otaclient.DownloadUpdateActivity;
import com.movimento.otaclient.R;

/*Async Task Checks with server that any update is available or not*/

/**
 * The type Checking update async task.
 */
public class CheckingUpdateAsyncTask extends AsyncTask<Integer, Integer, Boolean> {
    private Context context;
    private Intent intent;
    private Boolean UPDATE_AVAILABLE = true;

    /**
     * Instantiates a new Checking update async task.
     *
     * @param context the context
     */
    public CheckingUpdateAsyncTask(Context context) {
        this.context = context;
        intent = new Intent(context, DownloadUpdateActivity.class);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Integer... params) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return UPDATE_AVAILABLE;
    }


    @Override
    protected void onPostExecute(Boolean updateAvailable) {

        /*If Update is available user will be move to Download Update Screen*/
        if (updateAvailable) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
        super.onPostExecute(updateAvailable);

    }
}
