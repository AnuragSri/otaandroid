package com.movimento.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * The type Face type util.
 */
public class FaceTypeUtil {

    /**
     * The constant BAUHAUS_FONT.
     */
    public final static String BAUHAUS_FONT = "fonts/Bauhaus.ttf";
    /**
     * The constant EXTRA_LIGHT.
     */
    public final static String EXTRA_LIGHT = "fonts/Core_Sans_NW0125_ExtraLight.ttf";

    /**
     * Set type face.
     *
     * @param ctx  the ctx
     * @param view the view
     * @param ttf  the ttf
     */
    public static void setTypeFace(Context ctx, TextView view, String ttf) {
        Typeface headerTypeFace = Typeface.createFromAsset(ctx.getAssets(), ttf);
        view.setTypeface(headerTypeFace);
    }
}
