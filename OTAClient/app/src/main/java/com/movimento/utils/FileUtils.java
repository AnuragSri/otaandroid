package com.movimento.utils;


import android.os.Environment;

import java.io.File;
import java.text.DecimalFormat;

/**
 * The type File utils.
 */
public class FileUtils {
    private static final String TAG = FileUtils.class.getSimpleName();
    private static final String OTACLIENT_ROOT_FOLDER = Environment.getExternalStorageDirectory() + File.separator + "OTAClient" + File.separator;
    /**
     * The constant OTACLIENT_DOWNLOAD_FOLDER.
     */
    public static final String OTACLIENT_DOWNLOAD_FOLDER = "Download";
    /**
     * The constant CACHE_FOLDER.
     */
    public static final String CACHE_FOLDER = "/cache/fota/";
    /**
     * The constant OTACLIENT_LOG_FOLDER.
     */
    public static final String OTACLIENT_LOG_FOLDER = "Log";
    /**
     * The constant LOG_FILE_NAME.
     */
    public static final String LOG_FILE_NAME = "log.txt";


    private FileUtils() {
    }

    /**
     * Gets ota client folder.
     *
     * @param folderName the folder name
     * @return the ota client folder
     */
    public static File getOTAClientFolder(String folderName) {
        File directory = new File(OTACLIENT_ROOT_FOLDER + folderName);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        return directory;
    }

    /**
     * Gets cachet folder.
     *
     * @return the cachet folder
     */
    public static File getCachetFolder() {
        File directory = new File(CACHE_FOLDER);
        if (!directory.exists()) {
            directory.mkdirs();
            Utils.logMessage(TAG, "Cache folder created...");
        }
        return directory;
    }

    /**
     * Gets formatted size.
     *
     * @return formatted size in string form
     */
    public static String formatFileSize(long size) {
        String hrSize = null;
        double bytes = size;
        double kb = size / 1024.0;
        double mb = ((size / 1024.0) / 1024.0);
        double gb = (((size / 1024.0) / 1024.0) / 1024.0);
        double tb = ((((size / 1024.0) / 1024.0) / 1024.0) / 1024.0);

        DecimalFormat dec = new DecimalFormat("0.00");
        if (tb > 1) {
            hrSize = dec.format(tb).concat(" TB");
        } else if (gb > 1) {
            hrSize = dec.format(gb).concat(" GB");
        } else if (mb > 1) {
            hrSize = dec.format(mb).concat(" MB");
        } else if (kb > 1) {
            hrSize = dec.format(kb).concat(" KB");
        } else {
            hrSize = dec.format(bytes).concat(" Bytes");
        }

        return hrSize;
    }

    /*Delete the downloaded*/
    public static void deleteDownloadedFile() {
        File file = null;
        if (Constants.PRODUCTION_BUILD) {
            file = FileUtils.getCachetFolder();
        } else {
            file = FileUtils.getOTAClientFolder(FileUtils.OTACLIENT_DOWNLOAD_FOLDER);
        }
        if (file.isDirectory()) {
            String[] children = file.list();
            for (int i = 0; i < children.length; i++) {
                new File(file, children[i]).delete();
            }
        }

    }
}
