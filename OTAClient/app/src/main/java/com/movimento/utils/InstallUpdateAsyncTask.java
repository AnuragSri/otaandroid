package com.movimento.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.RecoverySystem;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.movimento.motreeparser.DownloadDescriptorParser;
import com.movimento.motreeparser.FumoStateParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.otaclient.R;
import com.movimento.otaclient.SuccessActivity;
import com.movimento.service.AlertIntentService;
import com.movimento.service.DlotaIntentService;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * The type Install update async task.
 */
/*Async Task will install the Downloaded update*/
public class InstallUpdateAsyncTask extends AsyncTask<Integer, Integer, String> {

    private final String TAG = InstallUpdateAsyncTask.class.getCanonicalName();
    /**
     * The M context.
     */
    Context mContext;
    /**
     * The M progress bar.
     */
    ProgressBar mProgressBar;
    /**
     * The Ll progress bar.
     */
    LinearLayout llProgressBar;
    /**
     * The M intent.
     */
    Intent mIntent;

    /**
     * Instantiates a new Install update async task.
     *
     * @param context       the context
     * @param mProgressBar  the m progress bar
     * @param llProgressBar the ll progress bar
     */
    public InstallUpdateAsyncTask(Context context, ProgressBar mProgressBar, LinearLayout llProgressBar) {
        this.mContext = context;
        this.mProgressBar = mProgressBar;
        this.llProgressBar = llProgressBar;
        mIntent = new Intent(context, SuccessActivity.class);
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.llProgressBar.setVisibility(View.VISIBLE);
        mProgressBar.setMax(100);
    }

    @Override
    protected String doInBackground(Integer... params) {

     /*Displaying the progress on the progressbar*/
        File otafileloc = null;

        try {
            DownloadDescriptorParser downloadDescriptorParser = (DownloadDescriptorParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_DOWNLOAD_DESCRIPTOR);
            String urlToDownloadFirmware = downloadDescriptorParser.getDownloadObjectURI().replaceAll(" ", "%20");
            Log.v(TAG, "DownloadURL: " + urlToDownloadFirmware);
            Utils.logMessage(TAG, "DownloadURL: " + urlToDownloadFirmware);
            String fileName = urlToDownloadFirmware.substring(urlToDownloadFirmware.lastIndexOf("/") + 1, urlToDownloadFirmware.indexOf("?"));
            otafileloc = new File(FileUtils.getCachetFolder(), fileName);
            if (otafileloc != null && otafileloc.exists()) {
                //String fileName = PrefUtils.getString(Constants.KEY_DLOTA_DOWNLOAD_FILENAME, "update.zip");
                //String fileName = intent.getStringExtra(Constants.DLOTA_DOWNLOAD_FILENAME);
                //otafileloc = new File("/cache/fota/" + fileName);

                // Verify the cryptographic signature before installing it.

                RecoverySystem.verifyPackage(otafileloc, progressListener, null);
                Utils.logMessage(TAG,"verifyPackage Done");

            }else{
                Log.i(TAG,"Either file not found or corrupted");
                Utils.logMessage(TAG,"Either file not found or corrupted");
            }
        } catch (IOException e) {
            Log.i(TAG, "Can not read the update package.");
            Utils.logMessage(TAG, "Can not read the update package.");
            e.printStackTrace();
            Utils.logMessage(TAG, Log.getStackTraceString(e));
        } catch (GeneralSecurityException e) {
            Log.i(TAG, "Update package verification failed. Can't verify the package");
            Utils.logMessage(TAG, "Update package verification failed. Can't verify the package");
            e.printStackTrace();
            Utils.logMessage(TAG, Log.getStackTraceString(e));
            //TODO send alert to server that File is Corrupted, update MO tree to IDLE/Restart
            sendDlotaAlert(Constants.FUMO_ALERT_TYPE_DOWNLOAD_AND_UPDATE, Constants.FUMO_SOURCE_URI_DOWNLOAD_AND_UPDATE,
                    Constants.FUMO_RESULT_CORRUPTED_FIRMWARE_PACKAGE, String.valueOf(Constants.FUMO_STATE_DOWNLOAD_FAILED));
            updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_DOWNLOAD_FAILED));
            restartDownloadService();

        } catch (Exception e) {
            e.printStackTrace();
            Utils.logMessage(TAG, Log.getStackTraceString(e));
        }


        try {
            if (otafileloc != null) {
                updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.DLOTA_STATE_UPDATE_PROGRESSING));
                Utils.logMessage(TAG, "Installation command started. Dlota state : "+ Utils.getDlotaState());
                PrefUtils.putBoolean(Constants.IS_INSTALLATION_STARTED,true);
                RecoverySystem.installPackage(OTAClientApp.getmAppInstance(), otafileloc);
                Utils.logMessage(TAG, "Installation command completed.");
            }
        } catch (IOException e) {
            Log.i(TAG, "Writing the recovery command file failed or the reboot itself failed.");
            Utils.logMessage(TAG, "Writing the recovery command file failed or the reboot itself failed.");
        }
        Utils.logMessage(TAG, "Task Completed.");
        return "Task Completed.";
    }

    private void restartDownloadService() {
        Utils.logMessage(TAG, "restartDownloadService  method called...");
        updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_IDLE_START));
        //TODO here to write logic to stop service and restart it.
        DlotaIntentService.shouldServiceRun = true;
        Intent intent = new Intent(OTAClientApp.getmAppInstance(), DlotaIntentService.class);
        OTAClientApp.getmAppInstance().startService(intent);
        Utils.logMessage(TAG, "DlotaIntentService called...");
    }


    @Override
    protected void onProgressUpdate(Integer... values) {
        mProgressBar.setProgress(values[0]);
        Log.v("Progress", "Once");
        Utils.logMessage(TAG, "Progress..... Once");
    }

    @Override
    protected void onPostExecute(String result) {
        /*Calling Success Activity*/
        Log.v("Progress", "Finished");
        Utils.logMessage(TAG, "Progress..... Finished");
        llProgressBar.setVisibility(View.GONE);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mContext.startActivity(mIntent);
        //((Activity) mContext).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }


    /**
     * The Progress listener.
     */
    RecoverySystem.ProgressListener progressListener = new RecoverySystem.ProgressListener() {
        @Override
        public void onProgress(int progress) {
            publishProgress(progress);
        }
    };


    private void sendDlotaAlert(String alertTypeResult, String serverURI, int resultCode, String state) {
        Utils.logMessage(TAG, "DlotaAlert sent...");
        Intent intent = new Intent(OTAClientApp.getmAppInstance(), AlertIntentService.class);
        intent.putExtra(Constants.KEY_ALERT_NOTIFICATION_TYPE, Constants.ALERT_TYPE_FUMO);
        intent.putExtra(Constants.KEY_ALERT_TYPE, alertTypeResult);
        intent.putExtra(Constants.KEY_ALERT_SERVER_URI, serverURI);
        intent.putExtra(Constants.KEY_ALERT_RESULT_CODE, resultCode);
        intent.putExtra(Constants.KEY_ALERT_STATE, state);
        OTAClientApp.getmAppInstance().startService(intent);
    }

    private void updateDlotaStates(String key, String value) {
        FumoStateParser fumoStateParser = (FumoStateParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO_STATE);
        if (fumoStateParser != null) {
            try {
                fumoStateParser.updateFumoState(key, value);
                Utils.logMessage(TAG, "Dlota states updated...key:" + key + " value:" + value);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logMessage(TAG, Log.getStackTraceString(e));
            }
        }
    }

}
