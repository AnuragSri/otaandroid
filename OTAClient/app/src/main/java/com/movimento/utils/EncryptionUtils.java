package com.movimento.utils;

import android.os.Environment;
import android.util.Base64;

import com.movimento.otaclient.OTAClientApp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

import javax.crypto.Cipher;

import static com.movimento.utils.Constants.ALGO_RSA;

/**
 * The type Encryption utils.
 */
public class EncryptionUtils {
    private static final String TAG = EncryptionUtils.class.getCanonicalName();

    /**
     * String to hold name of the encryption algorithm.
     */
    private static final String ALGORITHM = "RSA";
    private static final String CIPHER_ALGO = "RSA/None/PKCS1Padding";

    /**
     * String to hold the name of the private key file.
     */
    private static final String PRIVATE_KEY_FILE = "private.key";

    /**
     * String to hold name of the public key file.
     */
    private static final String PUBLIC_KEY_FILE = "public.key";

    /**
     * The constant MYFOLDER.
     */
    private static final String MYFOLDER = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "OMAClient" + File.separator;

    /**
     * The method checks if the pair of public and private key has been generated.
     *
     * @return flag indicating if the pair of keys were generated.
     */
    private static boolean areKeysPresent() {
        File privateKey = new File(MYFOLDER + PRIVATE_KEY_FILE);
        File publicKey = new File(MYFOLDER + PUBLIC_KEY_FILE);

        Environment.getExternalStorageDirectory().getAbsolutePath();
        return privateKey.exists() && publicKey.exists();
    }

    /**
     * Generate key which contains a pair of private and public key using 1024
     * bytes. Store the set of keys in Prvate.key and Public.key files.
     */
    private static void generateKey() {
        try {
            final KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITHM);
            keyGen.initialize(1024);
            final KeyPair key = keyGen.generateKeyPair();

            File privateKeyFile = new File(MYFOLDER + PRIVATE_KEY_FILE);
            File publicKeyFile = new File(MYFOLDER + PUBLIC_KEY_FILE);

            // Create files to store public and private key
            if (privateKeyFile.getParentFile() != null) {
                privateKeyFile.getParentFile().mkdirs();
            }
            privateKeyFile.createNewFile();

            if (publicKeyFile.getParentFile() != null) {
                publicKeyFile.getParentFile().mkdirs();
            }
            publicKeyFile.createNewFile();

            // Saving the Public key in a file
            ObjectOutputStream publicKeyOS = new ObjectOutputStream(
                    new FileOutputStream(publicKeyFile));
            publicKeyOS.writeObject(key.getPublic());
            publicKeyOS.close();

            // Saving the Private key in a file
            ObjectOutputStream privateKeyOS = new ObjectOutputStream(
                    new FileOutputStream(privateKeyFile));
            privateKeyOS.writeObject(key.getPrivate());
            privateKeyOS.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Encrypt the plain text using public key.
     *
     * @param text : original plain text
     * @param key  :The public key
     * @return Encrypted text
     * @throws Exception
     */
    private static byte[] encrypt(String text, PublicKey key) {
        byte[] cipherText = null;
        try {
            // get an RSA cipher object and print the provider
            final Cipher cipher = Cipher.getInstance(CIPHER_ALGO);
            // encrypt the plain text using the public key
            cipher.init(Cipher.ENCRYPT_MODE, key);
            cipherText = cipher.doFinal(text.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cipherText;
    }

    /**
     * Encrypt byte [ ].
     *
     * @param text the text
     * @param key  the key
     * @return the byte [ ]
     */
    private static byte[] encrypt(String text, PrivateKey key) {
        byte[] cipherText = null;
        try {
            // get an RSA cipher object and print the provider
            final Cipher cipher = Cipher.getInstance(CIPHER_ALGO);
            // encrypt the plain text using the public key
            cipher.init(Cipher.ENCRYPT_MODE, key);
            cipherText = cipher.doFinal(text.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cipherText;
    }

    /**
     * Decrypt text using private key.
     *
     * @param text :encrypted text
     * @param key  :The private key
     * @return plain text
     * @throws Exception
     */
    private static String decrypt(byte[] text, PrivateKey key) {
        byte[] dectyptedText = null;
        try {
            // get an RSA cipher object and print the provider
            final Cipher cipher = Cipher.getInstance(CIPHER_ALGO);

            // decrypt the text using the private key
            cipher.init(Cipher.DECRYPT_MODE, key);
            dectyptedText = cipher.doFinal(text);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new String(dectyptedText);
    }


    /**
     * Decrypt string.
     *
     * @param text the text
     * @param key  the key
     * @return the string
     */
    private static String decrypt(byte[] text, PublicKey key) {
        byte[] dectyptedText = null;
        try {
            // get an RSA cipher object and print the provider
            final Cipher cipher = Cipher.getInstance(CIPHER_ALGO);

            // decrypt the text using the private key
            cipher.init(Cipher.DECRYPT_MODE, key);
            //dectyptedText = cipher.doFinal(text);
            dectyptedText = cipher.doFinal(text);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return new String(dectyptedText);
    }


    /**
     * Encrypt key string.
     *
     * @param vinNumber the vin number
     * @param publicKey the public key
     * @return the string
     * @throws IOException the io exception
     */
    private static String encryptKey(String vinNumber, PublicKey publicKey) throws IOException {
        final byte[] cipherText = EncryptionUtils.encrypt(vinNumber, publicKey);
        //encoding  byte array into base 64
        byte[] encoded = Base64.encode(cipherText, Base64.DEFAULT);
        //   Log.d(TAG, "Encrypted VIN number: " + new String(encoded));
        return new String(encoded);
        // return  null;
    }


    /**
     * Encrypt key string.
     *
     * @param vinNumber  the vin number
     * @param privateKey the public key
     * @return the string
     * @throws IOException the io exception
     */
    public static String encryptKey(String vinNumber, PrivateKey privateKey) throws IOException {
        final byte[] cipherText = EncryptionUtils.encrypt(vinNumber, privateKey);
        //   Log.d(TAG, "cipherText: " + cipherText);
        //encoding  byte array into base 64
        byte[] encoded = Base64.encode(cipherText, Base64.DEFAULT);
        //   Log.d(TAG, "Encrypted VIN number: " + new String(encoded));
        return new String(encoded);
        // return  null;
    }

    /**
     * Encrypt key string.
     *
     * @param vinNumber the vin number
     * @param publicKey the public key
     * @return the string
     * @throws IOException the io exception
     */
    private static String decryptKey(String vinNumber, PublicKey publicKey) throws IOException {
        byte[] decoded = Base64.decode(vinNumber, Base64.DEFAULT);

        String deKey = decrypt(decoded, publicKey);

        //   Log.d(TAG, "cipherText: " + deKey);
        //encoding  byte array into base 64

        //   Log.d(TAG, "Decrypted String with public key: " + new String(deKey));
        return deKey;
        // return  null;
    }

    /**
     * Encrypt key string.
     *
     * @param vinNumber  the vin number
     * @param privateKey the public key
     * @return the string
     * @throws IOException the io exception
     */
    public static String decryptKey(String vinNumber, PrivateKey privateKey) throws IOException {
        byte[] decoded = Base64.decode(vinNumber, Base64.DEFAULT);

        String deKey = decrypt(decoded, privateKey);

        //   Log.d(TAG, "cipherText: " + deKey);
        //encoding  byte array into base 64

        //    Log.d(TAG, "Decrypted string with private key: " + new String(deKey));
        return deKey;
        // return  null;
    }

    /**
     * Gets public key.
     *
     * @param publicKeyFile the public key file
     * @return the public key
     */
    private static PublicKey getPublicKey(String publicKeyFile) {
        String publicKeyStr = Utils.loadJSONFromAsset(OTAClientApp.getmAppInstance(), publicKeyFile);
        byte[] publicBytes = Base64.decode(publicKeyStr, Base64.DEFAULT);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
        KeyFactory keyFactory = null;
        PublicKey publicKey = null;
        try {
            keyFactory = KeyFactory.getInstance(ALGO_RSA);
            publicKey = keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return publicKey;
    }

    /**
     * Gets private key.
     *
     * @param privateKeyFile the public key file
     * @return the public key
     */
    private static PrivateKey getaPrivateKey(String privateKeyFile) {
        String publicKeyStr = Utils.loadJSONFromAsset(OTAClientApp.getmAppInstance(), privateKeyFile);
        byte[] publicBytes = Base64.decode(publicKeyStr, Base64.DEFAULT);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
        KeyFactory keyFactory = null;
        PrivateKey privateKey = null;
        try {
            keyFactory = KeyFactory.getInstance(ALGO_RSA);
            privateKey = keyFactory.generatePrivate(keySpec);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return privateKey;
    }


    /**
     * Get private key.
     *
     * @param filename the filename
     * @return the private key
     */
    /*public static PrivateKey get(String filename) {
        PrivateKey privateKey = null;
        String publicKeyStr = Utils.loadJSONFromAsset(OTAClientApp.getmAppInstance(), filename);
        byte[] keyBytes = publicKeyStr.getBytes();
        //byte[] keyBytes = MediaStore.Files.readAllBytes(new File(filename).toPath());

        PKCS8EncodedKeySpec spec =
                new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = null;
        try {
            kf = KeyFactory.getInstance("RSA");
            privateKey = kf.generatePrivate(new X509EncodedKeySpec(Base64.decode(String.valueOf(spec), Base64.DEFAULT)));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return privateKey;
    }*/


    /**
     * Load private key private key.
     *
     * @param privateKeyFile the private key file
     * @return the private key
     */
    public static PrivateKey getPrivateKey(String privateKeyFile) {
        String key64 = Utils.loadJSONFromAsset(OTAClientApp.getmAppInstance(), privateKeyFile);
        byte[] clear = Base64.decode(key64, Base64.DEFAULT);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(clear);
        KeyFactory fact = null;
        PrivateKey priv = null;
        try {
            fact = KeyFactory.getInstance("RSA");
            priv = fact.generatePrivate(keySpec);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

        Arrays.fill(clear, (byte) 0);
        return priv;
    }


}
