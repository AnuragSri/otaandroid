package com.movimento.utils;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.movimento.otaclient.InstallUpdateActivity;

/**
 * The type Download update async task.
 */
/*Async task will perform the downloading operation*/
public class DownloadUpdateAsyncTask extends AsyncTask<Integer, Integer, String> {

    private Context mContext;
    private ProgressBar mProgressBar;
    private LinearLayout mLlProgressBar;
    private Intent mIntent;
    private Button mBtnCancelDownload;
    private static Boolean isCancelled;

    /**
     * Instantiates a new Download update async task.
     *
     * @param context           the context
     * @param mProgressBar      the m progress bar
     * @param llProgressBar     the ll progress bar
     * @param btnCancelDownload the btn cancel download
     */
    public DownloadUpdateAsyncTask(Context context, ProgressBar mProgressBar, LinearLayout llProgressBar, Button btnCancelDownload) {

        this.mContext = context;
        this.mProgressBar = mProgressBar;
        this.mLlProgressBar = llProgressBar;
        this.mBtnCancelDownload = btnCancelDownload;
        mIntent = new Intent(context, InstallUpdateActivity.class);
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.mLlProgressBar.setVisibility(View.VISIBLE);
        mBtnCancelDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLlProgressBar.setVisibility(View.GONE);
                isCancelled = true;
            }
        });
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        DownloadUpdateAsyncTask.this.cancel(true);

    }

    @Override
    protected String doInBackground(Integer... params) {

        Integer count = 0;

        isCancelled = isCancelled();
        while (count <= 100) {
            if (!isCancelled) {
                try {
                    Thread.sleep(1000);
                    publishProgress(count);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                count = count + 25;
            } else {
                DownloadUpdateAsyncTask.this.cancel(true);
                isCancelled = false;
            }

        }
        return "Task Completed.";
    }


    @Override
    protected void onProgressUpdate(Integer... values) {

        mProgressBar.setProgress(values[0]);
        Log.v("Progress", "Once");
    }

    @Override
    protected void onPostExecute(String result) {
        // async task finished
        Log.v("Progress", "Finished");
        mLlProgressBar.setVisibility(View.GONE);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mContext.startActivity(mIntent);
        //((Activity) mContext).overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
    }


}
