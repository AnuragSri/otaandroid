package com.movimento.utils;

/**
 * The interface Parser constants.
 */
public interface ParserConstants {

    /**
     * The constant MGMNT_TREE.
     */
    String MGMNT_TREE = "MgmtTree";
    /**
     * The constant VER_DTD.
     */
    String VER_DTD = "VerDTD";
    /**
     * The constant NODE.
     */
    String NODE = "Node";
    /**
     * The constant DF_PROPERTIES.
     */
    String DF_PROPERTIES = "DFProperties";
    /**
     * The constant DF_TITLE.
     */
    String DF_TITLE = "DFTitle";
    /**
     * The constant DF_TYPE.
     */
    String DF_TYPE = "DFType";
    /**
     * The constant MIME.
     */
    String MIME = "MIME";
    /**
     * The constant KEY_ACCESS_TYPE.
     */
    String KEY_ACCESS_TYPE = "AccessType";
    /**
     * The constant KEY_DDF_NAME.
     */
    String KEY_DDF_NAME = "DDFName";
    /**
     * The constant DF_FORMAT.
     */
    String DF_FORMAT = "DFFormat";
    /**
     * The constant DF_OCCURRENCE.
     */
    String DF_OCCURRENCE = "Occurrence";
    /**
     * The constant KEY_NODE_NAME.
     */
    String KEY_NODE_NAME = "NodeName";
    /**
     * The constant NODE_OPERATIONS.
     */
    String NODE_OPERATIONS = "Operations";
    /**
     * The constant NODE_APP_WIPE_LIST.
     */
    String NODE_APP_WIPE_LIST = "AvailableWipeList";
    /**
     * The constant LAWMO_JSON.
     */
    String LAWMO_JSON = "lawmoMOTree.json";
    /**
     * The constant NODE_EXT.
     */
// String LAWMO_TEXT_FILE_NAME = "lawmoJson.txt";
    String NODE_EXT = "EXT";
    /**
     * The constant DEVICE_INFO_TEXT_FILE_NAME.
     */
    String DEVICE_INFO_TEXT_FILE_NAME = "devInfo.txt";

    /**
     * The constant PARENT.
     */
    String PARENT = "parent";

    /**
     * The constant CAPABILITY_MO_TREE.
     */
//ManagementTree JSON Files in asset
    String CAPABILITY_MO_TREE = "CapabilitiesMOTree.json";
    /**
     * The constant DEV_ACC_MO_TREE.
     */
    String DEV_ACC_MO_TREE = "DevAccMOTree.json";
    /**
     * The constant DEV_INFO_MO_TREE.
     */
    String DEV_INFO_MO_TREE = "devInfoMOTree.json";
    /**
     * The constant FUMO_MO_TREE.
     */
    String FUMO_MO_TREE = "fumoMOTree.json";
    /**
     * The constant FUMO_STATES_MO_TREE.
     */
    String FUMO_STATES_MO_TREE = "fumoStatesMOTree.json";
    /**
     * The constant LAWNO_MO_TREE.
     */
    String LAWNO_MO_TREE = "lawmoMOTree.json";


    /**
     * The constant KEY_NODE_SRVID.
     */
    String KEY_NODE_SRVID = "SrvID";
    /**
     * The constant KEY_NODE_SRVURI.
     */
    String KEY_NODE_SRVURI = "SrvURI";
    /**
     * The constant KEY_NODE_AUTH_DATA.
     */
    String KEY_NODE_AUTH_DATA = "Data";
    /**
     * The constant KEY_NODE_SECRET.
     */
    String KEY_NODE_SECRET = "Secret";
    /**
     * The constant KEY_CLIENT_URI.
     */
    String KEY_CLIENT_URI = "ClientURI";

    /**
     * The constant LAWMO_NODE_FULLY_LOCKED.
     */
//LAWMO Operation Node Name:
    String LAWMO_NODE_FULLY_LOCKED = "FullyLock";
    /**
     * The constant LAWMO_NODE_UNLOCK.
     */
    String LAWMO_NODE_UNLOCK = "UnLock";
    /**
     * The constant LAWMO_NODE_FACTORY_RESET.
     */
    String LAWMO_NODE_FACTORY_RESET = "FactoryReset";
    /**
     * The constant LAWMO_NODE_WIPE.
     */
    String LAWMO_NODE_WIPE = "Wipe";

    /**
     * The constant LAWMO_OPERATON_EXEC.
     */
//LAWMO Operation NOde values:
    String LAWMO_OPERATON_EXEC = "EXEC";

    /**
     * The constant KEY_FUMO_DOWNLOAD_AND_UPDATE.
     */
//FUMO
    String KEY_FUMO_DOWNLOAD_AND_UPDATE = "DownloadAndUpdate";
    /**
     * The constant KEY_FUMO_DOWNLOAD_AND_UPDATE_PKG_URL.
     */
    String KEY_FUMO_DOWNLOAD_AND_UPDATE_PKG_URL = "PkgURL";
    /**
     * The constant KEY_FUMO_PKG_NAME.
     */
    String KEY_FUMO_PKG_NAME = "PkgName";
    /**
     * The constant KEY_FUMO_PKG_VERSION.
     */
    String KEY_FUMO_PKG_VERSION = "PkgVersion";
    /**
     * The constant KEY_FUMO_STATE.
     */
    String KEY_FUMO_STATE = "State";
    /**
     * The constant VALUE.
     */
    String VALUE = "value";
    /**
     * The constant KEY.
     */
    String KEY = "key";


    /**
     * The constant KEY_DEV_ID.
     */
// DevInfo
    String KEY_DEV_ID = "DevID";
    /**
     * The constant KEY_MAN.
     */
    String KEY_MAN = "Man";
    /**
     * The constant KEY_MOD.
     */
    String KEY_MOD = "Mod";
    /**
     * The constant KEY_DMV.
     */
    String KEY_DMV = "DmV";
    /**
     * The constant KEY_LANG.
     */
    String KEY_LANG = "Lang";
    /**
     * The constant KEY_DEV_TYPE.
     */
    String KEY_DEV_TYPE = "DevType";
    /**
     * The constant KEY_OEM.
     */
    String KEY_OEM = "OEM";
    /**
     * The constant KEY_FWV.
     */
    String KEY_FWV = "FwV";
    /**
     * The constant KEY_SWV.
     */
    String KEY_SWV = "SwV";
    /**
     * The constant KEY_HWV.
     */
    String KEY_HWV = "HwV";
    /**
     * The constant KEY_VIN.
     */
    String KEY_VIN = "VIN";
    /**
     * The constant KEY_ENCRYPTED_VIN.
     */
    String KEY_ENCRYPTED_VIN = "EncryptedVIN";
    /**
     * The constant KEY_COMPRESS_TECH.
     */
    String KEY_COMPRESS_TECH = "CompressTech";

    /**
     * The constant DOWNLOAD_FILE_SIZE.
     */
    String DOWNLOAD_FILE_SIZE = "download file size";
    /**
     * The constant TOTAL_FIRMWARE_SIZE.
     */
    String TOTAL_FIRMWARE_SIZE = "Total firmware size";
    /**
     * The constant CURRENT_RETRY.
     */
    String CURRENT_RETRY = "current retry";
    /**
     * The constant CURRENT_STATE.
     */
    String CURRENT_STATE = "current state";
    /**
     * The constant MAX_RETRY.
     */
    String MAX_RETRY = "max retry";

    /**
     * The constant DOWNLOAD_DESCRIPTION_MO_TREE.
     */
    String DOWNLOAD_DESCRIPTION_MO_TREE = "DownloadDescriptorMoTree.json";
    /**
     * The constant KEY_NAME.
     */
    String KEY_NAME = "name";
    /**
     * The constant KEY_INSTALL_NOTIFY_URI.
     */
    String KEY_INSTALL_NOTIFY_URI ="installNotifyURI";
    /**
     * The constant KEY_SUPPRESS_USER_CONFIRMATION.
     */
    String KEY_SUPPRESS_USER_CONFIRMATION = "suppressUserConfirmation";
    /**
     * The constant KEY_SIZE.
     */
    String KEY_SIZE ="size";
    /**
     * The constant KEY_INSTALL_SIZE.
     */
    String KEY_INSTALL_SIZE = "installSize";
    /**
     * The constant KEY_TYPE.
     */
    String KEY_TYPE="type";
    /**
     * The constant KEY_OBJECT_ID.
     */
    String KEY_OBJECT_ID = "objectID";
    /**
     * The constant KEY_OBJECT_VERSION.
     */
    String KEY_OBJECT_VERSION ="objectVersion";
    /**
     * The constant KEY_PROGRESSIVE_DOWNLOADING_FLAG.
     */
    String KEY_PROGRESSIVE_DOWNLOADING_FLAG = "progressiveDownloadingFlag";
    /**
     * The constant KEY_OBJECT_VALIDITY_TIME.
     */
    String KEY_OBJECT_VALIDITY_TIME = "objectValidityTime";
    /**
     * The constant KEY_VENDOR.
     */
    String KEY_VENDOR = "vendor";


}
