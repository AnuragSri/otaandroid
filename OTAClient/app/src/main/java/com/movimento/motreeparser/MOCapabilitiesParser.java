package com.movimento.motreeparser;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.movimento.motreeparser.capabilitymodel.MOCapabilitiesModel;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static com.movimento.utils.ParserConstants.CAPABILITY_MO_TREE;


/*This Class will parse Mo Capabilities Json  using Gson library*/


/**
 * The type Mo capabilities parser.
 */
public class MOCapabilitiesParser extends MTParser {

    private static MOCapabilitiesParser mMOCapabilitiesParserInstance;
    private Context mContext;
    private String mFileName = CAPABILITY_MO_TREE;
    private Gson gson;
    private String json;
    /**
     * The M mo capabilities model.
     */
    MOCapabilitiesModel mMoCapabilitiesModel = null;

    private MOCapabilitiesParser() {
        mContext = OTAClientApp.getmAppInstance();
        gson = new Gson();
        json = new String();
        try {
            parseMOCapabilities();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static MOCapabilitiesParser getInstance() {
        synchronized (MOCapabilitiesParser.class) {
            if (mMOCapabilitiesParserInstance == null)
                mMOCapabilitiesParserInstance = new MOCapabilitiesParser();
        }
        return mMOCapabilitiesParserInstance;
    }

    /*This method will parse the MO capabilities json in mMoCapabilitiesModel object
    * It will search the json file on disk if it is not present then
    * it will take the file from asset and save the file to disk and the parse it.
    * */
    private void parseMOCapabilities() throws IOException {
        AssetManager assetManager = mContext.getAssets();
        InputStream ims = null;

        File file = mContext.getFileStreamPath(mFileName);
        if (file.exists()) {
            json = Utils.readFromFile(mContext, mFileName);
        } else {
            ims = assetManager.open(CAPABILITY_MO_TREE);
            int size = ims.available();
            byte[] buffer = new byte[size];
            ims.read(buffer);
            ims.close();
            json = new String(buffer, "UTF-8");
        }
        mMoCapabilitiesModel = gson.fromJson(json, MOCapabilitiesModel.class);
    }


    /**
     * This method will return all the supported Mo Capabilities
     *
     * @return supported capabilities
     */
    public ArrayList<String> getSupportedCapabilities() {
        ArrayList<String> mSupportedCapabilities = new ArrayList<>();
        for (int i = 0; i < mMoCapabilitiesModel.getCapabilities().getSupported().length; i++) {
            //  mMoCapabilitiesModel.getCapabilities().getSupported()[i].getCAPABILITY_NAME();
            mSupportedCapabilities.add(mMoCapabilitiesModel.getCapabilities().getSupported()[i].getCAPABILITY_NAME());
        }

        return mSupportedCapabilities;
    }
}

