package com.movimento.motreeparser.capabilitymodel;

/**
 * The type Mo capabilities model.
 */
public class MOCapabilitiesModel {

    private Capabilities CAPABILITIES;

    /**
     * Gets capabilities.
     *
     * @return the capabilities
     */
    public Capabilities getCapabilities()
    {
        return CAPABILITIES;
    }

    /**
     * Sets capabilities.
     *
     * @param CAPABILITIES the capabilities
     */
    public void setCapabilities(Capabilities CAPABILITIES)
    {
        this.CAPABILITIES = CAPABILITIES;
    }

}
