package com.movimento.motreeparser;

import android.content.Context;
import android.util.Log;

import com.movimento.otaclient.OTAClientApp;
import com.movimento.utils.Constants;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;


/**
 * The type Lawmo parser.
 */
public class LawmoParser extends MTParser {

    private static LawmoParser mLawmoParserInstance;
    private Context mContext;
    private JSONObject jsonObject;
    //  private DeviceInfoParser mDeviceInfoHandler;
    private JSONObject mMgmtJObj;
    private JSONObject mNodeJObj;
    private JSONObject childNodesjObj;
    private JSONObject dfPropertiesJObj;
    private JSONArray newNodeJArray;
    private ArrayList<String> baseNodeAccessType = new ArrayList<>();
    private JSONArray childNodeJArray;
    private String nodeName = "";
    private JSONObject mNewChildNodeJObj;
    private JSONArray newGrandChildNodeJArray;
    private JSONArray newChildNodeJArray;
    private Utils mMyUtil;
    private JSONObject childDFPropJObj;
    private JSONArray accTyp;
    private ArrayList<JSONObject> mChildNodesArrObj;
    private ArrayList<JSONObject> mNewChildNodeArrObj;
    private ArrayList<JSONObject> mNewGrandChildeNodeArrObj;
    private JSONObject mGrandChildNodeJObj;
    private JSONObject newGrandChildNodeJObj;


    private LawmoParser() {

        mMyUtil = new Utils();

        try {
            getJson();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //     mDeviceInfoHandler = new DeviceInfoParser(mContext, false, false);
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static LawmoParser getInstance() {
        synchronized (LawmoParser.class) {
            if (mLawmoParserInstance == null)
                mLawmoParserInstance = new LawmoParser();
        }
        return mLawmoParserInstance;
    }

    /**
     * This method will parse the json file and initialise the json Objects .
     *
     * @throws JSONException the json exception
     */
    public void getJson() throws JSONException {
        mContext = OTAClientApp.getmAppInstance();
        try {
            File file = mContext.getFileStreamPath(ParserConstants.LAWNO_MO_TREE);

            if (file.exists()) {
                jsonObject = new JSONObject(Utils.readFromFile(mContext, ParserConstants.LAWNO_MO_TREE));
            } else {
                jsonObject = new JSONObject(Utils.getJsonString(ParserConstants.LAWMO_JSON, mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (jsonObject.has(ParserConstants.MGMNT_TREE)) {
            Iterator iterator = jsonObject.keys();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                if (Constants.DEBUGGABLE_MODE) {

                    Log.i("key :---", key);
                }
                mMgmtJObj = jsonObject.getJSONObject(key);


                String VerDTD = mMgmtJObj.getString(ParserConstants.VER_DTD);
                mNodeJObj = mMgmtJObj.getJSONObject(ParserConstants.NODE);

                if (mNodeJObj.has(ParserConstants.DF_PROPERTIES)) {


                    dfPropertiesJObj = mNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);

                    // JSONObject dfPropertiesJObj = mNodeJObj.getJSONObject("DFProperties");


                    getDFProperties(dfPropertiesJObj);


                }

                if (mNodeJObj.has(ParserConstants.NODE)) {


                    childNodeJArray = mNodeJObj.getJSONArray(ParserConstants.NODE);

                    int childNodeCount = childNodeJArray.length();
                    mChildNodesArrObj = new ArrayList<>();

                    for (int i = 0; i < childNodeCount; i++) {
                        childNodesjObj = childNodeJArray.getJSONObject(i);
                        mChildNodesArrObj.add(childNodesjObj);

                        if (childNodesjObj.has(ParserConstants.KEY_NODE_NAME)) {

                            nodeName = childNodesjObj.get(ParserConstants.KEY_NODE_NAME).toString();

                        }

                        if (childNodesjObj.has(ParserConstants.DF_PROPERTIES)) {


                            childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);


                            getDFProperties(childDFPropJObj);


                            if (nodeName.equalsIgnoreCase(ParserConstants.NODE_OPERATIONS)) {

                                if (childNodesjObj.has(ParserConstants.NODE)) {

                                    newChildNodeJArray = childNodesjObj.getJSONArray(ParserConstants.NODE);
                                    int newChildNodeCount = newChildNodeJArray.length();
                                    mNewChildNodeArrObj = new ArrayList<>();

                                    for (int j = 0; j < newChildNodeCount; j++) {
                                        mNewChildNodeJObj = newChildNodeJArray.getJSONObject(j);
                                        mNewChildNodeArrObj.add(mNewChildNodeJObj);

                                        if (mNewChildNodeJObj.has(ParserConstants.KEY_NODE_NAME)) {

                                            String newChildNodeName = mNewChildNodeJObj.getString(ParserConstants.KEY_NODE_NAME).toString();
                                        }

                                        if (mNewChildNodeJObj.has(ParserConstants.DF_PROPERTIES)) {


                                            JSONObject newChildDFPropJObj = mNewChildNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);
                                            getDFProperties(newChildDFPropJObj);
                                        }
                                    }
                                }
                            }

                            if (nodeName.equalsIgnoreCase(ParserConstants.NODE_APP_WIPE_LIST)) {
                                JSONObject childDFFormatJobj = childDFPropJObj.getJSONObject(ParserConstants.DF_FORMAT);


                                JSONObject childOccuranceJobj = childDFPropJObj.getJSONObject(ParserConstants.DF_OCCURRENCE);


                                if (childNodesjObj.has(ParserConstants.NODE)) {

                                    mGrandChildNodeJObj = childNodesjObj.getJSONObject(ParserConstants.NODE);

                                    JSONObject grandChildDFProperties = mGrandChildNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);


                                    getDFProperties(grandChildDFProperties);


                                    if (mGrandChildNodeJObj.has(ParserConstants.NODE)) {

                                        newGrandChildNodeJArray = mGrandChildNodeJObj.getJSONArray(ParserConstants.NODE);

                                        int newGrandChildeNodeCount = newGrandChildNodeJArray.length();
                                        mNewGrandChildeNodeArrObj = new ArrayList<>();


                                        for (int k = 0; k < newGrandChildeNodeCount; k++) {

                                            newGrandChildNodeJObj = newGrandChildNodeJArray.getJSONObject(k);
                                            mNewGrandChildeNodeArrObj.add(newGrandChildNodeJObj);

                                            if (newGrandChildNodeJObj.has(ParserConstants.KEY_NODE_NAME)) {
                                                String grandChildNodeName = newGrandChildNodeJObj.get(ParserConstants.KEY_NODE_NAME).toString();
                                            }

                                            if (newGrandChildNodeJObj.has(ParserConstants.DF_PROPERTIES)) {

                                                JSONObject grandChildDfPropJObj = newGrandChildNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);

                                                getDFProperties(grandChildDfPropJObj);


                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
          //  Log.i("NO Management","NO Management Tree available");
            //Toast.makeText(mContext, "NO Management Tree available.", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * his method provides all the attributes of "DFProperties" for all nodes
     *
     * @param dfPropertiesJObj the df properties j obj
     * @throws JSONException
     */
    public void getDFProperties(JSONObject dfPropertiesJObj) {

        try {
            if (dfPropertiesJObj.has(ParserConstants.KEY_ACCESS_TYPE)) {


                accTyp = dfPropertiesJObj.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);

                for (int k = 0; k < accTyp.length(); k++) {
                    baseNodeAccessType.add(accTyp.get(k).toString());
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.i("accessType :-", baseNodeAccessType.get(k));
                    }
                }
            }
            if (dfPropertiesJObj.has(ParserConstants.DF_TITLE)) {

                String DFTitle = dfPropertiesJObj.getString(ParserConstants.DF_TITLE);
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("DFTitle :-", DFTitle);
                }
            }
            if (dfPropertiesJObj.has(ParserConstants.DF_TYPE)) {
                JSONObject dfTypeJObj = dfPropertiesJObj.getJSONObject(ParserConstants.DF_TYPE);
                //    JSONObject dfTypeJObj = getKeyJsonObj("DFType", dfPropertiesJObj);


                Iterator iterator = dfTypeJObj.keys();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();

                    if (!key.equalsIgnoreCase("")) {

                        String DDFName = dfTypeJObj.getString(key);
                        if (Constants.DEBUGGABLE_MODE) {
                            Log.i("DDFName=====", DDFName);
                        }
                    }
                }
            }


            if (dfPropertiesJObj.has(ParserConstants.DF_FORMAT)) {
                JSONObject grandChildDFFormat = dfPropertiesJObj.getJSONObject(ParserConstants.DF_FORMAT);

            }
            if (dfPropertiesJObj.has(ParserConstants.DF_OCCURRENCE)) {
                JSONObject grandChildOccurance = dfPropertiesJObj.getJSONObject(ParserConstants.DF_OCCURRENCE);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    /**
     * This method returns the Json Object for the searched  node.
     *
     * @param nodeName - name of the node whose Json object is to be found.
     * @return - json object of the searched node.
     * @throws JSONException the json exception
     */
    public JSONObject getNodeJObj(String nodeName) throws JSONException {

        String nName = "";
        JSONObject nodeJObj = null;

        if (mNodeJObj.has(nodeName)) {
            nodeJObj = mNodeJObj.getJSONObject(nodeName);

        }

        if (nodeJObj == null) {
            nodeJObj = Utils.getJsonObj(nodeName, childNodeJArray);
        }
        if (nodeJObj == null) {
            nodeJObj = Utils.getJsonObj(nodeName, newChildNodeJArray);
        }

        if (nodeJObj == null) {

            nodeJObj = Utils.getJsonObj(nodeName, newGrandChildNodeJArray);
        }


        return nodeJObj;
    }

    /**
     * This method returns the Version Id from the root node
     *
     * @return - Version ID.
     * @throws JSONException the json exception
     */
    public String getVersionId() throws JSONException {
        String verDTD = null;
        if (null != mMgmtJObj && mMgmtJObj.has(ParserConstants.VER_DTD)) {
            verDTD = mMgmtJObj.getString(ParserConstants.VER_DTD);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("verDTD:-", verDTD);
            }

        } else {
            verDTD = null;
        }
        return verDTD;
    }

    /**
     * This method will update the Ver Id and save the in json file
     *
     * @param versionId - Version number that is to be updated.
     * @return - boolean value if the update was successful or not.
     * @throws JSONException the json exception
     */
    public boolean setVersionId(String versionId) throws JSONException {
        boolean isUpdated = false;
        if (null != mMgmtJObj && mMgmtJObj.has(ParserConstants.VER_DTD)) {
            mMgmtJObj.put(ParserConstants.VER_DTD, versionId);
            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);


            isUpdated = true;
        } else {
            isUpdated = false;
        }
        return isUpdated;
    }

    /**
     * This method is used to get the DFTitle of the first level node.
     *
     * @return - DFTitle of the first level node.
     */
    public String getParentNodeDFTitle() {
        String dfTitle = null;
        try {

            if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.DF_TITLE)) {
                String DFTitle = dfPropertiesJObj.getString(ParserConstants.DF_TITLE);
                dfTitle = DFTitle;
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("dfTitle", "====" + dfTitle);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("dfTitle", "====" + dfTitle);
            }
            dfTitle = null;

        }
        return dfTitle;
    }


    /**
     * This method returns the Access Type of the first level node .
     *
     * @return - List containing all the values of AccessType.
     * @throws JSONException the json exception
     */
    public ArrayList<String> getParentNodeAccessType() throws JSONException {
        String accVal = "";
        ArrayList<String> nodeAccTyp = new ArrayList<>();


        if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.KEY_ACCESS_TYPE)) {
            JSONArray accTyp = dfPropertiesJObj.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);

            for (int i = 0; i < accTyp.length(); i++) {
                accVal = accTyp.get(i).toString();
                nodeAccTyp.add(accVal);
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("accVal", accVal);
                }
            }

        } else {
            nodeAccTyp = null;

        }


        return nodeAccTyp;

    }


    /**
     * This method will set the value of Access type of the parent node and save it in file.
     *
     * @param accTypeVal
     * @return
     * @throws JSONException
     */
  /*  public boolean updateParentNodeAccessType(String accTypeVal) throws JSONException {

        boolean isUpdated = false;

        if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.KEY_ACCESS_TYPE)) {
            JSONArray accTyp = dfPropertiesJObj.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);
            accTyp.put(0, accTypeVal);
            isUpdated = true;
        } else {
            isUpdated = false;
        }
        return isUpdated;
    }*/


    /**
     * This method will return the DF Type of the first level node
     *
     * @return - Return DFType of the first level node.
     */
    /*public String getParentNodeDFType() {
        String DDFName = "";
        try {
            if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.DF_TYPE)) {
                JSONObject dfTypeJObj = dfPropertiesJObj.getJSONObject(ParserConstants.DF_TYPE);
                //    JSONObject dfTypeJObj = getKeyJsonObj(ParserConstants.DF_TYPE, dfPropertiesJObj);

                if (dfTypeJObj.has(ParserConstants.KEY_DDF_NAME)) {
                    DDFName = dfTypeJObj.getString(ParserConstants.KEY_DDF_NAME);
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.i("DDFName=====", DDFName);
                    }
                } else {
                    DDFName = null;
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.i("DDFName=====", DDFName);
                    }
                }

            } else {

                DDFName = null;
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("DDFName=====", DDFName);
                }

            }

        } catch (Exception e) {
            DDFName = null;
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("DDFName=====", DDFName);
            }
        }
        return DDFName;

    }*/

    /**
     * This method will return the MIME of the first level node
     *
     * @return - Return DFType of the first level node.
     */
    public String getParentMIME() {
        String mime = "";

        try {
            if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.DF_TYPE)) {
                JSONObject dfTypeJObj = dfPropertiesJObj.getJSONObject(ParserConstants.DF_TYPE);
                mime = dfTypeJObj.getString(ParserConstants.MIME);

                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("DDFName=====", ":---" + mime);
                }
            }


        } catch (Exception e) {
            mime = null;
        }
        return mime;
    }


    /**
     * This method will return the getParentDDFName of the first level node
     *
     * @return - Return DFType of the first level node.
     */
    public String getParentDDFName() {
        String ddfName = "";

        try {
            if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.DF_TYPE)) {
                JSONObject dfTypeJObj = dfPropertiesJObj.getJSONObject(ParserConstants.DF_TYPE);
                ddfName = dfTypeJObj.getString(ParserConstants.KEY_DDF_NAME);

                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("DDFName=====", ":---" + ddfName);
                }
            }


        } catch (Exception e) {
            ddfName = null;
        }
        return ddfName;
    }


    /**
     * This method will return the value of Access Type of the node name provided as parameter.
     *
     * @param nodeName -  Node name whose AccessType is to be determined.
     * @return - List of all the values of AccessType.
     * @throws JSONException the json exception
     */
    public ArrayList<String> getChildNodeAccessType(String nodeName) throws JSONException {

        String accTypVal = "";
        ArrayList<String> nodeAccTyp = new ArrayList<>();
        childNodesjObj = getNodeJObj(nodeName);

        if (childNodesjObj != null) {

            childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);

            if (null != childDFPropJObj && childDFPropJObj.has(ParserConstants.KEY_ACCESS_TYPE)) {
                JSONArray accTyp = childDFPropJObj.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);

                for (int i = 0; i < accTyp.length(); i++) {
                    accTypVal = accTyp.getString(i).toString();
                    nodeAccTyp.add(accTypVal);
                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.i("accTypVal:-", nodeAccTyp.get(i));
                    }

                }
            } else {
                nodeAccTyp = null;
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("accTypVal:-", accTypVal);
                }
            }

        } else {

            nodeAccTyp = null;
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("accTypVal:-", accTypVal);
            }


        }

        return nodeAccTyp;
    }

    /**
     * This method will set the DF title of the first level node and save in the file.
     *
     * @param dfTitleVal the df title val
     * @return - boolean value that if the update operation was successful or not.
     * @throws JSONException the json exception
     */
    public boolean updateParentNodeDFTitle(String dfTitleVal) throws JSONException {
        String DFTitle;
        boolean isUpdated = false;

        if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.DF_TITLE)) {
            DFTitle = dfPropertiesJObj.getString(ParserConstants.DF_TITLE);

            dfPropertiesJObj.put(ParserConstants.DF_TITLE, dfTitleVal);
            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
            isUpdated = true;

        } else {

            dfPropertiesJObj.put(ParserConstants.DF_TITLE, dfTitleVal);
        }
        return isUpdated;

    }

    /**
     * This method will set the Access type of the Node provided as the parameter and save it in file.
     *
     * @param nodeName
     * @param accTypVal
     * @return
     * @throws JSONException
     */
   /* public boolean updateChildNodeAccessTypeVal(String nodeName, String accTypVal) throws JSONException {

        childNodesjObj = getNodeJObj(nodeName);
        boolean isUpdated = false;

        if (childNodesjObj != null) {

            childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);


            if (null != childDFPropJObj && childDFPropJObj.has(ParserConstants.KEY_ACCESS_TYPE)) {
                ArrayList<String> childAccessType = new ArrayList<>();
                JSONArray childAccTyp = childDFPropJObj.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);

                // childAccTyp.remove(0);
                childAccTyp.put(0, accTypVal);

                childAccessType.add(childAccTyp.get(0).toString());
                String updatedAccType = childAccTyp.get(0).toString();

                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                isUpdated = true;
            } else {
                isUpdated = false;
            }
        } else {

            isUpdated = false;

        }
        return isUpdated;

    }*/

    /**
     * This method will return the DF Title of the node
     *
     * @param nodeName - Node whose DFTitle is to be determined.
     * @return - DFTitle
     * @throws JSONException the json exception
     */
    public String getChildNodeDFTitle(String nodeName) throws JSONException {

        childNodesjObj = getNodeJObj(nodeName);
        String dfTitle = "";

        if (childNodesjObj != null) {
            childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);

            if (null != childDFPropJObj && childDFPropJObj.has(ParserConstants.DF_TITLE)) {
                dfTitle = childDFPropJObj.getString(ParserConstants.DF_TITLE).toString();
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("dfTitle:-", "-----" + dfTitle);
                }


            } else {
                dfTitle = null;
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("dfTitle:-", "-----" + dfTitle);
                }
            }

        } else {

            dfTitle = null;
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("dfTitle:-", "-----" + dfTitle);
            }

        }
        return dfTitle;
    }


    /**
     * This method will set the DFTitle of node and save it in the file.
     *
     * @param nodeName   - node whose DFTitle is to be updated.
     * @param dfTitleVal - value to be set
     * @return - boolean value if the updation was successful.
     * @throws JSONException the json exception
     */
    public boolean updateChildNodeDFTitle(String nodeName, String dfTitleVal) throws JSONException {


        childNodesjObj = getNodeJObj(nodeName);
        boolean isUpdated = false;

        if (childNodesjObj != null) {
            childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);

            if (null != childDFPropJObj && childDFPropJObj.has(ParserConstants.DF_TITLE)) {
                childDFPropJObj.put(ParserConstants.DF_TITLE, dfTitleVal);
                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                isUpdated = true;
            } else {

                childDFPropJObj.put(ParserConstants.DF_TITLE, dfTitleVal);


            }
        } else {
            isUpdated = false;

        }
        return isUpdated;
    }

    /**
     * This method will return the DFType of the entered node.
     *
     * @param nodeName - Node name whose DFType is to be determined.
     * @return - List of all values of a DFType
     * @throws JSONException the json exception
     */
    public String getChildNodeDFType(String nodeName) throws JSONException {

        String attrVal = "";
        childNodesjObj = getNodeJObj(nodeName);
        //  ArrayList<String> keyValues = new ArrayList<>();

        if (childNodesjObj != null) {
            childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);


            if (null != childDFPropJObj && childDFPropJObj.has(ParserConstants.DF_TYPE)) {
                JSONObject dftypeJObj = childDFPropJObj.getJSONObject(ParserConstants.DF_TYPE);

                if (dftypeJObj.has(ParserConstants.MIME)) {
                    String mime = dftypeJObj.getString(ParserConstants.MIME);
                    //keyValues.add(mime);
                    attrVal = mime;
                }
                if (dftypeJObj.has(ParserConstants.KEY_DDF_NAME)) {

                    String ddfName = dftypeJObj.getString(ParserConstants.KEY_DDF_NAME);
                    //keyValues.add(ddfName);
                    attrVal = ddfName;
                }


            } else {
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("ddfName=====", attrVal);
                }
            }

        } else {
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("ddfName=====", attrVal);
            }
        }
        return attrVal;
    }


    /**
     * This method will set value of DF Type of the node and save it in the file
     *
     * @param nodeName      - node whose DFType is to be updated.
     * @param dfTypeVal     - DFType value
     * @param nodeAttribute the node attribute
     * @return the boolean
     */
    public boolean updateChildNodeDFType(String nodeName, String dfTypeVal, String nodeAttribute) {

        boolean isUpdated = false;
        String key = null;

        try {
            childNodesjObj = getNodeJObj(nodeName);
            if (childNodesjObj != null) {
                childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);
                String ddfName = "";
                if (null != childDFPropJObj && childDFPropJObj.has(ParserConstants.DF_TYPE)) {
                    JSONObject dftypeJObj = childDFPropJObj.getJSONObject(ParserConstants.DF_TYPE);
                    Iterator iterator = dftypeJObj.keys();

                    if (iterator.hasNext()) {
                        key = (String) iterator.next();
                        if (!key.equalsIgnoreCase("") && key.equalsIgnoreCase(nodeAttribute)) {
                            dftypeJObj.put(key, dfTypeVal);
                            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                            isUpdated = true;
                        }


                    } else {

                        if (nodeAttribute.equalsIgnoreCase(ParserConstants.MIME)) {
                            dftypeJObj.put(ParserConstants.MIME, dfTypeVal);
                            childDFPropJObj.put(ParserConstants.DF_TYPE, dftypeJObj);
                            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);

                        }
                        if (nodeAttribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                            dftypeJObj.put(ParserConstants.KEY_DDF_NAME, dfTypeVal);
                            childDFPropJObj.put(ParserConstants.DF_TYPE, dftypeJObj);
                            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                        }
                    }

                    if (!isUpdated && nodeAttribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {

                        dftypeJObj.put(ParserConstants.KEY_DDF_NAME, dfTypeVal);
                        childDFPropJObj.put(ParserConstants.DF_TYPE, dftypeJObj);
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                        isUpdated = true;

                    }

                    if (!isUpdated && nodeAttribute.equalsIgnoreCase(ParserConstants.MIME)) {
                        dftypeJObj.put(ParserConstants.MIME, dfTypeVal);
                        childDFPropJObj.put(ParserConstants.DF_TYPE, dftypeJObj);
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                        isUpdated = true;

                    }


                } else {
                    JSONObject dfType = new JSONObject();
                    dfType.put(nodeAttribute, dfTypeVal);
                    // dfType.put(ParserConstants.MIME, "urn:oma:mo:oma-dm-lawmo:1.0");
                    childDFPropJObj.put(ParserConstants.DF_TYPE, dfType);
                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                }
            }
        } catch (Exception e) {
            isUpdated = false;
        }


        return isUpdated;
    }


    /**
     * Gets df format.
     *
     * @param nodeName the node name
     * @throws JSONException the json exception
     */
    public void getDFFormat(String nodeName) throws JSONException {

        String dfFormat = "";

        JSONObject jObj = getNodeJObj(nodeName);

        if (jObj != null) {

            JSONObject dfPropJObj = jObj.getJSONObject(ParserConstants.DF_PROPERTIES);

            JSONObject dfFormatJObj = dfPropJObj.getJSONObject(ParserConstants.DF_FORMAT);
        } else {

        }

    }


    /**
     * Gets occurrence.
     *
     * @param nodeName the node name
     * @throws JSONException the json exception
     */
    public void getOccurrence(String nodeName) throws JSONException {

        JSONObject jObj = getNodeJObj(nodeName);
    }

    /**
     * This method will update the DFType of the first level node
     *
     * @param nodeAttribute - node attribute to be updated (MIME/DDFNAME)
     * @param dfTypeVal     - value of the attribute
     * @return - boolean value if the updation was successful.
     * @throws JSONException the json exception
     */
    public boolean updateParentNodeDFType(String nodeAttribute, String dfTypeVal) throws JSONException {
        boolean isUpdated = false;

        if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.DF_TYPE)) {
            JSONObject dfTypeJObj = dfPropertiesJObj.getJSONObject(ParserConstants.DF_TYPE);
            /*dfTypeJObj.put(ParserConstants.KEY_DDF_NAME, dfType);
            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
            isUpdated = true;*/


            if (dfTypeJObj.has(ParserConstants.MIME)) {

                if (nodeAttribute.equalsIgnoreCase(ParserConstants.MIME)) {
                    dfTypeJObj.put(ParserConstants.MIME, dfTypeVal);
                    isUpdated = true;
                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                }

            }

            if (!dfTypeJObj.has(ParserConstants.MIME) && nodeAttribute.equalsIgnoreCase(ParserConstants.MIME)) {
                dfTypeJObj.put(ParserConstants.MIME, dfTypeVal);
                isUpdated = true;
                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
            }


            if (dfTypeJObj.has(ParserConstants.KEY_DDF_NAME) && nodeAttribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                dfTypeJObj.put(ParserConstants.KEY_DDF_NAME, dfTypeVal);
                isUpdated = true;
                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);

            }

            if (!dfTypeJObj.has(ParserConstants.KEY_DDF_NAME) && nodeAttribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                dfTypeJObj.put(ParserConstants.KEY_DDF_NAME, dfTypeVal);
                isUpdated = true;
                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);

            }


        } else {
            JSONObject newDFType = new JSONObject();
            newDFType.put(nodeAttribute, dfTypeVal);
            // dfType.put(ParserConstants.MIME, "urn:oma:mo:oma-dm-lawmo:1.0");
            dfPropertiesJObj.put(ParserConstants.DF_TYPE, newDFType);
            isUpdated = true;
            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);

        }
        if (Constants.DEBUGGABLE_MODE) {
            Log.i("Mgmt Tree parentDFTYPE:", jsonObject.toString());
        }

        return isUpdated;
    }


    /**
     * This method deletes a particular attribute of az node.
     *
     * @param nodeName      - node whose attribute is to be deleted.
     * @param nodeAttribute - the attribute which is to be removed.
     * @return - boolean value if the node attribute was successfully deleted.
     * @throws JSONException the json exception
     */
    public boolean deleteNodeAttribute(String nodeName, String nodeAttribute) throws JSONException {

        boolean isUpdated = false;
        childNodesjObj = getNodeJObj(nodeName);

        if (childNodesjObj != null) {
            if (childNodesjObj.has(ParserConstants.DF_PROPERTIES)) {
                childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);

                if (childDFPropJObj != null && childDFPropJObj.has(ParserConstants.KEY_ACCESS_TYPE) && nodeAttribute.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {

                    childDFPropJObj.remove(ParserConstants.KEY_ACCESS_TYPE);
                    isUpdated = true;

                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);

                }
                if (childDFPropJObj != null && childDFPropJObj.has(ParserConstants.DF_OCCURRENCE) && nodeAttribute.equalsIgnoreCase(ParserConstants.DF_OCCURRENCE)) {
                    childDFPropJObj.remove(ParserConstants.DF_OCCURRENCE);
                    isUpdated = true;
                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                }

                if (childDFPropJObj != null && childDFPropJObj.has(ParserConstants.DF_FORMAT) && nodeAttribute.equalsIgnoreCase(ParserConstants.DF_FORMAT)) {
                    childDFPropJObj.remove(ParserConstants.DF_FORMAT);
                    isUpdated = true;
                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                }

                if (childDFPropJObj != null && childDFPropJObj.has(ParserConstants.DF_TITLE) && nodeAttribute.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
                    childDFPropJObj.remove(ParserConstants.DF_TITLE);
                    isUpdated = true;
                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                }

                if (childDFPropJObj != null && childDFPropJObj.has(ParserConstants.DF_TYPE) && nodeAttribute.equalsIgnoreCase(ParserConstants.MIME)) {

                    JSONObject childDfTypeJObj = childDFPropJObj.getJSONObject(ParserConstants.DF_TYPE);


                    if (childDfTypeJObj.has(ParserConstants.MIME)) {
                        childDfTypeJObj.remove(nodeAttribute);
                        isUpdated = true;
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                    }
                }

                if (childDFPropJObj != null && childDFPropJObj.has(ParserConstants.DF_TYPE) && nodeAttribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                    JSONObject childDfTypeJObj = childDFPropJObj.getJSONObject(ParserConstants.DF_TYPE);
                    if (childDfTypeJObj.has(ParserConstants.KEY_DDF_NAME)) {
                        childDfTypeJObj.remove(nodeAttribute);
                        isUpdated = true;
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                    }
                }
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("childDFPropJObj:", childDFPropJObj.toString());
                }

                if (childDFPropJObj != null && !childDFPropJObj.has(ParserConstants.KEY_ACCESS_TYPE) && !childDFPropJObj.has(ParserConstants.DF_FORMAT) && !childDFPropJObj.has(ParserConstants.DF_TITLE) && !childDFPropJObj.has(ParserConstants.DF_OCCURRENCE) && !childDFPropJObj.has(ParserConstants.DF_TYPE)) {

                    childNodesjObj.remove(ParserConstants.DF_PROPERTIES);
                    isUpdated = true;
                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                }
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("JsonObject 3: ", jsonObject.toString());
                }
            }
        } else {
            isUpdated = false;
        }

        return isUpdated;
    }

    /**
     * This method deletes a particular attribute of a first level node.
     *
     * @param attributeName - the attribute which is to be removed.
     * @return - boolean value if the node attribute was successfuly deleted.
     */
    public boolean deleteParentNodeAttribute(String attributeName) {
        boolean isDeleted = false;
        if (mNodeJObj != null) {

            try {

                JSONObject dfProp = mNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);

                if (dfProp != null) {

                    if (dfProp.has(ParserConstants.KEY_ACCESS_TYPE) && attributeName.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {
                        dfProp.remove(ParserConstants.KEY_ACCESS_TYPE);
                        isDeleted = true;
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);

                    }
                    if (dfProp.has(ParserConstants.DF_FORMAT) && attributeName.equalsIgnoreCase(ParserConstants.DF_FORMAT)) {
                        dfProp.remove(ParserConstants.DF_FORMAT);
                        isDeleted = true;
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                    }
                    if (dfProp.has(ParserConstants.DF_OCCURRENCE) && attributeName.equalsIgnoreCase(ParserConstants.DF_OCCURRENCE)) {
                        dfProp.remove(ParserConstants.DF_OCCURRENCE);
                        isDeleted = true;
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                    }
                    if (dfProp.has(ParserConstants.DF_TITLE) && attributeName.equals(ParserConstants.DF_TITLE)) {
                        dfProp.remove(ParserConstants.DF_TITLE);
                        isDeleted = true;
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                    }
                    if (dfProp.has(ParserConstants.DF_TYPE) && attributeName.equals(ParserConstants.MIME)) {
                        JSONObject dftype = dfProp.getJSONObject(ParserConstants.DF_TYPE);
                        if (dftype.has(ParserConstants.MIME)) {
                            dftype.remove(ParserConstants.MIME);
                            deleteEmptyDFType(dfProp, dftype);
                            isDeleted = true;
                            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                        }
                    }
                    if (dfProp.has(ParserConstants.DF_TYPE) && attributeName.equals(ParserConstants.KEY_DDF_NAME)) {
                        JSONObject dftype = dfProp.getJSONObject(ParserConstants.DF_TYPE);
                        if (dftype.has(ParserConstants.KEY_DDF_NAME)) {
                            dftype.remove(ParserConstants.KEY_DDF_NAME);
                            deleteEmptyDFType(dfProp, dftype);
                            isDeleted = true;
                            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                        }
                    }

                }


            } catch (Exception e) {
                isDeleted = false;
                e.printStackTrace();
            }
        }
        if (Constants.DEBUGGABLE_MODE) {
            Log.i("dlt prnt node attr: ", jsonObject.toString());
        }
        return isDeleted;
    }

    /**
     * This method deletes the empty DFType json object
     *
     * @param dfProp - Json object of DFProperties
     * @param dftype - Json object of DFType
     */
    public void deleteEmptyDFType(JSONObject dfProp, JSONObject dftype) {

        if (!dftype.has(ParserConstants.MIME) && !dftype.has(ParserConstants.KEY_DDF_NAME)) {
            dfProp.remove(ParserConstants.DF_TYPE);

        }


    }


    /**
     * This method delete a complete node
     *
     * @param nodeName - node which is to be deleted.
     * @return the boolean
     * @throws JSONException the json exception
     */
    public boolean deleteNode(String nodeName) throws JSONException {

        boolean isUpdated = false;
        JSONObject childNodesJObj = getNodeJObj(nodeName);

        if (mNodeJObj.has(nodeName)) {

            mNodeJObj.remove(nodeName);
            isUpdated = true;
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("Mgmt Tree obj 4:", jsonObject.toString());
            }

        }

        if (mChildNodesArrObj.contains(childNodesJObj)) {

            mChildNodesArrObj.remove(childNodesJObj);
            JSONArray jArrTemp = new JSONArray(mChildNodesArrObj);
            childNodeJArray = null;
            childNodeJArray = jArrTemp;
            mNodeJObj.put(ParserConstants.NODE, childNodeJArray);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("Mgmt Tree obj 1:", jsonObject.toString());
            }
            isUpdated = true;
            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
        }
        if (mNewChildNodeArrObj.contains(childNodesJObj)) {
            mNewChildNodeArrObj.remove(childNodesJObj);
            JSONArray jArrTemp = new JSONArray(mNewChildNodeArrObj);
            if (Constants.DEBUGGABLE_MODE) {
                Log.d("jArrTemp 1: ", jArrTemp.toString());
            }

            newChildNodeJArray = null;
            newChildNodeJArray = jArrTemp;
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("newChildNodeJArray 1:", newChildNodeJArray.toString());
            }


            childNodeJArray = mNodeJObj.getJSONArray(ParserConstants.NODE);

            for (int i = 0; i < childNodeJArray.length(); i++) {

                JSONObject jobj = childNodeJArray.getJSONObject(i);

                if (jobj.has(ParserConstants.NODE) && jobj.getString(ParserConstants.KEY_NODE_NAME).equalsIgnoreCase("Operations")) {

                    jobj.put(ParserConstants.NODE, newChildNodeJArray);

                }
                isUpdated = true;
                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);

            }
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("Mgmt Tree obj 2:", jsonObject.toString());
            }

        }
        if (mNewGrandChildeNodeArrObj.contains(childNodesJObj)) {

            mNewGrandChildeNodeArrObj.remove(childNodesJObj);
            JSONArray jArrTemp = new JSONArray(mNewGrandChildeNodeArrObj);
            newGrandChildNodeJArray = null;
            newGrandChildNodeJArray = jArrTemp;
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("newGrandChildNodeJArr:", newGrandChildNodeJArray.toString());
            }
            mGrandChildNodeJObj.put(ParserConstants.NODE, newGrandChildNodeJArray);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("mGrandChildNodeJObj :", mGrandChildNodeJObj.toString());
                Log.i("Mgmt Tree obj 3:", jsonObject.toString());
            }
            isUpdated = true;
            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);

        }


        if (Constants.DEBUGGABLE_MODE) {
            Log.i("Mgmt Tree obj 5:", jsonObject.toString());
        }
        return isUpdated;
    }

    /**
     * This method adds a new node.
     *
     * @param nodeName         - name of the node to be added.
     * @param parentNodeName   - parent node under which the new node is to be added.
     * @param nodeAttribute    - attribute to be set for the newly created node.
     * @param nodeAttributeVal - Value of the attribute.
     * @return - boolean value if the add operation was successful.
     * @throws JSONException the json exception
     */
    public boolean addNode(String nodeName, String parentNodeName, String nodeAttribute, String nodeAttributeVal) throws JSONException {
        JSONObject JObj = null;
        JSONObject parentJObj = null;
        boolean isAdded = false;

        if (!nodeName.equalsIgnoreCase("")) {
            JObj = getNodeJObj(nodeName);

            if (JObj == null && !parentNodeName.equalsIgnoreCase("")) {

                parentJObj = getNodeJObj(parentNodeName);
            }


        }
        if (JObj == null && parentJObj == null) {
            mNodeJObj.put(nodeName, returnNodePropertiesObj(nodeName, ParserConstants.KEY_ACCESS_TYPE, "new " + nodeAttribute + " Added"));
            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("Mgmt Tree addNode1:-", jsonObject.toString());
            }
            isAdded = true;
        }
        if (Constants.DEBUGGABLE_MODE) {
            Log.i("mNewChildNodeArrObj:-", mNewChildNodeArrObj.toString());
        }

        if (parentJObj != null && mNewChildNodeArrObj != null && !mNewChildNodeArrObj.contains(nodeName)) {

            if (parentJObj.getString(ParserConstants.KEY_NODE_NAME).equalsIgnoreCase("Operations")) {
                int arrSize = newChildNodeJArray.length();
                newChildNodeJArray.put(arrSize, returnNodePropertiesObj(nodeName, nodeAttribute, nodeAttributeVal));

                mNewChildNodeArrObj = new ArrayList<>();

                for (int i = 0; i < newChildNodeJArray.length(); i++) {

                    mNewChildNodeArrObj.add(newChildNodeJArray.getJSONObject(i));

                }

                Log.i("mNewChildNodeArrObj 2:", mNewChildNodeArrObj.toString());


                //  mNewChildNodeArrObj.add(returnNodePropertiesObj(nodeName, nodeAttribute, nodeAttributeVal));

                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("Mgmt Tree addNode2:-", jsonObject.toString());
                }
                isAdded = true;
            }
            if (parentJObj.getString(ParserConstants.KEY_NODE_NAME).equalsIgnoreCase("AvailableWipeList")) {

                int arrSize = newGrandChildNodeJArray.length();
                newGrandChildNodeJArray.put(arrSize, returnNodePropertiesObj(nodeName, nodeAttribute, nodeAttributeVal));
                //  mGrandChildNodeJObj.put(nodeName, returnNodePropertiesObj(nodeName, nodeAttribute, "new " + nodeAttribute + " Added"));

                //mNewGrandChildeNodeArrObj.add();

                mNewGrandChildeNodeArrObj = new ArrayList<>();
                for (int j = 0; j < newGrandChildNodeJArray.length(); j++) {
                    mNewGrandChildeNodeArrObj.add(newGrandChildNodeJArray.getJSONObject(j));

                }
                Log.i("mNwGrndChildNdArrObj2:-", jsonObject.toString());

                //mNewGrandChildeNodeArrObj.add(returnNodePropertiesObj(nodeName, nodeAttribute, nodeAttributeVal));


                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
                isAdded = true;
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("Mgmt Tree addNode3:-", jsonObject.toString());
                }
            }

        }
      /*  if (Constants.DEBUGGABLE_MODE) {
            Log.i("mNewGrandChldNdeArOb:", mNewGrandChildeNodeArrObj.toString());
        }*/


        /**
         * NOT TO BE DELETED. [FOR FUTURE USE].
         */

      /* if (parentJObj != null && mGrandChildNodeJObj.has(ParserConstants.NODE) ) {

            int arrSize = newGrandChildNodeJArray.length();
            newGrandChildNodeJArray.put(arrSize, returnNodePropertiesObj(nodeName));
            Log.i("newGrandChldNdeJArry:-", newGrandChildNodeJArray.toString());
            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);
            Log.i("Mgmt Tree addNode3:-", jsonObject.toString());
            isAdded = true;

        }*/


        return isAdded;
    }

    /**
     * This methos returns node object with all the node properties(Attributes) for a freshly created node.
     *
     * @param nodeName          - Name of the node to be created.
     * @param nodeAttrToBeAdded - The attribute whose value is to be set.
     * @param newAttrVal        - The value of the node attribue.
     * @return Json object of the node to be created.
     * @throws JSONException the json exception
     */
    public JSONObject returnNodePropertiesObj(String nodeName, String nodeAttrToBeAdded, String newAttrVal) throws JSONException {

        JSONObject node = new JSONObject();

        node.put(ParserConstants.KEY_NODE_NAME, nodeName);

        JSONObject DFProperties = new JSONObject();

        if (nodeAttrToBeAdded.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {

            updateNewNodeAttrVal(DFProperties, nodeAttrToBeAdded, newAttrVal);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("DFProperties1:", DFProperties.toString());
            }

        }
        if (nodeAttrToBeAdded.equalsIgnoreCase(ParserConstants.DF_FORMAT)) {
            updateNewNodeAttrVal(DFProperties, nodeAttrToBeAdded, newAttrVal);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("DFProperties2:", DFProperties.toString());
            }
        }
        if (nodeAttrToBeAdded.equalsIgnoreCase(ParserConstants.DF_OCCURRENCE)) {
            updateNewNodeAttrVal(DFProperties, nodeAttrToBeAdded, newAttrVal);

            if (Constants.DEBUGGABLE_MODE) {
                Log.i("DFProperties3:", DFProperties.toString());
            }
        }

        if (nodeAttrToBeAdded.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
            updateNewNodeAttrVal(DFProperties, nodeAttrToBeAdded, newAttrVal);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("DFProperties4:", DFProperties.toString());
            }
        }

        if (nodeAttrToBeAdded.equalsIgnoreCase(ParserConstants.MIME) || nodeAttrToBeAdded.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
            updateNewNodeAttrVal(DFProperties, nodeAttrToBeAdded, newAttrVal);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("DFProperties5:", DFProperties.toString());
            }
        }
        node.put(ParserConstants.DF_PROPERTIES, DFProperties);

        return node;
    }

    /**
     * This methos sets the node attribue value foe a freshly created node.
     *
     * @param jObj           - node object whose attribute is to be updated.
     * @param updateNodeAttr - node attribue whose value is to be set.
     * @param nodeAttrVal    - node attribute value to be set
     */
    public void updateNewNodeAttrVal(JSONObject jObj, String updateNodeAttr, String nodeAttrVal) {

        try {
            JSONArray jArr = new JSONArray();
            // jArr.put(0,nodeAttrVal);

            jObj.put(ParserConstants.KEY_ACCESS_TYPE, jArr);

            JSONObject dfFormatJObj = new JSONObject();
            // dfFormatJObj.put(ParserConstants.DF_FORMAT, "");
            jObj.put(ParserConstants.DF_FORMAT, dfFormatJObj);

            JSONObject dfOccurrence = new JSONObject();
            //dfOccurrence.put(ParserConstants.DF_OCCURRENCE, "");
            jObj.put(ParserConstants.DF_OCCURRENCE, dfOccurrence);

            jObj.put(ParserConstants.DF_TITLE, "");

            JSONObject dfType = new JSONObject();
            // dfType.put(updateNodeAttr, "");
            jObj.put(ParserConstants.DF_TYPE, dfType);

            if (updateNodeAttr.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {

                jArr.put(0, nodeAttrVal);
                jObj.put(ParserConstants.KEY_ACCESS_TYPE, jArr);
            }

            if (updateNodeAttr.equalsIgnoreCase(ParserConstants.DF_FORMAT)) {

                dfFormatJObj.put(ParserConstants.DF_FORMAT, nodeAttrVal);
                jObj.put(ParserConstants.DF_FORMAT, dfFormatJObj);
            }
            if (updateNodeAttr.equalsIgnoreCase(ParserConstants.DF_OCCURRENCE)) {
                dfOccurrence.put(ParserConstants.DF_OCCURRENCE, nodeAttrVal);
                jObj.put(ParserConstants.DF_OCCURRENCE, dfOccurrence);
            }
            if (updateNodeAttr.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
                jObj.put(ParserConstants.DF_TITLE, nodeAttrVal);
            }
            if (updateNodeAttr.equalsIgnoreCase(ParserConstants.MIME) || updateNodeAttr.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                dfType.put(updateNodeAttr, nodeAttrVal);
                jObj.put(ParserConstants.DF_TYPE, dfType);
            }
        } catch (Exception e) {

            if (Constants.DEBUGGABLE_MODE) {
                Log.i("EXception :-", e.toString());
            }
        }
    }

    /**
     * This method updated the attributes of a node
     *
     * @param nodeName            - the node name whose attributes you want to update
     * @param parentNode          the parent node
     * @param nodeAttrToBeUpdated - The node attribute to be updaed.
     * @param newAttrVal          - The attribute value
     * @return - boolean value if its updated or not.
     * @throws JSONException the json exception
     */
    public boolean updateNodeAttribute(String nodeName, String parentNode, String nodeAttrToBeUpdated, String newAttrVal) throws JSONException {
        boolean isUpdated = false;

        JSONObject jObj = getNodeJObj(nodeName);


        if (jObj == null) {
            isUpdated = addNode(nodeName, parentNode, nodeAttrToBeUpdated, newAttrVal);

        } else {

            if (nodeAttrToBeUpdated.equalsIgnoreCase(ParserConstants.DF_TITLE)) {

                isUpdated = updateChildNodeDFTitle(nodeName, newAttrVal);
                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);


            }
            if (nodeAttrToBeUpdated.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME) || nodeAttrToBeUpdated.equalsIgnoreCase(ParserConstants.MIME)) {

                isUpdated = updateChildNodeDFType(nodeName, newAttrVal, nodeAttrToBeUpdated);
                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.LAWMO_JSON);

            }

        }
        if (Constants.DEBUGGABLE_MODE) {
            Log.i("Mgmt updateNode method:", jsonObject.toString());
        }


        return isUpdated;
    }

    /**
     * Generic method to get the attributes of a node.
     *
     * @param nodeName     - node whose attribute is to be determined.
     * @param nodeProperty - node attribute to be determined.
     * @return the node attribute
     * @throws JSONException the json exception
     */
    public ArrayList<String> getNodeAttribute(String nodeName, String nodeProperty) throws JSONException {

        ArrayList<String> attributeVal = new ArrayList<>();

        if (nodeProperty.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
            String dftitle = getChildNodeDFTitle(nodeName);
            attributeVal.add(dftitle);

        }
        if ((nodeProperty.equalsIgnoreCase(ParserConstants.MIME))) {
            String mime = getChildNodeDFType(nodeName);
            attributeVal.add(mime);
        }
        if ((nodeProperty.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME))) {
            String ddfname = getChildNodeDFType(nodeName);
            attributeVal.add(ddfname);
        }
        if (nodeProperty.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {
            attributeVal = getChildNodeAccessType(nodeName);
        }

        if (nodeName.equalsIgnoreCase(ParserConstants.PARENT)) {

            if (nodeProperty.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {

                attributeVal = getParentNodeAccessType();
            }

            if (nodeProperty.equalsIgnoreCase(ParserConstants.MIME)) {
                String mime = getParentMIME();
                attributeVal.add(mime);

            }
            if (nodeProperty.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                String ddfname = getParentDDFName();
                attributeVal.add(ddfname);
            }
            if (nodeProperty.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
                String dftitle = getParentNodeDFTitle();
                attributeVal.add(dftitle);
            }


        }
        return attributeVal;

    }


}