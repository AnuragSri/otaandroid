package com.movimento.motreeparser.fumostatemodel;

/**
 * The type State.
 */
public class State {
    private String Current_state;

    private String Current_retry;

    private String Max_retry;

    /**
     * Gets current state.
     *
     * @return the current state
     */
    public String getCurrent_state() {
        return Current_state;
    }

    /**
     * Sets current state.
     *
     * @param Current_state the current state
     */
    public void setCurrent_state(String Current_state) {
        this.Current_state = Current_state;
    }

    /**
     * Gets current retry.
     *
     * @return the current retry
     */
    public String getCurrent_retry() {
        return Current_retry;
    }

    /**
     * Sets current retry.
     *
     * @param Current_retry the current retry
     */
    public void setCurrent_retry(String Current_retry) {
        this.Current_retry = Current_retry;
    }

    /**
     * Gets max retry.
     *
     * @return the max retry
     */
    public String getMax_retry() {
        return Max_retry;
    }

    /**
     * Sets max retry.
     *
     * @param Max_retry the max retry
     */
    public void setMax_retry(String Max_retry) {
        this.Max_retry = Max_retry;
    }

}
