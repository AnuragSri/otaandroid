package com.movimento.motreeparser;

import android.content.Context;
import android.util.Log;

import com.movimento.otaclient.OTAClientApp;
import com.movimento.utils.Constants;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


/**
 * The type Device info parser.
 */
public class DeviceInfoParser extends MTParser {


    private static DeviceInfoParser mDeviceInfoParserInstance;
    private Context mContext;

    private ArrayList<String> mNodeName = new ArrayList<>();

    private ArrayList<String> mChildNodeName = new ArrayList<>();
    private ArrayList<String> baseNodeAccessType = new ArrayList<>();

    private HashMap<String, String> mMap = new HashMap<>();
    private HashMap<String, List<String>> mNodeData = new HashMap<>();
    private ArrayList<String> mNodeProperties = new ArrayList<>();

    private JSONObject mMgmtJObj;
    private JSONObject mNodeJObj;
    private JSONObject childNodesjObj;

    private JSONArray childNodeJArray;
    private String json;
    private JSONObject dfPropertiesJObj;
    private JSONArray newNodeJArray;
    private JSONObject jsonObject;
    private static ArrayList<JSONObject> mChildNodesArrObj;
    private static ArrayList<JSONObject> mNewChildNodeArrObj;

    private Utils mMyUtil;

    private JSONObject childDFPropJObj;
    private JSONArray childAccTyp;
    private JSONObject childDfTypeJObj;


    private DeviceInfoParser() {
        mMyUtil = new Utils();
        getJson();

    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static DeviceInfoParser getInstance() {
        synchronized (DeviceInfoParser.class) {
            if (mDeviceInfoParserInstance == null)
                mDeviceInfoParserInstance = new DeviceInfoParser();
        }
        return mDeviceInfoParserInstance;
    }

    /**
     * This method will parse the json file and initialise the json Objects .
     */
    public void getJson() {
        mContext = OTAClientApp.getmAppInstance();

        try {
            File file = mContext.getFileStreamPath(ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);

            if (file.exists()) {
                jsonObject = new JSONObject(Utils.readFromFile(mContext, ParserConstants.DEVICE_INFO_TEXT_FILE_NAME));
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("jsonObject 1 :-", jsonObject.toString());
                }
            } else {
                jsonObject = new JSONObject(Utils.getJsonString(ParserConstants.DEV_INFO_MO_TREE, mContext));
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("jsonObject 2 :-", jsonObject.toString());
                }
            }

            if (jsonObject.has(ParserConstants.MGMNT_TREE)) {

                Iterator iterator = jsonObject.keys();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.i("key :---", key);
                    }
                    mMgmtJObj = jsonObject.getJSONObject(key);
                    String VerDTD = mMgmtJObj.getString(ParserConstants.VER_DTD);
                    mNodeJObj = mMgmtJObj.getJSONObject(ParserConstants.NODE);
                    if (mNodeJObj.has(ParserConstants.DF_PROPERTIES)) {


                        dfPropertiesJObj = mNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);

                        // JSONObject dfPropertiesJObj = mNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);
                        JSONArray accTyp = dfPropertiesJObj.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);

                        int size = accTyp.length();

                        for (int a = 0; a < size; a++) {

                            baseNodeAccessType.add(accTyp.get(a).toString());
                            if (Constants.DEBUGGABLE_MODE) {
                                Log.i("accessType:-", baseNodeAccessType.get(a));
                            }
                        }

                        if (dfPropertiesJObj.has(ParserConstants.DF_TITLE)) {
                            String DFTitle = dfPropertiesJObj.getString(ParserConstants.DF_TITLE);

                            if (Constants.DEBUGGABLE_MODE) {
                                Log.i("DFTitle :-", DFTitle);
                            }

                        }
                        if (dfPropertiesJObj.has(ParserConstants.DF_TYPE)) {
                            JSONObject dfTypeJObj = dfPropertiesJObj.getJSONObject(ParserConstants.DF_TYPE);
                            //    JSONObject dfTypeJObj = getKeyJsonObj(ParserConstants.DF_TYPE, dfPropertiesJObj);
                            String DDFName = dfTypeJObj.getString(ParserConstants.KEY_DDF_NAME);
                            if (Constants.DEBUGGABLE_MODE) {
                                Log.e("DDFName=====", DDFName);
                            }
                        }

                    }

                    if (mNodeJObj.has(ParserConstants.NODE)) {

                        childNodeJArray = mNodeJObj.getJSONArray(ParserConstants.NODE);


                        int childNodesCount = childNodeJArray.length();
                        mChildNodesArrObj = new ArrayList<>();

                        for (int i = 0; i < childNodesCount; i++) {

                            childNodesjObj = childNodeJArray.getJSONObject(i);

                            mChildNodesArrObj.add(childNodesjObj);


                            String nodeName = "";
                            if (childNodesjObj.has(ParserConstants.KEY_NODE_NAME)) {

                                nodeName = childNodesjObj.get(ParserConstants.KEY_NODE_NAME).toString();
                                mNodeName.add(nodeName);
                            }

                            if (childNodesjObj.has(ParserConstants.DF_PROPERTIES)) {

                                childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);


                                ArrayList<String> childAccessType = new ArrayList<>();
                                childAccTyp = childDFPropJObj.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);

                                int size = childAccTyp.length();

                                for (int b = 0; b < size; b++) {
                                    childAccessType.add(childAccTyp.get(b).toString());
                                    mNodeProperties.add(childAccTyp.get(b).toString());
                                    if (Constants.DEBUGGABLE_MODE) {
                                        Log.i("accessType :-", childAccessType.get(b));
                                    }
                                }

                                if (childDFPropJObj.has(ParserConstants.DF_TITLE)) {
                                    String childDFTitle = childDFPropJObj.getString(ParserConstants.DF_TITLE);
                                    if (Constants.DEBUGGABLE_MODE) {
                                        Log.i("DFTitle :-", childDFTitle);
                                    }
                                }

                                /*mMap.put(nodeName, childDFTitle);
                                mNodeProperties.add(childDFTitle);
*/
                                if (nodeName.equalsIgnoreCase(ParserConstants.NODE_EXT)) {
                                    JSONObject childDFFormatJobj = childDFPropJObj.getJSONObject(ParserConstants.DF_FORMAT);


                                    JSONObject childOccuranceJobj = childDFPropJObj.getJSONObject(ParserConstants.DF_OCCURRENCE);


                                    if (childDFPropJObj.has(ParserConstants.NODE)) {
                                        //  JSONObject newNodeJObj = childDFPropJObj.getJSONObject(ParserConstants.NODE);

                                        newNodeJArray = childDFPropJObj.getJSONArray(ParserConstants.NODE);
                                        //getChildNodeJObj("DmV");


                                        int newChildNodesCount = newNodeJArray.length();
                                        mNewChildNodeArrObj = new ArrayList<>();

                                        for (int j = 0; j < newChildNodesCount; j++) {

                                            JSONObject newNodeJObj = newNodeJArray.getJSONObject(j);

                                            mNewChildNodeArrObj.add(newNodeJObj);

                                            if (newNodeJObj.has(ParserConstants.KEY_NODE_NAME)) {

                                                String newChildNodeName = newNodeJObj.getString(ParserConstants.KEY_NODE_NAME).toString();
                                                mChildNodeName.add(newChildNodeName);

                                            }

                                            if (newNodeJObj.has(ParserConstants.DF_PROPERTIES)) {
                                                JSONObject newChildDFPropJObj = newNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);

                                                ArrayList<String> newChildAccessType = new ArrayList<>();


                                                JSONArray newChildAccTyp = newChildDFPropJObj.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);

                                                int newChildAccTypSize = newChildAccTyp.length();

                                                for (int c = 0; c < newChildAccTypSize; c++) {

                                                    newChildAccessType.add(newChildAccTyp.get(c).toString());

                                                }

                                                if (newChildDFPropJObj.has(ParserConstants.DF_TITLE)) {
                                                    String newChildDFTitle = newChildDFPropJObj.getString(ParserConstants.DF_TITLE);

                                                }

                                                if (newChildDFPropJObj.has(ParserConstants.DF_TYPE)) {

                                                    JSONObject newChildDFTypeJObj = newChildDFPropJObj.getJSONObject(ParserConstants.DF_TYPE);

                                                    String Mime = newChildDFTypeJObj.getString(ParserConstants.MIME);
                                                }
                                            }
                                        }

                                        int mChildNodesArrObjSize = mNewChildNodeArrObj.size();

                                    }
                                }
                                if (childDFPropJObj.has(ParserConstants.DF_TYPE)) {
                                    childDfTypeJObj = childDFPropJObj.getJSONObject(ParserConstants.DF_TYPE);
                                    //    JSONObject dfTypeJObj = getKeyJsonObj(ParserConstants.DF_TYPE, dfPropertiesJObj);
                                    if (nodeName.equalsIgnoreCase(ParserConstants.NODE_EXT)) {
                                        String DDFName = null;
                                        if (childDfTypeJObj.has(ParserConstants.KEY_DDF_NAME)) {

                                            DDFName = childDfTypeJObj.getString(ParserConstants.KEY_DDF_NAME);
                                        }

                                    } else if (childDfTypeJObj.has(ParserConstants.MIME)) {

                                        String Mime = childDfTypeJObj.getString(ParserConstants.MIME);

                                        mNodeProperties.add(Mime);

                                        mNodeData.put(nodeName, mNodeProperties);

                                    }
                                }
                            }

                        }
                        int mChildNodesJObjSize = mChildNodesArrObj.size();

                    }

                }
            }


        } catch (Exception e) {
         //       Log.i("NO management","NO management tree found");
            //Toast.makeText(mContext, "NO management tree found", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This method returns the Json Object for the searched  node.
     *
     * @param nodeName - name of the node whose Json object is to be found.
     * @return - json object of the searched node.
     * @throws JSONException the json exception
     */
    public JSONObject getChildNodeJObj(String nodeName) throws JSONException {

        JSONObject nodeJObj = null;

        if (mNodeJObj.has(nodeName)) {

            nodeJObj = mNodeJObj.getJSONObject(nodeName);
        }
        if (nodeJObj == null) {

            nodeJObj = Utils.getJsonObj(nodeName, childNodeJArray);
        }
        if (nodeJObj == null) {
            nodeJObj = Utils.getJsonObj(nodeName, newNodeJArray);
        }
        return nodeJObj;

    }


    /**
     * This method returns the Version Id from the root node
     *
     * @return - Version ID.
     * @throws JSONException the json exception
     */
    public String getVersionId() throws JSONException {
        String verionID = null;

        if (null != mMgmtJObj && mMgmtJObj.has(ParserConstants.VER_DTD)) {
            verionID = mMgmtJObj.getString(ParserConstants.VER_DTD);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("verionID---", verionID);
            }
        } else {
            return null;
        }
        return verionID;


    }


    /**
     * This method will update the Ver Id and save the in json file
     *
     * @param version - Version number that is to be updated.
     * @return - boolean value if the update was successful or not.
     */
    public boolean updateVersionID(String version) {

        boolean isUpdated = false;
        try {
            mMgmtJObj.put(ParserConstants.VER_DTD, version);

            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);

            //Utils.writeToFIle(mContext,mMgmtJObj.toString(),ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
            isUpdated = true;

        } catch (JSONException e) {
            e.printStackTrace();
            isUpdated = false;
        }

        return isUpdated;
    }


    /**
     * This method returns the Access Type of the first level node .
     *
     * @return - List containing all the values of AccessType.
     * @throws JSONException the json exception
     */
    public ArrayList<String> getParentNodeAccessType() throws JSONException {
        String accVal = "";

        ArrayList<String> baseNodeAccessType = new ArrayList<>();

        if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.KEY_ACCESS_TYPE)) {

            JSONArray accTyp = dfPropertiesJObj.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);


            for (int i = 0; i < accTyp.length(); i++) {
                baseNodeAccessType.add(accTyp.get(i).toString());
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("accessType :-", baseNodeAccessType.get(i));
                }
                // String accVal = accTyp.get(0).toString();
                accVal = baseNodeAccessType.get(i);

                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("getParentNodeAccessTyp:", accVal);
                }
            }

        } else {
            baseNodeAccessType = null;
        }

        return baseNodeAccessType;

    }

    /**
     * This method will set the value of Access type of the first level node and save it in file.
     * @param - AccessType value that is to be updated
     * @return - boolean value that if the update operation was successful or not.
     */
      /*  public boolean updateParentNodeAccessType(String accTypVal) throws JSONException {

            boolean isUpdated = false;

            if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.KEY_ACCESS_TYPE)) {
                JSONArray accTyp = dfPropertiesJObj.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);


                accTyp.put(0, accTypVal);

                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                isUpdated = true;
            } else {
                isUpdated = false;
            }
            return isUpdated;
        }
*/

    /**
     * This method is used to get the DFTitle of the first level node.
     *
     * @return - DFTitle of the first level node.
     */
    public String getParentNodeDFTitle() {
        String dfTitle = null;
        try {

            if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.DF_TITLE)) {
                String DFTitle = dfPropertiesJObj.getString(ParserConstants.DF_TITLE);
                dfTitle = DFTitle;
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("dfTitle", "====" + dfTitle);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("dfTitle", "====" + dfTitle);
            }
            dfTitle = null;

        }
        return dfTitle;
    }

    /**
     * This method will set the DF title of the first level node and save in the file.
     *
     * @param dfTitleVal the df title val
     * @return - boolean value that if the update operation was successful or not.
     * @throws JSONException the json exception
     */
    public boolean updateParentNodeDFTitle(String dfTitleVal) throws JSONException {
        String DFTitle;
        boolean isUpdated = false;

        if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.DF_TITLE)) {
            DFTitle = dfPropertiesJObj.getString(ParserConstants.DF_TITLE);

            dfPropertiesJObj.put(ParserConstants.DF_TITLE, dfTitleVal);
            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
            isUpdated = true;

        } else {

            dfPropertiesJObj.put(ParserConstants.DF_TITLE, dfTitleVal);
        }
        return isUpdated;

    }

    /**
     * This method will update the DFType of the first level node
     *
     * @param nodeAttribute - node attribute to be updated (MIME/DDFNAME)
     * @param dfTypeVal     - value of the attribute
     * @return - boolean value if the updation was successful.
     * @throws JSONException the json exception
     */
    public boolean updateParentNodeDFType(String nodeAttribute, String dfTypeVal) throws JSONException {
        boolean isUpdated = false;

        if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.DF_TYPE)) {
            JSONObject dfTypeJObj = dfPropertiesJObj.getJSONObject(ParserConstants.DF_TYPE);
            /*dfTypeJObj.put(ParserConstants.DDF_NAME, dfType);
            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
            isUpdated = true;*/


            if (dfTypeJObj.has(ParserConstants.MIME)) {

                if (nodeAttribute.equalsIgnoreCase(ParserConstants.MIME)) {
                    dfTypeJObj.put(ParserConstants.MIME, dfTypeVal);
                    isUpdated = true;
                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                }

            }

            if (!dfTypeJObj.has(ParserConstants.MIME) && nodeAttribute.equalsIgnoreCase(ParserConstants.MIME)) {
                dfTypeJObj.put(ParserConstants.MIME, dfTypeVal);
                isUpdated = true;
                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
            }


            if (dfTypeJObj.has(ParserConstants.KEY_DDF_NAME) && nodeAttribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                dfTypeJObj.put(ParserConstants.KEY_DDF_NAME, dfTypeVal);
                isUpdated = true;
                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);

            }

            if (!dfTypeJObj.has(ParserConstants.KEY_DDF_NAME) && nodeAttribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                dfTypeJObj.put(ParserConstants.KEY_DDF_NAME, dfTypeVal);
                isUpdated = true;
                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);

            }


        } else {
            JSONObject newDFType = new JSONObject();
            newDFType.put(nodeAttribute, dfTypeVal);
            dfPropertiesJObj.put(ParserConstants.DF_TYPE, newDFType);
            isUpdated = true;
            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);

        }
        if (Constants.DEBUGGABLE_MODE) {
            Log.i("Mgmt Tree parentDFTYPE:", jsonObject.toString());
        }
        return isUpdated;
    }

    /**
     * This method will return the DF Type of the first level node
     *
     * @return - Return DFType of the first level node.
     */
    public String getParentNodeDFType() {
        String DDFName = "";
        try {
            if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.DF_TYPE)) {
                JSONObject dfTypeJObj = dfPropertiesJObj.getJSONObject(ParserConstants.DF_TYPE);
                //    JSONObject dfTypeJObj = getKeyJsonObj(ParserConstants.DF_TYPE, dfPropertiesJObj);
                DDFName = dfTypeJObj.getString(ParserConstants.KEY_DDF_NAME);
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("DDFName=====", ":---" + DDFName);
                }
            }

        } catch (Exception e) {
            DDFName = null;
        }
        return DDFName;

    }

    /**
     * This method will return the MIME of the first level node
     *
     * @return - Return DFType of the first level node.
     */
    public String getParentMIME() {
        String mime = "";

        try {
            if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.DF_TYPE)) {
                JSONObject dfTypeJObj = dfPropertiesJObj.getJSONObject(ParserConstants.DF_TYPE);
                mime = dfTypeJObj.getString(ParserConstants.MIME);

                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("DDFName=====", ":---" + mime);
                }
            }


        } catch (Exception e) {
            mime = null;
        }
        return mime;
    }


    /**
     * This method will return the getParentDDFName of the first level node
     *
     * @return - Return DFType of the first level node.
     */
    public String getParentDDFName() {
        String ddfName = "";

        try {
            if (null != dfPropertiesJObj && dfPropertiesJObj.has(ParserConstants.DF_TYPE)) {
                JSONObject dfTypeJObj = dfPropertiesJObj.getJSONObject(ParserConstants.DF_TYPE);
                ddfName = dfTypeJObj.getString(ParserConstants.KEY_DDF_NAME);

                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("DDFName=====", ":---" + ddfName);
                }
            }


        } catch (Exception e) {
            ddfName = null;
        }
        return ddfName;
    }


    /**
     * This method will return the value of Access Type of the node name provided as parameter.
     *
     * @param nodeName -  Node name whose AccessType is to be determined.
     * @return - List of all the values of AccessType.
     * @throws JSONException the json exception
     */
    public ArrayList<String> getChildNodeAccessType(String nodeName) throws JSONException {
        String accTypVal = "";

        // JSONObject jObj = getChildNodeJObj(nodeName);

        ArrayList<String> childNodeAccTyp = new ArrayList<>();

        childNodesjObj = getChildNodeJObj(nodeName);

        if (childNodesjObj != null) {

            childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);

            if (null != childDFPropJObj && childDFPropJObj.has(ParserConstants.KEY_ACCESS_TYPE)) {
                JSONArray accTyp = childDFPropJObj.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);


                for (int i = 0; i < accTyp.length(); i++) {
                    accTypVal = accTyp.getString(i).toString();
                    childNodeAccTyp.add(accTypVal);
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.i("getChildNodeAccessType:", accTypVal);
                    }
                }
            } else {
                childNodeAccTyp = null;
            }


        } else {

            childNodeAccTyp = null;

        }

        return childNodeAccTyp;

    }

    /**
     *This method will set the Access type of the Node provided as the parameter and save it in file.
     * @param nodeName
     * @param accTypVal
     * @return
     * @throws JSONException
     */
      /*  public boolean updateChildNodeAccessTypeVal(String nodeName, String accTypVal) throws JSONException {

            boolean isUpdated = false;

            // JSONObject tobeUpdatedJObj = getChildNodeJObj(nodeName);

            childNodesjObj = getChildNodeJObj(nodeName);

            if (childNodesjObj != null) {

                childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);

                if (null != childDFPropJObj && childDFPropJObj.has(ParserConstants.KEY_ACCESS_TYPE)) {
                    ArrayList<String> childAccessType = new ArrayList<>();


                    childAccTyp = childDFPropJObj.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);

                    childAccTyp.put(0, accTypVal);

                    childAccessType.add(childAccTyp.get(0).toString());
                    String updatedAccType = childAccTyp.get(0).toString();
                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                    isUpdated = true;

                } else {
                    isUpdated = false;

                }

            } else {

                isUpdated = false;

            }
            return isUpdated;
        }*/


    /**
     * This method will return the DFType of the entered node.
     *
     * @param nodeName - Node name whose DFType is to be determined.
     * @return - List of all values of a DFType
     * @throws JSONException the json exception
     */
    public String getChildNodeDFType(String nodeName) throws JSONException {

        childNodesjObj = getChildNodeJObj(nodeName);

        //ArrayList<String> keyValues = new ArrayList<>();
        String attrVal = "";

        if (childNodesjObj != null) {
            childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);


            if (null != childDFPropJObj && childDFPropJObj.has(ParserConstants.DF_TYPE)) {
                JSONObject dftypeJObj = childDFPropJObj.getJSONObject(ParserConstants.DF_TYPE);


                if (dftypeJObj.has(ParserConstants.MIME)) {
                    String mime = dftypeJObj.getString(ParserConstants.MIME);
                    //keyValues.add(mime);
                    attrVal = mime;
                }
                if (dftypeJObj.has(ParserConstants.KEY_DDF_NAME)) {

                    String ddfName = dftypeJObj.getString(ParserConstants.KEY_DDF_NAME);
                    // keyValues.add(ddfName);
                    attrVal = ddfName;
                }
            } else {

                if (Constants.DEBUGGABLE_MODE) {
                    if (Constants.DEBUGGABLE_MODE) {
                        // Log.i("ddfName=====", keyValues.toString());
                    }
                }
            }

        }
        return attrVal;
    }


    /**
     * This method will set value of DF Type of the node and save it in the file
     *
     * @param nodeName      - node whose DFType is to be updated.
     * @param dfTypeVal     - DFType value
     * @param nodeAttribute the node attribute
     * @return the boolean
     * @throws JSONException the json exception
     */
    public boolean updateChildNodeDFType(String nodeName, String dfTypeVal, String nodeAttribute) throws JSONException {

        childNodesjObj = getChildNodeJObj(nodeName);
        boolean isUpdated = false;

        if (childNodesjObj != null) {

            childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);
            String ddfName = "";

            if (null != childDFPropJObj && childDFPropJObj.has(ParserConstants.DF_TYPE)) {
                childDfTypeJObj = childDFPropJObj.getJSONObject(ParserConstants.DF_TYPE);
                Iterator itr = childDfTypeJObj.keys();
                // String dfName = (String) itr.next();


                if (childDfTypeJObj.has(ParserConstants.MIME)) {

                    if (nodeAttribute.equalsIgnoreCase(ParserConstants.MIME)) {
                        childDfTypeJObj.put(ParserConstants.MIME, dfTypeVal);
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                        isUpdated = true;
                    }


                }
                if (!childDfTypeJObj.has(ParserConstants.MIME) && nodeAttribute.equalsIgnoreCase(ParserConstants.MIME)) {
                    childDfTypeJObj.put(ParserConstants.MIME, dfTypeVal);
                    isUpdated = true;
                }

                if (childDfTypeJObj.has(ParserConstants.KEY_DDF_NAME)) {

                    if (nodeAttribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                        childDfTypeJObj.put(ParserConstants.KEY_DDF_NAME, dfTypeVal);
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                        isUpdated = true;
                    }

                }

                if (!childDfTypeJObj.has(ParserConstants.KEY_DDF_NAME) && nodeAttribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                    childDfTypeJObj.put(ParserConstants.KEY_DDF_NAME, dfTypeVal);
                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                    isUpdated = true;
                }

            } else {
                JSONObject dfType = new JSONObject();
                dfType.put(nodeAttribute, dfTypeVal);
                // dfType.put(ParserConstants.MIME, "urn:oma:mo:oma-dm-lawmo:1.0");
                childDFPropJObj.put(ParserConstants.DF_TYPE, dfType);
                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
            }
        } else {


            isUpdated = false;

        }

        if (Constants.DEBUGGABLE_MODE) {
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("Mgmt tree jobj 1212:-", jsonObject.toString());
            }
        }
        return isUpdated;

    }

    /**
     * This method will return the DF Title of the node
     *
     * @param nodeName - Node whose DFTitle is to be determined.
     * @return - DFTitle
     * @throws JSONException the json exception
     */
    public String getChildNodeDFTitle(String nodeName) throws JSONException {
        String dfTitle = "";
        childNodesjObj = getChildNodeJObj(nodeName);

        if (childNodesjObj != null) {
            childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);

            if (null != childDFPropJObj && childDFPropJObj.has(ParserConstants.DF_TITLE)) {

                dfTitle = childDFPropJObj.getString(ParserConstants.DF_TITLE);

                if (Constants.DEBUGGABLE_MODE) {
                    Log.i(ParserConstants.DF_TITLE, "===" + dfTitle);
                }
            } else {
                dfTitle = null;
            }
        } else {

            dfTitle = null;

        }
        return dfTitle;

    }

    /**
     * This method deletes a particular attribute of az node.
     *
     * @param nodeName      - node whose attribute is to be deleted.
     * @param nodeAttribute - the attribute which is to be rmeoved.
     * @return - boolean value if the node attribute was successfuly deleted.
     * @throws JSONException the json exception
     */
    public boolean deleteNodeAttribute(String nodeName, String nodeAttribute) throws JSONException {

        boolean isUpdated = false;
        childNodesjObj = getChildNodeJObj(nodeName);

        if (childNodesjObj != null) {
            if (childNodesjObj.has(ParserConstants.DF_PROPERTIES)) {
                childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);

                if (childDFPropJObj.has(ParserConstants.KEY_ACCESS_TYPE) && nodeAttribute.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {

                    childDFPropJObj.remove(ParserConstants.KEY_ACCESS_TYPE);
                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);

                }
                if (childDFPropJObj.has(ParserConstants.DF_OCCURRENCE) && nodeAttribute.equalsIgnoreCase(ParserConstants.DF_OCCURRENCE)) {
                    childDFPropJObj.remove(ParserConstants.DF_OCCURRENCE);
                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                }

                if (childDFPropJObj.has(ParserConstants.DF_FORMAT) && nodeAttribute.equalsIgnoreCase(ParserConstants.DF_FORMAT)) {
                    childDFPropJObj.remove(ParserConstants.DF_FORMAT);
                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                }

                if (childDFPropJObj.has(ParserConstants.DF_TITLE) && nodeAttribute.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
                    childDFPropJObj.remove(ParserConstants.DF_TITLE);
                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                }

                if (childDFPropJObj.has(ParserConstants.DF_TYPE) && nodeAttribute.equalsIgnoreCase(ParserConstants.MIME)) {

                    childDfTypeJObj = childDFPropJObj.getJSONObject(ParserConstants.DF_TYPE);
                    if (childDfTypeJObj.has(nodeAttribute)) {
                        childDfTypeJObj.remove(nodeAttribute);
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                    }
                }
                if (childDFPropJObj.has(ParserConstants.DF_TYPE) && nodeAttribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {

                    childDfTypeJObj = childDFPropJObj.getJSONObject(ParserConstants.DF_TYPE);

                    if (childDfTypeJObj.has(nodeAttribute)) {
                        childDfTypeJObj.remove(nodeAttribute);
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                    }
                }

                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("childDFPropJObj:", childDFPropJObj.toString());
                }
                if (!childDFPropJObj.has(ParserConstants.KEY_ACCESS_TYPE) && !childDFPropJObj.has(ParserConstants.DF_FORMAT) && !childDFPropJObj.has(ParserConstants.DF_TITLE) && !childDFPropJObj.has(ParserConstants.DF_OCCURRENCE) && !childDFPropJObj.has(ParserConstants.DF_TYPE)) {

                    childNodesjObj.remove(ParserConstants.DF_PROPERTIES);
                    Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                }
            }


            if (Constants.DEBUGGABLE_MODE) {
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("JsonObject 3: ", jsonObject.toString());
                }
            }
            isUpdated = true;
        } else {

            if (Constants.DEBUGGABLE_MODE) {
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("JsonObject 4: ", jsonObject.toString());
                }
            }
            isUpdated = false;
        }
        return isUpdated;

    }

    /**
     * This method delete a complete node
     *
     * @param nodeName - node which is to be deleted.
     * @return the boolean
     * @throws JSONException the json exception
     */
    public boolean deleteNode(String nodeName) throws JSONException {

        boolean isUpdated = false;
        childNodesjObj = getChildNodeJObj(nodeName);


        if (mNodeJObj.has(nodeName)) {
            mNodeJObj.remove(nodeName);
            isUpdated = true;
            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
        }


        if (mChildNodesArrObj.contains(childNodesjObj)) {

            mChildNodesArrObj.remove(childNodesjObj);
            JSONArray jArrTemp = new JSONArray(mChildNodesArrObj);
            childNodeJArray = null;
            childNodeJArray = jArrTemp;
            mNodeJObj.put(ParserConstants.NODE, childNodeJArray);
            isUpdated = true;
            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
        }
        if (mNewChildNodeArrObj.contains(childNodesjObj)) {
            mNewChildNodeArrObj.remove(childNodesjObj);
            JSONArray jArrTemp = new JSONArray(mNewChildNodeArrObj);
            newNodeJArray = null;
            newNodeJArray = jArrTemp;

            childDFPropJObj.put(ParserConstants.NODE, newNodeJArray);
            isUpdated = true;
            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);

        }

        if (Constants.DEBUGGABLE_MODE) {

            Log.i("Mgmt Tree obj:", jsonObject.toString());
        }
        return isUpdated = true;
    }

    /**
     * This method will set the DFTitle of node and save it in the file.
     *
     * @param nodeName   - node whose DFTitle is to be updated.
     * @param dfTitleVal - value to be set
     * @return - boolean value if the updation was successful.
     * @throws JSONException the json exception
     */
    public boolean updateChildNodeDFTitle(String nodeName, String dfTitleVal) throws JSONException {


        childNodesjObj = getChildNodeJObj(nodeName);
        boolean isUpdated = false;

        if (childNodesjObj != null) {
            childDFPropJObj = childNodesjObj.getJSONObject(ParserConstants.DF_PROPERTIES);

            if (null != childDFPropJObj && childDFPropJObj.has(ParserConstants.DF_TITLE)) {
                childDFPropJObj.put(ParserConstants.DF_TITLE, dfTitleVal);
                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                isUpdated = true;
            } else {

                childDFPropJObj.put(ParserConstants.DF_TITLE, dfTitleVal);


            }
        } else {
            isUpdated = false;

        }

        if (Constants.DEBUGGABLE_MODE) {
            Log.i("Mgmt DFTITLE update", jsonObject.toString());
        }
        return isUpdated;
    }


    /**
     * This method adds a new node.
     *
     * @param nodeName         - name of the node to be added.
     * @param nodeAttribute    - attribute to be set for the newly created node.
     * @param nodeAttributeVal - Value of the attribute.
     * @param parentNodeName   the parent node name
     * @return - boolean value if the add operation was successful.
     * @throws JSONException the json exception
     */
    public boolean addNode(String nodeName, String nodeAttribute, String nodeAttributeVal, String parentNodeName) throws JSONException {
        JSONObject JObj = null;
        JSONObject parentJObj = null;
        boolean isAdded = false;

        if (!nodeName.equalsIgnoreCase("")) {
            JObj = getChildNodeJObj(nodeName);

            if (JObj == null && !parentNodeName.equalsIgnoreCase("")) {

                parentJObj = getChildNodeJObj(parentNodeName);
            }


        }

        if (JObj == null && parentJObj == null) {


            int arrSize = childNodeJArray.length();
            childNodeJArray.put(arrSize, returnNodePropertiesObj(nodeName, nodeAttribute, nodeAttributeVal));

            mChildNodesArrObj = new ArrayList<>();
            for (int i = 0; i < childNodeJArray.length(); i++) {

                mChildNodesArrObj.add(childNodeJArray.getJSONObject(i));


            }


            //   mChildNodesArrObj.add(returnNodePropertiesObj(nodeName, nodeAttribute, nodeAttributeVal));

            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("Mgmt Tree addNode 1:-", jsonObject.toString());
            }
            isAdded = true;

        }

        if (parentJObj != null && mChildNodesArrObj != null && mChildNodesArrObj.contains(parentJObj)) {
            if (parentJObj.getString(ParserConstants.KEY_NODE_NAME).equalsIgnoreCase(ParserConstants.NODE_EXT)) {

                int arrSize = newNodeJArray.length();
                newNodeJArray.put(arrSize, returnNodePropertiesObj(nodeName, nodeAttribute, nodeAttributeVal));


                mNewChildNodeArrObj = new ArrayList<>();
                for (int j = 0; j < newNodeJArray.length(); j++) {
                    mNewChildNodeArrObj.add(newNodeJArray.getJSONObject(j));
                }

                //  mNewChildNodeArrObj.add(returnNodePropertiesObj(nodeName, nodeAttribute, nodeAttributeVal));

                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("Mgmt Tree addNode 2:-", jsonObject.toString());
                }
                isAdded = true;

            }
        }


        return isAdded;

    }

    /**
     * This method returns node object with all the node properties(Attributes) for a freshly created node.
     *
     * @param nodeName          - Name of the node to be created.
     * @param nodeAttrToBeAdded - The attribute whose value is to be set.
     * @param newAttrVal        - The value of the node attribue.
     * @return - Json object of the node to be created.
     * @throws JSONException the json exception
     */
    public JSONObject returnNodePropertiesObj(String nodeName, String nodeAttrToBeAdded, String newAttrVal) throws JSONException {

        JSONObject node = new JSONObject();

        node.put(ParserConstants.KEY_NODE_NAME, nodeName);

        JSONObject DFProperties = new JSONObject();

        if (nodeAttrToBeAdded.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {

            updateNodeAttrVal(DFProperties, nodeAttrToBeAdded, newAttrVal);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("DFProperties1:", DFProperties.toString());
            }
        }
        if (nodeAttrToBeAdded.equalsIgnoreCase(ParserConstants.DF_FORMAT)) {
            updateNodeAttrVal(DFProperties, nodeAttrToBeAdded, newAttrVal);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("DFProperties2:", DFProperties.toString());
            }
        }
        if (nodeAttrToBeAdded.equalsIgnoreCase(ParserConstants.DF_OCCURRENCE)) {
            updateNodeAttrVal(DFProperties, nodeAttrToBeAdded, newAttrVal);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("DFProperties3:", DFProperties.toString());
            }
        }

        if (nodeAttrToBeAdded.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
            updateNodeAttrVal(DFProperties, nodeAttrToBeAdded, newAttrVal);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("DFProperties4:", DFProperties.toString());
            }
        }

        if (nodeAttrToBeAdded.equalsIgnoreCase(ParserConstants.MIME) || nodeAttrToBeAdded.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
            updateNodeAttrVal(DFProperties, nodeAttrToBeAdded, newAttrVal);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("DFProperties5:", DFProperties.toString());
            }
        }
        node.put(ParserConstants.DF_PROPERTIES, DFProperties);

        return node;
    }



     /*<------ This Json object has no data in it ------> */

    /**
     * Gets df format.
     *
     * @param nodeName the node name
     * @throws JSONException the json exception
     */
    public void getDFFormat(String nodeName) throws JSONException {

        String dfFormat = "";

        JSONObject jObj = getChildNodeJObj(nodeName);

        if (jObj != null) {

            JSONObject dfPropJObj = jObj.getJSONObject(ParserConstants.DF_PROPERTIES);
            if (dfPropJObj.has(ParserConstants.DF_FORMAT)) {

                JSONObject dfFormatJObj = dfPropJObj.getJSONObject(ParserConstants.DF_FORMAT);
            } else {
                dfFormat = null;
            }
        } else {

            dfFormat = null;

        }
    }


    /**
     * This method sets the node attribute value for a freshly created node.
     *
     * @param jObj           - node object whose attribute is to be updated.
     * @param updateNodeAttr - node attribue whose value is to be set.
     * @param nodeAttrVal    - node attribute value to be set
     */
    public void updateNodeAttrVal(JSONObject jObj, String updateNodeAttr, String nodeAttrVal) {

        try {
            JSONArray jArr = new JSONArray();
            // jArr.put(0,nodeAttrVal);

            jObj.put(ParserConstants.KEY_ACCESS_TYPE, jArr);

            JSONObject dfFormatJObj = new JSONObject();
            // dfFormatJObj.put(ParserConstants.DF_FORMAT, "");
            jObj.put(ParserConstants.DF_FORMAT, dfFormatJObj);

            JSONObject dfOccurrence = new JSONObject();
            //dfOccurrence.put(ParserConstants.DF_OCCURRENCE, "");
            jObj.put(ParserConstants.DF_OCCURRENCE, dfOccurrence);

            jObj.put(ParserConstants.DF_TITLE, "");

            JSONObject dfType = new JSONObject();
            // dfType.put(updateNodeAttr, "");
            jObj.put(ParserConstants.DF_TYPE, dfType);

            if (updateNodeAttr.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {
                jArr.put(0, nodeAttrVal);
                jObj.put(ParserConstants.KEY_ACCESS_TYPE, jArr);
            }

            if (updateNodeAttr.equalsIgnoreCase(ParserConstants.DF_FORMAT)) {

                dfFormatJObj.put(ParserConstants.DF_FORMAT, nodeAttrVal);
                jObj.put(ParserConstants.DF_FORMAT, dfFormatJObj);
            }
            if (updateNodeAttr.equalsIgnoreCase(ParserConstants.DF_OCCURRENCE)) {
                dfOccurrence.put(ParserConstants.DF_OCCURRENCE, nodeAttrVal);
                jObj.put(ParserConstants.DF_OCCURRENCE, dfOccurrence);
            }
            if (updateNodeAttr.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
                jObj.put(ParserConstants.DF_TITLE, nodeAttrVal);
            }
            if (updateNodeAttr.equalsIgnoreCase(ParserConstants.MIME) || updateNodeAttr.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {


                dfType.put(updateNodeAttr, nodeAttrVal);
                jObj.put(ParserConstants.DF_TYPE, dfType);
            }
        } catch (Exception e) {

            if (Constants.DEBUGGABLE_MODE) {
                Log.i("EXception :-", e.toString());
            }
        }
    }

    /**
     * This method will update the attribute of a node name provied. If the node attribute is not present then it will add the
     * attribute.
     *
     * @param nodeName            - name of the node whose attribute is to be updated.
     * @param nodeAttrToBeUpdated - the attribute whose value is to be updated.
     * @param newAttrVal          - The value to be set.
     * @param parentNodeName      the parent node name
     * @return - boolean value that if the update operation was successfull.
     * @throws JSONException the json exception
     */
    public boolean updateNodeAttribute(String nodeName, String nodeAttrToBeUpdated, String newAttrVal, String parentNodeName) throws JSONException {
        boolean isUpdated = false;

        JSONObject jObj = getChildNodeJObj(nodeName);


        if (jObj == null) {
            isUpdated = addNode(nodeName, nodeAttrToBeUpdated, newAttrVal, parentNodeName);

        } else {

            if (nodeAttrToBeUpdated.equalsIgnoreCase(ParserConstants.DF_TITLE)) {

                isUpdated = updateChildNodeDFTitle(nodeName, newAttrVal);
                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);


            }
            if (nodeAttrToBeUpdated.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME) || nodeAttrToBeUpdated.equalsIgnoreCase(ParserConstants.MIME)) {

                isUpdated = updateChildNodeDFType(nodeName, newAttrVal, nodeAttrToBeUpdated);
                Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);

            }

        }

        Log.i("Mgmt updateNode method:", jsonObject.toString());


        return isUpdated;
    }

    /**
     * Generic method to get the attributes of a node.
     *
     * @param nodeName     - node whose attribute is to be determined.
     * @param nodeProperty - node attribute to be determined.
     * @return the node attribute
     * @throws JSONException the json exception
     */
    public ArrayList<String> getNodeAttribute(String nodeName, String nodeProperty) throws JSONException {

        ArrayList<String> attributeVal = new ArrayList<>();

        if (nodeProperty.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
            attributeVal.add(getChildNodeDFTitle(nodeName));

        }
        if ((nodeProperty.equalsIgnoreCase(ParserConstants.MIME))) {
            attributeVal.add(getChildNodeDFType(nodeName));
        }
        if ((nodeProperty.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME))) {
            attributeVal.add(getChildNodeDFType(nodeName));
        }
        if (nodeProperty.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {
            attributeVal = getChildNodeAccessType(nodeName);
        }

        if (nodeName.equalsIgnoreCase(ParserConstants.PARENT)) {

            if (nodeProperty.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {

                attributeVal = getParentNodeAccessType();
            }

            if (nodeProperty.equalsIgnoreCase(ParserConstants.MIME)) {
                attributeVal.add(getParentMIME());

            }
            if (nodeProperty.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                attributeVal.add(getParentDDFName());
            }
            if (nodeProperty.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
                attributeVal.add(getParentNodeDFTitle());
            }


        }
        return attributeVal;

    }

    /**
     * This method deletes a particular attribute of a first level node.
     *
     * @param attributeName - the attribute which is to be removed.
     * @return - boolean value if the node attribute was successfuly deleted.
     */
    public boolean deleteParentNodeAttribute(String attributeName) {

        boolean isDeleted = false;
        if (mNodeJObj != null) {

            try {

                JSONObject dfProp = mNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);

                if (dfProp != null) {

                    if (dfProp.has(ParserConstants.KEY_ACCESS_TYPE) && attributeName.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {
                        dfProp.remove(ParserConstants.KEY_ACCESS_TYPE);
                        isDeleted = true;
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);

                    }
                    if (dfProp.has(ParserConstants.DF_FORMAT) && attributeName.equalsIgnoreCase(ParserConstants.DF_FORMAT)) {
                        dfProp.remove(ParserConstants.DF_FORMAT);
                        isDeleted = true;
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                    }
                    if (dfProp.has(ParserConstants.DF_OCCURRENCE) && attributeName.equalsIgnoreCase(ParserConstants.DF_OCCURRENCE)) {
                        dfProp.remove(ParserConstants.DF_OCCURRENCE);
                        isDeleted = true;
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                    }
                    if (dfProp.has(ParserConstants.DF_TITLE) && attributeName.equals(ParserConstants.DF_TITLE)) {
                        dfProp.remove(ParserConstants.DF_TITLE);
                        isDeleted = true;
                        Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                    }
                    if (dfProp.has(ParserConstants.DF_TYPE) && attributeName.equals(ParserConstants.MIME)) {
                        JSONObject dftype = dfProp.getJSONObject(ParserConstants.DF_TYPE);
                        if (dftype.has(ParserConstants.MIME)) {
                            dftype.remove(ParserConstants.MIME);
                            deleteEmptyDFType(dfProp, dftype);
                            isDeleted = true;
                            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                        }
                    }
                    if (dfProp.has(ParserConstants.DF_TYPE) && attributeName.equals(ParserConstants.KEY_DDF_NAME)) {
                        JSONObject dftype = dfProp.getJSONObject(ParserConstants.DF_TYPE);
                        if (dftype.has(ParserConstants.KEY_DDF_NAME)) {
                            dftype.remove(ParserConstants.KEY_DDF_NAME);
                            deleteEmptyDFType(dfProp, dftype);
                            isDeleted = true;
                            Utils.writeToFIle(mContext, jsonObject.toString(), ParserConstants.DEVICE_INFO_TEXT_FILE_NAME);
                        }
                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
                isDeleted = false;
            }
        }
        if (Constants.DEBUGGABLE_MODE) {
            Log.i("dlt prnt node attr: ", jsonObject.toString());
        }
        return isDeleted;
    }

    /**
     * This method deletes the empty DFType json object
     *
     * @param dfProp - Json object of DFProperties
     * @param dftype - Json object of DFType
     */
    public void deleteEmptyDFType(JSONObject dfProp, JSONObject dftype) {

        if (!dftype.has(ParserConstants.MIME) && !dftype.has(ParserConstants.KEY_DDF_NAME)) {
            dfProp.remove(ParserConstants.DF_TYPE);

        }


    }


}