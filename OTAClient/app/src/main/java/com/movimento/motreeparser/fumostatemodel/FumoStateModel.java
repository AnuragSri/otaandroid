package com.movimento.motreeparser.fumostatemodel;

/**
 * The type Fumo state model.
 */
public class FumoStateModel {
    private  Meta Meta;

    /**
     * Gets meta.
     *
     * @return the meta
     */
    public Meta getMeta ()
    {
        return Meta;
    }

    /**
     * Sets meta.
     *
     * @param Meta the meta
     */
    public void setMeta (Meta Meta)
    {
        this.Meta = Meta;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Meta = "+Meta+"]";
    }
}
