package com.movimento.motreeparser.fumostatemodel;

/**
 * The type Meta.
 */
public class Meta {
    private State State;

    private StateData StateData;

    /**
     * Gets state.
     *
     * @return the state
     */
    public State getState ()
    {
        return State;
    }

    /**
     * Sets state.
     *
     * @param State the state
     */
    public void setState (State State)
    {
        this.State = State;
    }

    /**
     * Gets state data.
     *
     * @return the state data
     */
    public StateData getStateData ()
    {
        return StateData;
    }

    /**
     * Sets state data.
     *
     * @param StateData the state data
     */
    public void setStateData (StateData StateData)
    {
        this.StateData = StateData;
    }
}
