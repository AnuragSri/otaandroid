package com.movimento.motreeparser.capabilitymodel;

/**
 * The type Capabilities.
 */
public class Capabilities {
    private Supported[] SUPPORTED;

    /**
     * Get supported supported [ ].
     *
     * @return the supported [ ]
     */
    public Supported[] getSupported()
    {
        return SUPPORTED;
    }

    /**
     * Sets supported.
     *
     * @param SUPPORTED the supported
     */
    public void setSupported(Supported[] SUPPORTED)
    {
        this.SUPPORTED = SUPPORTED;
    }
}
