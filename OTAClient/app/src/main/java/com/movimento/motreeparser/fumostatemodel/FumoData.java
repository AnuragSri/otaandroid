package com.movimento.motreeparser.fumostatemodel;

/**
 * The type Fumo data.
 */
public class FumoData {
    private String TotalFirmwareSize;

    private String DownloadedFileSize;

    /**
     * Gets total firmware size.
     *
     * @return the total firmware size
     */
    public String getTotalFirmwareSize ()
    {
        return TotalFirmwareSize;
    }

    /**
     * Sets total firmware size.
     *
     * @param TotalFirmwareSize the total firmware size
     */
    public void setTotalFirmwareSize (String TotalFirmwareSize)
    {
        this.TotalFirmwareSize = TotalFirmwareSize;
    }

    /**
     * Gets downloaded file size.
     *
     * @return the downloaded file size
     */
    public String getDownloadedFileSize ()
    {
        return DownloadedFileSize;
    }

    /**
     * Sets downloaded file size.
     *
     * @param DownloadedFileSize the downloaded file size
     */
    public void setDownloadedFileSize (String DownloadedFileSize)
    {
        this.DownloadedFileSize = DownloadedFileSize;
    }
}
