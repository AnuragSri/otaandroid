package com.movimento.motreeparser;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.google.gson.Gson;
import com.movimento.motreeparser.fumostatemodel.FumoStateModel;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.utils.Constants;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static com.movimento.utils.ParserConstants.FUMO_STATES_MO_TREE;


/*This Class will parse Fumo State Json  using Gson library*/


/**
 * The type Fumo state parser.
 */
public class FumoStateParser extends MTParser {

    private static FumoStateParser mFumoStateParserInstance;
    private Context mContext;
    private String mFileName = FUMO_STATES_MO_TREE;
    /**
     * The Fumo state model.
     */
    FumoStateModel fumoStateModel = null;
    private Gson gson;
    private String json;

    private FumoStateParser() {
        mContext = OTAClientApp.getmAppInstance();
        gson = new Gson();
        json = new String();
        try {
            parseFumoState();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static FumoStateParser getInstance() {
        synchronized (FumoStateParser.class) {
            if (mFumoStateParserInstance == null)
                mFumoStateParserInstance = new FumoStateParser();
        }
        return mFumoStateParserInstance;
    }

    /*This method will parse the Fumo state json in fumoStateModel object
    * It will search the json file on disk if it is not present then
    * it will take the file from asset and save the file to disk and the parse it.
    * */
    private void parseFumoState() throws IOException {

        AssetManager assetManager = mContext.getAssets();
        InputStream ims = null;


        File file = mContext.getFileStreamPath(mFileName);
        if (file.exists()) {
            json = Utils.readFromFile(mContext, mFileName);
        } else {

            ims = assetManager.open(mFileName);
            int size = ims.available();
            byte[] buffer = new byte[size];
            ims.read(buffer);
            ims.close();
            json = new String(buffer, "UTF-8");
        }

        fumoStateModel = gson.fromJson(json, FumoStateModel.class);
    }


    /**
     * This method will update the current retry, current state and max retry values in fumo state parser
     *
     * @param state : CURRENT_RETRY, CURRENT_STATE,MAX_RETRY
     * @param value the value
     * @return boolean
     */
    public boolean updateFumoState(String state, String value) {
        if (state.equalsIgnoreCase(ParserConstants.CURRENT_RETRY)) {
            fumoStateModel.getMeta().getState().setCurrent_retry(value);
            saveJson();
            return true;
        } else if (state.equalsIgnoreCase(ParserConstants.CURRENT_STATE)) {
            fumoStateModel.getMeta().getState().setCurrent_state(value);
            saveJson();
            return true;
        } else if (state.equalsIgnoreCase(ParserConstants.MAX_RETRY)) {
            fumoStateModel.getMeta().getState().setMax_retry(value);
            saveJson();
            return true;
        } else {
            return false;
        }

    }


    /**
     * This method will upadate DownloadFileSize and Totol FirmWareSize
     *
     * @param state : DOWNLOAD_FILE_SIZE,TOTAL_FIRMWARE_SIZE
     * @param value the value
     * @return the boolean
     */
    public boolean updateFumoData(String state, String value) {
        if (state.equalsIgnoreCase(ParserConstants.DOWNLOAD_FILE_SIZE)) {
            fumoStateModel.getMeta().getStateData().getFumoData().setDownloadedFileSize(value);
            saveJson();
            return true;
        } else if (state.equalsIgnoreCase(ParserConstants.TOTAL_FIRMWARE_SIZE)) {
            fumoStateModel.getMeta().getStateData().getFumoData().setTotalFirmwareSize(value);
            saveJson();
            return true;
        }
        return false;
    }


    /**
     * This method will return the value of fumo data: DOWNLOAD_FILE_SIZE and TOTAL_FIRMWARE_SIZE
     *
     * @param state :DOWNLOAD_FILE_SIZE or TOTAL_FIRMWARE_SIZE
     * @return fumo state data
     */
    public String getFumoStateData(String state) {
        String value = null;

        if (state.equalsIgnoreCase(ParserConstants.DOWNLOAD_FILE_SIZE)) {
            value = fumoStateModel.getMeta().getStateData().getFumoData().getDownloadedFileSize();
        } else if (state.equalsIgnoreCase(ParserConstants.TOTAL_FIRMWARE_SIZE)) {
            value = fumoStateModel.getMeta().getStateData().getFumoData().getTotalFirmwareSize();
        }

        return value;
    }

    /**
     * This method will convert to json and Save the updated content in the file.
     */
    private void saveJson() {
        String updatedJson = gson.toJson(fumoStateModel);
        Utils.writeToFIle(mContext, updatedJson, mFileName);

        if (Constants.DEBUGGABLE_MODE) {
            Log.d("Fumo state model", updatedJson);
        }
    }

    /**
     * This method will return the value of current retry, current state and max retry values in fumo state parser.
     *
     * @param state the state
     * @return state
     */
    public String getState(String state) {
        String value = null;

        if (state.equalsIgnoreCase(ParserConstants.CURRENT_RETRY)) {
            value = fumoStateModel.getMeta().getState().getCurrent_retry();
        } else if (state.equalsIgnoreCase(ParserConstants.CURRENT_STATE)) {
            value = fumoStateModel.getMeta().getState().getCurrent_state();
        } else if (state.equalsIgnoreCase(ParserConstants.MAX_RETRY)) {
            value = fumoStateModel.getMeta().getState().getMax_retry();
        }
        return value;

    }
}
