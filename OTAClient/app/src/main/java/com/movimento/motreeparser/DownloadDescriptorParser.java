package com.movimento.motreeparser;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.movimento.http.response.model.downdesc.DownloadDescModel;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static com.movimento.utils.ParserConstants.DOWNLOAD_DESCRIPTION_MO_TREE;

/**
 * This class parse the DownloadDescriptionMOTree json file an
 */
public class DownloadDescriptorParser extends MTParser {

    private static DownloadDescriptorParser mDownloadDescriptorParserInstance;
    private Context mContext;
    private String mFileName = DOWNLOAD_DESCRIPTION_MO_TREE;
    private DownloadDescModel mDownloadDescriptionModel = null;
    private Gson gson;
    private String json;


    private DownloadDescriptorParser() {
        mContext = OTAClientApp.getmAppInstance();
        gson = new Gson();
        json = new String();
        try {
            parseDownloadDescription();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static DownloadDescriptorParser getInstance() {
        mDownloadDescriptorParserInstance = new DownloadDescriptorParser();
        /*synchronized (DownloadDescriptorParser.class) {
            if (mDownloadDescriptorParserInstance == null)

        }*/
        return mDownloadDescriptorParserInstance;
    }

    private void parseDownloadDescription() throws IOException {
        AssetManager assetManager = mContext.getAssets();
        InputStream ims = null;

        //TODO: Need to change the logic to get the json after parser code is merged. Json file will not be saved in asset folder.

        File file = mContext.getFileStreamPath(mFileName);
        if (file.exists()) {
            json = Utils.readFromFile(mContext, mFileName);
        } else {

            ims = assetManager.open(mFileName);
            int size = ims.available();
            byte[] buffer = new byte[size];
            ims.read(buffer);
            ims.close();
            json = new String(buffer, "UTF-8");
        }

        mDownloadDescriptionModel = gson.fromJson(json, DownloadDescModel.class);
    }

    /* public String getVendor()
     {
         return mDownloadDescriptionModel.getMedia().getVendor().getName();
     }
    */


    /**
     * This method will return server Uri
     *
     * @return serverUri download object uri
     */
    public String getDownloadObjectURI() {
        String serverUri = null;
        if (null != mDownloadDescriptionModel) {
            serverUri = mDownloadDescriptionModel.getMedia().getProduct().getMediaObject().getObjectURI().getServer();
        }
        return serverUri;
    }


    /**
     * This method will return the value of key passed.
     * This method is used for all the keys of "media" object
     *
     * @param key the key
     * @return media obj meta
     */
    public String getMediaObjMeta(String key) {
        String value = null;
        if (null != mDownloadDescriptionModel) {
            if (key.equalsIgnoreCase(ParserConstants.KEY_NAME)) {
                value = mDownloadDescriptionModel.getMedia().getProduct().getMediaObject().getMeta().getName();
            } else if (key.equalsIgnoreCase(ParserConstants.KEY_INSTALL_NOTIFY_URI)) {
                value = mDownloadDescriptionModel.getMedia().getProduct().getMediaObject().getMeta().getInstallNotifyURI();
            } else if (key.equalsIgnoreCase(ParserConstants.KEY_SUPPRESS_USER_CONFIRMATION)) {
                value = mDownloadDescriptionModel.getMedia().getProduct().getMediaObject().getMeta().getSuppressUserConfirmation();
            } else if (key.equalsIgnoreCase(ParserConstants.KEY_SIZE)) {
                value = mDownloadDescriptionModel.getMedia().getProduct().getMediaObject().getSize();
            } else if (key.equalsIgnoreCase(ParserConstants.KEY_INSTALL_SIZE)) {
                value = mDownloadDescriptionModel.getMedia().getProduct().getMediaObject().getInstallSize();
            } else if (key.equalsIgnoreCase(ParserConstants.KEY_TYPE)) {
                value = mDownloadDescriptionModel.getMedia().getProduct().getMediaObject().getType();
            } else if (key.equalsIgnoreCase(ParserConstants.KEY_OBJECT_ID)) {
                value = mDownloadDescriptionModel.getMedia().getProduct().getMediaObject().getObjectID();
            } else if (key.equalsIgnoreCase(ParserConstants.KEY_OBJECT_VERSION)) {
                value = mDownloadDescriptionModel.getMedia().getProduct().getMediaObject().getObjectVersion();
            } else if (key.equalsIgnoreCase(ParserConstants.KEY_PROGRESSIVE_DOWNLOADING_FLAG)) {
                value = mDownloadDescriptionModel.getMedia().getProduct().getMediaObject().getProgressiveDownloadFlag();
            } else if (key.equalsIgnoreCase(ParserConstants.KEY_OBJECT_VALIDITY_TIME)) {
                value = mDownloadDescriptionModel.getMedia().getProduct().getMediaObject().getObjectValidityTime();
            } else if (key.equalsIgnoreCase(ParserConstants.KEY_VENDOR)) {
                value = mDownloadDescriptionModel.getMedia().getVendor().getName();
            }
        }
        return value;
    }
}
