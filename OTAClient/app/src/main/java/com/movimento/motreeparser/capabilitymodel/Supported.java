package com.movimento.motreeparser.capabilitymodel;

/**
 * The type Supported.
 */
public class Supported {

    private String CAPABILITY_NAME;

    /**
     * Gets capability name.
     *
     * @return the capability name
     */
    public String getCAPABILITY_NAME ()
    {
        return CAPABILITY_NAME;
    }

    /**
     * Sets capability name.
     *
     * @param CAPABILITY_NAME the capability name
     */
    public void setCAPABILITY_NAME (String CAPABILITY_NAME)
    {
        this.CAPABILITY_NAME = CAPABILITY_NAME;
    }
}
