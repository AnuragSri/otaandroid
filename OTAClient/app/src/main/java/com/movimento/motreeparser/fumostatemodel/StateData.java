package com.movimento.motreeparser.fumostatemodel;


/**
 * The type State data.
 */
public class StateData {
    private FumoData FumoData;

    /**
     * Gets fumo data.
     *
     * @return the fumo data
     */
    public FumoData getFumoData() {
        return FumoData;
    }

    /**
     * Sets fumo data.
     *
     * @param FumoData the fumo data
     */
    public void setFumoData(FumoData FumoData) {
        this.FumoData = FumoData;
    }

}
