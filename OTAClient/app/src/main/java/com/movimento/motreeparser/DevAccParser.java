package com.movimento.motreeparser;

import android.content.Context;
import android.util.Log;

import com.movimento.otaclient.OTAClientApp;
import com.movimento.utils.Constants;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import static com.movimento.utils.ParserConstants.DEV_ACC_MO_TREE;


/**
 * The type Dev acc parser.
 */
public class DevAccParser extends MTParser {
    private static DevAccParser mDevAccParserInstance;
    private JSONObject mngmntTreeJsonObj;
    private JSONObject mngmtTreeChildJsonObj;
    private static JSONArray secondLevelNodeJArray;
    private ArrayList<JSONObject> secondLevelNodeArray;
    private static JSONArray thirdLevelNodeJArray;
    private JSONArray fourthLevelNodeJArray;
    private ArrayList<JSONObject> thirdLevelNodeArray;
    private ArrayList<JSONObject> fourthLevelNodeArray;
    private JSONObject mReturnedJsonObject = null;
    private JSONObject firstLevelNodeJObj;
    private Context mContext;
    private JSONObject mDFPropertiesJsonObject;
    private JSONObject mDFTypeJsonObject;
    /**
     * The M file name.
     */
    String mFileName = DEV_ACC_MO_TREE;

    private DevAccParser() {

        try {
            parseDevAccJson();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static DevAccParser getInstance() {
       /* synchronized (DevAccParser.class) {
            if (mDevAccParserInstance == null)
                mDevAccParserInstance = new DevAccParser();
        }*/
        mDevAccParserInstance = new DevAccParser();
        return mDevAccParserInstance;
    }
    /*
    *This method will traverse the DevAcc json and breaks the complete json in jsonObjects and jsonArrays
    */
    private void parseDevAccJson() throws JSONException {
        String json = null;
        mContext = OTAClientApp.getmAppInstance();
        try {
            File file = mContext.getFileStreamPath(mFileName);
            if (file.exists()) {
                json = Utils.readFromFile(mContext, mFileName);
            } else {

                json = Utils.getJsonString(mFileName, mContext);
            }
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("Json String :--", json);
            }

            mngmntTreeJsonObj = new JSONObject(json);

            if (null != mngmntTreeJsonObj && mngmntTreeJsonObj.has(ParserConstants.MGMNT_TREE)) {

                Iterator iterator = mngmntTreeJsonObj.keys();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.i("key :---", key);
                    }

                    mngmtTreeChildJsonObj = mngmntTreeJsonObj.getJSONObject(key);

                    String VerDTD = mngmtTreeChildJsonObj.getString(ParserConstants.VER_DTD);

                    if (Constants.DEBUGGABLE_MODE) {
                        Log.i("VerDTD string :----", mngmtTreeChildJsonObj.getString(ParserConstants.VER_DTD));
                    }
                    firstLevelNodeJObj = mngmtTreeChildJsonObj.getJSONObject(ParserConstants.NODE);
                    //      if (jobj1.has("DFProperties") && jobj1.has(ParserConstants.NODE)) {

                    if (null != firstLevelNodeJObj && firstLevelNodeJObj.has(ParserConstants.DF_PROPERTIES)) {
                        getDFPropertiesFromJson(firstLevelNodeJObj);
                    } else {
                        if (Constants.DEBUGGABLE_MODE) {
                            Log.i("DF properties", " not found");
                        }
                    }
                    secondLevelNodeArray = new ArrayList<>();
                    secondLevelNodeJArray = firstLevelNodeJObj.getJSONArray(ParserConstants.NODE);
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.d("mNodeJsonArrayLength===", "" + secondLevelNodeJArray.length());
                    }

                    for (int i = 0; i < secondLevelNodeJArray.length(); i++) {
                        secondLevelNodeArray.add(secondLevelNodeJArray.getJSONObject(i));
                    }
                    for (int i = 0; i < secondLevelNodeArray.size(); i++) {
                        String nodeName = secondLevelNodeArray.get(i).getString(ParserConstants.KEY_NODE_NAME);

                        getDFPropertiesFromJson(secondLevelNodeJArray.getJSONObject(i));


                        if (secondLevelNodeJArray.getJSONObject(i).has(ParserConstants.NODE)) {
                            thirdLevelNodeArray = new ArrayList<>();
                            thirdLevelNodeJArray = secondLevelNodeJArray.getJSONObject(i).getJSONArray(ParserConstants.NODE);
                            for (int k = 0; k < thirdLevelNodeJArray.length(); k++) {
                                thirdLevelNodeArray.add(thirdLevelNodeJArray.getJSONObject(k));
                            }
                            for (int j = 0; j < thirdLevelNodeArray.size(); j++) {
                                String childNodeName = thirdLevelNodeArray.get(j).getString(ParserConstants.KEY_NODE_NAME);
                                if (Constants.DEBUGGABLE_MODE) {
                                    Log.i("thirdLevelNodeArray" + j, "Node Name" + childNodeName);
                                }
                                getDFPropertiesFromJson(secondLevelNodeJArray.getJSONObject(j));


                                if (thirdLevelNodeJArray.getJSONObject(j).has(ParserConstants.NODE)) {
                                    fourthLevelNodeArray = new ArrayList<>();
                                    fourthLevelNodeJArray = thirdLevelNodeJArray.getJSONObject(j).getJSONArray(ParserConstants.NODE);

                                    for (int l = 0; l < fourthLevelNodeJArray.length(); l++) {
                                        fourthLevelNodeArray.add(fourthLevelNodeJArray.getJSONObject(l));
                                    }
                                    for (int k = 0; k < fourthLevelNodeArray.size(); k++) {
                                        String newChildNodeName = fourthLevelNodeArray.get(k).getString(ParserConstants.KEY_NODE_NAME);
                                        if (Constants.DEBUGGABLE_MODE) {
                                            Log.i("fourthLevelNodeArray " + i, newChildNodeName);
                                        }
                                        getDFPropertiesFromJson(fourthLevelNodeJArray.getJSONObject(k));


                                    }
                                }
                            }

                        } else {
                            if (Constants.DEBUGGABLE_MODE) {
                                Log.i(" ", "Node not found");
                            }

                        }
                    }
                }
            } else {
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("", "Management tree not found");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    *This method provides all the attributes of "DFProperties" for all nodes
    * @param firstLevelNodeJObj
    */
    private void getDFPropertiesFromJson(JSONObject nodeJsonObject) {

        try {
            mDFPropertiesJsonObject = nodeJsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);

            if (mDFPropertiesJsonObject.has(ParserConstants.KEY_ACCESS_TYPE)) {
                ArrayList<String> accessType = new ArrayList<>();
                JSONArray mAccessTypeJsonArray = null;

                mAccessTypeJsonArray = mDFPropertiesJsonObject.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);

                for (int i = 0; i < mAccessTypeJsonArray.length(); i++) {
                    accessType.add(mAccessTypeJsonArray.get(0).toString());
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.d("accessType :-", accessType.get(0));
                    }
                }
            } else {
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("", "Access Type not found");
                }

            }

            if (mDFPropertiesJsonObject.has(ParserConstants.DF_TITLE)) {
                String DFTitle = mDFPropertiesJsonObject.getString(ParserConstants.DF_TITLE);
                if (Constants.DEBUGGABLE_MODE) {
                    Log.d("DFTitle :-", DFTitle);
                }

            } else {
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("", "Access Type not found");
                }

            }
            if (mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {

                JSONObject mDFTypeJsonObject = mDFPropertiesJsonObject.getJSONObject(ParserConstants.DF_TYPE);
                try {
                    String DDFName = mDFTypeJsonObject.getString(ParserConstants.KEY_DDF_NAME);
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.d("DDFName=====", DDFName);
                    }
                } catch (JSONException e) {
                    String mime = mDFTypeJsonObject.getString(ParserConstants.MIME);
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.d("MIME=====", mime);
                    }
                }
            } else {
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("", "DF Type not found");
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method will update the Dev Acc Ver Id and save the in json file
     *
     * @param newVerID the new ver id
     * @return dev acc ver id
     */
    public boolean setDevAccVerId(String newVerID) {
        try {
            mngmtTreeChildJsonObj.put(ParserConstants.VER_DTD, newVerID);
            Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }


    /**
     * This method returns the Dev Acc Ver Id from the root node
     *
     * @return dev acc ver id
     */
    public String getDevAccVerId() {
        try {
            return mngmtTreeChildJsonObj.get(ParserConstants.VER_DTD).toString();

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * This method will set the Access type of the Node provided as the parameter and save it in file.
     *
     * @param nodeName       the node name
     * @param newAccessValue the new access value
     * @return node access type
     * @throws JSONException the json exception
     */
    public boolean setNodeAccessType(String nodeName, String newAccessValue) throws JSONException {
        JSONObject searchedNodeJsonObject = searchNode(nodeName);
        mDFPropertiesJsonObject = searchedNodeJsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);

        if (null != mDFPropertiesJsonObject && mDFPropertiesJsonObject.has(ParserConstants.KEY_ACCESS_TYPE)) {
            ArrayList<String> accessType = new ArrayList<>();
            JSONArray mAccessTypeJsonArray = null;
            mAccessTypeJsonArray = mDFPropertiesJsonObject.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);
            mAccessTypeJsonArray.put(0, newAccessValue);
            if (Constants.DEBUGGABLE_MODE) {
                Log.e("changed ", "Access type Value" + mngmntTreeJsonObj.toString());
            }
            Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
            return true;
        } else
            return false;
    }


    /**
     * This method will return the value of Access Type of the node name provided as parameter.
     *
     * @param nodeName the node name
     * @return node access type
     * @throws JSONException the json exception
     */
    public ArrayList<String> getNodeAccessType(String nodeName) throws JSONException {
        JSONObject searchedNodeJsonObject = searchNode(nodeName);
        mDFPropertiesJsonObject = searchedNodeJsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);

        if (mDFPropertiesJsonObject.has(ParserConstants.KEY_ACCESS_TYPE)) {
            ArrayList<String> accessType = new ArrayList<>();
            JSONArray mAccessTypeJsonArray = null;
            mAccessTypeJsonArray = mDFPropertiesJsonObject.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);
            for (int i = 0; i < mAccessTypeJsonArray.length(); i++) {
                accessType.add(mAccessTypeJsonArray.get(i).toString());

            }
            return accessType;
        } else {
            return null;
        }
    }


    /**
     * This method returns the Access Type of the top node .
     *
     * @return parent access type
     * @throws JSONException the json exception
     */
    public String getParentAccessType() throws JSONException {
        if (firstLevelNodeJObj.has(ParserConstants.DF_PROPERTIES)) {

            mDFPropertiesJsonObject = firstLevelNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);

            if (mDFPropertiesJsonObject.has(ParserConstants.KEY_ACCESS_TYPE)) {
                ArrayList<String> accessType = new ArrayList<>();
                JSONArray mAccessTypeJsonArray = null;
                if (null != mDFPropertiesJsonObject.getJSONArray(ParserConstants.KEY_ACCESS_TYPE)) {
                    mAccessTypeJsonArray = mDFPropertiesJsonObject.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);

                    for (int i = 0; i < mAccessTypeJsonArray.length(); i++) {
                        accessType.add(mAccessTypeJsonArray.get(i).toString());
                        return accessType.get(i);
                    }
                    return null;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }

    }


    /**
     * This method will set the value of Access type of the parent node and save it in file.
     *
     * @param newAccessTypeValue the new access type value
     * @return parent access type
     * @throws JSONException the json exception
     */
    public boolean setParentAccessType(String newAccessTypeValue) throws JSONException {
        if (firstLevelNodeJObj.has(ParserConstants.DF_PROPERTIES)) {

            mDFPropertiesJsonObject = firstLevelNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);

            if (null != mDFPropertiesJsonObject && mDFPropertiesJsonObject.has(ParserConstants.KEY_ACCESS_TYPE)) {
                ArrayList<String> accessType = new ArrayList<>();
                JSONArray mAccessTypeJsonArray = null;

                mAccessTypeJsonArray = mDFPropertiesJsonObject.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);


                mAccessTypeJsonArray.put(0, newAccessTypeValue);
                Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    /**
     * This method will return the DF title of the top node
     *
     * @return parent df title
     * @throws JSONException the json exception
     */
    public String getParentDFTitle() throws JSONException {
        mDFPropertiesJsonObject = firstLevelNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);

        if (null != mDFPropertiesJsonObject && mDFPropertiesJsonObject.has(ParserConstants.DF_TITLE)) {
            return mDFPropertiesJsonObject.getString(ParserConstants.DF_TITLE);
        } else {
            return null;
        }
    }

    /**
     * This method will set the DF title of the top node and save in the file.
     *
     * @param dfTitle the df title
     * @return parent df title
     * @throws JSONException the json exception
     */
    public boolean setParentDFTitle(String dfTitle) throws JSONException {
        mDFPropertiesJsonObject = firstLevelNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);

        mDFPropertiesJsonObject.put(ParserConstants.DF_TITLE, dfTitle);
        Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
        return true;

    }

    /**
     * THis method will return the DF Type of the top node
     *
     * @return parent df type
     * @throws JSONException the json exception
     */
    public String getParentDFType() throws JSONException {
        mDFPropertiesJsonObject = firstLevelNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);
        if (null != mDFPropertiesJsonObject && mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {

            JSONObject mDFTypeJsonObject = mDFPropertiesJsonObject.getJSONObject(ParserConstants.DF_TYPE);

            return mDFTypeJsonObject.getString(ParserConstants.KEY_DDF_NAME);

        } else {
            return null;
        }
    }

    /**
     * This method will set the value if DF Type of the top node and save it in file
     *
     * @param dfType the df type
     * @param value  the value
     * @return parent df type
     * @throws JSONException the json exception
     */
    public boolean setParentDFType(String dfType, String value) throws JSONException {
        mDFPropertiesJsonObject = firstLevelNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);
        if (!mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {
            JSONObject mDfTypeJObj = new JSONObject();

            mDFPropertiesJsonObject.put(ParserConstants.DF_TYPE, mDfTypeJObj);
        }
        JSONObject mDFTypeJsonObject = mDFPropertiesJsonObject.getJSONObject(ParserConstants.DF_TYPE);
        if (dfType.equalsIgnoreCase(ParserConstants.MIME)) {
            mDFTypeJsonObject.put(ParserConstants.MIME, value);
        } else if (dfType.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
            mDFTypeJsonObject.put(ParserConstants.KEY_DDF_NAME, dfType);

        }
        Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
        return true;


    }


    /**
     * This method will return the DF Title of the node
     *
     * @param nodeName the node name
     * @return node df title
     * @throws JSONException the json exception
     */
    public String getNodeDfTitle(String nodeName) throws JSONException {
        JSONObject searchedNodeJsonObject = searchNode(nodeName);
        mDFPropertiesJsonObject = searchedNodeJsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);
        String s = null;

        if (null != mDFPropertiesJsonObject && mDFPropertiesJsonObject.has(ParserConstants.DF_TITLE)) {
            s = mDFPropertiesJsonObject.getString(ParserConstants.DF_TITLE);
        }
        return s;

    }


    /**
     * This method will set the DFTitle of node and save it in the file.
     *
     * @param nodeName the node name
     * @param dfTitle  the df title
     * @return node df title
     * @throws JSONException the json exception
     */
    public boolean setNodeDFTitle(String nodeName, String dfTitle) throws JSONException {
        JSONObject searchedNodeJsonObject = searchNode(nodeName);
        mDFPropertiesJsonObject = searchedNodeJsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);

        mDFPropertiesJsonObject.put(ParserConstants.DF_TITLE, dfTitle);
        Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
        if (Constants.DEBUGGABLE_MODE) {
            Log.d("After Writing", "Management Tree" + mngmntTreeJsonObj.toString());
        }
        return true;
    }


    /**
     * This method will return the DFType of the node.
     *
     * @param nodeName the node name
     * @return node df type
     * @throws JSONException the json exception
     */
    public ArrayList<String> getNodeDFType(String nodeName) throws JSONException {
        ArrayList<String> dfType = new ArrayList<String>();
        JSONObject searchedNodeJsonObject = searchNode(nodeName);
        mDFPropertiesJsonObject = searchedNodeJsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);
        if (mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {

            mDFTypeJsonObject = mDFPropertiesJsonObject.getJSONObject(ParserConstants.DF_TYPE);

            if (mDFTypeJsonObject.has(ParserConstants.KEY_DDF_NAME)) {
                dfType.add(mDFTypeJsonObject.getString(ParserConstants.KEY_DDF_NAME));
            }
            if (mDFTypeJsonObject.has(ParserConstants.MIME)) {
                dfType.add(mDFTypeJsonObject.getString(ParserConstants.MIME));
            }



           /* try {
                return mDFTypeJsonObject.getString(ParserConstants.MIME);
            } catch (Exception e) {
                return mDFTypeJsonObject.getString(ParserConstants.DDF_NAME);
                // e.printStackTrace();
            }*/


        }
        return dfType;
    }


    /**
     * This method will set value of DF Type of the node and save it in the file
     *
     * @param nodeName       the node name
     * @param attribute      the attribute
     * @param attributeValue the attribute value
     * @return node df type
     * @throws JSONException the json exception
     */
    public boolean setNodeDFType(String nodeName, String attribute, String attributeValue) throws JSONException {
        JSONObject searchedNodeJsonObject = searchNode(nodeName);
        mDFPropertiesJsonObject = searchedNodeJsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);
        JSONObject mDfTypeJObj = new JSONObject();
        if (null != mDFPropertiesJsonObject) {
            if (!mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {

                mDFPropertiesJsonObject.put(ParserConstants.DF_TYPE, mDfTypeJObj);
            }
            JSONObject mDFTypeJsonObject = mDFPropertiesJsonObject.getJSONObject(ParserConstants.DF_TYPE);

            if (attribute.equalsIgnoreCase(ParserConstants.MIME)) {
                if (!mDFTypeJsonObject.has(ParserConstants.MIME)) {
                    mDFTypeJsonObject.put(ParserConstants.MIME, "");
                }
                mDFTypeJsonObject.put(ParserConstants.MIME, attributeValue);

            } else if (attribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                if (!mDFTypeJsonObject.has(ParserConstants.KEY_DDF_NAME)) {
                    mDFTypeJsonObject.put(ParserConstants.KEY_DDF_NAME, "");
                }
                mDFTypeJsonObject.put(ParserConstants.KEY_DDF_NAME, attributeValue);
            }
            Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);

            return true;
        } else {
            return false;
        }

    }

    /**
     * This method will delete the complete node an update the json file
     *
     * @param nodeName the node name
     * @throws JSONException the json exception
     */
    public void deleteNode(String nodeName) throws JSONException {

        JSONObject jsonObject = searchNode(nodeName);

        if (null != secondLevelNodeArray && secondLevelNodeArray.contains(jsonObject)) {
            secondLevelNodeArray.remove(jsonObject);

            secondLevelNodeJArray = null;
            secondLevelNodeJArray = new JSONArray(secondLevelNodeArray);
            firstLevelNodeJObj.put(ParserConstants.NODE, secondLevelNodeJArray);

        } else if (null != thirdLevelNodeArray && thirdLevelNodeArray.contains(jsonObject)) {
            thirdLevelNodeArray.remove(jsonObject);
            thirdLevelNodeJArray = null;
            JSONArray temp = new JSONArray(thirdLevelNodeArray);
            thirdLevelNodeJArray = temp;
            for (int x = 0; x < secondLevelNodeJArray.length(); x++) {

                JSONObject jObj = secondLevelNodeJArray.getJSONObject(x);

                if (jObj.has(ParserConstants.NODE)) {


                    jObj.put(ParserConstants.NODE, thirdLevelNodeJArray);

                }
            }

        } else if (null != fourthLevelNodeArray && fourthLevelNodeArray.contains(jsonObject)) {
            fourthLevelNodeArray.remove(jsonObject);
            fourthLevelNodeJArray = null;
            JSONArray temp = new JSONArray(fourthLevelNodeArray);
            fourthLevelNodeJArray = temp;

            for (int x = 0; x < thirdLevelNodeJArray.length(); x++) {

                JSONObject jObj = thirdLevelNodeJArray.getJSONObject(x);

                if (jObj.has(ParserConstants.NODE)) {

                    jObj.put(ParserConstants.NODE, fourthLevelNodeJArray);
                }
            }
        }
        if (Constants.DEBUGGABLE_MODE) {
            Log.d("mngmntTreeJsonObj :-", mngmntTreeJsonObj.toString());
        }
        Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
    }


    /**
     * This method will delete the attribute of a node and save it file
     *
     * @param nodeName      the node name
     * @param nodeAttribute the node attribute
     * @throws JSONException the json exception
     */
    public void deleteNodeAttribute(String nodeName, String nodeAttribute) throws JSONException {
        JSONObject jsonObject = searchNode(nodeName);
        if (null != jsonObject && jsonObject.has(ParserConstants.DF_PROPERTIES)) {
            mDFPropertiesJsonObject = jsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);

            if (nodeAttribute.equalsIgnoreCase(ParserConstants.DF_TITLE) && mDFPropertiesJsonObject.has(ParserConstants.DF_TITLE)) {
                mDFPropertiesJsonObject.remove(ParserConstants.DF_TITLE);
            } else if (nodeAttribute.equalsIgnoreCase(ParserConstants.DF_TYPE) && mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {
                mDFPropertiesJsonObject.remove(ParserConstants.DF_TYPE);
            } else if (nodeAttribute.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE) && mDFPropertiesJsonObject.has(ParserConstants.KEY_ACCESS_TYPE)) {
                mDFPropertiesJsonObject.remove(ParserConstants.KEY_ACCESS_TYPE);
            }
        }
        if (!mDFPropertiesJsonObject.has(ParserConstants.KEY_ACCESS_TYPE) && !mDFPropertiesJsonObject.has(ParserConstants.DF_TITLE)
                && !mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {

            jsonObject.remove(ParserConstants.DF_PROPERTIES);

        }
        Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
        if (Constants.DEBUGGABLE_MODE) {
            Log.d("Management tree", mngmntTreeJsonObj.toString());
        }

    }


    /**
     * This method will search for the node if it does not exists it will create a new node in parent node array and return true.
     *
     * @param nodeName       the node name
     * @param attribute      :     pass MIME or DDF_NAME as required to add DF_TYPE.
     * @param attributeValue the attribute value
     * @return : Returns true if new node is added.
     * @throws JSONException the json exception
     */
    public boolean addNode(String nodeName, String attribute, String attributeValue) throws JSONException {

        if (null == searchNode(nodeName)) {
            JSONObject newNode = new JSONObject();

            newNode.put(ParserConstants.KEY_NODE_NAME, nodeName);


            JSONObject DFProperties = new JSONObject();

            JSONArray mAccessTypeJArray = new JSONArray();


            JSONObject mDfTypeJObj = new JSONObject();


            DFProperties.put(ParserConstants.KEY_ACCESS_TYPE, mAccessTypeJArray);
            DFProperties.put(ParserConstants.DF_TITLE, "");
            DFProperties.put(ParserConstants.DF_TYPE, mDfTypeJObj);

            if (attribute.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {
                mAccessTypeJArray.put(0, attributeValue);
                DFProperties.put(ParserConstants.KEY_ACCESS_TYPE, mAccessTypeJArray);

            } else if (attribute.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
                DFProperties.put(ParserConstants.DF_TITLE, attributeValue);

            } else if (attribute.equalsIgnoreCase(ParserConstants.MIME)) {
                mDfTypeJObj.put(ParserConstants.MIME, attributeValue);
                DFProperties.put(ParserConstants.DF_TYPE, mDfTypeJObj);

            } else if (attribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                mDfTypeJObj.put(ParserConstants.KEY_DDF_NAME, attributeValue);
                DFProperties.put(ParserConstants.DF_TYPE, mDfTypeJObj);

            }


            newNode.put(ParserConstants.DF_PROPERTIES, DFProperties);

            // secondLevelNodeJArray.put(nodeName, newNode);
            secondLevelNodeJArray.put(secondLevelNodeJArray.length(), newNode);
            Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("Mgmt Tree addNode:-", mngmntTreeJsonObj.toString());
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * This method will search the node with the node name provided in the complete tree and return the json object of the same.
     *
     * @param nodeName
     * @throws JSONException
     * @return: searched node json Object
     */
    private JSONObject searchNode(String nodeName) throws JSONException {

        if (null != firstLevelNodeJObj && firstLevelNodeJObj.has(nodeName)) {

            return firstLevelNodeJObj;
        }
        if (null != secondLevelNodeJArray) {
            for (int i = 0; i < secondLevelNodeJArray.length(); i++) {

                if (secondLevelNodeJArray.getJSONObject(i).getString(ParserConstants.KEY_NODE_NAME).equalsIgnoreCase(nodeName)) {
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.d("Node Found", nodeName);
                    }
                    mReturnedJsonObject = secondLevelNodeJArray.getJSONObject(i);
                    return mReturnedJsonObject;
                }
            }
        }
        if (null != thirdLevelNodeJArray) {
            for (int j = 0; j < thirdLevelNodeJArray.length(); j++) {

                if (thirdLevelNodeJArray.getJSONObject(j).getString(ParserConstants.KEY_NODE_NAME).equalsIgnoreCase(nodeName)) {
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.d("Node Found", "In second Array" + nodeName);
                    }
                    mReturnedJsonObject = thirdLevelNodeJArray.getJSONObject(j);
                    return mReturnedJsonObject;

                }
            }
        }
        if (null != fourthLevelNodeJArray) {
            for (int k = 0; k < fourthLevelNodeJArray.length(); k++) {
                if (fourthLevelNodeJArray.getJSONObject(k).getString(ParserConstants.KEY_NODE_NAME).equalsIgnoreCase(nodeName)) {
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.d("Node Found", "In third Array" + nodeName);
                    }
                    mReturnedJsonObject = fourthLevelNodeJArray.getJSONObject(k);
                    return mReturnedJsonObject;
                }
            }
        }
        return mReturnedJsonObject;
    }

    /**
     * This method will update the Value of Node attribute.
     *
     * @param nodeName          the node name
     * @param attribute         the attribute
     * @param attributeNewValue the attribute new value
     * @return : returns true if node is successfully updated.
     * @throws JSONException the json exception
     */
    public boolean updateNodeAttribute(String nodeName, String attribute, String attributeNewValue) throws JSONException {
        boolean isUpdated = addNode(nodeName, attribute, attributeNewValue);

        if (!isUpdated) {
            if (attribute.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
                isUpdated = setNodeDFTitle(nodeName, attributeNewValue);
            } else if (attribute.equalsIgnoreCase(ParserConstants.MIME) || attribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                //"attribute" should be MIME or DDF_Name
                isUpdated = setNodeDFType(nodeName, attribute, attributeNewValue);

            }
        }
        if (Constants.DEBUGGABLE_MODE) {
            Log.d("Management tree", mngmntTreeJsonObj.toString());
        }
        Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
        return isUpdated;
    }

    /**
     * This method will return the Value of Node Attribute.
     *
     * @param nodeName  the node name
     * @param attribute the attribute
     * @return : value of the attribute of node.
     * @throws JSONException the json exception
     */
    public ArrayList<String> getNodeAttribute(String nodeName, String attribute) throws JSONException {
        ArrayList<String> attributeValue = new ArrayList<String>();
        if (attribute.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {
            attributeValue = getNodeAccessType(nodeName);
        } else if (attribute.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
            attributeValue.add(getNodeDfTitle(nodeName));
        } else if (attribute.equalsIgnoreCase(ParserConstants.DF_TYPE)) {
            attributeValue = getNodeDFType(nodeName);
        }
        return attributeValue;
    }
}

