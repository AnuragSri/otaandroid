package com.movimento.motreeparser;


import com.movimento.utils.Constants;


/**
 * The type Mt parser factory.
 */
public class MTParserFactory {
    private static MTParserFactory mtParserFactoryInstance;

    private MTParserFactory() {
    }

    /**
     * Gets instance.
     *
     * @return the mtParserFactoryInstance
     */
    public static MTParserFactory getInstance() {
        synchronized (MTParserFactory.class) {
            if (mtParserFactoryInstance == null)
                mtParserFactoryInstance = new MTParserFactory();
        }
        return mtParserFactoryInstance;
    }

    /**
     * Gets mt parser.
     *
     * @param type the type
     * @return mt parser
     */
    public MTParser getMTParser(int type) {
        MTParser mtParser = null;
        switch (type) {
            case Constants.MT_PARSER_DEVICE_INFO:
                mtParser = DeviceInfoParser.getInstance();
                break;

            case Constants.MT_PARSER_DEVICE_ACCOUNT:
                mtParser = DevAccParser.getInstance();
                break;

            case Constants.MT_PARSER_LAWMO:
                mtParser = LawmoParser.getInstance();
                break;

            case Constants.MT_PARSER_FUMO:
                mtParser = FumoParser.getInstance();
                break;

            case Constants.MT_PARSER_CAPABILITY:
                mtParser = MOCapabilitiesParser.getInstance();
                break;

            case Constants.MT_PARSER_FUMO_STATE:
                mtParser = FumoStateParser.getInstance();
                break;

            case Constants.MT_PARSER_DOWNLOAD_DESCRIPTOR:
                mtParser = DownloadDescriptorParser.getInstance();
                break;


            default:
                break;
        }
        return mtParser;
    }

}
