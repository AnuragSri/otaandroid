package com.movimento.motreeparser;

import android.content.Context;
import android.util.Log;

import com.movimento.otaclient.OTAClientApp;
import com.movimento.utils.Constants;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import static com.movimento.utils.ParserConstants.FUMO_MO_TREE;

/**
 * The type Fumo parser.
 */
/*
* This class will parse "Fumo json" file and provide getter and setter methods for all the nodes
* */
public class FumoParser extends MTParser {
    private static FumoParser mFumoParserInstance;
    private final String TAG = FumoParser.class.getCanonicalName();
    private Context mContext;
    private JSONObject firstLevelNodeJObj;
    private JSONArray secondLevelNodeJArray;
    private JSONObject mngmntTreeJsonObj;
    private ArrayList<JSONObject> seondLevelNodeArray;
    private JSONObject mngmtTreeChildJsonObj;
    private JSONObject mReturnedJsonObject;
    private JSONObject thirdLevelNodeJobject;
    private String mFileName = FUMO_MO_TREE;
    private JSONObject mDFPropertiesJsonObject;
    private JSONObject mDFFormatJsonObject;
    private JSONObject mOccurrenceJsonObject;
    private JSONObject mDFTypeJsonObject;

    private FumoParser() {

        try {
            parseDevAccJson();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static FumoParser getInstance() {
        synchronized (FumoParser.class) {
            if (mFumoParserInstance == null)
                mFumoParserInstance = new FumoParser();
        }
        return mFumoParserInstance;
    }
    /**
     * This method will parse the json file and initialise the json Objects .
     *
     * @throws JSONException
     */
    private void parseDevAccJson() throws JSONException {
        String json = null;
        mContext = OTAClientApp.getmAppInstance();
        try {


            File file = mContext.getFileStreamPath(mFileName);
            if (file.exists()) {

                json = Utils.readFromFile(mContext, mFileName);
            } else {

                json = Utils.getJsonString(mFileName, mContext);
            }
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("Json String :--", json);
            }

            mngmntTreeJsonObj = new JSONObject(json);

            if (mngmntTreeJsonObj.has(ParserConstants.MGMNT_TREE)) {

                Iterator iterator = mngmntTreeJsonObj.keys();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.i("key :---", key);
                    }

                    mngmtTreeChildJsonObj = mngmntTreeJsonObj.getJSONObject(key);

                    String VerDTD = mngmtTreeChildJsonObj.getString(ParserConstants.VER_DTD);
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.i("VerDTD string :----", mngmtTreeChildJsonObj.getString(ParserConstants.VER_DTD));
                    }
                    firstLevelNodeJObj = mngmtTreeChildJsonObj.getJSONObject(ParserConstants.NODE);
                    if (firstLevelNodeJObj.has(ParserConstants.DF_PROPERTIES)) {
                        getDFPropertiesFromJson(firstLevelNodeJObj);
                    } else {
                        if (Constants.DEBUGGABLE_MODE) {
                            Log.i("DF properties", " not found");
                        }
                    }

                    seondLevelNodeArray = new ArrayList<>();
                    secondLevelNodeJArray = firstLevelNodeJObj.getJSONArray(ParserConstants.NODE);
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.i("mNodeJsonArrayLength===", "" + secondLevelNodeJArray.length());
                    }

                    for (int i = 0; i < secondLevelNodeJArray.length(); i++) {
                        seondLevelNodeArray.add(secondLevelNodeJArray.getJSONObject(i));
                    }

                    for (int i = 0; i < seondLevelNodeArray.size(); i++) {
                        String nodeName = seondLevelNodeArray.get(i).getString(ParserConstants.KEY_NODE_NAME);
                        if (Constants.DEBUGGABLE_MODE) {
                            Log.i(" Node name" + i, nodeName);
                        }
                        getDFPropertiesFromJson(secondLevelNodeJArray.getJSONObject(i));
                        if (secondLevelNodeJArray.getJSONObject(i).has(ParserConstants.NODE)) {

                            thirdLevelNodeJobject = secondLevelNodeJArray.getJSONObject(i).getJSONObject(ParserConstants.NODE);
                            String childNodeName = thirdLevelNodeJobject.getString(ParserConstants.KEY_NODE_NAME);
                            if (Constants.DEBUGGABLE_MODE) {
                                Log.i("Child Node:- ", "Node Name" + childNodeName);
                            }
                            getDFPropertiesFromJson(thirdLevelNodeJobject);
                        }
                    }
                }

            } else {
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("", "Management tree not found");
                }

            }
        } catch (Exception e) {

        }
    }

    /*
       *This method provides all the attributes of "DFProperties" for all nodes
       */
    private void getDFPropertiesFromJson(JSONObject nodeJsonObject) {

        try {
            mDFPropertiesJsonObject = nodeJsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);

            if (null != mDFPropertiesJsonObject && mDFPropertiesJsonObject.has(ParserConstants.KEY_ACCESS_TYPE)) {
                ArrayList<String> accessType = new ArrayList<>();
                JSONArray mAccessTypeJsonArray = null;

                mAccessTypeJsonArray = mDFPropertiesJsonObject.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);

                for (int i = 0; i < mAccessTypeJsonArray.length(); i++) {
                    accessType.add(mAccessTypeJsonArray.get(i).toString());
                    if (Constants.DEBUGGABLE_MODE) {
                        Log.d("AccessType :-", accessType.get(i));
                    }
                }

            } else {
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("", "Access Type not found");
                }

            }

            if (mDFPropertiesJsonObject.has(ParserConstants.DF_FORMAT)) {


                mDFFormatJsonObject = mDFPropertiesJsonObject.getJSONObject(ParserConstants.DF_FORMAT);
            } else {
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("", "DF Format not found");
                }

            }

            if (mDFPropertiesJsonObject.has(ParserConstants.DF_OCCURRENCE)) {

                mOccurrenceJsonObject = mDFPropertiesJsonObject.getJSONObject(ParserConstants.DF_OCCURRENCE);
            } else {
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("", "DF Occurrence not found");
                }

            }

            if (mDFPropertiesJsonObject.has(ParserConstants.DF_TITLE)) {
                String DFTitle = mDFPropertiesJsonObject.getString(ParserConstants.DF_TITLE);
                if (Constants.DEBUGGABLE_MODE) {
                    Log.d("DFTitle :-", DFTitle);
                }

            } else {
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("", "Access Type not found");
                }

            }
            if (mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {

                mDFTypeJsonObject = mDFPropertiesJsonObject.getJSONObject(ParserConstants.DF_TYPE);
                try {
                    if (mDFTypeJsonObject.has(ParserConstants.KEY_DDF_NAME)) {
                        String DDFName = mDFTypeJsonObject.getString(ParserConstants.KEY_DDF_NAME);
                        if (Constants.DEBUGGABLE_MODE) {
                            Log.d("DDFName=====", DDFName);
                        }
                    }
                } catch (JSONException e) {
                    if (mDFTypeJsonObject.has(ParserConstants.MIME)) {
                        String mime = mDFTypeJsonObject.getString(ParserConstants.MIME);
                        if (Constants.DEBUGGABLE_MODE) {
                            Log.d("MIME=====", mime);
                        }
                    }
                }
            } else {
                if (Constants.DEBUGGABLE_MODE) {
                    Log.i("", "DF Type not found");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * This method returns the Dev Acc Ver Id from the root node
     *
     * @return dev acc ver id
     */
    public String getDevAccVerId() {
        try {
            if (Constants.DEBUGGABLE_MODE) {
                Log.d("Updated Json", Utils.readFromFile(mContext, mFileName));
            }
            return mngmtTreeChildJsonObj.get(ParserConstants.VER_DTD).toString();


        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * This method will update the Dev Acc Ver Id and save the in json file
     *
     * @param newVerID the new ver id
     * @return dev acc ver id
     */
    public boolean setDevAccVerId(String newVerID) {
        try {
            mngmtTreeChildJsonObj.put(ParserConstants.VER_DTD, newVerID);
            if (Constants.DEBUGGABLE_MODE) {
                Log.d("VerDTD string :----", mngmtTreeChildJsonObj.getString(ParserConstants.VER_DTD));
            }
            Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }


    /**
     * This method returns the Access Type of the top node .
     *
     * @return parent access type
     * @throws JSONException the json exception
     */
    public ArrayList<String> getParentAccessType() throws JSONException {
        if (firstLevelNodeJObj.has(ParserConstants.DF_PROPERTIES)) {

            mDFPropertiesJsonObject = firstLevelNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);

            if (mDFPropertiesJsonObject.has(ParserConstants.KEY_ACCESS_TYPE)) {
                ArrayList<String> accessType = new ArrayList<>();
                JSONArray mAccessTypeJsonArray = null;

                mAccessTypeJsonArray = mDFPropertiesJsonObject.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);
                for (int i = 0; i < mAccessTypeJsonArray.length(); i++) {
                    accessType.add(mAccessTypeJsonArray.get(i).toString());
                    return accessType;
                }
                return null;

            } else {
                return null;
            }
        } else {
            return null;
        }
    }


    /**
     * This method will set the value of Access type of the parent node and save it in file.
     *
     * @param newAccessTypeValue the new access type value
     * @return parent access type
     * @throws JSONException the json exception
     */
    public boolean setParentAccessType(String newAccessTypeValue) throws JSONException {
        if (firstLevelNodeJObj.has(ParserConstants.DF_PROPERTIES)) {

            mDFPropertiesJsonObject = firstLevelNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);

            if (mDFPropertiesJsonObject.has(ParserConstants.KEY_ACCESS_TYPE)) {
                ArrayList<String> accessType = new ArrayList<>();
                JSONArray mAccessTypeJsonArray = null;

                mAccessTypeJsonArray = mDFPropertiesJsonObject.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);

                mAccessTypeJsonArray.put(0, newAccessTypeValue);
                Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    /**
     * This method will return the DFF Format of the top node
     */
    public void getParentDFFormat() {
        //TODO: As of now DFFormat tag is null. So leaving this method blank.
    }


    /**
     * This method will set the DFF Format of the top node and save it the File
     */
    public void setParentDFFormat() {
        //TODO: As of now DFFormat tag is null. So leaving this method blank.
    }

    /**
     * This method will return the Occurrence tag of the top node
     */
    public void getParentOccurrence() {
        //TODO: As of now ParentOccurrence tag is null, So leaving this method blank
    }


    /**
     * This method will set the value if Occurrence Tag of the top node and save it in the file
     */
    public void setParentOccurrence() {
        //TODO: As of now ParentOccurrence tag is null, So leaving this method blank
    }


    /**
     * This method will return the DF title of the top node
     *
     * @return parent df title
     * @throws JSONException the json exception
     */
    public String getParentDFTitle() throws JSONException {
        mDFPropertiesJsonObject = firstLevelNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);
        if (mDFPropertiesJsonObject.has(ParserConstants.DF_TITLE)) {
            return mDFPropertiesJsonObject.getString(ParserConstants.DF_TITLE);
        } else {
            return null;
        }
    }


    /**
     * This method will set the DF title of the top node and save in the file.
     *
     * @param dfTitle the df title
     * @return parent df title
     * @throws JSONException the json exception
     */
    public boolean setParentDFTitle(String dfTitle) throws JSONException {
        mDFPropertiesJsonObject = firstLevelNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);

        mDFPropertiesJsonObject.put(ParserConstants.DF_TITLE, dfTitle);
        if (Constants.DEBUGGABLE_MODE) {
            Log.d("Parent DfTitle", mngmntTreeJsonObj.toString());
        }
        Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
        return true;

    }


    /**
     * THis method will return the DF Type of the top node
     *
     * @return parent df type
     * @throws JSONException the json exception
     */
    public ArrayList<String> getParentDFType() throws JSONException {
        ArrayList<String> parentDFType = new ArrayList<String>();
        mDFPropertiesJsonObject = firstLevelNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);
        if (mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {

            mDFTypeJsonObject = mDFPropertiesJsonObject.getJSONObject(ParserConstants.DF_TYPE);


            if (mDFTypeJsonObject.has(ParserConstants.KEY_DDF_NAME)) {
                parentDFType.add(mDFTypeJsonObject.getString(ParserConstants.KEY_DDF_NAME));
            }
            if (mDFTypeJsonObject.has(ParserConstants.MIME)) {
                parentDFType.add(mDFTypeJsonObject.getString(ParserConstants.MIME));
            }
            return parentDFType;


        } else {
            return null;
        }
    }

    /**
     * This method will set the value if DF Type of the top node and save it in file
     *
     * @param dfType the df type
     * @param value  the value
     * @return parent df type
     * @throws JSONException the json exception
     */
    public boolean setParentDFType(String dfType, String value) throws JSONException {
        mDFPropertiesJsonObject = firstLevelNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);
        if (!mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {
            JSONObject mDfTypeJObj = new JSONObject();

            mDFPropertiesJsonObject.put(ParserConstants.DF_TYPE, mDfTypeJObj);
        }
        mDFTypeJsonObject = mDFPropertiesJsonObject.getJSONObject(ParserConstants.DF_TYPE);
        if (dfType.equalsIgnoreCase(ParserConstants.MIME)) {
            mDFTypeJsonObject.put(ParserConstants.MIME, value);
        } else if (dfType.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
            //mDFTypeJsonObject.put(ParserConstants.KEY_DDF_NAME, dfType);
            mDFTypeJsonObject.put(ParserConstants.KEY_DDF_NAME, value);

        }
        Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
        return true;


    }

    /**
     * This method will set the Access type of the Node provided as the parameter and save it in file.
     *
     * @param nodeName       the node name
     * @param newAccessValue the new access value
     * @param nodeParent     :    name of parent of nodeName, it is required to handle the case where there are more than one node with same node name(Download and DownloadAndUpdate)                       In case of "PkgURl" node pass this parameter as "Download" or "DownloadAndUpdate" else we can pass it null.
     * @return node access type
     * @throws JSONException the json exception
     */
    public boolean setNodeAccessType(String nodeName, String newAccessValue, String nodeParent) throws JSONException {
        JSONObject searchedNodeJsonObject = searchNode(nodeName, nodeParent);
        mDFPropertiesJsonObject = searchedNodeJsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);

        if (mDFPropertiesJsonObject.has(ParserConstants.KEY_ACCESS_TYPE)) {
            JSONArray mAccessTypeJsonArray = null;
            mAccessTypeJsonArray = mDFPropertiesJsonObject.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);
            mAccessTypeJsonArray.put(0, newAccessValue);
            if (Constants.DEBUGGABLE_MODE) {
                Log.d("changed ", "Access type Value" + mAccessTypeJsonArray.get(0).toString());
            }
            Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);

            return true;
        } else
            return false;
    }

    /**
     * This method will return the value of Access Type of the node name provided as parameter.
     *
     * @param nodeName   the node name
     * @param nodeParent : name of parent of nodeName, it is required to handle the case where there are more than one node with same node name(Download and DownloadAndUpdate)                   In case of "PkgURl" node pass this parameter as "Download" or "DownloadAndUpdate" else we can pass it null.
     * @return node access type
     * @throws JSONException the json exception
     */
    public ArrayList<String> getNodeAccessType(String nodeName, String nodeParent) throws JSONException {
        JSONObject searchedNodeJsonObject = searchNode(nodeName, nodeParent);
        mDFPropertiesJsonObject = searchedNodeJsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);

        if (null != mDFPropertiesJsonObject && mDFPropertiesJsonObject.has(ParserConstants.KEY_ACCESS_TYPE)) {
            JSONArray mAccessTypeJsonArray = null;
            mAccessTypeJsonArray = mDFPropertiesJsonObject.getJSONArray(ParserConstants.KEY_ACCESS_TYPE);
            ArrayList<String> accessType = new ArrayList<>();
            for (int i = 0; i < mAccessTypeJsonArray.length(); i++) {
                accessType.add(mAccessTypeJsonArray.get(i).toString());

            }
            return accessType;
        }
        return null;
    }


    /**
     * This method will return the DF Format of the Node
     */
    public void getNodeDFFormat() {
        //TODO: As of now DFFormat tag is null. So leaving this method blank
    }

    /**
     * This method will update the DF Format of the node and save it in file.
     */
    public void setNodeDFFormat() {
        //TODO: As of now DFFormat tag is null. So leaving this method blank
    }


    /**
     * This method will return the Occurrence of the Node
     */
    public void getNodeOccurrence() {
        //TODO: As of now ParentOccurrence tag is null, So leaving this method blank
    }

    /**
     * Sets node occurrence.
     */
    public void setNodeOccurrence() {
        //TODO: As of now ParentOccurrence tag is null, So leaving this method blank
    }

    /**
     * This method will return the DF Title of the node
     *
     * @param nodeName   the node name
     * @param nodeParent : name of parent of nodeName, it is required to handle the case where there are more than one node with same node name(Download and DownloadAndUpdate)                   In case of "PkgURl" node pass this parameter as "Download" or "DownloadAndUpdate" else we can pass it null.
     * @return node df title
     * @throws JSONException the json exception
     */
    public String getNodeDfTitle(String nodeName, String nodeParent) throws JSONException {
        JSONObject searchedNodeJsonObject = searchNode(nodeName, nodeParent);
        mDFPropertiesJsonObject = searchedNodeJsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);
        if (mDFPropertiesJsonObject.has(ParserConstants.DF_TITLE)) {
            return mDFPropertiesJsonObject.getString(ParserConstants.DF_TITLE);
        }
        return null;
    }


    /**
     * This method will set the DFTitle of node and save it in the file.
     *
     * @param nodeName   the node name
     * @param dfTitle    the df title
     * @param nodeParent : name of parent of nodeName, it is required to handle the case where there are more than one node with same node name(Download and DownloadAndUpdate)                    In case of "PkgURl" node pass this parameter as "Download" or "DownloadAndUpdate" else we can pass it null.
     * @return node df title
     * @throws JSONException the json exception
     */
    public boolean setNodeDFTitle(String nodeName, String dfTitle, String nodeParent) throws JSONException {
        JSONObject searchedNodeJsonObject = searchNode(nodeName, nodeParent);
        mDFPropertiesJsonObject = searchedNodeJsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);

        mDFPropertiesJsonObject.put(ParserConstants.DF_TITLE, dfTitle);
        Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
        if (Constants.DEBUGGABLE_MODE) {
            Log.d("After Writing", "Mngmnt Tree" + mngmntTreeJsonObj.toString());
        }
        return true;


    }


    /**
     * This method will return the DFType of the node.
     *
     * @param nodeName   the node name
     * @param nodeParent : name of parent of nodeName, it is required to handle the case where there are more than one node with same node name(Download and DownloadAndUpdate)                    In case of "PkgURl" node pass this parameter as "Download" or "DownloadAndUpdate" else we can pass it null.
     * @return node df type
     * @throws JSONException the json exception
     */
    public ArrayList<String> getNodeDFType(String nodeName, String nodeParent) throws JSONException {
        ArrayList<String> dfType = new ArrayList<String>();
        JSONObject searchedNodeJsonObject = searchNode(nodeName, nodeParent);
        mDFPropertiesJsonObject = searchedNodeJsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);
        if (mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {

            mDFTypeJsonObject = mDFPropertiesJsonObject.getJSONObject(ParserConstants.DF_TYPE);

            if (mDFTypeJsonObject.has(ParserConstants.KEY_DDF_NAME)) {
                dfType.add(mDFTypeJsonObject.getString(ParserConstants.KEY_DDF_NAME));
            }
            if (mDFTypeJsonObject.has(ParserConstants.MIME)) {
                dfType.add(mDFTypeJsonObject.getString(ParserConstants.MIME));
            }



           /* try {
                return mDFTypeJsonObject.getString(ParserConstants.MIME);
            } catch (Exception e) {
                return mDFTypeJsonObject.getString(ParserConstants.DDF_NAME);
                // e.printStackTrace();
            }*/


        }
        return dfType;
    }


    /**
     * This method will set value of DF Type of the node and save it in the file
     *
     * @param nodeName       the node name
     * @param attributeValue the attribute value
     * @param nodeParent     :    name of parent of nodeName, it is required to handle the case where there are more than one node with same node name(Download and DownloadAndUpdate)                       In case of "PkgURl" node pass this parameter as "Download" or "DownloadAndUpdate" else we can pass it null.
     * @param attribute      the attribute
     * @return node df type
     * @throws JSONException the json exception
     */
    public boolean setNodeDFType(String nodeName, String attributeValue, String nodeParent, String attribute) throws JSONException {
        JSONObject searchedNodeJsonObject = searchNode(nodeName, nodeParent);
        mDFPropertiesJsonObject = searchedNodeJsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);
        JSONObject mDfTypeJObj = new JSONObject();
        if (null != mDFPropertiesJsonObject) {
            if (!mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {

                mDFPropertiesJsonObject.put(ParserConstants.DF_TYPE, mDfTypeJObj);
            }
            JSONObject mDFTypeJsonObject = mDFPropertiesJsonObject.getJSONObject(ParserConstants.DF_TYPE);

            if (attribute.equalsIgnoreCase(ParserConstants.MIME)) {
                if (!mDFTypeJsonObject.has(ParserConstants.MIME)) {
                    mDFTypeJsonObject.put(ParserConstants.MIME, "");

                }
                mDFTypeJsonObject.put(ParserConstants.MIME, attributeValue);

            } else if (attribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                if (!mDFTypeJsonObject.has(ParserConstants.KEY_DDF_NAME)) {
                    mDFTypeJsonObject.put(ParserConstants.KEY_DDF_NAME, "");
                }
                mDFTypeJsonObject.put(ParserConstants.KEY_DDF_NAME, attributeValue);
            }
            if (Constants.DEBUGGABLE_MODE) {
                Log.d("After Writing", "Mngmnt Tree" + mngmntTreeJsonObj.toString());
            }
            Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);

            return true;
        } else {
            return false;
        }
    }


    /**
     * This method will delete the Complete node ans write the
     *
     * @param nodeName   the node name
     * @param nodeParent : name of parent of nodeName, it is required to handle the case where there are more than one node with same node name(Download and DownloadAndUpdate)                   In case of "PkgURl" node pass this parameter as "Download" or "DownloadAndUpdate" else we can pass it null.
     * @throws JSONException the json exception
     */
    public void deleteNode(String nodeName, String nodeParent) throws JSONException {

        JSONObject jsonObject = searchNode(nodeName, nodeParent);

        if (null != seondLevelNodeArray && seondLevelNodeArray.contains(jsonObject)) {
            seondLevelNodeArray.remove(jsonObject);
            secondLevelNodeJArray = null;
            JSONArray temp = new JSONArray(seondLevelNodeArray);
            secondLevelNodeJArray = temp;
            firstLevelNodeJObj.put(ParserConstants.NODE, secondLevelNodeJArray);

        } else {
            for (int i = 0; i < secondLevelNodeJArray.length(); i++) {
                if (secondLevelNodeJArray.getJSONObject(i).has(ParserConstants.NODE)) {

                    thirdLevelNodeJobject = secondLevelNodeJArray.getJSONObject(i).getJSONObject(ParserConstants.NODE);
                    if (thirdLevelNodeJobject.getString(ParserConstants.KEY_NODE_NAME).equalsIgnoreCase(nodeName) && nodeParent.equalsIgnoreCase("")) {
                        secondLevelNodeJArray.getJSONObject(i).remove(ParserConstants.NODE);
                    } else if (secondLevelNodeJArray.getJSONObject(i).getString(ParserConstants.KEY_NODE_NAME).equalsIgnoreCase(nodeParent) && thirdLevelNodeJobject.getString(ParserConstants.KEY_NODE_NAME).equalsIgnoreCase(nodeName)) {
                        secondLevelNodeJArray.getJSONObject(i).remove(ParserConstants.NODE);
                    }
                }
            }
        }
        if (Constants.DEBUGGABLE_MODE) {
            Log.d("Management tree", mngmntTreeJsonObj.toString());
        }
        Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
    }


    /**
     * This method will delete the attribute of a node and save it file
     *
     * @param nodeName      the node name
     * @param nodeParent    :name of parent of nodeName, it is required to handle the case where there are more than one node with same node name(Download and DownloadAndUpdate)                      In case of "PkgURl" node pass this parameter as "Download" or "DownloadAndUpdate" else we can pass it null
     * @param nodeAttribute the node attribute
     * @throws JSONException the json exception
     */
    public void deleteNodeAttribute(String nodeName, String nodeParent, String nodeAttribute) throws JSONException {
        JSONObject jsonObject = searchNode(nodeName, nodeParent);
        if (null != jsonObject && jsonObject.has(ParserConstants.DF_PROPERTIES)) {
            mDFPropertiesJsonObject = jsonObject.getJSONObject(ParserConstants.DF_PROPERTIES);

            if (nodeAttribute.equalsIgnoreCase(ParserConstants.DF_TITLE) && mDFPropertiesJsonObject.has(ParserConstants.DF_TITLE)) {
                mDFPropertiesJsonObject.remove(ParserConstants.DF_TITLE);
            } else if (nodeAttribute.equalsIgnoreCase(ParserConstants.DF_FORMAT) && mDFPropertiesJsonObject.has(ParserConstants.DF_FORMAT)) {
                mDFPropertiesJsonObject.remove(ParserConstants.DF_FORMAT);
            } else if (nodeAttribute.equalsIgnoreCase(ParserConstants.DF_OCCURRENCE) && mDFPropertiesJsonObject.has(ParserConstants.DF_OCCURRENCE)) {
                mDFPropertiesJsonObject.remove(ParserConstants.DF_OCCURRENCE);
            } else if (nodeAttribute.equalsIgnoreCase(ParserConstants.DF_TYPE) && mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {
                mDFPropertiesJsonObject.remove(ParserConstants.DF_TYPE);
            } else if (nodeAttribute.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE) && mDFPropertiesJsonObject.has(ParserConstants.KEY_ACCESS_TYPE)) {
                mDFPropertiesJsonObject.remove(ParserConstants.KEY_ACCESS_TYPE);
            }
        }
        if (!mDFPropertiesJsonObject.has(ParserConstants.KEY_ACCESS_TYPE) && !mDFPropertiesJsonObject.has(ParserConstants.DF_FORMAT) && !mDFPropertiesJsonObject.has(ParserConstants.DF_TITLE) && !mDFPropertiesJsonObject.has(ParserConstants.DF_OCCURRENCE)
                && !mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {

            jsonObject.remove(ParserConstants.DF_PROPERTIES);

        }

        Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
        if (Constants.DEBUGGABLE_MODE) {
            Log.d("Management tree", mngmntTreeJsonObj.toString());
        }
    }

    /**
     * Add node boolean.
     *
     * @param nodeName       the node name
     * @param nodeParent     :name of parent of nodeName, it is required to handle the case where there are more than one node with same node name(Download and DownloadAndUpdate)                       In case of "PkgURl" node pass this parameter as "Download" or "DownloadAndUpdate" else we can pass it null
     * @param attribute      the attribute
     * @param attributeValue the attribute value
     * @return boolean
     * @throws JSONException the json exception
     */
    public boolean addNode(String nodeName, String nodeParent, String attribute, String attributeValue) throws JSONException {
        if (null == searchNode(nodeName, nodeParent)) {
            JSONObject newNode = new JSONObject();

            newNode.put(ParserConstants.KEY_NODE_NAME, nodeName);


            JSONObject DFProperties = new JSONObject();

            JSONArray mAccessTypeJArray = new JSONArray();

            JSONObject mDfFormatJObj = new JSONObject();
            mDfFormatJObj.put(ParserConstants.DF_FORMAT, "");

            JSONObject mDfOccurrenceJObj = new JSONObject();
            mDfOccurrenceJObj.put(ParserConstants.DF_OCCURRENCE, "");

            JSONObject mDfTypeJObj = new JSONObject();


            DFProperties.put(ParserConstants.KEY_ACCESS_TYPE, mAccessTypeJArray);
            DFProperties.put(ParserConstants.DF_FORMAT, mDfFormatJObj);
            DFProperties.put(ParserConstants.DF_OCCURRENCE, mDfOccurrenceJObj);
            DFProperties.put(ParserConstants.DF_TITLE, "");
            DFProperties.put(ParserConstants.DF_TYPE, mDfTypeJObj);

            if (attribute.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {
                mAccessTypeJArray.put(0, attributeValue);
                DFProperties.put(ParserConstants.KEY_ACCESS_TYPE, mAccessTypeJArray);

            } else if (attribute.equalsIgnoreCase(ParserConstants.DF_FORMAT)) {
                mDfFormatJObj.put(ParserConstants.DF_FORMAT, attributeValue);
                DFProperties.put(ParserConstants.DF_FORMAT, mDfFormatJObj);

            } else if (attribute.equalsIgnoreCase(ParserConstants.DF_OCCURRENCE)) {
                mDfOccurrenceJObj.put(ParserConstants.DF_OCCURRENCE, attributeValue);
                DFProperties.put(ParserConstants.DF_OCCURRENCE, mDfOccurrenceJObj);

            } else if (attribute.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
                DFProperties.put(ParserConstants.DF_TITLE, attributeValue);

            } else if (attribute.equalsIgnoreCase(ParserConstants.MIME)) {

                mDfTypeJObj.put(ParserConstants.MIME, attributeValue);
                DFProperties.put(ParserConstants.DF_TYPE, mDfTypeJObj);

            } else if (attribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {

                mDfTypeJObj.put(ParserConstants.KEY_DDF_NAME, attributeValue);
                DFProperties.put(ParserConstants.DF_TYPE, mDfTypeJObj);

            }


            newNode.put(ParserConstants.DF_PROPERTIES, DFProperties);


            // secondLevelNodeJArray.put(nodeName, newNode);
            secondLevelNodeJArray.put(secondLevelNodeJArray.length(), newNode);
            Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
            if (Constants.DEBUGGABLE_MODE) {
                Log.i("Mgmt Tree addNode:-", mngmntTreeJsonObj.toString());
            }
            return true;

        } else {
            return false;
        }
    }


    /**
     * This method will search the node with the node name provided in the complete tree and return the json object of the same.
     *
     * @param nodeName       the node name
     * @param parentNodeName the parent node name
     * @return json object
     * @throws JSONException the json exception
     */
    public JSONObject searchNode(String nodeName, String parentNodeName) throws JSONException {
        mReturnedJsonObject = null;
        if (null != firstLevelNodeJObj && firstLevelNodeJObj.has(nodeName)) {

            return firstLevelNodeJObj;
        }
        for (int i = 0; i < secondLevelNodeJArray.length(); i++) {

            if (secondLevelNodeJArray.getJSONObject(i).getString(ParserConstants.KEY_NODE_NAME).equalsIgnoreCase(nodeName)) {
                if (Constants.DEBUGGABLE_MODE) {
                    Log.d("Node Found", nodeName);
                }
                mReturnedJsonObject = secondLevelNodeJArray.getJSONObject(i);
                return mReturnedJsonObject;
            } else if (secondLevelNodeJArray.getJSONObject(i).has(ParserConstants.NODE)) {
                thirdLevelNodeJobject = secondLevelNodeJArray.getJSONObject(i).getJSONObject(ParserConstants.NODE);

                if (thirdLevelNodeJobject.getString(ParserConstants.KEY_NODE_NAME).equalsIgnoreCase(nodeName)
                        && secondLevelNodeJArray.getJSONObject(i).getString(ParserConstants.KEY_NODE_NAME).equalsIgnoreCase(parentNodeName)) {
                    {
                        if (Constants.DEBUGGABLE_MODE) {
                            Log.d("Node Found", "In second Array" + nodeName + secondLevelNodeJArray.getJSONObject(i).toString());
                        }
                        mReturnedJsonObject = thirdLevelNodeJobject;
                        return mReturnedJsonObject;
                    }
                }
               /* if (thirdLevelNodeJobject.getString(ParserConstants.NODE_NAME).equalsIgnoreCase(nodeName) && parentNodeName.equalsIgnoreCase("Download")) {
                    mReturnedJsonObject = thirdLevelNodeJobject;
                    return mReturnedJsonObject;
                }

                if (thirdLevelNodeJobject.getString(ParserConstants.NODE_NAME).equalsIgnoreCase(nodeName) && parentNodeName.equalsIgnoreCase("DownloadAndUpdate")) {
                    mReturnedJsonObject = thirdLevelNodeJobject;
                    return mReturnedJsonObject;
                }*/
            }
        }
        return mReturnedJsonObject;
    }

    /**
     * This method will update the attribute of the node and if node is not present it will create the node and set the attribute.
     *
     * @param nodeName          the node name
     * @param nodeParent        :name of parent of nodeName, it is required to handle the case where there are more than one node with same node name(Download and DownloadAndUpdate)                          In case of "PkgURl" node pass this parameter as "Download" or "DownloadAndUpdate" else we can pass it null
     * @param attribute         the attribute
     * @param attributeNewValue the attribute new value
     * @return the boolean
     * @throws JSONException the json exception
     */
    public boolean updateNodeAttribute(String nodeName, String nodeParent, String attribute, String attributeNewValue) throws JSONException {

        boolean isUpdated = false;
        boolean isNodeAdded = addNode(nodeName, nodeParent, attribute, attributeNewValue);
        isUpdated = isNodeAdded;
        if (!isNodeAdded) {
            if (attribute.equalsIgnoreCase(ParserConstants.DF_FORMAT)) {
                setNodeDFFormat();
            } else if (attribute.equalsIgnoreCase(ParserConstants.DF_OCCURRENCE)) {
                setNodeOccurrence();
            } else if (attribute.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
                isUpdated = setNodeDFTitle(nodeName, attributeNewValue, nodeParent);
            } else if (attribute.equalsIgnoreCase(ParserConstants.MIME) || attribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
                isUpdated = setNodeDFType(nodeName, attributeNewValue, nodeParent, attribute);
            }
        }
        if (Constants.DEBUGGABLE_MODE) {
            Log.d("Management tree", mngmntTreeJsonObj.toString());
        }
        Utils.writeToFIle(mContext, mngmntTreeJsonObj.toString(), mFileName);
        return isUpdated;
    }

    /**
     * This method will return the Value of Node Attribute.
     *
     * @param nodeName   the node name
     * @param nodeParent :name of parent of nodeName, it is required to handle the case where there are more than one node with same node name(Download and DownloadAndUpdate)                        In case of "PkgURl" node pass this parameter as "Download" or "DownloadAndUpdate" else we can pass it null
     * @param attribute  the attribute
     * @return node attribute
     * @throws JSONException the json exception
     */
    public ArrayList<String> getNodeAttribute(String nodeName, String nodeParent, String attribute) throws JSONException {

        ArrayList<String> nodeAttributeValue = new ArrayList<>();
        if (attribute.equalsIgnoreCase(ParserConstants.KEY_ACCESS_TYPE)) {
            nodeAttributeValue = getNodeAccessType(nodeName, nodeParent);
        } else if (attribute.equalsIgnoreCase(ParserConstants.DF_FORMAT)) {
            getNodeDFFormat();
        } else if (attribute.equalsIgnoreCase(ParserConstants.DF_OCCURRENCE)) {
            getNodeOccurrence();
        } else if (attribute.equalsIgnoreCase(ParserConstants.DF_TITLE)) {
            String dfTitle = getNodeDfTitle(nodeName, nodeParent);
            nodeAttributeValue.add(dfTitle);
        } else if (attribute.equalsIgnoreCase(ParserConstants.MIME) || attribute.equalsIgnoreCase(ParserConstants.KEY_DDF_NAME)) {
            //"Attribute" should be MIME or DDF_Name
            nodeAttributeValue = getNodeDFType(nodeName, nodeParent);
        }
        return nodeAttributeValue;
    }


    /**
     * This method will return the Mime value of the parent
     *
     * @return parent mime
     * @throws JSONException the json exception
     */
    public String getParentMIME() throws JSONException {

        mDFPropertiesJsonObject = firstLevelNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);
        if (mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {

            mDFTypeJsonObject = mDFPropertiesJsonObject.getJSONObject(ParserConstants.DF_TYPE);

            if (mDFTypeJsonObject.has(ParserConstants.MIME)) {
                return (mDFTypeJsonObject.getString(ParserConstants.MIME));
            }

            return null;

        } else {
            return null;
        }
    }


    /**
     * This method will return the DDFName value of the parent
     *
     * @return parent ddf name
     * @throws JSONException the json exception
     */
    public String getParentDDFName() throws JSONException {

        mDFPropertiesJsonObject = firstLevelNodeJObj.getJSONObject(ParserConstants.DF_PROPERTIES);
        if (mDFPropertiesJsonObject.has(ParserConstants.DF_TYPE)) {

            mDFTypeJsonObject = mDFPropertiesJsonObject.getJSONObject(ParserConstants.DF_TYPE);

            if (mDFTypeJsonObject.has(ParserConstants.KEY_DDF_NAME)) {
                Log.v(TAG, "DDF Name: "+mDFTypeJsonObject.getString(ParserConstants.KEY_DDF_NAME));
                return (mDFTypeJsonObject.getString(ParserConstants.KEY_DDF_NAME));
            }

            return null;

        } else {
            return null;
        }
    }

}
