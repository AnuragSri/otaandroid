package com.movimento.task;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.movimento.motreeparser.DevAccParser;
import com.movimento.motreeparser.FumoParser;
import com.movimento.motreeparser.FumoStateParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.otaclient.DownloadUpdateActivity;
import com.movimento.otaclient.InstallUpdateActivity;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.service.DevInfoIntentService;
import com.movimento.service.FumoIntentService;
import com.movimento.utils.Constants;
import com.movimento.utils.ParserConstants;

import org.json.JSONException;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Ota task executor.
 */
public class OTATaskExecutor implements Runnable {
    private final String TAG = OTATaskExecutor.class.getCanonicalName();
    private final int DELAY_IN_MILLIS = 1000;


    private final BlockingQueue<List<String>> sharedQueue;

    /**
     * Instantiates a new Ota task executor.
     *
     * @param sharedQueue the shared queue
     */
    public OTATaskExecutor(BlockingQueue<List<String>> sharedQueue) {
        this.sharedQueue = sharedQueue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                List<String> tasks = OTAClientApp.sharedQueue.take();
                if (tasks != null && !tasks.isEmpty() && tasks.size() >= 2) {
                    Log.i(TAG, "Command : " + tasks.get(0) + " : " + tasks.get(1));
                }
                if (tasks.get(0).equals("GET")) {
                    if (tasks.get(1).toLowerCase().contains("fumo")) {
                        Intent fumoIntent = new Intent(OTAClientApp.getmAppInstance(), FumoIntentService.class);
                        String ddfName = tasks.get(1);
                        fumoIntent.putExtra(ParserConstants.KEY, ParserConstants.KEY_DDF_NAME);
                        fumoIntent.putExtra(ParserConstants.VALUE, ddfName);
                        OTAClientApp.getmAppInstance().startService(fumoIntent);
                    }else if (tasks.get(1).toLowerCase().contains("devinfo")) {
                        Intent devInfoIntent = new Intent(OTAClientApp.getmAppInstance(), DevInfoIntentService.class);
                        OTAClientApp.getmAppInstance().startService(devInfoIntent);
                    }

                } else if (tasks.get(0).equals("HGET")) {

                    if (tasks.get(2).toLowerCase().contains("fumo")) {
                        // this one is taking care of dlota : making download request
                        try {
                            FumoParser fumoParser = (FumoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO);
                            fumoParser.setNodeDFTitle(ParserConstants.KEY_FUMO_DOWNLOAD_AND_UPDATE_PKG_URL, tasks.get(1), ParserConstants.KEY_FUMO_DOWNLOAD_AND_UPDATE);
                            fumoParser.setNodeDFTitle(ParserConstants.KEY_FUMO_DOWNLOAD_AND_UPDATE, tasks.get(2), "");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        //Sending broadcast to finish Cheching Update Activity.
                        Intent sendUpdateStatusIntent = new Intent();
                        sendUpdateStatusIntent.setAction(Constants.CHECKING_UPDATE_STATUS_EVENT);
                        LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).sendBroadcast(sendUpdateStatusIntent);

                    } else if (tasks.get(2).toLowerCase().contains("dmacc")) {
                        if (/*tasks.get(2).toLowerCase().contains("urn:oma:mo:oma-dm-dmacc") && */tasks.get(2).toLowerCase().contains("Auth/Trsp/Secret")) {
                            DevAccParser devAccParser = (DevAccParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_DEVICE_ACCOUNT);
                            try {
                                devAccParser.setNodeDFTitle(ParserConstants.KEY_NODE_SECRET, tasks.get(1));
                                Log.i(TAG, "Saved new secret.");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                } else if (tasks.get(0).equals("EXEC")) {
                    if (tasks.get(1).toLowerCase().contains("fumo")) {
                        // starting activity to notify user about update

                        FumoStateParser fumoStateParser = (FumoStateParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO_STATE);
                        Log.i(TAG, "FUMO : CURRENT_STATE : " + fumoStateParser.getState(ParserConstants.CURRENT_STATE));

                        /*Integer.parseInt(fumoStateParser.getState(ParserConstants.CURRENT_STATE)) != Constants.DLOTA_STATE_DOWNLOAD_PROGRESSING &&*/
                        if (Integer.parseInt(fumoStateParser.getState(ParserConstants.CURRENT_STATE)) != Constants.DLOTA_STATE_READY_TO_UPDATE) {
                            Intent downloadIntent = new Intent(OTAClientApp.getmAppInstance(), DownloadUpdateActivity.class);
                            downloadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            OTAClientApp.getmAppInstance().startActivity(downloadIntent);
                        } else if (Integer.parseInt(fumoStateParser.getState(ParserConstants.CURRENT_STATE)) == Constants.DLOTA_STATE_READY_TO_UPDATE) {
                            Intent downloadIntent = new Intent(OTAClientApp.getmAppInstance(), InstallUpdateActivity.class);
                            downloadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            OTAClientApp.getmAppInstance().startActivity(downloadIntent);
                        }

                        //Sending broadcast to finish Checking Update Activity.
                        Intent sendUpdateStatusIntent = new Intent();
                        sendUpdateStatusIntent.setAction(Constants.CHECKING_UPDATE_STATUS_EVENT);
                        LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).sendBroadcast(sendUpdateStatusIntent);

                    }
                }

                Thread.sleep(DELAY_IN_MILLIS);

            } catch (InterruptedException ex) {
                Logger.getLogger(OTATaskExecutor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }


}
