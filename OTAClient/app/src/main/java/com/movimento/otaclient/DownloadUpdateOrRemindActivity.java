package com.movimento.otaclient;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.movimento.utils.Utils;

import java.util.GregorianCalendar;

/**
 * The type Download update activity.
 */
/*
This Activity will be called if any update is available, It will ask the User to download the Update.
*/
public class DownloadUpdateOrRemindActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mContext;
    private TextView tvHeader;

    private Button btnDownloadAndInstall;
    private ProgressBar mProgressBar;
    private LinearLayout llProgressBar;
    private TextView mLearnMoreLink;
    private Button mBtnRemindLater;
    private Button mBtnCancelDownload;
    private AlertDialog levelDialog;
    private AlertDialog.Builder builder;
    private AlarmManager alarmManager;
    private long mLastClickTime = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_update_or_remind_later);
        mContext = this;

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(this.getResources().getColor(R.color.yellow)));
        //alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        initUI();
    }


    @Override
    protected void onResume() {
        super.onResume();
        /*Enable the buttons*/
        btnDownloadAndInstall.setClickable(true);
        mLearnMoreLink.setClickable(true);
        mBtnRemindLater.setClickable(true);

    }

    @Override
    public void onClick(View view) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        if (view == btnDownloadAndInstall) {
            if (!llProgressBar.isShown()) {

            } else {
                    /*Disable the button when downloading progress is displayed */
                btnDownloadAndInstall.setClickable(false);
            }

        } else if (view == mLearnMoreLink) {
            /*Handles if Learn More Link is clicked by user*/
            if (!llProgressBar.isShown()) {
                Toast.makeText(DownloadUpdateOrRemindActivity.this, getResources().getString(R.string.work_in_progress_msg), Toast.LENGTH_SHORT).show();
            } else {
                mLearnMoreLink.setClickable(false);
            }

        } else if (view == mBtnRemindLater) {
              /*On click of cancel button application will be closed.
                If Progress dialog is displayed cancel button is disabled. */
            if (!llProgressBar.isShown()) {
                AlertDialogView(mContext);

            } else {
                mBtnRemindLater.setClickable(false);
            }
        }
    }


    /**
     * Alert dialog view.
     *
     * @param context the context
     */
    public void AlertDialogView(final Context context) {

        final CharSequence[] items = {" 1 Hour ", " 6 Hour ", " 1 Day ", " 1 Week "};

        builder = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

        builder.setTitle("Remind me in");
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        // Your code when first option selected
                        Utils.setReminderFlag(true, 1, context);
                        //1*60*60*1000
                        scheduleAlarm(8 * 1000);
                        finish();

                        break;
                    case 1:
                        // Your code when 2nd  option selected
                        Utils.setReminderFlag(true, 6, context);
                        // 6*60*60*1000
                        scheduleAlarm(8 * 1000);
                        finish();

                        break;
                    case 2:
                        // Your code when 3rd option selected
                        Utils.setReminderFlag(true, 10, context);
                        // 10*60*60*1000
                        scheduleAlarm(8 * 1000);
                        finish();
                        break;
                    case 3:
                        // Your code when 4th  option selected
                        Utils.setReminderFlag(true, 12, context);
                        // 7*24*60*60*1000
                        scheduleAlarm(8 * 1000);
                        finish();

                        break;
                }
                levelDialog.dismiss();
            }
        });
        levelDialog = builder.create();
        levelDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        levelDialog.show();
    }

    /**
     * Schedule alarm.
     *
     * @param duration the duration
     */
    public void scheduleAlarm(int duration) {
        Long time = new GregorianCalendar().getTimeInMillis() + duration;
        Intent intentAlarm = new Intent(this, AlarmReciever.class);
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        alarmManager.set(AlarmManager.RTC_WAKEUP, time, PendingIntent.getBroadcast(this, 1,
                intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));

        Toast.makeText(this, "Alarm is set", Toast.LENGTH_LONG).show();
    }


    private void initUI() {
        //Init Header
        tvHeader = (TextView) findViewById(R.id.header_text);
        Utils.setHeader(mContext, tvHeader);

        /*Initializing view*/
        btnDownloadAndInstall = (Button) findViewById(R.id.btnDownloadAndInstall);
        mProgressBar = (ProgressBar) findViewById(R.id.progressUpdate);
        llProgressBar = (LinearLayout) findViewById(R.id.llProgressBar);
        mLearnMoreLink = (TextView) findViewById(R.id.tv_learn_more_link);
        mBtnRemindLater = (Button) findViewById(R.id.btn_remind);
        // mBtnCancelDownload = (Button) findViewById(R.id.btn_cancel_download);


        btnDownloadAndInstall.setOnClickListener(this);
        mLearnMoreLink.setOnClickListener(this);
        mBtnRemindLater.setOnClickListener(this);
    }


}