package com.movimento.otaclient;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.movimento.utils.Constants;
import com.movimento.utils.InstallUpdateAsyncTask;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.PrefUtils;
import com.movimento.utils.Utils;

/**
 * The type Install update activity.
 */
/*This Actiity will be called after the update is successfully downloaded and it will ask user to install the update*/
public class InstallUpdateActivity extends AppCompatActivity implements View.OnClickListener {
    private final String TAG = InstallUpdateActivity.class.getSimpleName();
    private Context mContext;

    private Button mAgreeButton;
    private ProgressBar mProgressBar;
    private LinearLayout llProgressBar;
    private TextView mLoadingText;
    private TextView mTvClickToCancel;

    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_install_update);
        mContext = this;
        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(Constants.DDA_CHANGED_STATUS_EVENT);
        LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).registerReceiver(mDDACheckUpdateListener, filter);
    }


    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).unregisterReceiver(mDDACheckUpdateListener);
    }

    @Override
    public void onClick(View view) {
      /*  if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();*/

        if (view == mAgreeButton) {
            Utils.logMessage(TAG, "Agree button pressed....");
            /*If User agrees to install the update Dialog box to reboot the device will be
             * displayed and install update asynctask should be called
             */
            /*
             * Dialog will be displayed with ok and cancel button.
             * On OK button device should be restart and application  should be
             * install in the background. and on cancel button dialog should be closed
             * */
            //TODO: Temporarily added Installation Error and Certificate Error Screen.
            final Dialog mRebootDialog = new Dialog(InstallUpdateActivity.this);
            mRebootDialog.setContentView(R.layout.reboot_dailog_layout);
            Button mRebootOkButton = (Button) mRebootDialog.findViewById(R.id.btn_reboot_ok);
            Button mRebootCancelButton = (Button) mRebootDialog.findViewById(R.id.btn_reboot_cancel);
            mRebootDialog.getWindow().setBackgroundDrawableResource(R.color.dialogBackground);
            mRebootDialog.setTitle(Html.fromHtml("<b>Info</b>"));
            mRebootDialog.getWindow().setTitleColor(getResources().getColor(R.color.textColorWhite));

            if (Build.VERSION.SDK_INT < 19) {
                int titleDividerId = mRebootDialog.getContext().getResources().getIdentifier("titleDivider", "id", "android");
                View titleDivider = mRebootDialog.getWindow().getDecorView().findViewById(titleDividerId);
                titleDivider.setBackgroundColor(getResources().getColor(R.color.yellow));
            }
            mRebootOkButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mRebootDialog.dismiss();
                    Utils.logMessage(TAG, "Ok button pressed....");
                    //TODO : uncomment the InstallUpdateAsyncTask to verify and install the update package.
                    if (Constants.PRODUCTION_BUILD /*&& !OTAClientApp.getDDARemoteFlag()*/) {
                        new InstallUpdateAsyncTask(OTAClientApp.getmAppInstance(), mProgressBar, llProgressBar).execute();
                        Utils.logMessage(TAG, "InstallUpdateAsyncTask called...");
                    } else {
                        Utils.logMessage(TAG, "Installation command started. Dlota state : "+ Utils.getDlotaState());
                        Utils.logMessage(TAG, "Non production build : InstallUpdateActivity finished...");
                        PrefUtils.putBoolean(Constants.IS_INSTALLATION_STARTED,true);
                        Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_UPDATE_PROGRESSING));
                        InstallUpdateActivity.this.finish();
                    }
                }
            });

            mRebootCancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mRebootDialog.dismiss();
                }
            });
            if (mRebootDialog != null) {
                mRebootDialog.show();
            }


        } else if (view == mTvClickToCancel) {
            /*Application will be closed on click of cancel button*/
            InstallUpdateActivity.this.finish();
            Utils.logMessage(TAG, "Cancel button clicked...");
        }

    }

    private BroadcastReceiver mDDACheckUpdateListener = new BroadcastReceiver() {
        private final String TAG = "LocalBroadcastReceiver-->" + DownloadUpdateActivity.class.getCanonicalName();

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction().equals(Constants.DDA_CHANGED_STATUS_EVENT)) {
                InstallUpdateActivity.this.finish();
            }
        }
    };

    private void sendNetworkError() {
        Intent intent = new Intent(mContext, ErrorActivity.class);
        intent.putExtra(Constants.KEY_ERRROR_TYPE, Constants.ERROR_TYPE_NETWORK_CONNECTION);
        intent.putExtra(Constants.KEY_ERROR_HEADER, getResources().getString(R.string.connection_error_header));
        intent.putExtra(Constants.KEY_ERROR_INFO, getResources().getString(R.string.connection_error_info_text));
        startActivity(intent);
    }

    private void initUI() {
        Utils.createCustomActionBarTitle(mContext);
          /*Initializing View*/
        mAgreeButton = (Button) findViewById(R.id.agree);
        mProgressBar = (ProgressBar) findViewById(R.id.progressUpdate);
        llProgressBar = (LinearLayout) findViewById(R.id.llProgressBar);
        mTvClickToCancel = (TextView) findViewById(R.id.tv_click_to_cancel);
        mLoadingText = (TextView) findViewById(R.id.loadingText);
        mAgreeButton.setOnClickListener(this);
        mTvClickToCancel.setOnClickListener(this);
    }

}
