package com.movimento.otaclient;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.delphi.dea600.bba_dda_remote.VirtualDDAListener;
import com.delphi.dea600.bba_dda_remote.bba_dda_remote;
import com.movimento.motreeparser.FumoParser;
import com.movimento.motreeparser.FumoStateParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.service.DlotaIntentService;
import com.movimento.task.OTATaskExecutor;
import com.movimento.utils.Constants;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.PrefUtils;
import com.movimento.utils.Utils;

import org.json.JSONException;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


/**
 * The type Ota client app.
 */
public class OTAClientApp extends Application {
    private final String TAG = OTAClientApp.class.getCanonicalName();
    private static Context mAppInstance;

    private static boolean ddaModeFlag;
    /**
     * The Shared queue.
     */
    public static BlockingQueue<List<String>> sharedQueue = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppInstance = this;
        sharedQueue = new LinkedBlockingQueue<List<String>>();
        Thread otaTaskExecutor = new Thread(new OTATaskExecutor(sharedQueue));
        otaTaskExecutor.start();

        //DDA Listener implementation
        dda_remote.ddaOnresume(this.getApplicationContext());
        ddaModeFlag = dda_remote.isDDAModeFlag(this.getApplicationContext());
        //dda_signal.setText("dda signal= " + ddaModeFlag);
        Log.v(TAG, "dda signal= " + ddaModeFlag);
       // Utils.logMessage(TAG, "dda signal= " + ddaModeFlag);
        savePackageVersion();
    }

    private void savePackageVersion(){
        Utils.logMessage(TAG,"savePackageVersion called");
        if (PrefUtils.getBoolean(Constants.FIRST_APP_RUN, true)) {
            try {
                FumoParser fumoParser = (FumoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO);
                fumoParser.setNodeDFTitle(ParserConstants.KEY_FUMO_PKG_VERSION, Build.VERSION.INCREMENTAL, "");
                Utils.logMessage(TAG,ParserConstants.KEY_FUMO_PKG_VERSION+" updated");
            } catch (JSONException e) {
                e.printStackTrace();
                Utils.logMessage(TAG, Log.getStackTraceString(e));
            }
            PrefUtils.putBoolean(Constants.FIRST_APP_RUN, false);
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.v(TAG, "onLowMemory()");
     //   Utils.logMessage(TAG, "onLowMemory()");
        dda_remote.ddaOnpause(this.getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.v(TAG, "onTerminate()");
    //    Utils.logMessage(TAG, "onTerminate()");
        dda_remote.ddaOnpause(this.getApplicationContext());
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v(TAG, "onTrimMemory()");
    //    Utils.logMessage(TAG, "onTrimMemory()");
    }


    bba_dda_remote dda_remote = new bba_dda_remote(new VirtualDDAListener() {
        @Override
        public void virtualDDAStatus(boolean flag) {
            //This DDA conditional flag will update if there is some event in system(board)
            ddaModeFlag = flag;

            Log.i("DDAReceiver", "ddaModeFlag is :" + flag);
            /*TODO: WHEN DDA CHANGE OCCURS AND APP IS OPENED THIS FUNCTION IS CALLED, HERE
            YOU CAN ADD YOUR OWN IMPLEMENTATION, IN CASE YOU DON´T NEED TO IMPLEMENT A
            CHANGE, PLEASE LEAVE IT EMPTY*/

            //TODO here to fire local broadcast to put visible activities in background
            if (ddaModeFlag) {
              /*  Intent intent = new Intent();
                intent.setAction(Constants.DDA_CHANGED_STATUS_EVENT);
                LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).sendBroadcast(intent);*/
            } else {
              /*  FumoStateParser fumoStateParser = (FumoStateParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO_STATE);
                String currentState = fumoStateParser.getState(ParserConstants.CURRENT_STATE);
                if (currentState.equals(String.valueOf(Constants.FUMO_STATE_READY_TO_UPDATE))) {
                    startActivity(new Intent(OTAClientApp.getmAppInstance(), InstallUpdateActivity.class));
                } else if (currentState.equals(String.valueOf(Constants.FUMO_STATE_DOWNLOAD_PROGRESSING))) {
                    if (Utils.isServiceRunning(DlotaIntentService.class)) {
                        Intent dlotaIntent = new Intent(OTAClientApp.getmAppInstance(), DlotaIntentService.class);
                        DlotaIntentService.shouldServiceRun = true;
                        startService(dlotaIntent);
                    }
                    startActivity(new Intent(OTAClientApp.getmAppInstance(), DownloadUpdateActivity.class));
                }*/
            }
        }
    });

    public static boolean getDDARemoteFlag() {
        return ddaModeFlag;
    }

    /**
     * Put ota task.
     *
     * @param task the task
     */
    public static void putOTATask(final List<String> task) {
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    sharedQueue.put(task);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }


    /**
     * Getm app instance context.
     *
     * @return the context
     */
    public static Context getmAppInstance() {
        return mAppInstance;
    }

}
