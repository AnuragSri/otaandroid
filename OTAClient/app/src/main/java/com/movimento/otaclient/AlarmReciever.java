package com.movimento.otaclient;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


/**
 * The type Alarm reciever.
 */
public class AlarmReciever extends BroadcastReceiver {
    @Override
    public void onReceive(final Context context, Intent intent) {
        Intent newIntent = new Intent(context, AlertDialogActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        newIntent.setFlags(newIntent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
        context.startActivity(newIntent);
    }
}
