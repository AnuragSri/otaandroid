package com.movimento.otaclient;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import com.movimento.motreeparser.DeviceInfoParser;
import com.movimento.motreeparser.FumoParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.service.OTAClientService;
import com.movimento.utils.Constants;
import com.movimento.utils.FaceTypeUtil;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.PrefUtils;
import com.movimento.utils.Utils;

import org.json.JSONException;

import java.util.Timer;
import java.util.TimerTask;

import static com.movimento.utils.Utils.WIRELESS_PORT;

/*Launcher Activity: Splash screen Will be displayed when application is launched.*/

/**
 * The type Splash activity.
 */
public class SplashActivity extends Activity {
    private final String TAG = SplashActivity.class.getCanonicalName();
    private Context mContext;
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    private TextView mHeaderText;
    private TextView mSubHeaderText;

    private Timer mTimer;
    /**
     * The Mac address.
     */
    String MAC_ADDRESS = "";
    /**
     * The Is wifi address avail.
     */
    boolean isWifiAddressAvail = false;
    /**
     * The Wifi initial state.
     */
    boolean wifiInitialState = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mContext = this;
        mHeaderText = (TextView) findViewById(R.id.header_text);
        mSubHeaderText = (TextView) findViewById(R.id.splash_header_bottom);

        /*Setting the Font Styles on the text*/
        String update = getResources().getString(R.string.header_update);
        String mobile = "<font color='#F5EF2F'>Device</font>";
        FaceTypeUtil.setTypeFace(mContext, mHeaderText, FaceTypeUtil.BAUHAUS_FONT);
        mHeaderText.setText(Html.fromHtml(mobile + " " + update));

        FaceTypeUtil.setTypeFace(mContext, mSubHeaderText, FaceTypeUtil.EXTRA_LIGHT);

        Log.i(TAG,"Current Device Version is : "+ Build.VERSION.INCREMENTAL);
        Utils.logMessage(TAG, "Current Device Version is : "+ Build.VERSION.INCREMENTAL);

        if (PrefUtils.getBoolean(Constants.FIRST_APP_RUN, true)) {
            try {
                FumoParser fumoParser = (FumoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO);
                fumoParser.setNodeDFTitle(ParserConstants.KEY_FUMO_PKG_VERSION, Build.VERSION.INCREMENTAL, "");
            } catch (JSONException e) {
                e.printStackTrace();
                Utils.logMessage(TAG, Log.getStackTraceString(e));
            }
            PrefUtils.putBoolean(Constants.FIRST_APP_RUN, false);
        }

        //Start timer here to fetch Wifi MAC Address for all cases and store MAC id into MO Tree.
        DeviceInfoParser deviceInfoParser = (DeviceInfoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_DEVICE_INFO);
        if (deviceInfoParser != null) {
            try {
                //update devId and VIN(aka MAC_ADDRESS) in devInfoMOTree
                String deviceId = deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_DEV_ID);
                if (deviceId == null || deviceId.isEmpty()) {
                    deviceInfoParser.updateChildNodeDFTitle(ParserConstants.KEY_DEV_ID, Utils.getMACAddress(WIRELESS_PORT));
                }
                String vinMacAddress = deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_VIN);
                if (vinMacAddress == null || vinMacAddress.isEmpty()) {
                    startTimer();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        /**
         * Splash screen will be displayed for 3 seconds and then it will call Checking Update Activity
         */
        Handler handler = new Handler(getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                launchMainActivity();
            }
        }, SPLASH_DISPLAY_LENGTH);

    }

    @Override
    protected void onDestroy() {
        stopTimer();
        disableWifi();
        super.onDestroy();
        Log.v(TAG, "Activity Destroyed.");
    }

    private boolean saveMacId() {
        // TODO : get and save mac_id(VIN no) & encrypted mac_id in motree
        //boolean flag = false;
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_WIFI)) {
            isWifiAddressAvail = false;
            Log.v(TAG, "Device is not equipped with Wifi Card");
            //Device is not equipped with Wifi Card
        } else {
            MAC_ADDRESS = Utils.getMACAddress(WIRELESS_PORT);
            if (MAC_ADDRESS == null || MAC_ADDRESS.isEmpty()) {
                final WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                wifiManager.setWifiEnabled(true);
                wifiInitialState = true;

                Handler handler = new Handler(getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                MAC_ADDRESS = Utils.getMACAddress(WIRELESS_PORT);
                                if (MAC_ADDRESS != null && !MAC_ADDRESS.isEmpty()) {
                                    isWifiAddressAvail = Utils.updateMacAddress(MAC_ADDRESS);
                                    disableWifi();
                                    stopTimer();
                                }
                            }
                        });
                    }
                }, 1000);
            } else {
                isWifiAddressAvail = Utils.updateMacAddress(MAC_ADDRESS);
                disableWifi();
                stopTimer();
            }
        }
        return isWifiAddressAvail;
    }


    private void initMainService() {
        if (!Utils.isServiceRunning(OTAClientService.class)) {
            startService(new Intent(OTAClientApp.getmAppInstance(), OTAClientService.class));
            Log.v(TAG, "OTACLinet service is started from splash screen...");
        }
    }

    private void launchMainActivity() {
        Intent mainIntent = new Intent(mContext, CheckingUpdateActivity.class);
        SplashActivity.this.startActivity(mainIntent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

        // TODO : Delete above code and uncomment below code when actual build version check implemeted on server side.

        /* if (Utils.ifPreviousVersion()) {
            Log.i(TAG,"Not updated. Device current version : "+ Build.VERSION.INCREMENTAL);
            Intent mainIntent = new Intent(mContext, CheckingUpdateActivity.class);
            SplashActivity.this.startActivity(mainIntent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        } else {
            Log.i(TAG,"Device updated to : "+ Build.VERSION.INCREMENTAL);
            Utils.logMessage(TAG,"Device updated to : "+ Build.VERSION.INCREMENTAL);
            Utils.doIfUpdated();

             Intent successIntent = new Intent(OTAClientApp.getmAppInstance(), SuccessActivity.class);
            successIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            OTAClientApp.getmAppInstance().startActivity(successIntent);

        }*/
    }

    private void startTimer() {
        try {
            if (mTimer == null) {
                mTimer = new Timer();
            }
            CheckWifiScheduler checkWifiScheduler = new CheckWifiScheduler();
            mTimer.scheduleAtFixedRate(checkWifiScheduler, 0, 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    private class CheckWifiScheduler extends TimerTask {
        @Override
        public void run() {
            Log.v(TAG, "Run....");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    boolean savedMacAddr = saveMacId();
                    if (savedMacAddr) {
                        initMainService();
                        disableWifi();
                    }
                }
            });
        }
    }

    private void disableWifi() {
        //This code block only call when it is intentionally make wifi enable to fetch MAC Address.
        if (wifiInitialState) {
            final WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            wifiManager.setWifiEnabled(false);
            wifiInitialState = false;
        }
    }
}




