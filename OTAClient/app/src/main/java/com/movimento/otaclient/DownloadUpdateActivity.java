package com.movimento.otaclient;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.movimento.motreeparser.DownloadDescriptorParser;
import com.movimento.motreeparser.FumoParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.service.DlotaIntentService;
import com.movimento.service.DownloadDescIntentService;
import com.movimento.utils.Constants;
import com.movimento.utils.FileUtils;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.Utils;

import static com.movimento.utils.Constants.KEY_DOWNLOAD_DESC_URL;

/**
 * The type Download update activity.
 */
/*
This Activity will be called if any update is available, It will ask the User to download the Update.
*/
public class DownloadUpdateActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG=DownloadUpdateActivity.class.getSimpleName();
    private Context mContext;
    private TextView tvHeader;
    private TextView mTVLoadingText;

    private Button btnDownloadAndInstall;
    private ProgressBar mProgressBar;
    private LinearLayout llProgressBar;
    private TextView mLearnMoreLink;
    private Button mBtnCancel;
    private Button mBtnCancelDownload;

    private ProgressDialog mProgressDialog = null;
    private long mLastClickTime = 0;
    private TextView mUpdateSizeInfoText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_update);
        mContext = this;
        initUI();
        displayInstallationSize();
    }


    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filters = new IntentFilter(Constants.DLOTA_DOWNLOAD_STATUS_EVENT);
        filters.addAction(Constants.DDA_CHANGED_STATUS_EVENT);
        LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).registerReceiver(mDownloadAndUpdateListener, filters);
        /*Enable the buttons*/
        btnDownloadAndInstall.setClickable(true);
        mLearnMoreLink.setClickable(true);
        mBtnCancel.setClickable(true);
    }


    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).unregisterReceiver(mDownloadAndUpdateListener);
    }

    @Override
    public void onClick(View view) {
       /* if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();*/
        Utils.logMessage(TAG, "on Click called ");
        if (view == btnDownloadAndInstall) {
            if (!Utils.isNetworkAvailable()) {
                Utils.sendError(mContext, Constants.ERROR_TYPE_NETWORK_CONNECTION, getResources().getString(R.string.connection_error_header), "");
                return;
            }
            Utils.logMessage(TAG, "after network check ");
            DlotaIntentService.shouldServiceRun = true;
            if (!llProgressBar.isShown()) {
                try {
                    FumoParser fumoParser = (FumoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO);
                    String url = fumoParser.getNodeDfTitle(ParserConstants.KEY_FUMO_DOWNLOAD_AND_UPDATE_PKG_URL, ParserConstants.KEY_FUMO_DOWNLOAD_AND_UPDATE);

                    Intent downDescIntentService = new Intent(OTAClientApp.getmAppInstance(), DownloadDescIntentService.class);
                    downDescIntentService.putExtra(KEY_DOWNLOAD_DESC_URL, url);
                    Utils.logMessage(TAG, "Download desc url : "+ url);
                    OTAClientApp.getmAppInstance().startService(downDescIntentService);
                } catch (Exception e) {
                    Utils.logMessage(TAG, Log.getStackTraceString(e));
                    e.printStackTrace();
                }
                startProgressDialog();
            } else {
                Utils.logMessage(TAG, "clickable false ");
                /*Disable the button when downloading progress is displayed */
                btnDownloadAndInstall.setClickable(false);
            }

        } else if (view == mLearnMoreLink) {
            /*Handles if Learn More Link is clicked by user*/
            if (!llProgressBar.isShown()) {
                Toast.makeText(DownloadUpdateActivity.this, getResources().getString(R.string.work_in_progress_msg), Toast.LENGTH_SHORT).show();
            } else {
                mLearnMoreLink.setClickable(false);
            }

        } else if (view == mBtnCancel) {
              /*On click of cancel button application will be closed.
                If Progress dialog is displayed cancel button is disabled. */
            if (!llProgressBar.isShown()) {
                finish();
            } else {
                mBtnCancel.setClickable(false);
            }
        } else if (view == mBtnCancelDownload) {
            DlotaIntentService.shouldServiceRun = false;
            if (llProgressBar != null && llProgressBar.isShown()) {
                mProgressBar.invalidate();
                mProgressBar.setVisibility(View.GONE);
                llProgressBar.setVisibility(View.GONE);
            }
        }
    }

    private BroadcastReceiver mDownloadAndUpdateListener = new BroadcastReceiver() {
        private final String TAG = "LocalBroadcastReceiver-->" + DownloadUpdateActivity.class.getCanonicalName();

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction().equals(Constants.DLOTA_DOWNLOAD_STATUS_EVENT)) {
                stopProgressDialog();
                if (intent != null && intent.getStringExtra(Constants.DLOTA_DOWNLOAD_STATUS).equalsIgnoreCase(Constants.DLOTA_DOWNLOAD_IN_PROGRESS)) {
                    int percentage = intent.getIntExtra(Constants.DLOTA_DOWNLOAD_PERCENTAGE, 0);
                    System.out.println(TAG + "Perc Download: " + percentage);
                    if (llProgressBar != null && !llProgressBar.isShown()) {
                        llProgressBar.setVisibility(View.VISIBLE);
                        mProgressBar.setVisibility(View.VISIBLE);

                    }
                    mProgressBar.setProgress(percentage);
                    if (mTVLoadingText != null) {
                        mTVLoadingText.setText(getResources().getString(R.string.update_screen_update_downloading) + " " + percentage + "%");
                    }

                } else if (intent != null && intent.getStringExtra(Constants.DLOTA_DOWNLOAD_STATUS).equalsIgnoreCase(Constants.DLOTA_DOWNLOAD_SUCCESS)) {

                    shutdownProgressbar();
                    shutdownActivity();

                } else if(intent!=null && intent.getAction().equals(Constants.DDA_CHANGED_STATUS_EVENT)) {
                    //Here to shutdown activitiy on DDA change
                    DownloadUpdateActivity.this.finish();
                } else {
                    //TODO : Need to handle case when download gets fail.
                }
            }
        }
    };

    private void shutdownActivity() {
        //TODO here to perform shutdown tasks of this activity
        DownloadUpdateActivity.this.finish();
    }



    private void shutdownProgressbar() {
        if (mProgressBar != null && mProgressBar.isShown()) {
            mProgressBar.setProgress(0);
            mProgressBar.setVisibility(View.GONE);
        }
        if (llProgressBar != null && llProgressBar.isShown()) {
            llProgressBar.setVisibility(View.GONE);
        }
    }

    private void startProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle(getResources().getString(R.string.progress_dialog_download_update_titile));
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_download_update_msg));
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.show();
    }

    private void stopProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void initUI() {
        Utils.createCustomActionBarTitle(mContext);
        //Init Header
        tvHeader = (TextView) findViewById(R.id.header_text);
        mTVLoadingText = (TextView) findViewById(R.id.loadingText);
        Utils.setHeader(mContext, tvHeader);

        /*Initializing view*/
        btnDownloadAndInstall = (Button) findViewById(R.id.btnDownloadAndInstall);
        mProgressBar = (ProgressBar) findViewById(R.id.progressUpdate);
        llProgressBar = (LinearLayout) findViewById(R.id.llProgressBar);
        mLearnMoreLink = (TextView) findViewById(R.id.tv_learn_more_link);
        mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        mBtnCancelDownload = (Button) findViewById(R.id.btn_cancel_download);
        mUpdateSizeInfoText = (TextView) findViewById(R.id.update_size_text);
        btnDownloadAndInstall.setOnClickListener(this);
        mLearnMoreLink.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mBtnCancelDownload.setOnClickListener(this);
    }
    private void displayInstallationSize(){
        DownloadDescriptorParser downloadDescriptorParser= (DownloadDescriptorParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_DOWNLOAD_DESCRIPTOR);
        String fileSize= FileUtils.formatFileSize(Long.parseLong(downloadDescriptorParser.getMediaObjMeta(ParserConstants.KEY_INSTALL_SIZE)));
        mUpdateSizeInfoText.setText(getResources().getString(R.string.update_size_info,fileSize));

    }

}