package com.movimento.otaclient;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.movimento.service.CapabilityIntentService;
import com.movimento.service.RegistrationIntentService;
import com.movimento.utils.Constants;
import com.movimento.utils.PrefUtils;
import com.movimento.utils.Utils;

import java.util.Timer;
import java.util.TimerTask;

import static com.movimento.utils.Constants.KEY_REGISTRATION_STATUS;
import static com.movimento.utils.Constants.REGISTRATION_STATUS_EVENT;

/*This activity will check with the server if an update is available or not*/

/**
 * The type Checking update activity.
 */
public class CheckingUpdateActivity extends AppCompatActivity {
    private final String TAG = CheckingUpdateActivity.class.getCanonicalName();
    private Context mContext;

    private TextView tvHeader;
    private TextView tvLoadingStatus;
    private TextView tvCheckUpdateHeader;

    private final int INITIAL_DELAY = 5 * 1000; //5 seconds
    private final int TIME_PERIOD = 5 * 1000; //5seconds
    private final int MAX_TIME_OUT = 30; //in seconds
    private int mCounter = 0;
    private Timer mCountdownTimer;

    private AlertDialog mAlertDialog = null;
    private final int TYPE_TIME_OUT = 1001;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checking_update);
        Utils.logMessage(TAG,"IS Registered : "+ PrefUtils.getBoolean(Constants.IS_REGISTERED,false));
        mContext = this;
        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filters = new IntentFilter(Constants.CHECKING_UPDATE_STATUS_EVENT);
        filters.addAction(Constants.DDA_CHANGED_STATUS_EVENT);
        LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).registerReceiver(mCheckingUpdateListener, filters);
        startCountdownTimer();
    }


    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).unregisterReceiver(mCheckingUpdateListener);
        stopCountdownTimer();
        closeDialog();
    }

    private void startCountdownTimer() {
        try {
            if (mCountdownTimer == null) {
                mCountdownTimer = new Timer();
            }
            CheckUpdateScheduler checkUpdateScheduler = new CheckUpdateScheduler();
            mCountdownTimer.scheduleAtFixedRate(checkUpdateScheduler, INITIAL_DELAY, TIME_PERIOD);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopCountdownTimer() {
        if (mCountdownTimer != null) {
            mCountdownTimer.cancel();
            mCountdownTimer = null;
        }
    }

    private void initUI() {
        Utils.createCustomActionBarTitle(mContext);
        tvLoadingStatus = (TextView) findViewById(R.id.tvLoadingStatus);
        tvCheckUpdateHeader = (TextView) findViewById(R.id.tvCheckUpdateHeader);
        tvHeader = (TextView) findViewById(R.id.header_text);
        Utils.setHeader(mContext, tvHeader);
    }


    private BroadcastReceiver mCheckingUpdateListener = new BroadcastReceiver() {
        private final String TAG = "LocalBroadcastReceiver-->" + DownloadUpdateActivity.class.getCanonicalName();

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction().equals(Constants.CHECKING_UPDATE_STATUS_EVENT)) {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        CheckingUpdateActivity.this.finish();
                    }
                }, 2000);

            } else if (intent != null && intent.getAction().equals(REGISTRATION_STATUS_EVENT)) {
                //TODO here to set Registration event to screen.
                String status = intent.getStringExtra(KEY_REGISTRATION_STATUS);
                tvLoadingStatus.setText(status);
                if (status.equals(getResources().getString(R.string.device_registration_failed))) {
                    Log.v("CheckingUpdateActivity", "Device RegistrationStatus:" + status);
                    openDialog(getResources().getString(R.string.checking_update_no_update_availabe), TYPE_TIME_OUT);
                }
            } else if(intent!=null && intent.getAction().equals(Constants.DDA_CHANGED_STATUS_EVENT)) {
                //Here to shutdown activitiy on DDA change
                CheckingUpdateActivity.this.finish();
            }
        }
    };

    private void openDialog(String headerTxt, final int type) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setIcon(R.drawable.app_logo);
        alertDialogBuilder.setTitle(getResources().getString(R.string.dialog_checking_update_title));
        alertDialogBuilder.setMessage(headerTxt);
        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.dialog_button_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (type == TYPE_TIME_OUT) {
                            CheckingUpdateActivity.this.finish();
                        }
                    }
                });

        mAlertDialog = alertDialogBuilder.create();
        mAlertDialog.show();
    }

    private void closeDialog() {
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.cancel();
        }
    }

    private class CheckUpdateScheduler extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.v(TAG, "Inner Run...");
                    mCounter = mCounter + 5;
                    if (mCounter >= MAX_TIME_OUT) {
                        mCounter = 0;
                        //Toast.makeText(mContext, "TIME OUT", Toast.LENGTH_SHORT).show();
                        Log.v(TAG, "TIME OUT");
                        tvCheckUpdateHeader.setText(getResources().getString(R.string.checking_update_no_update_availabe));
                        openDialog(getResources().getString(R.string.checking_update_no_update_availabe), TYPE_TIME_OUT);
                    } else if (!Utils.isNetworkAvailable()) {
                        tvCheckUpdateHeader.setText(getResources().getString(R.string.connection_error_header));
                        Utils.sendError(mContext, Constants.ERROR_TYPE_NETWORK_CONNECTION, getResources().getString(R.string.connection_error_header), "");
                        finish();
                    } else if (!PrefUtils.getBoolean(Constants.IS_REGISTERED, false)) {
                        Intent intent = new Intent(OTAClientApp.getmAppInstance(), RegistrationIntentService.class);
                        OTAClientApp.getmAppInstance().startService(intent);

                        //Device Not Registered
                        Log.v(TAG, "Device Not Registered!");
                        tvCheckUpdateHeader.setText(getResources().getString(R.string.device_not_registered_msg));
                    } else {
                        Intent intent = new Intent(OTAClientApp.getmAppInstance(), CapabilityIntentService.class);
                        OTAClientApp.getmAppInstance().startService(intent);
                    }
                }
            });
        }
    }
}