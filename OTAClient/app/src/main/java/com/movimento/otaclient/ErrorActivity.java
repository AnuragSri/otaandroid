package com.movimento.otaclient;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.movimento.utils.Constants;
import com.movimento.utils.PrefUtils;
import com.movimento.utils.Utils;


/**
 * The type Error activity.
 */
public class ErrorActivity extends AppCompatActivity implements View.OnClickListener {
    private final String TAG = ErrorActivity.class.getCanonicalName();
    private Context mContext;

    private Button btnErrorOk;
    private TextView tvErrorHeader;
    private TextView tvErrorInfo;
    private TextView tvHeader;
    private long mLastClickTime = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.error_activity);
        mContext = this;
        initUI();
        initIntent(getIntent());
    }

    private void initIntent(Intent intent) {
        String errorType = intent.getStringExtra(Constants.KEY_ERRROR_TYPE);
        String errorHeader = intent.getStringExtra(Constants.KEY_ERROR_HEADER);
        String errorInfo = intent.getStringExtra(Constants.KEY_ERROR_INFO);

        tvErrorHeader.setText(errorHeader);
        tvErrorInfo.setText(errorInfo);
    }

    @Override
    public void onClick(View view) {
        /*if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();*/

        if (view == btnErrorOk) {
            PrefUtils.putLong(Constants.CERTIFICATE_ERROR_TIME,System.currentTimeMillis());
            finish();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(Constants.DDA_CHANGED_STATUS_EVENT);
        LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).registerReceiver(mDDACheckUpdateListener, filter);
    }


    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).unregisterReceiver(mDDACheckUpdateListener);
    }

    private BroadcastReceiver mDDACheckUpdateListener = new BroadcastReceiver() {
        private final String TAG = "LocalBroadcastReceiver-->" + DownloadUpdateActivity.class.getCanonicalName();

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction().equals(Constants.DDA_CHANGED_STATUS_EVENT)) {
                ErrorActivity.this.finish();
            }
        }
    };

    private void initUI() {
        Utils.createCustomActionBarTitle(mContext);
        tvHeader = (TextView) findViewById(R.id.header_text);
        Utils.setHeader(mContext, tvHeader);

        tvErrorHeader = (TextView) findViewById(R.id.tvErrorHeader);
        tvErrorInfo = (TextView) findViewById(R.id.tvErrorInfo);
        btnErrorOk = (Button) findViewById(R.id.btnErrorOK);

        btnErrorOk.setOnClickListener(this);
    }
}
