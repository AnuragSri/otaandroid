package com.movimento.otaclient;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.movimento.utils.Utils;

import java.util.GregorianCalendar;

/**
 * The type Alert dialog activity.
 */
public class AlertDialogActivity extends Activity {

    private DownloadUpdateOrRemindActivity mDownloadUpdateOrRemindActivity;
    private Context mContext;
    private android.app.AlertDialog.Builder builder;
    private AlarmManager alarmManager;
    private android.app.AlertDialog levelDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        showAlert();
    }

    /**
     * Show alert.
     */
    public void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder

                .setMessage("Are you ready to install the updates?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Intent newIntent = new Intent(getApplicationContext(), DownloadUpdateOrRemindActivity.class);
                        mContext.startActivity(newIntent);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        AlertDialogView(mContext);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    /**
     * Alert dialog view.
     *
     * @param context the context
     */
    public void AlertDialogView(final Context context) {

        final CharSequence[] items = {" 1 Hour ", " 6 Hour ", " 1 Day ", " 1 Week "};

        builder = new android.app.AlertDialog.Builder(context, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

        builder.setTitle("Remind me in");
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        // Your code when first option selected
                        Utils.setReminderFlag(true, 1, context);
                        //1*60*60*1000
                        scheduleAlarm(8 * 1000);
                        finish();


                        break;
                    case 1:
                        // Your code when 2nd  option selected
                        Utils.setReminderFlag(true, 6, context);
                        // 6*60*60*1000
                        scheduleAlarm(8 * 1000);
                        finish();

                        break;
                    case 2:
                        // Your code when 3rd option selected
                        Utils.setReminderFlag(true, 10, context);
                        // 10*60*60*1000
                        scheduleAlarm(8 * 1000);
                        finish();
                        break;
                    case 3:
                        // Your code when 4th  option selected
                        Utils.setReminderFlag(true, 12, context);
                        // 7*24*60*60*1000
                        scheduleAlarm(8 * 1000);
                        finish();

                        break;

                }
                levelDialog.dismiss();
            }
        });
        levelDialog = builder.create();
        levelDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        levelDialog.show();


    }

    /**
     * Schedule alarm.
     *
     * @param duration the duration
     */
    public void scheduleAlarm(int duration) {

        Long time = new GregorianCalendar().getTimeInMillis() + duration;
        Intent intentAlarm = new Intent(this, AlarmReciever.class);
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        alarmManager.set(AlarmManager.RTC_WAKEUP, time, PendingIntent.getBroadcast(this, 1,
                intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));

        Toast.makeText(this, "Alarm is set", Toast.LENGTH_LONG).show();

    }


}
