package com.movimento.otaclient;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.movimento.utils.Constants;
import com.movimento.utils.Utils;

/*This Activity will be displayed when update is successfully installed*/

/**
 * The type Success activity.
 */
public class SuccessActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    private TextView tvHeader;
    private Button mOkButton;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_success);
        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(Constants.DDA_CHANGED_STATUS_EVENT);
        LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).registerReceiver(mDDACheckUpdateListener, filter);
    }


    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).unregisterReceiver(mDDACheckUpdateListener);
    }

    @Override
    public void onClick(View view) {
       /* if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();*/

        if (view == mOkButton) {
            ActivityCompat.finishAffinity(this);
        }

    }

    private BroadcastReceiver mDDACheckUpdateListener = new BroadcastReceiver() {
        private final String TAG = "LocalBroadcastReceiver-->" + DownloadUpdateActivity.class.getCanonicalName();

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction().equals(Constants.DDA_CHANGED_STATUS_EVENT)) {
                SuccessActivity.this.finish();
            }
        }
    };

    private void initUI() {
        Utils.createCustomActionBarTitle(mContext);
        //Init Header
        tvHeader = (TextView) findViewById(R.id.header_text);
        Utils.setHeader(mContext, tvHeader);

        /*On Ok button application will be closed*/
        mOkButton = (Button) findViewById(R.id.ok_button);
        mOkButton.setOnClickListener(this);

    }
}
