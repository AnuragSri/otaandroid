package com.movimento.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.movimento.http.ConnectionHandler;
import com.movimento.http.request.model.RequestModel;


/**
 * The type Http client async task.
 */
public class HttpClientAsyncTask extends AsyncTask<String, Integer, String> {
    private final String TAG = HttpClientAsyncTask.class.getCanonicalName();
    private RequestModel mRequestModel;


    /**
     * Instantiates a new Http client async task.
     *
     * @param requestModel the request model
     */
    public HttpClientAsyncTask(RequestModel requestModel) {
        this.mRequestModel = requestModel;
    }

    @Override
    protected String doInBackground(String... params) {
        String responseString = null;
        try {
            ConnectionHandler handler = new ConnectionHandler();
            responseString = handler.postRequest(mRequestModel);
            Log.v(TAG, "Response: " + responseString);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseString;
    }
}
