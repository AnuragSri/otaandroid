package com.movimento.http.response.model;

import java.util.List;

/**
 * The type Cmd response model.
 */
public class CmdResponseModel {
    private List<List<String>> CMD;

    /**
     * Gets cmd.
     *
     * @return the cmd
     */
    public List<List<String>> getCMD() {
        return CMD;
    }

    /**
     * Sets cmd.
     *
     * @param CMD the cmd
     */
    public void setCMD(List<List<String>> CMD) {
        this.CMD = CMD;
    }
}
