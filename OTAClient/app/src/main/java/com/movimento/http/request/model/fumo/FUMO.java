package com.movimento.http.request.model.fumo;


/**
 * The type Fumo.
 */
public class FUMO {
    private String PkgName;
    private String PkgVersion;
    private DownloadAndUpdate DownloadAndUpdate;
    private String State;

    /**
     * Gets pkg name.
     *
     * @return the pkg name
     */
    public String getPkgName() {
        return PkgName;
    }

    /**
     * Sets pkg name.
     *
     * @param pkgName the pkg name
     */
    public void setPkgName(String pkgName) {
        PkgName = pkgName;
    }

    /**
     * Gets pkg version.
     *
     * @return the pkg version
     */
    public String getPkgVersion() {
        return PkgVersion;
    }

    /**
     * Sets pkg version.
     *
     * @param pkgVersion the pkg version
     */
    public void setPkgVersion(String pkgVersion) {
        PkgVersion = pkgVersion;
    }

    /**
     * Gets download and update.
     *
     * @return the download and update
     */
    public com.movimento.http.request.model.fumo.DownloadAndUpdate getDownloadAndUpdate() {
        return DownloadAndUpdate;
    }

    /**
     * Sets download and update.
     *
     * @param downloadAndUpdate the download and update
     */
    public void setDownloadAndUpdate(com.movimento.http.request.model.fumo.DownloadAndUpdate downloadAndUpdate) {
        DownloadAndUpdate = downloadAndUpdate;
    }

    /**
     * Gets state.
     *
     * @return the state
     */
    public String getState() {
        return State;
    }

    /**
     * Sets state.
     *
     * @param state the state
     */
    public void setState(String state) {
        State = state;
    }
}
