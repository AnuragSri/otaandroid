package com.movimento.http.request.model.registration;

import com.google.gson.Gson;
import com.movimento.http.request.model.MO;
import com.movimento.motreeparser.DeviceInfoParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.utils.Constants;
import com.movimento.utils.EncryptionUtils;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.Utils;

import org.json.JSONException;

import java.io.IOException;

/**
 * The type Registration model.
 */
public class RegistrationModel extends MO {
    private String ClientURI;
    private RegMOData MOData;

    /**
     * Gets client uri.
     *
     * @return the client uri
     */
    public String getClientURI() {
        return ClientURI;
    }

    /**
     * Sets client uri.
     *
     * @param clientURI the client uri
     */
    public void setClientURI(String clientURI) {
        ClientURI = clientURI;
    }

    /**
     * Gets mo data.
     *
     * @return the mo data
     */
    public RegMOData getMOData() {
        return MOData;
    }

    /**
     * Sets mo data.
     *
     * @param MOData the mo data
     */
    public void setMOData(RegMOData MOData) {
        this.MOData = MOData;
    }

    @Override
    public String createJson() {

        String requestJsonBody = Utils.loadJSONFromAsset(OTAClientApp.getmAppInstance(), "registration.txt");
        RegistrationModel registerRequest = new Gson().fromJson(requestJsonBody, RegistrationModel.class);
        // TODO : fill data from management tree here
        //TODO get the VIN number for  sharedPrefs
        DeviceInfoParser deviceInfoParser = (DeviceInfoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_DEVICE_INFO);
        registerRequest.setClientURI(deviceInfoParser.getParentNodeDFType());

        RegDevInfo regDevInfo = registerRequest.getMOData().getDevInfo();
        RegExt regExt = regDevInfo.getExt();
        try {
            regDevInfo.setDevID(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_DEV_ID));
            regDevInfo.setMan(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_MAN));
            regDevInfo.setMod(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_MOD));
            regDevInfo.setDmV(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_DMV));
            regDevInfo.setLang(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_LANG));
            regDevInfo.setDevType(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_DEV_TYPE));
            regDevInfo.setOEM(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_OEM));
            regDevInfo.setFwV(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_FWV));
            regDevInfo.setSwV(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_SWV));
            regDevInfo.setHwV(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_HWV));

            String encyptedVIN = EncryptionUtils.encryptKey(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_VIN), EncryptionUtils.getPrivateKey("private.key"));
            regExt.setVIN(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_VIN));

            //TODO : get encrypted vin from mo tree
            regExt.setEncryptedVIN(encyptedVIN/*deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_ENCRYPTED_VIN)*/);
            regExt.setCompressTech(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_COMPRESS_TECH));
            registerRequest.getMOData().setDevInfo(regDevInfo);
            regDevInfo.setExt(regExt);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new Gson().toJson(registerRequest, RegistrationModel.class);
    }


}
