package com.movimento.http.request.model.alert;

import com.google.gson.Gson;
import com.movimento.http.request.model.MO;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.utils.Utils;

import java.util.List;


/**
 * The type Alert model.
 */
public class AlertModel extends MO {
    private List<Alert> Alert;

    /**
     * Gets alert.
     *
     * @return the alert
     */
    public List<com.movimento.http.request.model.alert.Alert> getAlert() {
        return Alert;
    }

    /**
     * Sets alert.
     *
     * @param alert the alert
     */
    public void setAlert(List<com.movimento.http.request.model.alert.Alert> alert) {
        Alert = alert;
    }

    @Override
    public String createJson() {
        String requestJsonBody = Utils.loadJSONFromAsset(OTAClientApp.getmAppInstance(), "alert.txt");
        AlertModel alertModel = new Gson().fromJson(requestJsonBody, AlertModel.class);
        // TODO : fill data from management tree here
        return new Gson().toJson(alertModel, AlertModel.class);
    }
}
