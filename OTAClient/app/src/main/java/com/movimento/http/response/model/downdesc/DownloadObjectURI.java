package com.movimento.http.response.model.downdesc;

/**
 * The type Download object uri.
 */
public class DownloadObjectURI {
    private String server;

    /**
     * Gets server.
     *
     * @return the server
     */
    public String getServer() {
        return server;
    }

    /**
     * Sets server.
     *
     * @param server the server
     */
    public void setServer(String server) {
        this.server = server;
    }
}
