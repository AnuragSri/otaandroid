package com.movimento.http;

import com.movimento.http.handler.HandlerFactory;
import com.movimento.http.handler.ResponseHandler;
import com.movimento.http.request.model.MOFactory;
import com.movimento.http.request.model.RequestModel;
import com.movimento.utils.Constants;

import java.util.Map;

import okhttp3.Headers;

import static com.movimento.utils.Constants.BASE_URL;
import static com.movimento.utils.Constants.URL_ALERT_NOTIF;
import static com.movimento.utils.Constants.URL_CAPABILITY;
import static com.movimento.utils.Constants.URL_DEVICE_FUMO;
import static com.movimento.utils.Constants.URL_DEVICE_INFO;
import static com.movimento.utils.Constants.URL_DEVICE_LAWMO;
import static com.movimento.utils.Constants.URL_REGISTRATION;


/**
 * The type Request utils.
 */
public class RequestUtils {
    private final String TAG = RequestUtils.class.getCanonicalName();

    private RequestUtils() {
    }

    /**
     * Create request request model.
     *
     * @param type the type
     * @return the request model
     */
    public static RequestModel createRequest(int type) {
        String URL = getUrl(type);
        String jsonBody = MOFactory.getInstance().getMO(type).createJson();
        Map<String, String> mapHeaders = RequestHeader.getInstance().getHeaders(type);
        ResponseHandler responseHandler = HandlerFactory.getInstance().getResponseHandler(type);
        Headers headers = getHeaders(mapHeaders);

        RequestModel reqModel = new RequestModel(URL, jsonBody, headers, responseHandler);
        return reqModel;
    }

    private static String getUrl(int MOType) {
        String url = null;

        switch (MOType) {
            case Constants.MO_TYPE_REGISTRATION:
                url = BASE_URL + URL_REGISTRATION;
                break;
            case Constants.MO_TYPE_DEV_INFO:
                url = BASE_URL + URL_DEVICE_INFO;
                break;
            case Constants.MO_TYPE_FIRMWARE:
                url = BASE_URL + URL_DEVICE_FUMO;
                break;
            case Constants.MO_TYPE_LOCK_AND_WIPE:
                url = BASE_URL + URL_DEVICE_LAWMO;
                break;

            case Constants.MO_TYPE_ALERT:
                url = BASE_URL + URL_ALERT_NOTIF;
                break;
            case Constants.MO_TYPE_CAPABILITY:
                url = BASE_URL + URL_CAPABILITY;
                break;
            default:
                break;
        }
        return url;
    }

    /**
     * Gets headers.
     *
     * @param mapHeaders the map headers
     * @return the headers
     */
    private static Headers getHeaders(Map<String, String> mapHeaders) {
        Map<String, String> someStrings = mapHeaders;
        Headers.Builder hb = new Headers.Builder();
        for (Map.Entry<String, String> entry : someStrings.entrySet()) {
            hb.add(entry.getKey(), entry.getValue());
        }
        return hb.build();
    }
}
