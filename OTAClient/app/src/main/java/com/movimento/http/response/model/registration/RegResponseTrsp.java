package com.movimento.http.response.model.registration;

/**
 * The type Reg response trsp.
 */
public class RegResponseTrsp {

    private String Data;
    private String Secret;

    /**
     * Gets secret.
     *
     * @return the secret
     */
    public String getSecret() {
        return Secret;
    }

    /**
     * Sets secret.
     *
     * @param secret the secret
     */
    public void setSecret(String secret) {
        Secret = secret;
    }

    /**
     * Gets data.
     *
     * @return the data
     */
    public String getData() {
        return Data;
    }

    /**
     * Sets data.
     *
     * @param data the data
     */
    public void setData(String data) {
        Data = data;
    }


}
