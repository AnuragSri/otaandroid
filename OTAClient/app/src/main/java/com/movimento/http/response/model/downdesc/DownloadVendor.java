package com.movimento.http.response.model.downdesc;


/**
 * The type Download vendor.
 */
public class DownloadVendor {
    private String name;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }
}
