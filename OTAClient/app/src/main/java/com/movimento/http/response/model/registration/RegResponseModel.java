package com.movimento.http.response.model.registration;

/**
 * The type Reg response model.
 */
public class RegResponseModel {
    private String ClientURI;
    private RegResponseMoData MOData;

    /**
     * Gets client uri.
     *
     * @return the client uri
     */
    public String getClientURI() {
        return ClientURI;
    }

    /**
     * Sets client uri.
     *
     * @param clientURI the client uri
     */
    public void setClientURI(String clientURI) {
        ClientURI = clientURI;
    }

    /**
     * Gets mo data.
     *
     * @return the mo data
     */
    public RegResponseMoData getMOData() {
        return MOData;
    }

    /**
     * Sets mo data.
     *
     * @param MOData the mo data
     */
    public void setMOData(RegResponseMoData MOData) {
        this.MOData = MOData;
    }
}
