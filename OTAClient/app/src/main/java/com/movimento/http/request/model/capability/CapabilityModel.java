package com.movimento.http.request.model.capability;

import com.google.gson.Gson;
import com.movimento.http.request.model.MO;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.utils.Utils;

import java.util.List;


/**
 * The type Capability model.
 */
public class CapabilityModel extends MO {

    private List<CapabilityMOS> MOS;

    /**
     * Gets mos.
     *
     * @return the mos
     */
    public List<CapabilityMOS> getMOS() {
        return MOS;
    }

    /**
     * Sets mos.
     *
     * @param MOS the mos
     */
    public void setMOS(List<CapabilityMOS> MOS) {
        this.MOS = MOS;
    }

    @Override
    public String createJson() {
        String requestJsonBody = Utils.loadJSONFromAsset(OTAClientApp.getmAppInstance(), "capability.txt");
        CapabilityModel capabilityModel = new Gson().fromJson(requestJsonBody, CapabilityModel.class);
        // TODO : fill data from management tree here
        return new Gson().toJson(capabilityModel, CapabilityModel.class);
    }
}
