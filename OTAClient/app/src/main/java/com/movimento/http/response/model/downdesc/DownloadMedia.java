package com.movimento.http.response.model.downdesc;

/**
 * The type Download media.
 */
public class DownloadMedia {
    private DownloadVendor vendor;
    private DownloadProduct product;

    /**
     * Gets vendor.
     *
     * @return the vendor
     */
    public DownloadVendor getVendor() {
        return vendor;
    }

    /**
     * Sets vendor.
     *
     * @param vendor the vendor
     */
    public void setVendor(DownloadVendor vendor) {
        this.vendor = vendor;
    }

    /**
     * Gets product.
     *
     * @return the product
     */
    public DownloadProduct getProduct() {
        return product;
    }

    /**
     * Sets product.
     *
     * @param product the product
     */
    public void setProduct(DownloadProduct product) {
        this.product = product;
    }
}
