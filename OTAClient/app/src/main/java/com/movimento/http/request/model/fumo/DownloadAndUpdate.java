package com.movimento.http.request.model.fumo;


/**
 * The type Download and update.
 */
public class DownloadAndUpdate {
    private String PkgURL;

    /**
     * Gets pkg url.
     *
     * @return the pkg url
     */
    public String getPkgURL() {
        return PkgURL;
    }

    /**
     * Sets pkg url.
     *
     * @param pkgURL the pkg url
     */
    public void setPkgURL(String pkgURL) {
        PkgURL = pkgURL;
    }
}
