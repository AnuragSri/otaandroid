package com.movimento.http.request.model;

/**
 * The type Mo.
 */
public abstract class MO {
    /**
     * Create json string.
     *
     * @return the string
     */
    public abstract String createJson();
}
