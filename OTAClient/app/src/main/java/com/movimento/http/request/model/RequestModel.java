package com.movimento.http.request.model;

import com.movimento.http.handler.ResponseHandler;

import okhttp3.Headers;


/**
 * The type Request model.
 */
public class RequestModel {

    /**
     * The Json body.
     */
    String jsonBody;
    /**
     * The Response handler.
     */
    ResponseHandler responseHandler;
    /**
     * The M headers.
     */
    Headers mHeaders;
    /**
     * The Url.
     */
    String url;


    /**
     * Instantiates a new Request model.
     *
     * @param url             the url
     * @param jsonBody        the json body
     * @param headers         the headers
     * @param responseHandler the response handler
     */
    public RequestModel(String url, String jsonBody, Headers headers, ResponseHandler responseHandler) {
        this.jsonBody = jsonBody;
        this.mHeaders = headers;
        this.responseHandler = responseHandler;
        this.url = url;
    }

    /**
     * Gets json body.
     *
     * @return the json body
     */
    public String getJsonBody() {
        return jsonBody;
    }

    /**
     * Sets json body.
     *
     * @param jsonBody the json body
     */
    public void setJsonBody(String jsonBody) {
        this.jsonBody = jsonBody;
    }

    /**
     * Gets response handler.
     *
     * @return the response handler
     */
    public ResponseHandler getResponseHandler() {
        return responseHandler;
    }

    /**
     * Sets response handler.
     *
     * @param responseHandler the response handler
     */
    public void setResponseHandler(ResponseHandler responseHandler) {
        this.responseHandler = responseHandler;
    }

    /**
     * Gets headers.
     *
     * @return the headers
     */
    public Headers getmHeaders() {
        return mHeaders;
    }

    /**
     * Sets headers.
     *
     * @param mHeaders the m headers
     */
    public void setmHeaders(Headers mHeaders) {
        this.mHeaders = mHeaders;
    }

    /**
     * Gets url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets url.
     *
     * @param url the url
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
