package com.movimento.http.response.model.downdesc;


/**
 * The type Download desc model.
 */
public class DownloadDescModel {
    private DownloadMedia media;

    /**
     * Gets media.
     *
     * @return the media
     */
    public DownloadMedia getMedia() {
        return media;
    }

    /**
     * Sets media.
     *
     * @param media the media
     */
    public void setMedia(DownloadMedia media) {
        this.media = media;
    }
}
