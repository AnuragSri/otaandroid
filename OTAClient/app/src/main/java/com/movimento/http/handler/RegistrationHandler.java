package com.movimento.http.handler;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;
import com.movimento.http.response.model.RespErrorModel;
import com.movimento.http.response.model.registration.RegResponseModel;
import com.movimento.motreeparser.DevAccParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.otaclient.R;
import com.movimento.service.AlertIntentService;
import com.movimento.utils.Constants;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.PrefUtils;

import org.json.JSONException;

import static com.movimento.utils.Constants.KEY_REGISTRATION_STATUS;

/**
 * The type Registration handler.
 */
public class RegistrationHandler implements ResponseHandler {
    private final String TAG = RegistrationHandler.class.getCanonicalName();

    @Override
    public void handleResponse(String data) {
        //We have to process the response data and execute the command.
        // Here we parse the command and send it to Blocked queue to execute.
        Log.v(TAG, "Response Data: " + data);
        if (data != null && !data.isEmpty()) {
            sendRegistrationBroadcast(OTAClientApp.getmAppInstance().getResources().getString(R.string.device_registration_completed));
            RegResponseModel regResponseModel = new Gson().fromJson(data, RegResponseModel.class);
            if (regResponseModel != null) {
                PrefUtils.putBoolean(Constants.IS_REGISTERED, true);
                String clientURI = regResponseModel.getClientURI();
                String srvID = regResponseModel.getMOData().getDmAcc().getSrvID();
                String srvURI = regResponseModel.getMOData().getDmAcc().getSrvURI();
                String secret = regResponseModel.getMOData().getDmAcc().getAuth().getTrsp().getSecret();
                String authData = regResponseModel.getMOData().getDmAcc().getAuth().getTrsp().getData();
                Log.v(TAG, "SrvID: " + srvID);
                Log.v(TAG, "SrvURI: " + srvURI);
                Log.v(TAG, "Secret: " + secret);
                Log.v(TAG, "Data: " + authData);
                if (srvID != null && !srvID.isEmpty())
                    PrefUtils.putString(Constants.REG_SRV_ID, srvID);

                if (srvURI != null && !srvURI.isEmpty())
                    PrefUtils.putString(Constants.REG_SRV_URI, srvURI);

                if (secret != null && !secret.isEmpty())
                    PrefUtils.putString(Constants.REGI_AUTH_TRSP_SECRET, secret);

                if (authData != null && !authData.isEmpty())
                    PrefUtils.putString(Constants.REG_AUTH_TRSP_DATA, authData);

                updateDevAccMOTree(clientURI, srvID, srvURI, secret, authData);

                sendBootstrapCompleteAlert();
            }
        }
    }

    private void sendRegistrationBroadcast(String status) {
        //Sending broadcast of registration to Checking Update Activity.
        Intent sendUpdateStatusIntent = new Intent();
        sendUpdateStatusIntent.setAction(Constants.REGISTRATION_STATUS_EVENT);
        sendUpdateStatusIntent.putExtra(KEY_REGISTRATION_STATUS, status);
        LocalBroadcastManager.getInstance(OTAClientApp.getmAppInstance()).sendBroadcast(sendUpdateStatusIntent);
    }

    private void updateDevAccMOTree(String clientURI, String srvID, String srvURI, String secret, String authData) {
        DevAccParser devAccParser = (DevAccParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_DEVICE_ACCOUNT);
        try {
            devAccParser.setParentDFTitle(clientURI);
            devAccParser.setNodeDFTitle(ParserConstants.KEY_NODE_SRVID, srvID);
            devAccParser.setNodeDFTitle(ParserConstants.KEY_NODE_SRVURI, srvURI);
            devAccParser.setNodeDFTitle(ParserConstants.KEY_NODE_AUTH_DATA, authData);
            devAccParser.setNodeDFTitle(ParserConstants.KEY_NODE_SECRET, secret);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleError(String data) {
        if (data != null && !data.isEmpty()) {
            sendRegistrationBroadcast(OTAClientApp.getmAppInstance().getResources().getString(R.string.device_registration_failed));
            RespErrorModel respErrorModel = new Gson().fromJson(data, RespErrorModel.class);
            if (respErrorModel != null) {
                String errorCode = respErrorModel.getResponsecode();
                String description = respErrorModel.getDescription();
                Log.v(TAG, "ErrorCode: " + errorCode);
                Log.v(TAG, "Description: " + description);
            }
        }
    }

    private void sendBootstrapCompleteAlert() {
        Intent intent = new Intent(OTAClientApp.getmAppInstance(), AlertIntentService.class);
        intent.putExtra(Constants.KEY_ALERT_NOTIFICATION_TYPE, Constants.ALERT_TYPE_REGISTRATION);
        intent.putExtra(Constants.KEY_ALERT_TYPE, Constants.REG_ALERT_TYPE);
        intent.putExtra(Constants.KEY_ALERT_RESULT_CODE, Constants.REG_ALERT_RESULT_CODE);
        //  intent.putExtra(Constants.KEY_ALERT_STATE, Constants.REG_STATE);
        OTAClientApp.getmAppInstance().startService(intent);
    }
}
