package com.movimento.http.request.model.registration;

/**
 * The type Reg ext.
 */
public class RegExt {
    private String VIN;
    private String EncryptedVIN;
    private String CompressTech;
    private String CarModel;

    /**
     * Gets vin.
     *
     * @return the vin
     */
    public String getVIN() {
        return VIN;
    }

    /**
     * Sets vin.
     *
     * @param VIN the vin
     */
    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    /**
     * Gets encrypted vin.
     *
     * @return the encrypted vin
     */
    public String getEncryptedVIN() {
        return EncryptedVIN;
    }

    /**
     * Sets encrypted vin.
     *
     * @param encryptedVIN the encrypted vin
     */
    public void setEncryptedVIN(String encryptedVIN) {
        EncryptedVIN = encryptedVIN;
    }

    /**
     * Gets compress tech.
     *
     * @return the compress tech
     */
    public String getCompressTech() {
        return CompressTech;
    }

    /**
     * Sets compress tech.
     *
     * @param compressTech the compress tech
     */
    public void setCompressTech(String compressTech) {
        CompressTech = compressTech;
    }

    /**
     * Gets car model.
     *
     * @return the car model
     */
    public String getCarModel() {
        return CarModel;
    }

    /**
     * Sets car model.
     *
     * @param carModel the car model
     */
    public void setCarModel(String carModel) {
        CarModel = carModel;
    }

}
