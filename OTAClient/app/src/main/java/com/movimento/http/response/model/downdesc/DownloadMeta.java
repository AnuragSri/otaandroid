package com.movimento.http.response.model.downdesc;

/**
 * The type Download meta.
 */
public class DownloadMeta {
    private String name;
    private String installNotifyURI;
    private String suppressUserConfirmation;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets install notify uri.
     *
     * @return the install notify uri
     */
    public String getInstallNotifyURI() {
        return installNotifyURI;
    }

    /**
     * Sets install notify uri.
     *
     * @param installNotifyURI the install notify uri
     */
    public void setInstallNotifyURI(String installNotifyURI) {
        this.installNotifyURI = installNotifyURI;
    }

    /**
     * Gets suppress user confirmation.
     *
     * @return the suppress user confirmation
     */
    public String getSuppressUserConfirmation() {
        return suppressUserConfirmation;
    }

    /**
     * Sets suppress user confirmation.
     *
     * @param suppressUserConfirmation the suppress user confirmation
     */
    public void setSuppressUserConfirmation(String suppressUserConfirmation) {
        this.suppressUserConfirmation = suppressUserConfirmation;
    }
}
