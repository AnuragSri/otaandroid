package com.movimento.http.request.model.fumo;


/**
 * The type Fumo status.
 */
public class FumoStatus {
    private int sc;

    /**
     * Gets sc.
     *
     * @return the sc
     */
    public int getSc() {
        return sc;
    }

    /**
     * Sets sc.
     *
     * @param sc the sc
     */
    public void setSc(int sc) {
        this.sc = sc;
    }
}
