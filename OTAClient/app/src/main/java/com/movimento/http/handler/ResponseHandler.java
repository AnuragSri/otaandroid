package com.movimento.http.handler;

/**
 * The interface Response handler.
 */
public interface ResponseHandler {
    /**
     * Handle response.
     *
     * @param data the data
     */
    void handleResponse(String data);

    /**
     * Handle error.
     *
     * @param data the data
     */
//TODO : Error implementation.
    void handleError(String data);
}
