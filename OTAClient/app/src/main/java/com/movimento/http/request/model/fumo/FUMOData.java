package com.movimento.http.request.model.fumo;


/**
 * The type Fumo data.
 */
public class FUMOData {
    private FUMO fumo;

    /**
     * Gets fumo.
     *
     * @return the fumo
     */
    public FUMO getFumo() {
        return fumo;
    }

    /**
     * Sets fumo.
     *
     * @param fumo the fumo
     */
    public void setFumo(FUMO fumo) {
        this.fumo = fumo;
    }
}
