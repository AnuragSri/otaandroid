package com.movimento.http.response.model.registration;

/**
 * The type Reg response dm acc.
 */
public class RegResponseDMAcc {
    private String SrvURI;
    private RegResponseAuth Auth;
    private String SrvID;

    /**
     * Gets srv uri.
     *
     * @return the srv uri
     */
    public String getSrvURI() {
        return SrvURI;
    }

    /**
     * Sets srv uri.
     *
     * @param srvURI the srv uri
     */
    public void setSrvURI(String srvURI) {
        SrvURI = srvURI;
    }

    /**
     * Gets auth.
     *
     * @return the auth
     */
    public RegResponseAuth getAuth() {
        return Auth;
    }

    /**
     * Sets auth.
     *
     * @param auth the auth
     */
    public void setAuth(RegResponseAuth auth) {
        Auth = auth;
    }

    /**
     * Gets srv id.
     *
     * @return the srv id
     */
    public String getSrvID() {
        return SrvID;
    }

    /**
     * Sets srv id.
     *
     * @param srvID the srv id
     */
    public void setSrvID(String srvID) {
        SrvID = srvID;
    }
}
