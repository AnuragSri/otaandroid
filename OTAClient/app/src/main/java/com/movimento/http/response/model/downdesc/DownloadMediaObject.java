package com.movimento.http.response.model.downdesc;

/**
 * The type Download media object.
 */
public class DownloadMediaObject {
    private DownloadMeta meta;
    private String size;
    private String installSize;
    private String type;
    private String objectID;
    private String objectVersion;
    private String progressiveDownloadFlag;
    private String objectValidityTime;
    private DownloadObjectURI objectURI;

    /**
     * Gets meta.
     *
     * @return the meta
     */
    public DownloadMeta getMeta() {
        return meta;
    }

    /**
     * Sets meta.
     *
     * @param meta the meta
     */
    public void setMeta(DownloadMeta meta) {
        this.meta = meta;
    }

    /**
     * Gets size.
     *
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * Sets size.
     *
     * @param size the size
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * Gets install size.
     *
     * @return the install size
     */
    public String getInstallSize() {
        return installSize;
    }

    /**
     * Sets install size.
     *
     * @param installSize the install size
     */
    public void setInstallSize(String installSize) {
        this.installSize = installSize;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets object id.
     *
     * @return the object id
     */
    public String getObjectID() {
        return objectID;
    }

    /**
     * Sets object id.
     *
     * @param objectID the object id
     */
    public void setObjectID(String objectID) {
        this.objectID = objectID;
    }

    /**
     * Gets object version.
     *
     * @return the object version
     */
    public String getObjectVersion() {
        return objectVersion;
    }

    /**
     * Sets object version.
     *
     * @param objectVersion the object version
     */
    public void setObjectVersion(String objectVersion) {
        this.objectVersion = objectVersion;
    }

    /**
     * Gets progressive download flag.
     *
     * @return the progressive download flag
     */
    public String getProgressiveDownloadFlag() {
        return progressiveDownloadFlag;
    }

    /**
     * Sets progressive download flag.
     *
     * @param progressiveDownloadFlag the progressive download flag
     */
    public void setProgressiveDownloadFlag(String progressiveDownloadFlag) {
        this.progressiveDownloadFlag = progressiveDownloadFlag;
    }

    /**
     * Gets object validity time.
     *
     * @return the object validity time
     */
    public String getObjectValidityTime() {
        return objectValidityTime;
    }

    /**
     * Sets object validity time.
     *
     * @param objectValidityTime the object validity time
     */
    public void setObjectValidityTime(String objectValidityTime) {
        this.objectValidityTime = objectValidityTime;
    }

    /**
     * Gets object uri.
     *
     * @return the object uri
     */
    public DownloadObjectURI getObjectURI() {
        return objectURI;
    }

    /**
     * Sets object uri.
     *
     * @param objectURI the object uri
     */
    public void setObjectURI(DownloadObjectURI objectURI) {
        this.objectURI = objectURI;
    }
}
