package com.movimento.http.request.model.alert;


/**
 * The type Data.
 */
public class Data {
    private int ResultCode;
    private String State;

    /**
     * Gets result code.
     *
     * @return the result code
     */
    public int getResultCode() {
        return ResultCode;
    }

    /**
     * Sets result code.
     *
     * @param resultCode the result code
     */
    public void setResultCode(int resultCode) {
        ResultCode = resultCode;
    }

    /**
     * Gets state.
     *
     * @return the state
     */
    public String getState() {
        return State;
    }

    /**
     * Sets state.
     *
     * @param state the state
     */
    public void setState(String state) {
        State = state;
    }
}
