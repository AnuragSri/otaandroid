package com.movimento.http;

import android.util.Log;

import com.google.gson.Gson;
import com.movimento.http.request.model.RequestModel;
import com.movimento.http.response.model.RespErrorModel;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.otaclient.R;
import com.movimento.utils.Constants;
import com.movimento.utils.PrefUtils;
import com.movimento.utils.Utils;

import java.io.IOException;

import javax.net.ssl.SSLHandshakeException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * The type Connection handler.
 */
public class ConnectionHandler {
    private final String TAG = ConnectionHandler.class.getCanonicalName();

    /**
     * The constant JSON.
     */
    public static final MediaType JSON
            = MediaType.parse("application/vnd.oma.dm.initiation+json; charset=utf-8");
    private OkHttpClient client = new OkHttpClient();

    /**
     * Post request string.
     *
     * @param model the model
     * @return the string
     */
    public String postRequest(RequestModel model) {
        String responseString = null;

        Request request = new Request.Builder()
                .headers(model.getmHeaders())
                .url(model.getUrl())
                .post(RequestBody.create(JSON, model.getJsonBody()))
                .build();

        Utils.logMessage(TAG, "Request URL and Body : " + model.getUrl() + " : " + model.getJsonBody() + "\n model.getmHeaders() : " + model.getmHeaders());
        Log.v(TAG, "Request URL and Body : " + model.getUrl() + " : " + model.getJsonBody() + "\n model.getmHeaders() : " + model.getmHeaders());
        Response response = null;
        try {
            response = client.newCall(request).execute();
            int httpStatus = response.code();
            responseString = response.body().string();
            Log.v(TAG, "Raw Response Body : " + responseString);
            Utils.logMessage(TAG, "Raw Response Body : " + responseString);
            Log.v(TAG, "response.isSuccessful() : " + response.isSuccessful() + ", Http response code : " + response.code());

            if (response.isSuccessful()) {
                if (responseString.contains("Responsecode")) {
                    RespErrorModel respErrorModel = new Gson().fromJson(responseString, RespErrorModel.class);
                    if (Integer.parseInt(respErrorModel.getResponsecode()) != 200) {
                        model.getResponseHandler().handleError(responseString);
                    } else {
                        Log.i(TAG, model.getResponseHandler().getClass() + " : Result successful but no action required.");
                    }
                } else {
                    model.getResponseHandler().handleResponse(responseString);
                }
            } else {
                if (Constants.DEBUG)
                    Log.i(TAG, "Error Response : " + response.code() + " : " + response.message());
                // model.getResponseHandler().handleError(responseString);
            }

        } catch (SSLHandshakeException s) {
            Log.i(TAG,"Certificate error ");
            if ((System.currentTimeMillis() - PrefUtils.getLong(Constants.CERTIFICATE_ERROR_TIME, 0)) / (60 * 1000) > 5) {
                Utils.sendError(OTAClientApp.getmAppInstance(), Constants.ERROR_TYPE_CERTIFICATE, OTAClientApp.getmAppInstance().getResources().getString(R.string.ssl_error_header), OTAClientApp.getmAppInstance().getResources().getString(R.string.ssl_error_info_text));
            }
        } catch (Exception e) {
            e.printStackTrace();
            //Error fire.
        }
        return responseString;
    }


    /**
     * GET request string.
     *
     * @param fileAccessURL the String
     * @return the string
     */
    public String getRequest(String fileAccessURL) {
        Request request = new Request.Builder()
                .url(fileAccessURL)
                .build();
        String responseString = null;
        Response response = null;

        try {
            response = client.newCall(request).execute();
            responseString = response.body().string();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return responseString;
    }

}


