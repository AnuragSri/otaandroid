package com.movimento.http.response.model.registration;

/**
 * The type Reg response mo data.
 */
public class RegResponseMoData {
    private RegResponseDMAcc DmAcc;

    /**
     * Gets dm acc.
     *
     * @return the dm acc
     */
    public RegResponseDMAcc getDmAcc() {
        return DmAcc;
    }

    /**
     * Sets dm acc.
     *
     * @param dmAcc the dm acc
     */
    public void setDmAcc(RegResponseDMAcc dmAcc) {
        DmAcc = dmAcc;
    }
}
