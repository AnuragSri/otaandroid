package com.movimento.http.request.model.devinfo;

/**
 * The type Dev info mo data.
 */
public class DevInfoMOData {
    private DeviceInfo DevInfo;

    /**
     * Gets dev info.
     *
     * @return the dev info
     */
    public DeviceInfo getDevInfo() {
        return DevInfo;
    }

    /**
     * Sets dev info.
     *
     * @param devInfo the dev info
     */
    public void setDevInfo(DeviceInfo devInfo) {
        DevInfo = devInfo;
    }

}
