package com.movimento.http.response.model;

import com.movimento.otaclient.OTAClientApp;

import java.util.List;


/**
 * The type Cmd command handler.
 */
public class CmdCommandHandler {


    /**
     * Handle command.
     *
     * @param commands the cmds
     */
    public static void handleCommand(List<String> commands) {
        if (commands.size() == 0)
            return;

        if (commands.get(0).equals("END")) {
            return;
        } else {
            // this will add command to blocking queue
            OTAClientApp.putOTATask(commands);
        }

    }
}
