package com.movimento.http.request.model.fumo;

import com.google.gson.Gson;
import com.movimento.http.request.model.MO;
import com.movimento.motreeparser.FumoParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.utils.Constants;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.Utils;

import org.json.JSONException;

import java.util.List;


/**
 * The type Fumo model.
 */
public class FumoModel extends MO {
    private List<FumoStatus> Status;
    private String ClientURI;
    private FUMOData MOData;

    /**
     * Gets status.
     *
     * @return the status
     */
    public List<FumoStatus> getStatus() {
        return Status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(List<FumoStatus> status) {
        Status = status;
    }

    /**
     * Gets client uri.
     *
     * @return the client uri
     */
    public String getClientURI() {
        return ClientURI;
    }

    /**
     * Sets client uri.
     *
     * @param clientURI the client uri
     */
    public void setClientURI(String clientURI) {
        ClientURI = clientURI;
    }

    /**
     * Gets mo data.
     *
     * @return the mo data
     */
    public FUMOData getMOData() {
        return MOData;
    }

    /**
     * Sets mo data.
     *
     * @param MOData the mo data
     */
    public void setMOData(FUMOData MOData) {
        this.MOData = MOData;
    }


    @Override
    public String createJson() {
        String requestJsonBody = Utils.loadJSONFromAsset(OTAClientApp.getmAppInstance(), "device_fumo.txt");
        FumoModel fumoModel = new Gson().fromJson(requestJsonBody, FumoModel.class);
        FumoParser fumoParser = (FumoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO);

        try {
            FumoStatus fumoStatus = fumoModel.getStatus().get(0);
            fumoStatus.setSc(Constants.FUMO_STATE_IDLE_START);
            fumoModel.getStatus().set(0, fumoStatus);

            fumoModel.setClientURI(fumoParser.getParentDDFName());

            FUMO fumo = fumoModel.getMOData().getFumo();
            fumo.setPkgName(fumoParser.getNodeDfTitle(ParserConstants.KEY_FUMO_PKG_NAME, ""));
            //fumo.setPkgVersion("IT170405_00");
            fumo.setPkgVersion(fumoParser.getNodeDfTitle(ParserConstants.KEY_FUMO_PKG_VERSION, ""));
            fumo.setState(fumoParser.getNodeDfTitle(ParserConstants.KEY_FUMO_STATE, ""));

            DownloadAndUpdate downloadAndUpdate = fumo.getDownloadAndUpdate();
            //downloadAndUpdate.setPkgURL(fumoParser.getNodeDfTitle(ParserConstants.KEY_FUMO_DOWNLOAD_AND_UPDATE, ParserConstants.KEY_FUMO_DOWNLOAD_AND_UPDATE_PKG_URL));
            downloadAndUpdate.setPkgURL(fumoParser.getNodeDfTitle(ParserConstants.KEY_FUMO_DOWNLOAD_AND_UPDATE_PKG_URL, ParserConstants.KEY_FUMO_DOWNLOAD_AND_UPDATE));
            fumo.setDownloadAndUpdate(downloadAndUpdate);
            fumoModel.getMOData().setFumo(fumo);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new Gson().toJson(fumoModel, FumoModel.class);
    }

}




