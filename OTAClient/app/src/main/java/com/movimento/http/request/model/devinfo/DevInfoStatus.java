package com.movimento.http.request.model.devinfo;


/**
 * The type Dev info status.
 */
public class DevInfoStatus {
    private int sc;

    /**
     * Gets sc.
     *
     * @return the sc
     */
    public int getSc() {
        return sc;
    }

    /**
     * Sets sc.
     *
     * @param sc the sc
     */
    public void setSc(int sc) {
        this.sc = sc;
    }




}
