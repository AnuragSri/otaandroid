package com.movimento.http.response.model;

/**
 * The type Resp error model.
 */
public class RespErrorModel {
    private String Description;
    private String Responsecode;

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        Description = description;
    }

    /**
     * Gets responsecode.
     *
     * @return the responsecode
     */
    public String getResponsecode() {
        return Responsecode;
    }

    /**
     * Sets responsecode.
     *
     * @param responsecode the responsecode
     */
    public void setResponsecode(String responsecode) {
        Responsecode = responsecode;
    }
}
