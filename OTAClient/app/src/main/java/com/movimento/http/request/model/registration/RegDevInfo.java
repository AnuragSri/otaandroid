package com.movimento.http.request.model.registration;

/**
 * The type Reg dev info.
 */
public class RegDevInfo {
    private String DevID;
    private String Man;
    private String Mod;
    private String DmV;
    private String Lang;
    private String DevType;
    private String OEM;
    private String FwV;
    private String SwV;
    private String HwV;
    private RegExt Ext;

    /**
     * Gets ext.
     *
     * @return the ext
     */
    public RegExt getExt() {
        return Ext;
    }

    /**
     * Sets ext.
     *
     * @param ext the ext
     */
    public void setExt(RegExt ext) {
        Ext = ext;
    }


    /**
     * Gets dev id.
     *
     * @return the dev id
     */
    public String getDevID() {
        return DevID;
    }

    /**
     * Sets dev id.
     *
     * @param devID the dev id
     */
    public void setDevID(String devID) {
        DevID = devID;
    }

    /**
     * Gets man.
     *
     * @return the man
     */
    public String getMan() {
        return Man;
    }

    /**
     * Sets man.
     *
     * @param man the man
     */
    public void setMan(String man) {
        Man = man;
    }

    /**
     * Gets mod.
     *
     * @return the mod
     */
    public String getMod() {
        return Mod;
    }

    /**
     * Sets mod.
     *
     * @param mod the mod
     */
    public void setMod(String mod) {
        Mod = mod;
    }

    /**
     * Gets dm v.
     *
     * @return the dm v
     */
    public String getDmV() {
        return DmV;
    }

    /**
     * Sets dm v.
     *
     * @param dmV the dm v
     */
    public void setDmV(String dmV) {
        DmV = dmV;
    }

    /**
     * Gets lang.
     *
     * @return the lang
     */
    public String getLang() {
        return Lang;
    }

    /**
     * Sets lang.
     *
     * @param lang the lang
     */
    public void setLang(String lang) {
        Lang = lang;
    }

    /**
     * Gets dev type.
     *
     * @return the dev type
     */
    public String getDevType() {
        return DevType;
    }

    /**
     * Sets dev type.
     *
     * @param devType the dev type
     */
    public void setDevType(String devType) {
        DevType = devType;
    }

    /**
     * Gets oem.
     *
     * @return the oem
     */
    public String getOEM() {
        return OEM;
    }

    /**
     * Sets oem.
     *
     * @param OEM the oem
     */
    public void setOEM(String OEM) {
        this.OEM = OEM;
    }

    /**
     * Gets fw v.
     *
     * @return the fw v
     */
    public String getFwV() {
        return FwV;
    }

    /**
     * Sets fw v.
     *
     * @param fwV the fw v
     */
    public void setFwV(String fwV) {
        FwV = fwV;
    }

    /**
     * Gets sw v.
     *
     * @return the sw v
     */
    public String getSwV() {
        return SwV;
    }

    /**
     * Sets sw v.
     *
     * @param swV the sw v
     */
    public void setSwV(String swV) {
        SwV = swV;
    }

    /**
     * Gets hw v.
     *
     * @return the hw v
     */
    public String getHwV() {
        return HwV;
    }

    /**
     * Sets hw v.
     *
     * @param hwV the hw v
     */
    public void setHwV(String hwV) {
        HwV = hwV;
    }

}
