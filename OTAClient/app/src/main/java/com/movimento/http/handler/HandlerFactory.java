package com.movimento.http.handler;

import com.movimento.utils.Constants;

/**
 * The type Handler factory.
 */
public class HandlerFactory {
    private static HandlerFactory instance;

    private HandlerFactory() {
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static HandlerFactory getInstance() {
        synchronized (HandlerFactory.class) {
            if (instance == null) {
                instance = new HandlerFactory();
            }
        }
        return instance;
    }

    /**
     * Gets response handler.
     *
     * @param type the type
     * @return the response handler
     */
    public ResponseHandler getResponseHandler(int type) {
        ResponseHandler responseHandler = null;

        switch (type) {
            case Constants.MO_TYPE_REGISTRATION:
                responseHandler = new RegistrationHandler();
                break;
            case Constants.MO_TYPE_DEV_INFO:
                responseHandler = new DevInfoHandler();
                break;
            case Constants.MO_TYPE_FIRMWARE:
                responseHandler = new FumoHandler();
                break;

            case Constants.MO_TYPE_DLOTA:
                responseHandler = new DlotaHandler();
                break;

            case Constants.MO_TYPE_LOCK_AND_WIPE:
                responseHandler = new LawmoHandler();
                break;
            case Constants.MO_TYPE_ALERT:
                responseHandler = new AlertHandler();
                break;
            case Constants.MO_TYPE_CAPABILITY:
                responseHandler = new CapabilityHandler();
                break;

            default:
                break;
        }

        return responseHandler;
    }
}
