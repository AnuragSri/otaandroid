package com.movimento.http.request.model.devinfo;

import com.google.gson.Gson;
import com.movimento.http.request.model.MO;
import com.movimento.motreeparser.DeviceInfoParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.utils.Constants;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.Utils;

import java.util.List;

/**
 * The type Device info model.
 */
public class DeviceInfoModel extends MO {
    private List<DevInfoStatus> Status;
    private String ClientURI;
    private DevInfoMOData MOData;

    /**
     * Gets mo data.
     *
     * @return the mo data
     */
    public DevInfoMOData getMOData() {
        return MOData;
    }

    /**
     * Sets mo data.
     *
     * @param MOData the mo data
     */
    public void setMOData(DevInfoMOData MOData) {
        this.MOData = MOData;
    }

    /**
     * Gets client uri.
     *
     * @return the client uri
     */
    public String getClientURI() {
        return ClientURI;
    }

    /**
     * Sets client uri.
     *
     * @param clientURI the client uri
     */
    public void setClientURI(String clientURI) {
        ClientURI = clientURI;
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public List<DevInfoStatus> getStatus() {
        return Status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(List<DevInfoStatus> status) {
        Status = status;
    }


    @Override
    public String createJson() {
        String requestJsonBody = Utils.loadJSONFromAsset(OTAClientApp.getmAppInstance(), "device_info.txt");
        DeviceInfoModel deviceInfoModel = new Gson().fromJson(requestJsonBody, DeviceInfoModel.class);
        DeviceInfoParser deviceInfoParser = (DeviceInfoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_DEVICE_INFO);

        try {
            DevInfoStatus devInfoStatus = deviceInfoModel.getStatus().get(0);
            devInfoStatus.setSc(Constants.DEV_INFO_STATUS_CODE);
            deviceInfoModel.getStatus().set(0, devInfoStatus);
            deviceInfoModel.setClientURI(deviceInfoParser.getParentNodeDFType());

            DeviceInfo deviceInfo = deviceInfoModel.getMOData().getDevInfo();
            deviceInfo.setDevID(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_DEV_ID));
            deviceInfo.setMan(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_MAN));
            deviceInfo.setMod(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_MOD));
            deviceInfo.setDmV(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_DMV));
            deviceInfo.setLang(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_LANG));
            deviceInfo.setDevType(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_DEV_TYPE));
            deviceInfo.setOEM(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_OEM));
            deviceInfo.setFwV(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_FWV));
            deviceInfo.setSwV(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_SWV));
            deviceInfo.setHwV(deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_HWV));

            deviceInfoModel.getMOData().setDevInfo(deviceInfo);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new Gson().toJson(deviceInfoModel, DeviceInfoModel.class);
    }
}

