package com.movimento.http.request.model.alert;


/**
 * The type Alert.
 */
public class Alert {
    private String AlertType;
    private String Mark;
    private String DataType;
    private Data Data;

    /**
     * Gets alert type.
     *
     * @return the alert type
     */
    public String getAlertType() {
        return AlertType;
    }

    /**
     * Sets alert type.
     *
     * @param alertType the alert type
     */
    public void setAlertType(String alertType) {
        AlertType = alertType;
    }

    /**
     * Gets mark.
     *
     * @return the mark
     */
    public String getMark() {
        return Mark;
    }

    /**
     * Sets mark.
     *
     * @param mark the mark
     */
    public void setMark(String mark) {
        Mark = mark;
    }

    /**
     * Gets data type.
     *
     * @return the data type
     */
    public String getDataType() {
        return DataType;
    }

    /**
     * Sets data type.
     *
     * @param dataType the data type
     */
    public void setDataType(String dataType) {
        DataType = dataType;
    }

    /**
     * Gets data.
     *
     * @return the data
     */
    public com.movimento.http.request.model.alert.Data getData() {
        return Data;
    }

    /**
     * Sets data.
     *
     * @param data the data
     */
    public void setData(com.movimento.http.request.model.alert.Data data) {
        Data = data;
    }
}
