package com.movimento.http.response.model.registration;

/**
 * The type Reg response auth.
 */
public class RegResponseAuth {
    private RegResponseTrsp Trsp;

    /**
     * Gets trsp.
     *
     * @return the trsp
     */
    public RegResponseTrsp getTrsp() {
        return Trsp;
    }

    /**
     * Sets trsp.
     *
     * @param trsp the trsp
     */
    public void setTrsp(RegResponseTrsp trsp) {
        Trsp = trsp;
    }

}
