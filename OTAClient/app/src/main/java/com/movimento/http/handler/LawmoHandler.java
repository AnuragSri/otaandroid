package com.movimento.http.handler;

import android.util.Log;

import com.google.gson.Gson;
import com.movimento.http.response.model.CmdCommandHandler;
import com.movimento.http.response.model.CmdResponseModel;
import com.movimento.http.response.model.RespErrorModel;

import java.util.List;

/**
 * The type Lawmo handler.
 */
public class LawmoHandler implements ResponseHandler {
    private final String TAG = LawmoHandler.class.getCanonicalName();


    @Override
    public void handleResponse(String data) {
        Log.v(TAG, "Lawmo response data:" + data);
        if (data != null && !data.isEmpty()) {
            CmdResponseModel cmdResponseModel = new Gson().fromJson(data, CmdResponseModel.class);
            if (cmdResponseModel != null) {
                List<List<String>> cmdList = cmdResponseModel.getCMD();
                for (List<String> innerList : cmdList) {
                    CmdCommandHandler.handleCommand(innerList);
                }
            }
        }
    }

    @Override
    public void handleError(String data) {
        if (data != null && !data.isEmpty()) {
            RespErrorModel respErrorModel = new Gson().fromJson(data, RespErrorModel.class);
            if (respErrorModel != null) {
                String errorCode = respErrorModel.getResponsecode();
                String description = respErrorModel.getDescription();
                Log.v(TAG, "ErrorCode: " + errorCode);
                Log.v(TAG, "Description: " + description);
            }
        }
    }
}
