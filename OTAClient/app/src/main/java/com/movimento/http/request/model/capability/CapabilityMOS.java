package com.movimento.http.request.model.capability;


/**
 * The type Capability mos.
 */
public class CapabilityMOS {
    private String MOID;
    private String[] MIID;

    /**
     * Gets moid.
     *
     * @return the moid
     */
    public String getMOID() {
        return MOID;
    }

    /**
     * Sets moid.
     *
     * @param MOID the moid
     */
    public void setMOID(String MOID) {
        this.MOID = MOID;
    }

    /**
     * Get miid string [ ].
     *
     * @return the string [ ]
     */
    public String[] getMIID() {
        return MIID;
    }

    /**
     * Sets miid.
     *
     * @param MIID the miid
     */
    public void setMIID(String[] MIID) {
        this.MIID = MIID;
    }
}
