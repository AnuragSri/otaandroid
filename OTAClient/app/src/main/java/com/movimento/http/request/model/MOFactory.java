package com.movimento.http.request.model;

import com.movimento.http.request.model.alert.AlertModel;
import com.movimento.http.request.model.capability.CapabilityModel;
import com.movimento.http.request.model.devinfo.DeviceInfoModel;
import com.movimento.http.request.model.fumo.FumoModel;
import com.movimento.http.request.model.registration.RegistrationModel;
import com.movimento.utils.Constants;

/**
 * The type Mo factory.
 */
public class MOFactory {
    private static MOFactory instance;

    private MOFactory() {
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static MOFactory getInstance() {
        synchronized (MOFactory.class) {
            if (instance == null)
                instance = new MOFactory();
        }
        return instance;
    }

    /**
     * Gets mo.
     *
     * @param type the type
     * @return the mo
     */
    public MO getMO(int type) {
        MO mo = null;
        switch (type) {
            case Constants.MO_TYPE_REGISTRATION:
                mo = new RegistrationModel();
                break;
            case Constants.MO_TYPE_DEV_INFO:
                mo = new DeviceInfoModel();
                break;
            case Constants.MO_TYPE_FIRMWARE:
                mo = new FumoModel();
                break;
           case Constants.MO_TYPE_ALERT:
                mo = new AlertModel();
                break;
            case Constants.MO_TYPE_CAPABILITY:
                mo = new CapabilityModel();
                break;
            default:
                break;
        }
        return mo;
    }
}
