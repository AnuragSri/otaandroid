package com.movimento.http.response.model.downdesc;

/**
 * The type Download product.
 */
public class DownloadProduct {
    private DownloadMediaObject mediaObject;

    /**
     * Gets media object.
     *
     * @return the media object
     */
    public DownloadMediaObject getMediaObject() {
        return mediaObject;
    }

    /**
     * Sets media object.
     *
     * @param mediaObject the media object
     */
    public void setMediaObject(DownloadMediaObject mediaObject) {
        this.mediaObject = mediaObject;
    }
}
