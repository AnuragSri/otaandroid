package com.movimento.http;

import com.movimento.http.request.model.MOFactory;
import com.movimento.motreeparser.DevAccParser;
import com.movimento.motreeparser.DeviceInfoParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.utils.Constants;
import com.movimento.utils.EncryptionUtils;
import com.movimento.utils.ParserConstants;

import java.util.HashMap;
import java.util.Map;

/**
 * The type Request header.
 */
public class RequestHeader {
    private static RequestHeader instance;
    /**
     * The Key content type.
     */
    String KEY_CONTENT_TYPE = "Content-Type";
    /**
     * The Key accept.
     */
    String KEY_ACCEPT = "Accept";
    /**
     * The Key devid.
     */
    String KEY_DEVID = "OMADM-DevID";
    /**
     * The Key vin.
     */
    String KEY_VIN = "VIN";
    /**
     * The Key token.
     */
    String KEY_TOKEN = "Token";
    /**
     * The Key host.
     */
    String KEY_HOST = "Host";

    private RequestHeader() {
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static RequestHeader getInstance() {
        synchronized (MOFactory.class) {
            if (instance == null)
                instance = new RequestHeader();
        }
        return instance;
    }

    /**
     * Gets headers.
     *
     * @param type the type
     * @return the headers
     */
    public Map<String, String> getHeaders(int type) {
        DeviceInfoParser deviceInfoParser = (DeviceInfoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_DEVICE_INFO);
        DevAccParser devAccParser = (DevAccParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_DEVICE_ACCOUNT);

        Map<String, String> headers = new HashMap<>();
        try {
            headers.put(KEY_CONTENT_TYPE, Constants.CONTENT_TYPE_VALUE);
            headers.put(KEY_ACCEPT, Constants.ACCEPT_VALUE);
            headers.put(KEY_DEVID, deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_DEV_ID));
            headers.put(KEY_VIN, deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_VIN));


            if (type != Constants.MO_TYPE_REGISTRATION) {
                String header = devAccParser.getNodeDfTitle(ParserConstants.KEY_NODE_SECRET);

                //String deHeader = EncryptionUtils.decryptKey(header, EncryptionUtils.getPublicKey("public.key"));
                String deHeader = EncryptionUtils.decryptKey(header, EncryptionUtils.getPrivateKey("private.key"));
                //  Log.i("deHeader","*"+deHeader);
                String enHeader = EncryptionUtils.encryptKey(deHeader, EncryptionUtils.getPrivateKey("private.key"));
                //  Log.i("enHeader","*"+enHeader);
                headers.put(KEY_TOKEN, enHeader.replaceAll("\n", "").replaceAll(" ", ""));
                //  Log.i("final enHeader","*"+enHeader);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return headers;
    }

}

