package com.movimento.http.request.model.registration;

/**
 * The type Reg mo data.
 */
public class RegMOData {
    private RegDevInfo DevInfo;

    /**
     * Gets dev info.
     *
     * @return the dev info
     */
    public RegDevInfo getDevInfo() {
        return DevInfo;
    }

    /**
     * Sets dev info.
     *
     * @param devInfo the dev info
     */
    public void setDevInfo(RegDevInfo devInfo) {
        DevInfo = devInfo;
    }
}
