package com.movimento.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.movimento.motreeparser.DeviceInfoParser;
import com.movimento.motreeparser.FumoStateParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.otaclient.ErrorActivity;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.otaclient.R;
import com.movimento.otaclient.SuccessActivity;
import com.movimento.service.OTAClientService;
import com.movimento.utils.Constants;
import com.movimento.utils.FileUtils;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.PrefUtils;
import com.movimento.utils.Utils;

import org.json.JSONException;

import java.util.Timer;
import java.util.TimerTask;

import static com.movimento.utils.Utils.WIRELESS_PORT;


/**
 * The type Service starter.
 */
public class ServiceStarter extends BroadcastReceiver {
    private final String TAG = ServiceStarter.class.getCanonicalName();
    private Timer mTimer = null;
    private SendAlertScheduler sendAlertScheduler=null;
    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {

        mContext=context;
        checkAndUpdateDevAndMacId();
        if(PrefUtils.getBoolean(Constants.IS_INSTALLATION_STARTED,false)){
            startTimer();
            Utils.logMessage(TAG,"startTimer");
        }
    }

    private boolean setDeviceMacId(Context context) {
        boolean flag;
        if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_WIFI)) {
            flag = false;
            Log.v(TAG, "Device is not equipped with Wifi Card");
            //Device is not equipped with Wifi Card
        } else {
            String macAddress = Utils.getMACAddress(WIRELESS_PORT);
            if (macAddress != null && !macAddress.isEmpty()) {
                flag = Utils.updateMacAddress(macAddress);
                Log.v(TAG, "MAC address is updated in MO Tree");
            } else {
                //TODO handle special case of few tablet which not allow to fetch MAC Address if Wifi is disabled.
                Log.v(TAG, "handle special case of few tablet which not allow to fetch MAC Address if Wifi is disabled.");
                flag = false;
            }
        }
        return flag;
    }

    private void startMainService(Context context) {
        //Start OTAClient Service again.
        context.startService(new Intent(OTAClientApp.getmAppInstance(), OTAClientService.class));
        Log.i(TAG, "Device Rebooted and OTAClient Service started from ServiceStarter...");
    }

    private void checkAndUpdateDevAndMacId(){
        DeviceInfoParser deviceInfoParser = (DeviceInfoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_DEVICE_INFO);
        if (deviceInfoParser != null) {
            try {
                //update devId and VIN(aka MAC_ADDRESS) in devInfoMOTree
                String deviceId = deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_DEV_ID);
                if (deviceId == null || deviceId.isEmpty()) {
                    deviceInfoParser.updateChildNodeDFTitle(ParserConstants.KEY_DEV_ID, Utils.getMACAddress(WIRELESS_PORT));
                    Log.v(TAG, "Device Id set in MO Tree");
                    Utils.logMessage(TAG,"Device Id set in MO Tree");

                }
                String vinMacAddress = deviceInfoParser.getChildNodeDFTitle(ParserConstants.KEY_VIN);
                if (vinMacAddress != null && !vinMacAddress.isEmpty() && vinMacAddress.length() == 12) {
                    /*startMainService(mContext);*/
                    Log.v(TAG, "MAC Address Already have set in MO Tree.");
                    Utils.logMessage(TAG,"MAC Address Already have set in MO Tree.");

                } else {
                    boolean isMacIdSet = setDeviceMacId(mContext);
                    if (isMacIdSet) {
                       /* startMainService(mContext);*/
                        Utils.logMessage(TAG,"MAC Address set in MO Tree.");

                        Log.v(TAG, "MAC Address set in MO Tree.");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void startTimer() {
        if (mTimer == null) {
            mTimer = new Timer();
        }
        if(sendAlertScheduler==null)
            sendAlertScheduler = new SendAlertScheduler();
        mTimer.scheduleAtFixedRate(sendAlertScheduler, 20000, 2000);
    }

    public void stopTimer() {
        Utils.logMessage(TAG,"stopTimer");
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
            sendAlertScheduler.cancel();
            sendAlertScheduler=null;
        }
    }


    public class SendAlertScheduler extends TimerTask {

        @Override
        public void run() {
            Utils.logMessage(TAG,"schedular running...");
            if (Utils.isNetworkAvailable()) {
                Utils.logMessage(TAG,"schedular running...Network available");
                stopTimer();
                FumoStateParser fumoStateParser = (FumoStateParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO_STATE);
                if (!Utils.ifPreviousVersion()) {
                    Log.i(TAG, "Device updated to : " + Build.VERSION.INCREMENTAL);
                    Utils.logMessage(TAG, "Device updated to : " + Build.VERSION.INCREMENTAL);
                    Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_UPDATE_SUCCESSFUL_HAVE_DATA));
                    FileUtils.deleteDownloadedFile();
                    Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_UPDATE_SUCCESSFUL_NO_DATA));
                    Utils.doIfUpdated();
                    PrefUtils.putBoolean(Constants.IS_INSTALLATION_STARTED,false);
                    Intent successIntent = new Intent(OTAClientApp.getmAppInstance(), SuccessActivity.class);
                    successIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    OTAClientApp.getmAppInstance().startActivity(successIntent);

                } else if (Utils.ifPreviousVersion() && Integer.parseInt(fumoStateParser.getState(ParserConstants.CURRENT_STATE)) == Constants.DLOTA_STATE_UPDATE_PROGRESSING
                        && PrefUtils.getBoolean(Constants.IS_INSTALLATION_STARTED,false)) {
                    Utils.logMessage(TAG,"(Utils.ifPreviousVersion(): Installation Failed");
                    Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_UPDATE_FAILED_HAVE_DATA));
                    FileUtils.deleteDownloadedFile();
                    Utils.updateDlotaStates(ParserConstants.CURRENT_STATE, String.valueOf(Constants.FUMO_STATE_UPDATE_FAILED_NO_DATA));
                    Utils.doIfUpdateFailed();
                    PrefUtils.putBoolean(Constants.IS_INSTALLATION_STARTED,false);
                    Intent failureIntent = new Intent(OTAClientApp.getmAppInstance(), ErrorActivity.class);
                    failureIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    failureIntent.putExtra(Constants.KEY_ERRROR_TYPE, Constants.ERROR_TYPE_INSTALLATION);
                    failureIntent.putExtra(Constants.KEY_ERROR_HEADER,  OTAClientApp.getmAppInstance().getResources().getString(R.string.failure_screen_header));
                    failureIntent.putExtra(Constants.KEY_ERROR_INFO,OTAClientApp.getmAppInstance().getResources().getString(R.string.failure_screen_info_text));
                    OTAClientApp.getmAppInstance().startActivity(failureIntent);
                }

                /** Restart polling after 2 mins to avoid immediate download screen. */
                Handler handler=new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startMainService(mContext);
                    }
                },2*60*1000);


            }
        }
    }


}
