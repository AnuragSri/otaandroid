package com.movimento.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.movimento.motreeparser.FumoParser;
import com.movimento.motreeparser.FumoStateParser;
import com.movimento.motreeparser.MTParserFactory;
import com.movimento.otaclient.OTAClientApp;
import com.movimento.service.DlotaIntentService;
import com.movimento.service.DownloadDescIntentService;
import com.movimento.utils.Constants;
import com.movimento.utils.ParserConstants;
import com.movimento.utils.Utils;

import static com.movimento.utils.Constants.KEY_DOWNLOAD_DESC_URL;

/**
 * The type Network state tracker.
 */
public class NetworkStateTracker extends BroadcastReceiver {
    private final String TAG = NetworkStateTracker.class.getCanonicalName();

    @Override
    public void onReceive(Context context, Intent intent) {

        if (Utils.isNetworkAvailable()) {
            //TODO : Check if the downloading service is already running on not, if it's not then start it
            Log.v(TAG, "Network Connected");
            FumoStateParser fumoStateParser = (FumoStateParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO_STATE);

            if (!Utils.isServiceRunning(DownloadDescIntentService.class) && fumoStateParser.getState(ParserConstants.CURRENT_STATE) != null &&
                    Integer.parseInt(fumoStateParser.getState(ParserConstants.CURRENT_STATE)) != Constants.DLOTA_STATE_IDLE &&
                    Integer.parseInt(fumoStateParser.getState(ParserConstants.CURRENT_STATE)) != Constants.DLOTA_STATE_UPDATE_PROGRESSING) {
                try {
                    FumoParser fumoParser = (FumoParser) MTParserFactory.getInstance().getMTParser(Constants.MT_PARSER_FUMO);
                    String url = fumoParser.getNodeDfTitle(ParserConstants.KEY_FUMO_DOWNLOAD_AND_UPDATE_PKG_URL, ParserConstants.KEY_FUMO_DOWNLOAD_AND_UPDATE);
                    Intent downDescIntentService = new Intent(OTAClientApp.getmAppInstance(), DownloadDescIntentService.class);
                    downDescIntentService.putExtra(KEY_DOWNLOAD_DESC_URL, url);
                    OTAClientApp.getmAppInstance().startService(downDescIntentService);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else {
            Log.v(TAG, "Network Dis-Connected");
            //TODO Stop DownloadDescriptor Service & Download Service when no Internet is there
            DlotaIntentService.shouldServiceRun=false;
            Utils.stopRunningService(DownloadDescIntentService.class);
            Utils.stopRunningService(DlotaIntentService.class);
        }

    }
}
