
package com.movimento.errors;

/**
 * The type No memory exception.
 */
public class NoMemoryException extends DownloadException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new No memory exception.
     *
     * @param message the message
     */
    public NoMemoryException(String message) {

        super(message);
        // TODO Auto-generated constructor stub
    }

}
