
package com.movimento.errors;

/**
 * The type File already exist exception.
 */
public class FileAlreadyExistException extends DownloadException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new File already exist exception.
     *
     * @param message the message
     */
    public FileAlreadyExistException(String message) {

        super(message);
    }

}
