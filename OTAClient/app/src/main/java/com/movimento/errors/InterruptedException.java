package com.movimento.errors;


/**
 * The type Interrupted exception.
 */
public class InterruptedException extends DownloadException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new Interrupted exception.
     *
     * @param message the message
     */
    public InterruptedException(String message) {

        super(message);
        // TODO Auto-generated constructor stub
    }

}