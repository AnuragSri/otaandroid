
package com.movimento.errors;

/**
 * The type Download exception.
 */
public class DownloadException extends Exception {
    private static final long serialVersionUID = 1L;

    private String mExtra;

    /**
     * Instantiates a new Download exception.
     *
     * @param message the message
     */
    public DownloadException(String message) {

        super(message);
    }

    /**
     * Instantiates a new Download exception.
     *
     * @param message the message
     * @param extra   the extra
     */
    public DownloadException(String message, String extra) {

        super(message);
        mExtra = extra;
    }

    /**
     * Gets extra.
     *
     * @return the extra
     */
    public String getExtra() {

        return mExtra;
    }
}
